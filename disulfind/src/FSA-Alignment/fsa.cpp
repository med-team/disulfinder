#include "fsa.h"
#include "../Common/CommonMethods.h"

void FiniteStateAutomata::Clear() 
{ 
  no_outputs=0;
  no_states=0;
  starting_states.clear();
  ending_states.clear();
  transitions.clear(); 
}

void FiniteStateAutomata::Read( istream &is ) 
{
  this->Clear();

  string tag;
  string line; 

  // read number of outputs
  is >> tag;
  Exception::Assert(tag=="NoOutputs", "Unexpected tag <%s> in model file while looking for <NoOutputs>", tag.c_str());
  is >> no_outputs;
  Exception::Assert(no_outputs>0, "Invalid number of outputs: %d", no_outputs);

  // read number of states
  is >> tag;
  Exception::Assert(tag=="NoStates", "Unexpected tag <%s> in model file while looking for <NoStates>", tag.c_str());  
  is >> no_states;
  Exception::Assert(no_states>0, "Invalid number of states: %d", no_states);

  // read starting states
  is >> tag;  
  Exception::Assert(tag=="StartStates", "Unexpected tag <%s> in model file while looking for <StartStates>", tag.c_str());
  getline(is,line);
  istringstream iss(line);
  int s_state;
  while(iss >> s_state) {
    Exception::Assert(s_state>=0 && s_state<no_states, "Invalid starting state: %d", s_state);
    starting_states.push_back(s_state);
  }

  // read ending states
  is >> tag;
  Exception::Assert(tag=="EndStates", "Unexpected tag <%s> in model file while looking for <EndStates>", tag.c_str());
  getline(is,line);
  istringstream ies(line);
  int e_state;
  while(ies >> e_state) {
    Exception::Assert(e_state>=0 && e_state<no_states, "Invalid ending state: %d", e_state);
    ending_states.push_back(e_state);
  }
  
  // read transitions
  getline(is,line);
  istringstream ibs(line);
  ibs >> tag;
  Exception::Assert(tag=="Begin", "Unexpected tag <%s> in model file while looking for <Begin>", tag.c_str());
  transitions.resize(no_states);
  while( getline(is,line) ) {
    istringstream its(line);

    Exception::Assert(its >> tag, "Parse Error while reading tag");
    Exception::Assert(tag=="T" || tag=="End", "Unexpected tag <%s> in model file while looking for <T> or <End>", tag.c_str());
    if( tag=="End" )
      break;

    int fstate,nstate,out;
    double prob;    
    Exception::Assert(its >> fstate, "Parse Error while reading from state");
    Exception::Assert(its >> nstate, "Parse Error while reading to state");
    Exception::Assert(its >> out, "Parse Error while reading output");
    Exception::Assert(its >> prob, "Parse Error while reading probability");

    Exception::Assert(fstate>=0 && fstate<no_states, "Invalid first state for transition: %d", fstate);
    Exception::Assert(nstate>=0 && nstate<no_states, "Invalid next state for transition: %d", nstate);
    Exception::Assert(out>=0 && out<no_outputs, "Invalid output for transition: %d", out);
    Exception::Assert(prob>0, "Invalid probability for transition: %lf", prob);

    transitions[fstate].push_back(Transition(nstate,out,prob));        
  }  
}

void FiniteStateAutomata::Write( ostream &os ) 
{
  // write number of outputs
  os << "NoOutputs " << no_outputs << endl;
  
  // write number of states
  os << "NoStates " << no_states << endl;  

  // write starting states
  os << "StartStates ";
  for( uint i=0; i<starting_states.size(); i++ )
    os << starting_states[i] << " ";
  os << endl;

  // write ending states
  os << "EndStates ";
  for( uint i=0; i<ending_states.size(); i++ )
    os << ending_states[i] << " ";
  os << endl;

  // write transitions
  os << "Begin" << endl;
  for( int i=0; i<no_states; i++ ) {
    for( uint l=0; l<transitions[i].size(); l++ ) {
      os << "T " << i << " " << transitions[i][l].next_state << " "
	 << transitions[i][l].output << " " 
	 << transitions[i][l].probability << endl;
    }
  }
  os << "End" << endl;
}

void FSACollection::Read(istream &is ) 
{  
  string tag;

  // read number of automata
  is >> tag;
  Exception::Assert(tag=="NoAutomata", "Unexpected tag <%s> in model file while looking for <NoAutomata>", tag.c_str());
  is >> no_automata;

  // read automata collection
  automata_collection.resize(no_automata);
  for( int i=0; i<no_automata; i++ ) {
    // read header
    is >> tag;
    Exception::Assert(tag=="BeginAutomata", "Unexpected tag <%s> in model file while looking for <BeginAutomata>", tag.c_str());

    // read automata
    automata_collection[i].Read(is);

    // read tail
    is >> tag;
    Exception::Assert(tag=="EndAutomata", "Unexpected tag <%s> in model file while looking for <EndAutomata>", tag.c_str()); 
  }
}

void FSACollection::Write( ostream &os ) 
{
  os << "NoAutomata " << no_automata << endl;
  for( int i=0; i<no_automata; i++ ) {
      os << "BeginAutomata" << endl;
      automata_collection[i].Write(os);
      os << "EndAutomata" << endl;      
  }
}
