#include <getopt.h>
#include <fstream>
#include "../Common/CommonMethods.h"
#include "fsa.h"

class Options
{
private:

  typedef unsigned int uint;
  static const option optionNames[];

public:

  ////////////////////////////
  // Command line variables //
  ////////////////////////////

  string folds_file;
  string automata_file;
  bool model_segments;
  bool allow_modifications;
  double modifications_probability;
  
  // Constructor
  Options(int argc = 0, char *argv[]=NULL);
  void usage(char* progname);
};


const option Options::optionNames[] =
{ 
  { "model_segments",      no_argument,       0, 's'},
  { "allow_modifications", required_argument, 0, 'm'},
  { "help",                no_argument,       0, '?'},
  { 0,                     0,                 0, 0  }
};
  


Options::Options(int argc, char *argv[]) :
  folds_file(""),
  automata_file(""),
  model_segments(false),
  allow_modifications(false)
{
  int c;
  int* opti = 0;
  char *progname = argv[0];
  
  // you must put ':' only after parameters that require an argument
  // so after 'a' and '?' we don't put ':'

  while ((c = getopt_long(argc, argv, "sm:?", optionNames, opti)) != -1) {
    switch(c) {
    case 's':
      model_segments = true;
      break;
    case 'm':
      allow_modifications = true;
      modifications_probability = atof(optarg);
      break;

    case '?':
    default:
      usage(progname);      
    }
  }

  if( optind > argc-2 ) {
    cerr << "Missing parameters" << endl;
    usage(progname);
  }
  else if( optind < argc-2) {
    cerr << "Invalid parameters" << endl;
    usage(progname);
  }
  else {
    folds_file = argv[optind];
    automata_file = argv[optind+1];
  }
}

void Options::usage(char* progname) 
{
  cerr << "usage: " << progname << " [options] <folds_file> <automata_file>" << endl;
  cerr << "\tfolds_file: file with secondary structure sequences (REQUIRED)" << endl;
  cerr << "\tautomata_file: file to save automata (REQUIRED)" << endl;
  cerr << "Options:" << endl;
  cerr << "\t-s, --model_segments: model segments of SS with specific automata (default=false)" << endl;
  cerr << "\t-m, --allow_modifications:prob: modify the automata to allow changes in the sequence of SS with probability <prob> (default=false)" << endl;

  cerr << "\t-?, --help: this message" << endl;
  exit(0);
}

int sscode(char c) {
  if( c=='H' ) 
    return 0;
  else if( c=='E' )
    return 1;
  else if ( c=='C' )
    return 2;
  else 
    Exception::Throw("Unrecognized character %c", c);
  return -1;
}

int AddHelix(FiniteStateAutomata &automata, int last_state, bool model_segment) 
{
  int new_state = automata.no_states;
  if( model_segment ) {
    automata.transitions.push_back(vector<Transition>());
    automata.transitions.push_back(vector<Transition>());
    automata.transitions.push_back(vector<Transition>());
    automata.transitions.push_back(vector<Transition>());
    automata.no_states += 4;
    
    automata.transitions[last_state].push_back(Transition(new_state,0,1.));
    automata.transitions[new_state+0].push_back(Transition(new_state+1,0,1.));
    automata.transitions[new_state+1].push_back(Transition(new_state+2,0,1.));
    automata.transitions[new_state+2].push_back(Transition(new_state+3,0,1.));
    automata.transitions[new_state+3].push_back(Transition(new_state+3,0,1.));
  }
  else {
    automata.transitions.push_back(vector<Transition>());
    automata.no_states += 1;
    
    automata.transitions[last_state].push_back(Transition(new_state,0,1.));
  }
  return (automata.no_states-1);
}

int AddSheet(FiniteStateAutomata &automata, int last_state, bool model_segment) 
{
  int new_state = automata.no_states;
  if( model_segment ) {
    automata.transitions.push_back(vector<Transition>());
    automata.transitions.push_back(vector<Transition>());
    automata.no_states += 2;
    
    automata.transitions[last_state].push_back(Transition(new_state,1,1.));
    automata.transitions[new_state+0].push_back(Transition(new_state+1,1,1.));
    automata.transitions[new_state+1].push_back(Transition(new_state+1,1,1.));
  }
  else {
    automata.transitions.push_back(vector<Transition>());
    automata.no_states += 1;
    
    automata.transitions[last_state].push_back(Transition(new_state,1,1.));
  }
  return (automata.no_states-1);
}

int AddCoil(FiniteStateAutomata &automata, int last_state, bool model_segment) 
{
  int new_state = automata.no_states;
  if( model_segment ) {
    automata.transitions.push_back(vector<Transition>());
    automata.no_states += 1;

    automata.transitions[last_state].push_back(Transition(new_state,2,1.));
    automata.transitions[new_state].push_back(Transition(new_state,2,1.));
  }
  else {
    automata.transitions.push_back(vector<Transition>());
    automata.no_states += 1;
    
    automata.transitions[last_state].push_back(Transition(new_state,2,1.));
  }
  return (automata.no_states-1);
}

int main(int argc, char *argv[]) 
{
  Options options(argc,argv);
  try {
    Exception::Assert(!(options.model_segments && options.allow_modifications),"Cannot allow modifications when modeling segments");

    ifstream iffs(options.folds_file.c_str());
    Exception::Assert(iffs.good(), "Error while opening folds file <%s> for reading", argv[1]);

    string line;
    FSACollection collection;
    for(int count=1;;count++) {
      getline(iffs,line);
      if( !iffs.good() ) 
	break;
      
      // create an automa for each fold
      FiniteStateAutomata automata;
      automata.no_outputs = 3;
      automata.no_states = 0; // starting state

      // add starting state 
      int first_fold_state = automata.no_states; // start of the state sequence for this fold
      automata.transitions.push_back(vector<Transition>());
      automata.starting_states.push_back(first_fold_state);
      automata.no_states += 1;

      // model fold
      int last_fold_state = first_fold_state; // end of the state sequence for this fold
      for(uint i=0; i<line.length(); ) { 
	char ss = line[i++];

	if( options.model_segments ) { // compress the segment
	  for( ; line[i]==ss && i<line.length(); i++ ); //look for the end of the segment
	}

	if( ss=='H' )
	  last_fold_state = AddHelix(automata,last_fold_state,options.model_segments);
	else if( ss=='E' )
	  last_fold_state = AddSheet(automata,last_fold_state,options.model_segments);
	else if( ss=='C' )
	  last_fold_state = AddCoil(automata,last_fold_state,options.model_segments);
	else
	  Exception::Throw("Unrecognized character <%c> in fold #%d",ss,count);
      }
      automata.ending_states.push_back(last_fold_state);

      // allow modifications
      if( options.allow_modifications ) {
	int delete_state = first_fold_state; // first useful state (avoid starting state) of the sequence for this fold
	for( uint i=0; i<line.length(); ) {
	  // look for the end of the segment
	  char ss = line[i];
	  int segment_size = 1;
	  for( ++i; line[i]==ss && i<line.length(); i++, segment_size++ ); 
	  	  
	  // insertion
	  int insert_state = first_fold_state+i; // end of the segment
	  automata.transitions[insert_state].push_back(Transition(insert_state,sscode(ss),options.modifications_probability));
	  
	  // deletion	 	  
	  double prob=1.;
	  int last_state;
	  if( ss=='H' ) 
	    last_state = insert_state-3;
	  else if( ss=='E' )
	    last_state = insert_state-1;
	  else last_state=insert_state;
	  for( int next_state = delete_state+2; 
	       next_state<=last_state; 
	       next_state++ ) {
	    prob *= options.modifications_probability;
	    automata.transitions[delete_state].push_back(Transition(next_state,sscode(ss),prob));
	  }
	  delete_state = insert_state;
	}
      }
      
      collection.automata_collection.push_back(automata);
      collection.no_automata++;
    }
    
    // write automata
    ofstream ofas(options.automata_file.c_str());
    Exception::Assert(ofas.good(), "Error while opening automata file <%s> for writing", argv[2]);
    collection.Write(ofas);	
  }
  catch(Exception *e) {
    e->PrintMessage();
    delete e;
    return -1;
  }
  return 0;       

  
}
