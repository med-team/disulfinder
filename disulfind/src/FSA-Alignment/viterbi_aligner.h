#include <iostream>
#include "../Common/CommonMethods.h"
#include "fsa.h"

int viterbi_aligner(const char * automata, const char * probabilities, std::ostream& out);
