#include <values.h>
#include <fstream>
#include "viterbi_aligner.h"

int viterbi_aligner(const char * automata, const char * probabilities, ostream & out){
  
    
  try{
	//read automata collection
    ifstream ifas(automata);
    Exception::Assert(ifas.good(), "Error while opening automata file <%s> for reading", automata);
    FSACollection collection;        
    collection.Read(ifas);
    ifas.close();
    
    //read probabilities
    ifstream ifys(probabilities);
    Exception::Assert(ifys.good(), "Error while opening probabilities file <%s> for reading", probabilities);
    Matrix<double> PYt(ifys);
    int T = PYt.GetNoRows();
    ifys.close();   

    double max_automata_score = NEGINF;
    vector<int> best_output_sequence;
    for( int a=0; a<collection.no_automata; a++ ) {
      FiniteStateAutomata automata = collection.automata_collection[a];
      Exception::Assert(PYt.GetNoCols()==automata.no_outputs,"Invalid number of columns (%d) for matrix of probabilities, expected %d", PYt.GetNoCols(), automata.no_outputs);

      //------------------------------
      // CREATE ALIGNMENTS
      //------------------------------

      Matrix<double> Scores(T+1,automata.no_states);
      Matrix<int> Y(T,automata.no_states);
      Matrix<int> Back(T,automata.no_states);  
      
      // init lattice
      for( int s=0; s<automata.no_states; s++ )
	Scores(0,s) = NEGINF;
      for( uint i=0; i<automata.starting_states.size(); i++ ) 
	Scores(0,automata.starting_states[i]) = 0.;
      for( int t=0; t<T; t++ ) {
	for( int s=0; s<automata.no_states; s++ ) {
	  Scores(t+1,s) = NEGINF;      		
	  Back(t,0) = 0;
	  Y(t,0) = 0;
	}
      }

      // create lattice
      for( int t=0; t<T; t++ ) {    
	for( int s1=0; s1<automata.no_states; s1++ ) {  // for each first state
	  if( Scores(t,s1)==NEGINF )
	    continue;
	  for( uint i=0; i<automata.transitions[s1].size(); i++ ) { // for each next state
	    int s2 = automata.transitions[s1][i].next_state;
	    double prob = automata.transitions[s1][i].probability;
	    int output = automata.transitions[s1][i].output;
	    double score = Scores(t,s1) + log(prob) + log(PYt(t,output));
	    if( score>Scores(t+1,s2) ) {
	      Scores(t+1,s2) = score;
	      Back(t,s2) = s1;
	      Y(t,s2) = output;
	    }	    	  
	  }
	}      
      }  

      //------------------------------
      // GET BEST ALIGNMENT
      //------------------------------
      
      // get best ending state
      double maxscore = NEGINF;
      int maxind = -1;
      for( uint i=0; i<automata.ending_states.size(); i++ ) {
	if( Scores(T,automata.ending_states[i])>=maxscore ) {
	  maxscore = Scores(T,automata.ending_states[i]);
	  maxind = automata.ending_states[i];
	}	
      }
   
      // choose best automata
      if( maxscore > max_automata_score ) {
	// get best state sequence
	vector<int> inv_state_sequence;
	vector<int> inv_out_sequence;
	int laststate = maxind;
	for( int t=T-1; t>=0; t-- ) {
	  inv_state_sequence.push_back(laststate);
	  inv_out_sequence.push_back(Y(t,laststate));
	  laststate = Back(t,laststate);
	}	
	best_output_sequence = inv_out_sequence;
	max_automata_score = maxscore;
      }
    }
    
    Exception::Assert( max_automata_score>NEGINF, "Sequence does not match");
      
    for( int t=T-1; t>=0; t-- ) 
      out << best_output_sequence[t] << endl;   
  }  
  catch(Exception *e) {
    e->PrintMessage();
    delete e;
    return -1;
  }
  return 0;       
}

#ifdef STANDALONE
int main(int argc, char *argv[]) 
{
  if( argc<3 ) {
    cerr << StringF("Usage: %s <automata> <probabilities>", argv[0]) << endl;   
    return -1;
  }

  return viterbi_aligner(argv[1],argv[2],cout);
}
#endif
