#include <stdio.h>
#include <math.h>
#include <fstream>
#include "../Common/CommonMethods.h"

void PrintNormalized(Matrix<int> &frequencies) 
{
  int norows = frequencies.GetNoRows();
  int nocols = frequencies.GetNoCols();
  for( int r=0; r<norows; r++ ) {
    double sum = 0;
    for( int c=0; c<nocols; c++ ) 
      sum += frequencies(r,c);
    for( int c=0; c<nocols; c++ ) 
      cout << (double)frequencies(r,c)/(double)sum << ' ';
  }
  cout << endl;
}


int main(int argc, char *argv[]) 
{ 
  if( argc<3 ) {
    cerr << StringF("Usage: %s <noclasses> <labels_files...>",argv[0]) << endl;
    return -1;
  }

  int no_classes = atoi(argv[1]);
  Matrix<int> starting_class(1,no_classes);
  Matrix<int> ending_class(1,no_classes);
  Matrix<int> prob_class(1,no_classes);
  Matrix<int> static_transitions(no_classes,no_classes);
  vector<Matrix<int> > timed_transitions;
  vector<Matrix<int> > durations;
  try
  {
    for( int i=2; i<argc; i++ ) {
      // read labels
      ifstream ifs(argv[i]);
      Matrix<int> labels(ifs);
      Exception::Assert(labels.GetNoCols()==1,"Invalid number of columns in label's file");
      Exception::Assert(labels.GetNoRows()>0,"Empty label's file");
      int T = labels.GetNoRows();    
      
      // calc durations
      starting_class(0,labels(0,0))++;
      prob_class(0,labels(0,0))++;
      int last_class = labels(0,0);
      uint duration = 1;
      for( int t=1; t<T; t++ ) {
	// calc frequency in each class
	prob_class(0,labels(t,0))++;

	// update static transition
	static_transitions(last_class,labels(t,0))++;
	
	// make space if necessary
	if( timed_transitions.size()==duration-1 )
	  timed_transitions.push_back(Matrix<int>(no_classes,no_classes));	
	if( durations.size()==duration-1 )
	  durations.push_back(Matrix<int>(1,no_classes));

	// update timed transitions
	timed_transitions[duration-1].elem(last_class,labels(t,0))++;      

	// update duration
	if( labels(t,0)!=last_class ) {
	  durations[duration-1].elem(0,last_class)++;
	  duration = 1;
	}
	else
	  duration ++;
	last_class = labels(t,0);
      }
      if( durations.size()==duration-1 )
	durations.push_back(Matrix<int>(1,no_classes));
      durations[duration-1].elem(0,last_class)++;
      ending_class(0,labels(T-1,0))++;
    }
    
    // calculate average durations;
    Matrix<double> mean_durations(1,no_classes);
    Matrix<int> sum_frequencies(1,no_classes);
    for( uint t=0; t<durations.size(); t++ ) {
      for( int c=0; c<no_classes; c++ ) {
	mean_durations(0,c) += (t+1)*durations[t].elem(0,c);
	sum_frequencies(0,c) += durations[t].elem(0,c);
      }      
    }
    for( int c=0; c<no_classes; c++ ) 
      mean_durations(0,c) /= (double)sum_frequencies(0,c);     

    // print probabilities
    
    cout << "Starting probabilities" << endl;
    PrintNormalized(starting_class);
    cout << endl;

    cout << "Ending probabilities" << endl;
    PrintNormalized(ending_class);
    cout << endl;

    cout << "Class probabilities" << endl;
    PrintNormalized(prob_class);
    cout << endl;
    
    cout << "Average durations" << endl;
    for( int c=0; c<no_classes; c++)
      cout << mean_durations(0,c) << ' ';
    cout << endl << endl;

    cout << "Durations histograms" << endl;
    for( uint t=0; t<durations.size(); t++ ) {
      cout << t+1 << ": ";
      for( int c=0; c<no_classes; c++ ) 
	cout << durations[t].elem(0,c) << ' ';
      cout << endl;
    }

    cout << "Static transitions probabilities" << endl;
    PrintNormalized(static_transitions);
    cout << endl;

    cout << "Timed transition probabilities" << endl;
    for( uint t=0; t<timed_transitions.size(); t++ ) {
      cout << t+1 << ": ";
      PrintNormalized(timed_transitions[t]);
    }

  }
  catch(Exception *e) {
    e->PrintMessage();
    delete e;
    return -1;
  }
  return 0;       

  

}
