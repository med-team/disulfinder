#ifndef __FSA_H
#define __FSA_H
#include <vector>
#include <string>

using namespace std;

struct Transition 
{
  //construction
  Transition(int nstate=-1, int out=0, double prob=1.) { next_state=nstate; output=out; probability=prob; }

  //data  
  int next_state;
  int output;
  double probability;
};

struct FiniteStateAutomata 
{
  //construction
  FiniteStateAutomata()            { no_outputs=0; no_states=0; }
  FiniteStateAutomata(istream &is) { no_outputs=0; no_states=0; Read(is); }
  void Clear();
  
  //serialization
  void Read(istream &is);
  void Write(ostream &os);
  
  //data
  int no_outputs;
  int no_states;
  vector<int> starting_states;
  vector<int> ending_states;
  vector<vector<Transition> > transitions;   
};

struct FSACollection 
{
  FSACollection() { no_automata=0; }
  FSACollection(istream &is) { no_automata=0; Read(is); }

  //serialization
  void Read(istream &is);
  void Write(ostream &os);
  
  // data
  int no_automata;
  vector<FiniteStateAutomata> automata_collection;   
};


#endif
