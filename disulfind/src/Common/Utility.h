#include <vector>
#include <string>

// DEPRECATED: use Util instead

void removeEmptyStrings(std::vector<std::string>& s);
void clean(std::vector<std::string>& sa);
void clean(std::string& s);  
std::string uppercase(const std::string& s);
double average(double* d, int size);
std::vector<std::string> tokenize(const std::string& s, const std::string& token = " ");
double Max(double* d, int size);
