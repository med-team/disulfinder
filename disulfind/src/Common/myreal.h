#ifndef MYREAL_H
#define MYREAL_H
#include <values.h>

typedef double REAL;
#define REAL_SIZE       8
#define REAL_LOG_SIZE   3
#define REAL_THREE_SIZE 24

#define NEGINF -DBL_MAX
#define POSINF DBL_MAX

#endif
