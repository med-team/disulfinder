#ifndef __CONFUSIONMATRIX_H
#define __CONFUSIONMATRIX_H

#include <iostream>

using namespace std;

class ConfusionMatrix
{
 private:

  int m_noclasses;
  int *buffer;
  int& elem(int r, int c) { return buffer[r*m_noclasses+c]; }
  
 public:
  
  ConfusionMatrix(int noclasses=2);
  ~ConfusionMatrix() { delete []buffer; }
  
  void Add(int prediction, int target) { elem(prediction,target)++; }
  
  void Add(int* predictions, int* targets, int noexamples) {
    for( int i=0; i<noexamples; i++ )
      Add(predictions[i],targets[i]);
  }
  
  double Accuracy();
  double Precision(int indclass);
  double Recall(int indclass);
  void Output( ostream &out=cout );
};
#endif
