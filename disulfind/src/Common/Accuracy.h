#ifndef __ACCURACY_H
#define __ACCURACY_H
#include <iostream>
using namespace std;

class Accuracy
{
 public:

  int true_positives;
  int false_positives;

  int true_negatives;
  int false_negatives;

  int true_examples;
  int no_examples;

public:

  Accuracy() {
    true_positives = 0;
    false_positives = 0;

    true_negatives = 0;
    false_negatives = 0;
  
    true_examples = 0;
    no_examples = 0;
  }

  void Add(int target, int prediction ) {
    if( prediction==target ) {
      true_examples++;
      if( target>=0 )
	true_positives++;
      else
	true_negatives++;
    }
    else {
      if( target>=0 )
	false_negatives++;
      else
	false_positives++;
    }	
    no_examples++;   
  }

  double GetAccuracy()  { return (double)true_examples/(double)no_examples; }
  double GetPrecision() { return (double)true_positives/(double)(true_positives+false_positives); }
  double GetRecall()    { return (double)true_positives/(double)(true_positives+false_negatives); }
  double GetFMeasure()  { 
    if( (true_positives+false_positives>0) && (true_positives+false_negatives)>0 ) {
      double p=GetPrecision(); 
      double r=GetRecall(); 
      return (2.*p*r/(p+r)); 
    }	
    else
      return 0.;
  }

  void Output(ostream &os) {
    os<<"Accuracy= "  << (GetAccuracy()*100.)<<"\%\n";
    os<<"Precision= " << (GetPrecision()*100.)<<"\%\n";
    os<<"Recall= "    << (GetRecall()*100.)<<"\%\n";		     
    os<<"FMeasure= "  << (GetFMeasure()*100.)<<"\%\n";
  }
};

#endif
