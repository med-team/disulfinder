#ifndef __COMMONMETHODS_H
#define __COMMONMETHODS_H

// DEPRECATED: use Util instead

typedef unsigned int uint;

#define MAX(x,y) ((x>y) ?x :y)
#define MIN(x,y) ((x<y) ?x :y)
#define MAX3(x,y,z) ((x>y) ?((x>z)?x:z) :((y>z)?y:z))
#define MIN3(x,y,z) ((x<y) ?((x<z)?x:z) :((y<z)?y:z))

#include <string>
#include "coords.h"
#include "Matrix.h"
#include "Exception.h"
#include "Accuracy.h"
#include "ConfusionMatrix.h"
#include "Statistics.h"
#include "Utility.h"
using namespace std;


string CheckDir(const string& folder);

void OutProgress(int count, int period, ostream& out=cout);
void EndProgress(int end, ostream &out=cout);

string StringF(const char *const format,...);

double unirand(double min, double max);

#endif
