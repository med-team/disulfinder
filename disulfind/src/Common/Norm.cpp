#include "CommonMethods.h"
#include <iostream>
#include <cassert>

int main(int argc, char ** argv){

  Matrix<double> M;

  if(argc > 1){
    cerr << "\nUsage:\n\t" << argv[0] << " < <matrix>\n\n";
    exit(1);
  }
  
  M.Read(cin);
  
  M.Norm().Write(cout);
}
