#ifndef COORDS_H
#define COORDS_H
#include "myreal.h"

#define COORDS REAL*

// initialization operations
COORDS MAKEVEC(REAL x, REAL y, REAL z);
COORDS MAKEVEC2(REAL *V);
#define ZERO(V)            bzero(V,REAL_THREE_SIZE)
#define COPY(D,S)          memcpy(D,S,REAL_THREE_SIZE)

// linear operations
#define INC(V,A)           { V[0]+=A[0]; V[1]+=A[1]; V[2]+=A[2]; } 
#define DEC(V,A)           { V[0]-=A[0]; V[1]-=A[1]; V[2]-=A[2]; } 
#define NEG(V,A)           { V[0]=-A[0]; V[1]=-A[1]; V[2]=-A[2]; }
#define MUL(V,A,s)         { V[0]=A[0]*s; V[1]=A[1]*s; V[2]=A[2]*s; }
#define DIV(V,A,s)         { V[0]=A[0]/s; V[1]=A[1]/s; V[2]=A[2]/s; }
#define SUM(V,A,B)         { V[0]=A[0]+B[0]; V[1]=A[1]+B[1]; V[2]=A[2]+B[2]; }
#define SUB(V,A,B)         { V[0]=A[0]-B[0]; V[1]=A[1]-B[1]; V[2]=A[2]-B[2]; }
#define DIVSUB(V,A,B,s)    { V[0]=(A[0]-B[0])/(s); V[1]=(A[1]-B[1])/(s); V[2]=(A[2]-B[2])/(s); }
#define WSUM(V,A,a,B,b)    { V[0]=A[0]*(a)+B[0]*(b); V[1]=A[1]*(a)+B[1]*(b); V[2]=A[2]*(a)+B[2]*(b); }

#define SCALAR(A,B)        (A[0]*B[0]+A[1]*B[1]+A[2]*B[2])

// vector product
#define CROSS(V,P,R)       { V[0]=P[1]*R[2]-P[2]*R[1]; V[1]=P[2]*R[0]-P[0]*R[2]; V[2]=P[0]*R[1]-P[1]*R[0]; }
#define DIVCROSS(V,P,R,s)  { V[0]=(P[1]*R[2]-P[2]*R[1])/(s); V[1]=(P[2]*R[0]-P[0]*R[2])/(s); V[2]=(P[0]*R[1]-P[1]*R[0])/(s); }
#define CROSSX(P,R)        (P[1]*R[2]-P[2]*R[1])
#define CROSSY(P,R)        (P[2]*R[0]-P[0]*R[2])
#define CROSSZ(P,R)        (P[0]*R[1]-P[1]*R[0])

// norm operations
#define DIST(P,R)          sqrt((P[0]-R[0])*(P[0]-R[0])+(P[1]-R[1])*(P[1]-R[1])+(P[2]-R[2])*(P[2]-R[2]))
#define NORM(P)            sqrt(P[0]*P[0]+P[1]*P[1]+P[2]*P[2])
#define NORMALIZE(P)       {REAL n=NORM(P); P[0]/=n; P[1]/=n; P[2]/=n; }

// output
#define TOSTREAM(V)        V[0] << " " << V[1] << " " << V[2]
#define PRINT(out,V)       (out << V[0] << " " << V[1] << " " << V[2] << " ")
#define PRINTLN(out,V)     (out << V[0] << " " << V[1] << " " << V[2] << endl)

// base change: the inverse of an orthonormal base matrix is equal to its transpose
#define BASE(D,Vx,Vy,Vz,S) { REAL x=S[0]*Vx[0]+S[1]*Vx[1]+S[2]*Vx[2]; REAL y=S[0]*Vy[0]+S[1]*Vy[1]+S[2]*Vy[2]; REAL z=S[0]*Vz[0]+S[1]*Vz[1]+S[2]*Vz[2]; D[0]=x; D[1]=y; D[2]=z; }

#endif
