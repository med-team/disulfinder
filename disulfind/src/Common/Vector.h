#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>
#include <math.h>

using namespace std;

class Vector {
  
 public:
  
  double x;
  double y;
  double z;

 public:
  
  Vector() {
    x=y=z=0.;
  }

  Vector(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }

  Vector(const Vector &src) {
    *this = src;
  }

  Vector& operator = (const Vector &src) {    
    x=src.x;
    y=src.y;
    z=src.z;
    return *this;
  }

  Vector operator + (const Vector &op2) const {
    Vector res;
    res.x = this->x + op2.x;
    res.y = this->y + op2.y;
    res.z = this->z + op2.z;
    return res;
  }

  Vector& operator += (const Vector &op2) {
    this->x = this->x + op2.x;
    this->y = this->y + op2.y;
    this->z = this->z + op2.z;
    return *this;
  }

  Vector operator - (const Vector &op2) const {
    Vector res;
    res.x = this->x - op2.x;
    res.y = this->y - op2.y;
    res.z = this->z - op2.z;
    return res;
  }

  Vector operator -= (const Vector &op2) {
    this->x = this->x - op2.x;
    this->y = this->y - op2.y;
    this->z = this->z - op2.z;
    return *this;
  }

  Vector operator % (const Vector &op2) {
    Vector res;
    res.x = this->y*op2.z - this->z*op2.y;
    res.y = this->z*op2.x - this->x*op2.z;
    res.z = this->x*op2.y - this->y*op2.x;
    return res;
  }

  Vector& operator %= (const Vector &op2) {
    return (*this = (*this)%op2);
  }

  Vector operator + (double s) const {
    Vector res;
    res.x = this->x+s;
    res.y = this->y+s;
    res.z = this->z+s;
    return res;
  }

  Vector& operator += (double s) {
    this->x = this->x+s;
    this->y = this->y+s;
    this->z = this->z+s;
    return *this;
  }

  Vector operator - (double s) const {
    Vector res;
    res.x = this->x-s;
    res.y = this->y-s;
    res.z = this->z-s;
    return res;
  }

  Vector& operator -= (double s) {
    this->x = this->x-s;
    this->y = this->y-s;
    this->z = this->z-s;
    return *this;
  }

  Vector operator * (double s) const {
    Vector res;
    res.x = this->x*s;
    res.y = this->y*s;
    res.z = this->z*s;
    return res;
  }

  Vector& operator *= (double s) {
    this->x = this->x*s;
    this->y = this->y*s;
    this->z = this->z*s;
    return *this;
  }

  Vector operator / (double s) const {
    Vector res;
    res.x = this->x/s;
    res.y = this->y/s;
    res.z = this->z/s;
    return res;
  }

  Vector& operator /= (double s) {
    this->x = this->x/s;
    this->y = this->y/s;
    this->z = this->z/s;
    return *this;
  }

  Vector normalize() const {
    return (*this / this->norm());
  }

  double operator * (const Vector &op2) const {
    return (this->x*op2.x + this->y*op2.y + this->z*op2.z);
  }

  double norm() const {
    return sqrt(x*x+y*y+z*z);
  }

  double dist(const Vector &op2) const {
    return sqrt((x-op2.x)*(x-op2.x)+(y-op2.y)*(y-op2.y)+(z-op2.z)*(z-op2.z));
  }  

};


Vector operator + (double s, const Vector &op);
Vector operator - (double s, const Vector &op);
Vector operator * (double s, const Vector &op);
  
Vector normalize(const Vector &op);

double norm(const Vector &op);
double dist(const Vector &op1, const Vector &op2);

istream& operator >> (istream &is, Vector &op);
ostream& operator << (ostream &os, const Vector &op);

#endif
