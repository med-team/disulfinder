#include "Vector.h"

Vector operator + (double s, const Vector &op) {
  Vector res;
  res.x = op.x+s;
  res.y = op.y+s;
  res.z = op.z+s;
  return res;
}

Vector operator - (double s, const Vector &op) {
  Vector res;
  res.x = op.x-s;
  res.y = op.y-s;
  res.z = op.z-s;
  return res;
}

Vector operator * (double s, const Vector &op) {
  Vector res;
  res.x = op.x*s;
  res.y = op.y*s;
  res.z = op.z*s;
  return res;
}


Vector normalize(const Vector &op) {
  return (op/op.norm());
}

double norm(const Vector &op) {
  return op.norm();
}


double dist(const Vector &op1, const Vector &op2) {
  return op1.dist(op2);
}

istream& operator >> (istream &is, Vector &op) {
  return (is >> op.x >> op.y >> op.z);
}

ostream& operator << (ostream &os, const Vector &op) {
  return (os << op.x << ' ' << op.y << ' ' << op.z);
}
