#include <stdlib.h>
#include <time.h>
#include "Util.h"

namespace Util {  

  //---------------------
  // STRING METHODS

  string StringF(const char *const format,...) {
    va_list arglist;
    
    char mex[10000];
    va_start(arglist,format);
    vsprintf(mex,format,arglist);
    
    return mex;
  }


  void removeEmptyStrings(vector<string>& s) {
    //removes empty Strings like: "";
    for(vector<string>::iterator k = s.begin(); k != s.end();){
      if(*k == ""){
	vector<string>::iterator m = k;
	k++;
	s.erase(m);
      }
      else
	k++;
    }
  } 

  string Clean(string s) {
    char *r = new char[s.length()];

    unsigned int c = 0;
    for(unsigned int i=0; i<s.length(); i++ ) {
      if( s[i]!=' ' && s[i]!='\n' && s[i]!='\t' ) {
	r[c++] = s[i];
      }
    }
    r[c] = '\0';
    
    return r;
  }

  void clean(string& s) {
    for(string::iterator k = s.begin(); k != s.end();){
      if((*k) == ' ' || (*k)=='\n' || (*k)=='\t' ){
	string::iterator m = k;
	k++;
	s.erase(m);
      }
      else
	k++;
    }
  }

  void clean(vector<string>& sa) {  
    for(unsigned int i = 0; i < sa.size(); i++)
      clean(sa[i]);
  }

  string chomp(const string& s) {
    if( s.substr(s.length()-1,1)=="\n" )
      return s.substr(0,s.length()-1);
    return s;
  }

  string squeeze(const string& s, char tosearch) {
    string s2 = "";
    
    char last = 0;
    for(unsigned int i = 0; i < s.size(); i++){
      if( i==0 || s[i]!=tosearch || last!=tosearch ) 
	s2 += s[i];
      last = s[i];
    }
    return s2;    
  }
  
  string replace(const string& s, char tosearch, char toreplace) {
    string s2 = "";
    for(unsigned int i = 0; i < s.size(); i++){
      if( s[i]==tosearch)
	s2 += toreplace;
      else
	s2 += s[i];
    }
    return s2;
  }
  
  string uppercase(const string& s) {
    string s2 = "";
    for(unsigned int i = 0; i < s.size(); i++){
      if(isalpha(s[i]))
	s2 += ::toupper(s[i]);
      else
	s2 += s[i];
    }
    return s2;
  }

  string toupper(const string& s) {
    return uppercase(s);
  }

  string lowercase(const string& s) {
    string s2 = "";
    for(unsigned int i = 0; i < s.size(); i++){
      if(isalpha(s[i]))
	s2 += ::tolower(s[i]);
      else
	s2 += s[i];
    }
    return s2;
  }

  string tolower(const string& s) {
    return lowercase(s);
  }
		     
  vector<string> tokenize(const string& s, const string& delim, bool squeeze_delim) {
    vector<string> tokenized;
    
    char *str = new char[s.length()+1];
    strcpy(str,s.c_str());
    for( char *nav = str; nav!=NULL; ) {
      char *token = strsep(&nav,delim.c_str());
      if( '\0'==token[0] && squeeze_delim ) 
	continue;
      tokenized.push_back(token);
    }
    delete str;
    return tokenized;
  }

  string trim(string in) {
    unsigned int i,l;
    for( i=0; i<in.size() && (in.c_str()[i]==' ' || in.c_str()[i]=='\t'); i++ );
    for( l=in.size()-1; l>=0 && (in.c_str()[l]==' ' || in.c_str()[l]=='\t'); l-- );
    return in.substr(i,l-i+1);
  }  

  string Trim(string in) {
    return trim(in);
  }    

 
  //-------------------
  // PIPE METHODS

  bool readPipe(vector<string> &dest, string command) {     
    FILE *fp = popen(command.c_str(), "r");
    if( fp==NULL ) 
      return false;

    size_t read = 0;
    char *buffer = NULL;
    while( getline(&buffer,&read,fp)!=-1 ) 	
      dest.push_back(trim(chomp(buffer)));
      
    pclose(fp);
    return true; 
  }

  string runCommand(string command) {     
    FILE *fp = popen(command.c_str(), "r");
    if( fp==NULL ) 
      return false;

    size_t read = 0;
    char *buffer = NULL;
    stringstream ss;
    while( getline(&buffer,&read,fp)!=-1 ) {
      ss << buffer;
    }
      
    pclose(fp);
    return ss.str(); 
  }
  //-------------------
  // FILENAME METHODS
  
  string CheckDir(const string& folder) {
    if( folder.size()==0 )
      return "/";
    if( folder[folder.size()-1]=='/' )
      return folder;
    return folder+'/';
  }
  
  const char* basename(const char* filename) {
    string buf = filename;
    string::size_type offset = buf.find_last_of("/");
    if(offset != string::npos && offset < buf.size()-1)
      buf = buf.substr(offset+1);
    return buf.c_str();
  }


  //------------------
  // STREAM METHODS
  
  void outProgress(int progress, int period, ostream& out) {
    if( ((progress+1)%period)==0 ) {
      out << StringF( "%d",progress+1 );
      out.flush();
    }
    else if( ((progress+1)%period)==(period/2) ) {
      out << ".";
      out.flush();
    }
  }

  void OutProgress(int progress, int period, ostream& out) {
    outProgress(progress,period,out);
  }


  void endProgress(int end, ostream &out) {
    out << StringF("..%d\n",end);
    out.flush();
  }

  void EndProgress(int end, ostream &out) {
    endProgress(end,out);
  }

  void outProgressPercentage(int progress, int end, ostream& out) {
    double p = (double)(progress*100)/(double)end;
    int ip   = (int)p;
    bool isint = (((double)ip)==p);

    if( isint ) {
      if( (ip%10)==0 ) 
	out << p << "." << flush;
      else if( (ip%10)==5 ) 
	out << "." << flush;
    }
  }
  
  void endProgressPercentage(ostream& out) {
    out << ".100%" << flush;
  }

  
  istream& readTag(istream &is, const char *tag) {
  
    string read_tag;
    Exception::Assert(is >> read_tag, "Error while reading <%s>", tag);
    Exception::Assert(read_tag==tag,  "Error while reading <%s>: unexpected value <%s>", tag, read_tag.c_str()); 
    
    return is;
  }

  ostream& writeTag(ostream &os, const char *tag) {
  
    Exception::Assert(os << tag << endl, "Error while writing <%s>", tag);
    
    return os;
  }

  //------------------
  // MATH METHODS

  void initrand() {
    srand(time(NULL));
  }

  int irand(int min, int max) {
    return (min + (rand()%(max-min+1)));
  }
  
  double urand(double min, double max) {
    double p = (double)rand()/(double)RAND_MAX;
    return (p*(max-min)+min);
  }

  double grand(double mean, double std) {
    double x = 0.;    
    for( int i=0; i<30; i++ ) 
      x += urand(0.,1.);
    
    return (0.6325*x-9.487);
  }

  double square(double v) {
    return v*v;
  }

  bool firstCombination( vector<uint> &counters, uint n, uint k) {
    if( k>n )
      return false;
    
    if( k==0 ) {
      counters.clear();
      return true;
    }

    counters.resize(k);
    for( uint i=0; i<k; i++ )
      counters[i] = i;
    return true;
  }
  
  bool nextCombination( vector<uint> &counters, uint n, uint k) {
    if( k==0 )
      return false;

    // find first counter to update
    uint c = k-1;
    for(; c>0 && counters[c]>=n+c-k; c--);
    if( c==0 && counters[c]==n-k ) {
      // last combination reached
      return false;
    }

    // update counters
    for(uint v = counters[c]+1; c<k; c++, v++ )
      counters[c] = v;
    
    return true;
  }
  
  void printCombination( vector<uint> &counters, uint n, uint k ) {
    cout << StringF("(%d/%d) ", n, k);
    for( uint i=0; i<k; i++ )
      cout << counters[i] << " ";
    cout << endl;
  }  

}
