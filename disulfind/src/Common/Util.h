#ifndef __UTIL_H
#define __UTIL_H

#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include "../Common/Exception.h"

#define foreach(i,v) for(typeof(v.begin()) i=v.begin(); i!=v.end(); i++)
#define foreachr(i,v) for(typeof(v.rbegin()) i=v.rbegin(); i!=v.rend(); i++)

namespace Util {  

  template <class ValueType>
    ValueType maxval(ValueType v1, ValueType v2) {
    return ((v1>v2) ?v1 :v2);
  }

  template <class ValueType>
    ValueType minval(ValueType v1, ValueType v2) {
    return ((v1<v2) ?v1 :v2);
  }  

  //---------------------
  // STRING METHODS

  string StringF(const char *const format,...);

  void removeEmptyStrings(vector<string>& s);

  string Clean(string s);
  void clean(string& s);

  void clean(vector<string>& sa);

  string chomp(const string& s);

  string squeeze(const string& s, char tosearch = ' ');
  
  string replace(const string& s, char tosearch, char toreplace);
  
  string uppercase(const string& s);
  string toupper(const string& s);

  string lowercase(const string& s);
  string tolower(const string& s);
		     
  vector<string> tokenize(const string& s, const string& delim = " ", bool squeeze_delim=false);

  string trim(string in);
  string Trim(string in);

  template <class ReturnType, class ValueType>
    ReturnType cast(const ValueType &in_value) {
    
    ReturnType ret_value;

    stringstream ss;
    ss << in_value;
    ss >> ret_value;
    
    return ret_value;
  }

  template <class T> bool getnext(char **nav, T& val) {
    if( nav!=NULL && *nav!=NULL) {
      val = cast<T>(strsep(nav,":"));    
      return true;
    }
    return false;
  }

  template <class T> bool getnext(vector<string>::iterator &nav, vector<string>::iterator &end, T& val) {
    if( nav==end )
      return false;

    if( nav->length()>0 )
      val = cast<T>(*nav);

    nav++;
    return true;
  }
 
  //-------------------
  // PIPE METHODS

  bool readPipe(vector<string> &dest, string command);
  string runCommand(string command);

  //-------------------
  // FILENAME METHODS
  
  string CheckDir(const string& folder);
  
  const char* basename(const char* filename);
  
  //------------------
  // STREAM METHODS

  template <class ReturnType>
    ReturnType get(istream &is) {
    
    ReturnType t;
    Exception::Assert(is >> t, "Error while reading from stream");
    return t;
  }

  template <class T>
    void readfile(vector<T> &dst, string filename) {

    // empty array
    dst.clear();

    // open file
    ifstream ifs(filename.c_str());
    Exception::Assert(ifs,"Cannot open file <%s> for reading", filename.c_str());
    
    // read entries
    T dummy;
    while( ifs >> dummy ) 
      dst.push_back(dummy);
  }
  
  void outProgress(int progress, int period, ostream& out = cout);
  void OutProgress(int progress, int period, ostream& out = cout);

  void endProgress(int end, ostream &out = cout);
  void EndProgress(int end, ostream &out = cout);

  void outProgressPercentage(int progress, int end, ostream& out = cout);
  
  void endProgressPercentage(ostream& out = cout);
  
  istream& readTag(istream &is, const char *tag);

  ostream& writeTag(ostream &os, const char *tag);

  template <class ValueType>
    istream& readOption(istream &is, const char *tag, ValueType &value) {
    
    readTag(is,tag); 
    Exception::Assert(is >> value, "Error while reading <%s>", tag);
    
    return is;
  }

  template <class ValueType>
    istream& readOptionArray(istream &is, const char *tag, ValueType* values, int size) {

    readTag(is,tag);
    for( int i=0; i<size; i++ ) 
      Exception::Assert(is >> values[i], "Error while reading <%s>", tag);
    
    return is;
  }

  template <class ValueType>
    ostream& writeOption(ostream &os, const char *tag, ValueType &value) {
    Exception::Assert(os << tag << " " << value << endl, "Error while writing <%s>", tag);
    return os;
  }

  template <class ValueType>
    ostream& writeOptionArray(ostream &os, const char *tag, ValueType* values, int size) {
    Exception::Assert(os << tag << " ", "Error while writing <%s>", tag);
    for( int i=0; i<size; i++ ) 
      Exception::Assert(os << values[i] << " ", "Error while writing <%s>", tag);
    Exception::Assert(os << endl, "Error while writing <%s>", tag);
    
    return os;
  }

  //------------------
  // MATH METHODS

  template <class T>
    T square(T x) {
    return x*x;
  }

  void initrand();

  int irand(int min, int max);
  
  double urand(double min, double max);

  double grand(double mean, double std);

  double square(double v);

  bool firstCombination( vector<uint> &counters, uint n, uint k);
  bool nextCombination( vector<uint> &counters, uint n, uint k);
  void printCombination( vector<uint> &counters, uint n, uint k );
    
}

#endif
