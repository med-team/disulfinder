#ifndef __TUPLE_H
#define __TUPLE_H

#include <iostream>

namespace tuple {

  template<class TF, class TS>
    class duple {
    
    public:
    
    TF first;
    TS second;

    public:

    duple() {
    }
    
    duple(const TF &f, const TS &s) {
      first = f;
      second = s;
    }

    duple(const duple<TF,TS>& src) {
      first = src.first;
      second = src.second;
    }

    virtual ~duple() {
    }

    duple<TF,TS>& operator = (const duple<TF,TS>& src) {
      first = src.first;
      second = src.second;
      return *this;
    }

    bool operator < (const duple<TF,TS>& op2) const {
      if( this->first<op2.first )
	return true;
      if( this->first>op2.first )
	return false;
      return (this->second<op2.second);
    }

    bool operator > (const duple<TF,TS>& op2) const {
      if( this->first>op2.first )
	return true;
      if( this->first<op2.first )
	return false;
      return (this->second>op2.second);
    }

    istream& read(istream &is) {
      is >> first;
      is >> second;
      return is;
    }
        
  };
  
  template<class TF, class TS, class TT>
    class triplet {
    
    public:
    
    TF first;
    TS second;
    TT third;

    public:

    triplet() {
    }
    
    triplet(const TF &f, const TS &s, const TT &t) {
      first  = f;
      second = s;
      third  = t;
    }

    triplet(const triplet<TF,TS,TT>& src) {
      first  = src.first;
      second = src.second;
      third  = src.third;
    }

    virtual ~triplet() {
    }

    triplet<TF,TS,TT>& operator = (const triplet<TF,TS,TT>& src) {
      first  = src.first;
      second = src.second;
      third  = src.third;
      return *this;
    }

    bool operator < (const triplet<TF,TS,TT>& op2) const {
      if( this->first<op2.first )
	return true;
      if( this->first>op2.first )
	return false;
      if( this->second<op2.second )
	return true;
      if( this->second>op2.second )
	return false;
      return (this->third<op2.third);
    }

    bool operator > (const triplet<TF,TS,TT>& op2) const {
      if( this->first>op2.first )
	return true;
      if( this->first<op2.first )
	return false;
      if( this->second>op2.second )
	return true;
      if( this->second<op2.second )
	return false;
      return (this->third>op2.third);
    }

    istream& read(istream &is) {
      is >> first;
      is >> second;
      is >> third;
      return is;
    }
        
  };


  template<class TF, class TS>
    duple<TF,TS> make_tuple(const TF &f, const TS &s) {
    return duple<TF,TS>(f,s);
  }

  template<class TF, class TS, class TT>
    triplet<TF,TS,TT> make_tuple(const TF &f, const TS &s, const TT &t) {
    return triplet<TF,TS,TT>(f,s,t);
  }

  template<class TF, class TS>
    istream& operator >> (istream &is, duple<TF,TS>& dst) {
    return (is >> dst.first >> dst.second);
  }

  template<class TF, class TS, class TT>
  istream& operator >> (istream &is, triplet<TF,TS,TT>& dst) {
    return (is >> dst.first >> dst.second >> dst.third);
  } 
  
  template<class TF, class TS>
    ostream& operator << (ostream &os, const duple<TF,TS>& src) {
    return (os << src.first << " " << src.second);
  }
  
  template<class TF, class TS, class TT>
    ostream& operator << (ostream &os, const triplet<TF,TS,TT>& src) {
    return (os << src.first << " " << src.second << " " << src.third);
  }

}

#endif
