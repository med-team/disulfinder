#ifndef __hmatrix_H
#define __hmatrix_H

#include <Common/Util.h>

using namespace Util;
using namespace std;

template<class T>
class matrix {

 private:
  
  uint norows;
  uint nocols;
  vector<vector<T> > buffer;

 public:

  matrix(uint r=0, uint c=0, T init = T()) {
    norows = nocols = 0;
    resize(r,c,init);
  }

  matrix(const matrix &src) {    
    norows = src.norows;
    nocols = src.nocols;
    buffer = src.buffer;    
  }

  matrix(istream &is) {
    read(is);
  }

  void clear() {
    norows = 0;
    nocols = 0;
    buffer.clear();
  }

  void resize(uint r=0, uint c=0, T init=T()) {
    norows = r;
    nocols = c;

    buffer.resize(norows);
    for(uint i=0; i<r; i++ ) 
      buffer[i].resize(nocols,init);
  }

  T** getData() const {
    if( norows>0 ) {
      T** data = new T*[norows];
      for( uint r=0; r<norows; r++ ) {
	if( nocols>0 ) {
	  data[r] = new T[nocols];
	  for( uint c=0; c<nocols; c++ ) 
	    data[r][c] = buffer[r][c];
	}
	else {
	  data[r] = NULL;
	}
      }
      return data;
    }
    return NULL;
  }

  const T& operator () (uint i, uint j) const {
    return buffer[i][j];
  }

  T& operator () (uint i, uint j) {
    return buffer[i][j];
  }

  const vector<T>& operator [] (uint i) const {
    return buffer[i];
  }

  vector<T>& operator [] (uint i) {
    return buffer[i];
  }
  
  uint getNoRows() const {
    return norows;
  }

  uint getNoCols() const {
    return nocols;
  }

  void addRow(T init = T()) {
    buffer.push_back(vector<T>(nocols,init));
    norows++;
  }

  void addCol(T init = T()) {
    if( norows>0 ) {
      for( unsigned int i=0; i<norows; i++ ) 
	buffer[i].push_back(init);
    }
    nocols++;
  }

  void output(ostream &os=cout ) {
    for( uint r=0; r<norows; r++ ) {
      for( uint c=0; c<nocols; c++ ) {
	os << buffer[r][c] << " ";
      }
      os << endl;    
    }
    os << endl;
  }

  void read(istream &is) {    
    resize(get<uint>(is),get<uint>(is));
    for( uint r=0; r<norows; r++ ) 
      for( uint c=0; c<nocols; c++ ) 
	buffer[r][c] = get<T>(is);
  }
  
  void write(ostream &os) {
    os << norows << " " << nocols << endl;
    for( uint r=0; r<norows; r++ ) {
      for( uint c=0; c<nocols; c++ ) 
	os << buffer[r][c] << " ";     
      os << endl;
    }
  }

};

#endif
