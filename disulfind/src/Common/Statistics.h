#ifndef __STATISTICS_H
#define __STATISTICS_H

#include <math.h>
#include <string>
#include <iostream>
using namespace std;

class Statistics 
{
 protected:

  string var_name;
  int no_values;
  double sum;  
  double square_sum;
  double min;
  double max;

 public:

  Statistics(const string &name="") {
    var_name = name;
    clear();
  }

  void add(double value) {
    Add(value);
  }

  void Add(double value) {
    sum += value;
    square_sum += value*value;
    min = (no_values==0 || value<min) ?value :min;
    max = (no_values==0 || value>max) ?value :max;
    no_values++;
  }

  void clear() {
    no_values = 0;
    sum = square_sum = 0.;
    min = 0.;
    max = 0.;
  }

  int getNoValues() const { return no_values; }

  double mean() const    { return getMean(); }
  double getMean() const { return ((no_values>0) ?(sum/(double)no_values) :0.); }

  double stddev() const    { return getStdDev(); }
  double getStdDev() const { return ((no_values>0) ?sqrt(square_sum/(double)no_values-pow(getMean(),2.)) :0.); }

  double var() const    { return getVar(); }
  double getVar() const { return ((no_values>0) ?(square_sum/(double)no_values-pow(getMean(),2.)) :0.); }
  
  double rms() const {
    return getRMS();
  }

  double getRMS() const {
    return ((no_values>0) ?sqrt(square_sum/(double)no_values) :0.);
  }

  double getMin() const {
    return min;
  }

  double getMax() const {
    return max;
  }

  void Output(ostream &os=cout) const {
    os << var_name << " statistics: ";
    os << "mean=" << getMean() << " dev=" << getStdDev() << " min=" << getMin() << " max=" << getMax();
  }

  friend ostream& operator << (ostream& os, const Statistics& stat);
};

class Correlation
{
 protected:
    
  Statistics xy;
  Statistics x;
  Statistics y;  

 public:
  
  Correlation(const string &x_name="", const string &y_name="") 
    :xy("product"), x(x_name), y(y_name)
    {}

  void add(double x_value, double y_value) {
    Add(x_value,y_value);
  }

  void Add(double x_value, double y_value) {
    xy.Add(x_value*y_value);
    x.Add(x_value);
    y.Add(y_value);  
  }
  
  double correlation() const    { return getCorrelation(); }
  double getCorrelation() const; 
};


#endif
