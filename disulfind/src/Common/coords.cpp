#include <string.h>
#include "coords.h"

REAL* MAKEVEC(REAL x, REAL y, REAL z) { 
  REAL *V = new REAL[3]; 
  V[0]=x; V[1]=y; V[2]=z; 
  return V;
}

REAL* MAKEVEC2(REAL *src) { 
  REAL *V = new REAL[3]; 
  COPY(V,src);
  return V;
}
