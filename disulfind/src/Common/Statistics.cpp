#include "Statistics.h"


ostream& operator << (ostream& os, const Statistics& stat)
{
  stat.Output(os);  
  return os;
}

double Correlation::getCorrelation() const 
{ 
  double x_dev = x.stddev();
  double y_dev = y.stddev();
  if( x_dev==0. || y_dev==0. )
    return 0.;   
  return ((xy.mean() - x.mean()*y.mean())/(x_dev*y_dev));
}	
