#include <iostream>
#include <fstream>
#include <cassert>
#include "CommonMethods.h"

int main(int argc, char ** argv){

  Matrix<double> M,E,V;

  if(argc < 4){
    cerr << "\nUsage:\n\t" << argv[0] << " <matrix> <eigenvalues> <eigenvectors>\n\n";
    exit(1);
  }
  
  ifstream in(argv[1]);
  ofstream out_v(argv[2]);
  ofstream out_e(argv[3]);
  assert(in.good() && out_v.good() && out_e.good());

  M.Read(in);
  
  M.Eig(V,E);
  
  E.Write(out_e);
  V.Write(out_v);
}
