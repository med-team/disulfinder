#include "Utility.h"
#include <boost/tokenizer.hpp>

using namespace std;

void clean(vector<string>& sa)
{  
  for(unsigned int i = 0; i < sa.size(); i++)
    clean(sa[i]);
}

void clean(string& s)
{
  for(string::iterator k = s.begin(); k != s.end();){
    if((*k) == ' '){
      string::iterator m = k;
      k++;
      s.erase(m);
    }
    else
      k++;
  }
}

void removeEmptyStrings(vector<string>& s)
{
  //removes empty Strings like: "";
  for(vector<string>::iterator k = s.begin(); k != s.end();){
    if(*k == ""){
      vector<string>::iterator m = k;
      k++;
      s.erase(m);
    }
    else
      k++;
  }
}

vector<string> tokenize(const string& s, const string& token)
{
  vector<string> tokenized;
  typedef boost::tokenizer<boost::char_separator<char> > 
    tokenizer;
  boost::char_separator<char> sep(token.c_str());
  tokenizer tok(s, sep);
  for (tokenizer::iterator beg=tok.begin(); beg!=tok.end();beg++)
    tokenized.push_back(*beg);
  return tokenized;
}

string uppercase(const string& s)
{
  string s2;
  for(unsigned int i = 0; i < s.size(); i++){
    if(isalpha(s[i]))
      s2 += toupper(s[i]);
    else
      s2 += s[i];
  }
  return s2;
}

double average(double* d, int size)
{
  double result = 0;
  for(int i = 0; i < size; i++){
    result += d[i];
  }
  return result/(double)size;
}

double Max(double* d, int size)
{
  double result = 0;
  for(int i = 0; i < size; i++){
    if(d[i] > result){
      result = d[i];
    }
  }
  return result;
}

//const char* basename(const char* filename)
//{
// string buf = filename;
//  int offset = buf.find_last_of("/");
//  if(offset != string::npos && offset < buf.size()-1)
//    buf = buf.substr(offset+1);
//  return buf.c_str();
//}
