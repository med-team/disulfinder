#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include "CommonMethods.h"

string CheckDir(const string& folder) {
  if( folder.size()==0 )
    return "/";
  if( folder[folder.size()-1]=='/' )
    return folder;
  return folder+'/';
}

void OutProgress(int progress, int period, ostream& out) {
  if( ((progress+1)%period)==0 ) {
    out << StringF( "%d",progress+1 );
    out.flush();
  }
  else if( ((progress+1)%period)==(period/2) ) {
    out << ".";
    out.flush();
  }
}

void EndProgress(int end, ostream &out) {
  out << StringF("..%d\n",end);
  out.flush();
}

string StringF(const char *const format,...) {
  va_list arglist;
  
  char mex[10000];
  va_start(arglist,format);
  vsprintf(mex,format,arglist);
  
  return mex;
}

REAL unirand(REAL a, REAL b) { 
  REAL p = (REAL)rand()/(REAL)RAND_MAX;
  return (a + p*(b-a));
}
