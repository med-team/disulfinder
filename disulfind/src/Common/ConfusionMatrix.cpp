#include <stdio.h>
#include "Exception.h"
#include "ConfusionMatrix.h"

ConfusionMatrix::ConfusionMatrix(int noclasses) 
{ 
  m_noclasses=noclasses; 
  buffer=new int[m_noclasses*m_noclasses]; 
  for( int r=0; r<noclasses; r++ )
    for( int c=0; c<noclasses; c++ )
      elem(r,c)=0;
}

double ConfusionMatrix::Accuracy()
{
  double num=0.;
  double div=0.;
  for(int i=0; i<m_noclasses; i++ ) {
    num += (double)elem(i,i);
    for( int j=0; j<m_noclasses; j++ )
      div += (double)elem(i,j);
  }
  return (num/div);
}

double ConfusionMatrix::Precision(int indclass)
{
  double div=0.;
  double num = (double)elem(indclass,indclass);
  for(int i=0; i<m_noclasses; i++ )
    div += (double)elem(indclass,i);
  return (num/div);
}

double ConfusionMatrix::Recall(int indclass)
{
  double div=0.;
  double num = (double)elem(indclass,indclass);
  for(int i=0; i<m_noclasses; i++ )
    div += (double)elem(i,indclass);
  return (num/div);
}

void ConfusionMatrix::Output( ostream &out )
{
  out << "Accuracy = " << (Accuracy()*100.) << "\%\n";
  for( int c=0; c<m_noclasses; c++ ) {
    out << "Precision class " << c << " = " << (Precision(c)*100.) << "\%\n";
    out << "Recall class    " << c << " = " << (Recall(c)*100.)    << "\%\n";
  }
  
  //out.setf(ios_base::left,ios_base::adjustfield);
  char str[10];
  out<<"\n\nConfusion matrix: \n\n";
  for( int r=0; r<m_noclasses; r++ ) {
    for( int c=0; c<m_noclasses; c++ ) {
      sprintf( str, "%6d ", elem(r,c) );
      out << str;
    }
    out << endl;
  }
}
