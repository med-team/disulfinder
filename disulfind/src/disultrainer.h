#include <stdlib.h>
#include <iostream>
#include <string>
#include <string.h>
#include <getopt.h>

using namespace std;

class CommandLineOption
{
private:

  typedef unsigned int uint;
  static const option optionNames[];

public:

  ////////////////////////////
  // Command line variables //
  ////////////////////////////

  bool train;              // -t --train: train SVM
  bool test;               // -T --test: test SVM
  string input;            // -i --input: file containing list of input chains
  string wdir;             // -w --workdir: working directory (default=PWD)
  string modeldir;         // -m --modeldir: model directory (default=PWD)
  string psidir;           // -p --psidir: profiles directory (default=PWD)
  string descstats;        // -d --descstats: file containing statistics for global descriptor (default = "")  
  string labeldir;         // -l --labeldir: labels directory (default="")
  unsigned int w;          // -W --window: one-sided window size (default=7)  
  bool use_pssm;           // -P --usepssm: use pssm instead of counts for profiles (default=false)
  string kernelparams;     // -k --kernelparams: string containing kernel parameters (default = "-t 1 -d 3")
  string brnnparams;       // -b --brnnparams: string containing brnn parameters (default = "-f -l 1e-4 -b -m 0.1 -n 1e-5 -i 300")
  unsigned int brnntrials; // -r --brnntrials: number of network training trials, choosing best on validation set (default = 10)
  unsigned int folds;      // -f --folds: number of folds for kfold cross validation (default = 3)
  // Constructor
  CommandLineOption(int argc = 0, char *argv[]=NULL);
  void usage(char* progname);
  

 private:

  void parse_args(int argc, char *argv[]);

};


const option CommandLineOption::optionNames[] =
{ 
  { "train",         no_argument, 0, 't'},
  { "test",          no_argument, 0, 'T'},
  { "input",         required_argument, 0, 'i'},
  { "workdir",       required_argument, 0, 'w'},
  { "modeldir",      required_argument, 0, 'm'},
  { "psidir",        required_argument, 0, 'p'},
  { "descstats",     required_argument, 0, 'd'},
  { "labeldir",      required_argument, 0, 'l'},
  { "window",        required_argument, 0, 'W'},
  { "usepssm",       no_argument, 0, 'P'},
  { "kernelparams",  required_argument, 0, 'k'},
  { "brnnparams",    required_argument, 0, 'b'},
  { "brnntrials",    required_argument, 0, 'r'},
  { "folds",         required_argument, 0, 'f'},
  { "help",          no_argument,       0, '?'},
  { 0,              0,                 0, 0  }
};
  
  


CommandLineOption::CommandLineOption(int argc, char *argv[]) :
  train(false),
     test(false),
     input(""),     
     wdir(getenv("PWD")),
     modeldir(getenv("PWD")),
     psidir(getenv("PWD")),
     descstats(""),
     labeldir(""),
     w(7),
     use_pssm(false),
     kernelparams("-t 1 -d 3"),
     brnnparams("-f -l 1e-4 -b -m 0.1 -n 1e-5 -i 300"),
	 brnntrials(10),
     folds(3)
{
  int c;
  int* opti = 0;
  
  // you must put ':' only after parameters that require an argument
  // so after 'a' and '?' we don't put ':'

  while ((c = getopt_long(argc, argv, "tTi:w:m:p:d:l:W:k:b:r:f:P?", optionNames, opti)) != -1) {
    switch(c) {
    case 't':
      train = true;
      break;
    case 'T':
      test = true;
      break;
    case 'i':
      input = optarg;
      break;
    case 'w':
      wdir = optarg;
      break;
    case 'm':
      modeldir = optarg;
      break;
    case 'p':
      psidir = optarg;
      break;
    case 'd':
      descstats = optarg;
      break;     
    case 'l':
      labeldir = optarg;
      break;
    case 'W':
      w = atoi(optarg);
      break;
    case 'P':
      use_pssm = true;
      break;
    case 'k':
      kernelparams = optarg;
      break;
    case 'b':
      brnnparams = optarg;
      break;
    case 'r':
      brnntrials = atoi(optarg);
      break;      
    case 'f':
      folds = atoi(optarg);
      break;      
    case '?':
    default:
      usage(*argv);      
    }
  }
}

void CommandLineOption::usage(char* progname) {
  cerr << "usage: " << basename(progname) << " ..." << endl;
  cerr << "\t-t --train: train SVM" << endl;
  cerr << "\t-T --test: test SVM" << endl;
  cerr << "\t-i --input: file containing list of input chains" << endl;
  cerr << "\t-w --workdir: working directory (default=PWD)" << endl;
  cerr << "\t-m --modeldir: model directory (default=PWD)" << endl;
  cerr << "\t-p --psidir: profiles directory (default=PWD)" << endl;
  cerr << "\t-d --descstats: file containing statistics for global descriptor (default = \"\")  " << endl;
  cerr << "\t-l --labeldir: labels directory (default=\"\")" << endl;
  cerr << "\t-W --window: one-sided window size (default=7)" << endl;
  cerr << "\t-P --usepssm: use pssm instead of counts for profiles (default=false)" << endl;
  cerr << "\t-k --kernelparams: string containing kernel parameters (default = \"-t 1 -d 3\")" << endl;
  cerr << "\t-b --brnnparams: string containing brnn parameters (default = \"-f -l 1e-4 -b -m 0.1 -n 1e-5 -i 300\")" << endl;
  cerr << "\t-r --brnntrials: number of network training trials, choosing best on validation set (default = 10)" << endl;
  cerr << "\t-f --folds: number of folds for kfold cross validation (default = 3)" << endl;
  cerr << "\t-?, --help: this message" << endl;
  exit(0);
}
