#ifndef __PROTEIN_H
#define __PROTEIN_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
using namespace std;

//------------------------------------------------------------------------------------------------
// Amino-acid single letter code
//------------------------------------------------------------------------------------------------

#define NO_AA 20
#define AA_NONE -1
static const char *aa_codes = "VLIMFWYGAPSTCHRKQENDBZX"; // as ordered in profile files
static const char *tdb_aa_codes = "ARNDCQEGHILKMFPSTWYVBZX"; // as ordered in tdb profile files

static const char *aa_three_codes[23] = {
  "VAL", "LEU", "ILE", "MET", "PHE", 
  "TRP", "TYR", "GLY", "ALA", "PRO", 
  "SER", "THR", "CYS", "HIS", "ARG", 
  "LYS", "GLN", "GLU", "ASN", "ASP",
  "d|n", "q|e", "all"
};

//------------------------------------------------------------------------------------------------
// Amino-acid features
//------------------------------------------------------------------------------------------------

#define NO_AA_FEAT 9

//static const char* feat_names[NO_AA_FEAT] = { "Volume", "Area", "Hydrophobicity", "Polarity", "Non_polarity", "Aromatic", "Aliphatic", "Pos_charge", "Neg_charged" };

static const double aa_features[NO_AA][NO_AA_FEAT] = {
{( 88.6-60.1)/(227.8-60.1), (115.-75.)/(255.-75.),  0.4, 0., 1., 0., 0., 0., 0. }, // A
{(173.4-60.1)/(227.8-60.1), (225.-75.)/(255.-75.), -0.7, 1., 0., 0., 0., 1., 0. }, // R
{(111.1-60.1)/(227.8-60.1), (150.-75.)/(255.-75.), -0.4, 1., 0., 0., 0., 0., 1. }, // D
{(114.1-60.1)/(227.8-60.1), (160.-75.)/(255.-75.), -0.3, 1., 0., 0., 0., 0., 0. }, // N
{(108.5-60.1)/(227.8-60.1), (135.-75.)/(255.-75.),  0.9, 0., 1., 0., 0., 0., 0. }, // C
{(138.4-60.1)/(227.8-60.1), (190.-75.)/(255.-75.), -0.5, 1., 0., 0., 0., 0., 1. }, // E
{(143.8-60.1)/(227.8-60.1), (180.-75.)/(255.-75.), -0.5, 1., 0., 0., 0., 0., 0. }, // Q
{( 60.1-60.1)/(227.8-60.1), ( 75.-75.)/(255.-75.),  0.4, 0., 1., 0., 0., 0., 0. }, // G
{(153.2-60.1)/(227.8-60.1), (195.-75.)/(255.-75.),  0.1, 1., 1., 1., 0., 1., 0. }, // H
{(166.7-60.1)/(227.8-60.1), (175.-75.)/(255.-75.),  0.8, 0., 1., 0., 1., 0., 0. }, // I
{(166.7-60.1)/(227.8-60.1), (170.-75.)/(255.-75.),  0.6, 0., 1., 0., 1., 0., 0. }, // L
{(168.6-60.1)/(227.8-60.1), (200.-75.)/(255.-75.), -0.9, 1., 1., 0., 0., 1., 0. }, // K
{(162.9-60.1)/(227.8-60.1), (185.-75.)/(255.-75.),  0.5, 0., 1., 0., 0., 0., 0. }, // M
{(189.9-60.1)/(227.8-60.1), (210.-75.)/(255.-75.),  0.6, 0., 1., 1., 0., 0., 0. }, // F
{(112.7-60.1)/(227.8-60.1), (145.-75.)/(255.-75.), -0.1, 0., 0., 0., 0., 0., 0. }, // P
{( 89.0-60.1)/(227.8-60.1), (115.-75.)/(255.-75.),  0.1, 1., 0., 0., 0., 0., 0. }, // S
{(116.1-60.1)/(227.8-60.1), (140.-75.)/(255.-75.),  0.0, 1., 0., 0., 0., 0., 0. }, // T
{(227.8-60.1)/(227.8-60.1), (255.-75.)/(255.-75.),  0.4, 1., 1., 1., 0., 0., 0. }, // W
{(193.6-60.1)/(227.8-60.1), (230.-75.)/(255.-75.), -0.2, 1., 1., 1., 0., 0., 0. }, // Y
{(140.0-60.1)/(227.8-60.1), (155.-75.)/(255.-75.),  0.7, 0., 1., 0., 1., 0., 0. }, // V
};



//------------------------------------------------------------------------------------------------
// Protein container
//------------------------------------------------------------------------------------------------

class Protein
{
 public:
  
  //data types
  
  static int  AAnum(char aa);
  static char AAcode(const char *aa);
  static char CheckAA(char aa);
  static char CheckSS(char ss);
  
  enum SSClass { SS_H=0, SS_E=1, SS_C=2, SS_UNKNOWN=3 };
  static const char *SSClassNames;
  static SSClass SSCode(char ss_class);
  
  enum DisulfideClass{ DS_NONE=0, DS_ALL=1, DS_MIXED=2, DS_UNKNOWN=3 };
  
 public:
  
  // constructors
  Protein() { dsclass=DS_UNKNOWN; no_cys_total=0; no_cys_linked=0; }
  Protein(const Protein &src) { *this=src; }
  
  const Protein& operator = (const Protein &src);
  bool operator >  (const Protein &src) const { return (this->name > src.name); }
  bool operator <  (const Protein &src) const { return (this->name < src.name); }
  bool operator == (const Protein &src) const { return (this->name == src.name); }
  friend bool operator >  (const Protein &op1,const Protein &op2) { return (op1.name > op2.name); }
  friend bool operator <  (const Protein &op1,const Protein &op2) { return (op1.name < op2.name); }
  friend bool operator == (const Protein &op1,const Protein &op2) { return (op1.name == op2.name); }

  // methods
  void ReadCB513(istream &fp);
  void ReadHSSP(istream &fp, double prior=0.5);
  void ReadDSSP(istream &fp, char chain=0, bool skip_unknown=false);
  void ReadStride(istream &fp, char chain=0);
  void ReadSequence(istream &fp, bool treat_inter_as_bonded=false);
  void ReadProfile(istream &fp);
  void ReadPsiBlast0(istream &is);
  void ReadPsiBlast2(istream &is, double prior=0.5);
  void ReadTDB(istream &is);
  void ReadSwissProt(istream &is);
  
  static void ReadCB513Set(vector<Protein> &infos, const string &data_dir, bool output);
  static void ReadHSSPSet(vector<Protein> &infos, const string &data_dir, bool output, double prior=0.5);
  static void ReadDSSPSet(vector<Protein> &infos, const string &dssp_dir, const string &hssp_dir, bool readprofile, bool output, double prior=0.5);
  static void ReadPSIBLASTSet(vector<Protein> &infos, const string &seqdir, const string &profiledir, bool output, bool treat_inter_as_bonded=false);
  void Clean();
  
  string GetSequence() const;
  int IsDisulfideClass(Protein::DisulfideClass dsclass) const { return ((this->dsclass==dsclass) ?1 :-1); }
  
  // protein informations
  string name;
  vector<char> sequence;
  vector<int>  sequence_num;
  vector<string>  sequence_num_str;
  vector<char> chain_code;
  vector<char> sec_structure;
  vector<char> sec_structure_dssp;
  vector<map<char,double> > profile;
  vector<map<char,double> > pssm;
  vector<double> aligned_num;
  vector<double> entropy;
  vector<double> weight;
  vector<double> aa_kappa; // angles
  vector<double> aa_alpha;
  vector<double> aa_phi; 
  vector<double> aa_psi;
  vector<double> aa_x;   //coordinates
  vector<double> aa_y;
  vector<double> aa_z;
  vector<int> accessibility;
  vector<int> bonding_state_all; // 0:not cys, -1:not bonded, 1:bonded, 2:inter chain
  vector<int> beta_partner1;      // -1:no partner, -2:unknown partner, -2:inter chain, >=0:position of the partner
  vector<int> beta_partner2;      // -1:no partner, -2:unknown partner, -2:inter chain, >=0:position of the partner
  vector<char> beta_id;          // character identifying beta-sheet
  
  // values correspondent to cysteines
  vector<bool> bonding_state; // true:bonded, false:not bonded
  vector<int> cys_position;   // position of the cystein in the sequence
  vector<int> cys_coupled;    // -1:no partner, -2:unknown partner, -3:inter_chain, >=0:position of the partner 

  // global values
  
  DisulfideClass dsclass;
  int no_cys_total;
  int no_cys_linked;	
  vector<double> descriptors;
  
 private: // private methods
  
  static void AddDescriptors(vector<Protein> &infos);
  static void SkipLine(istream &fp);
  static bool ReadCharLine(vector<char> &line, istream &fp, string &identifier, int linenum);
};


#endif


