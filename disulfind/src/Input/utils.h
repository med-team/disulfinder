#include "../Common/Matrix.h"
#include "Protein.h"

Matrix<double> makeWindow(const Matrix<double>& M, int k);
void makeSparseVector(const Matrix<double>& M, ostream& out);
void buildXfolds(const char * example_file, unsigned int folds_number);
