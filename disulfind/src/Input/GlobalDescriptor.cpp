#include <string.h>
#include <fstream>
#include "utils.h"
#include "GlobalDescriptor.h"

#define NUMAMINOS 20
#define NO_BARS 5
#define AMINOS "ACDEFGHIKLMNPQRSTVWY"
#define MINFREQ -1

Matrix<double> GlobalDescriptor::toMatrix() const
{
  unsigned int cols = NUMAMINOS + 4 + NO_BARS;
  Matrix<double> D(1,cols);
  unsigned int i = 0;
  for(; i < NUMAMINOS; i++)
    D(0,i) = m_aminoacid_frequency[i];
  D(0,i++) = m_rel_seq_length;
  D(0,i++) = m_abs_cys_num;
  D(0,i++) = m_rel_cys_num;
  D(0,i++) = (double)m_parity;
  for(unsigned int j = 0; j < NO_BARS; j++)
    D(0,i+j) = m_cyscon[j];   
  
  return D;
}

void GlobalDescriptor::Write(ostream & out) const
{
  for(unsigned int i = 0; i < NUMAMINOS; i++)
    out << m_aminoacid_frequency[i] << " ";
  out << m_rel_seq_length << " " 
      << m_abs_cys_num << " " 
      << m_rel_cys_num << " " 
      << m_parity;
  for(unsigned int i = 0; i < NO_BARS; i++)
    out << " " << m_cyscon[i];   
}

GlobalDescriptorMaker::GlobalDescriptorMaker()
{
  init();
}  

GlobalDescriptorMaker::GlobalDescriptorMaker(const char * infile)
{
  ifstream in(infile);
  Exception::Assert(in.good(), "Error: could not read file %s", infile); 
  Read(in);
}

GlobalDescriptorMaker::GlobalDescriptorMaker(istream & in)
{
  Read(in);
}

void GlobalDescriptorMaker::Read(istream & in)
{
  char amino;
  int freq;
  for(int i = 0; i < NUMAMINOS; i++){
    in >> amino >> freq >> ws;
    m_overall_aminoacid_frequency[amino] = freq;
  }  
  in >> m_mean_sequence_length >> ws;
}

void GlobalDescriptorMaker::Write(ostream & out)
{
  for(int i = 0; i < NUMAMINOS; i++)
    out << AMINOS[i] << " " << m_overall_aminoacid_frequency[AMINOS[i]] << endl;
  out << m_mean_sequence_length << endl;    
}

void GlobalDescriptorMaker::computeDescriptor(const char * infile, ostream& out) const
{  
  GlobalDescriptor glob = computeDescriptor(infile);
  glob.Write(out);
}

GlobalDescriptor GlobalDescriptorMaker::computeDescriptor(const char * infile) const
{
  ifstream in(infile);
  Exception::Assert(in.good(), "Error: could not read file %s", infile); 
  return computeDescriptor(in);
}

GlobalDescriptor GlobalDescriptorMaker::computeDescriptor(istream& in) const
{
  Protein info;
  info.ReadPsiBlast2(in);
  return computeDescriptor(info);
}
  
GlobalDescriptor GlobalDescriptorMaker::computeDescriptor(Protein& info) const
{

  GlobalDescriptor glob;
  // calculate sequence size
  unsigned int seq_length = info.sequence.size();
  
  // calculate local frequencies 
  map<char,int> localAAfreqs;
  for( unsigned int c=0; c<seq_length; c++ )
    localAAfreqs[info.sequence[c]]++;
  // relative AA frequencies        
  for(int i = 0; i < NUMAMINOS; i++){
	map<char,int>::iterator k = localAAfreqs.find(AMINOS[i]);	  
    glob.m_aminoacid_frequency[i] = (k == localAAfreqs.end()) ? 
			MINFREQ : log(k->second/m_overall_aminoacid_frequency.find(AMINOS[i])->second)/10;
  }
  int no_cys = localAAfreqs['C'];
  
  // relative sequence size
  glob.m_rel_seq_length = log(seq_length/m_mean_sequence_length);

  // absolute number of cysteines
  glob.m_abs_cys_num = no_cys / (float)MAXCYS;  
  // relative number of cysteines
  glob.m_rel_cys_num = no_cys / (float)seq_length;
  // parity 
  glob.m_parity = no_cys % 2;
  
  int bars[NO_BARS];
  for( int c=0; c<NO_BARS; c++ )	
    bars[c]=0;
  
  //double bar_size = 1./(float)NO_BARS;
  for( unsigned int c=0; c<seq_length; c++ )
    if( info.sequence[c]=='C' ) {
      double prof = info.profile[c]['C'];
      if( prof<=(1./(float)NO_BARS) )
	bars[0]++;
      else if( prof<=(2./(float)NO_BARS) )
	bars[1]++;
      else if( prof<=(3./(float)NO_BARS) )
	bars[2]++;
      else if( prof<=(4./(float)NO_BARS) )
	bars[3]++;
      else if( prof<=(5./(float)NO_BARS) )
	bars[4]++;
      else
	Exception::Assert(false,"error while creating descriptors for protein %s profile=%lf",info.name.c_str(),prof);
    }
  for( int c=0; c<NO_BARS; c++ )
    glob.m_cyscon[c] = ( no_cys > 0 ) ? (double)bars[c]/(double)no_cys : 0.;		
  
  return glob;
}

void GlobalDescriptorMaker::init()
{
  m_mean_sequence_length = 0.;
}


#ifdef STANDALONE
int main(int argc, char ** argv){

  if(argc < 3){
    cerr << "Usage: " << basename(argv[0]) << " <stats file> <pssm file>\n";
    exit(1);
  }  
  GlobalDescriptorMaker glob(argv[1]);  
  glob.computeDescriptor(argv[2],cout);
  
}
#endif
