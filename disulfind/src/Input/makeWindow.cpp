#include "../Common/Matrix.h"

Matrix<double> makeWindow(const Matrix<double>& M, int k){

  int no_cols = M.GetNoCols();
  int no_rows = M.GetNoRows();
  int w = 2*k+1;

  int no_out_cols = w * no_cols;
  
  Matrix<double> W(no_rows,no_out_cols,0.);
  
  for(int r = 0; r < no_rows; r++){
    // write left context
    for(int sx = k-1; sx >= 0; sx--){
      int rsx = r - k + sx;
      if(rsx < 0)
	break;
      for(int i = 0; i < no_cols; i++)
	W(r,(sx*no_cols)+i) = M(rsx,i);
    } 
    // write element 
    for(int i = 0; i < no_cols; i++)
      W(r,(k*no_cols)+i) = M(r,i);
    // write right context
    for(int dx = 1; dx <= k; dx++){
      int rdx = r + dx;
      if(rdx >= no_rows)
	break;
      for(int i = 0; i < no_cols; i++)
	W(r,((k+dx)*no_cols)+i) = M(rdx,i);
    }     
  }
  return W;
}

#ifdef STANDALONE
int main(int argc, char ** argv){

  if(argc < 2){
    cerr << "Usage:\n\t" << argv[0] << " <k>\n" << endl;
    exit(1);
  }

  int k = atoi(argv[1]);

  Matrix<double> M,W;
  M.Read(cin);
  W = makeWindow(M,k);
  W.Write(cout);
}
#endif
