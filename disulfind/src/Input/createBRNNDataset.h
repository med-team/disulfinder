#include "utils.h"

const char * cysteinConservation(double cons);
const char * onehot(int x);
double sigmoid(double x);
void createDataBRNN(const string& psifile,
		    istream& insvmpred,
		    istream& insvmdata,
		    ofstream& outdata,
		    ofstream& outlabel,
		    ofstream& outsize);

void createDataBRNN(Protein& info,
		    istream& insvmpred,
		    istream& insvmdata,
		    ofstream& outdata,
		    ofstream& outlabel,
		    ofstream& outsize);

void createDataBRNN(Protein& info, 
		    const string& svmdata, 
		    const string& svmprediction, 
		    const string& brnndata, 
		    const string& brnnlabel, 
		    const string& brnnsize);

void createBRNNDataset(const string & chainlist, 		       
		       const string & profiledir,
		       const string & svmprediction,
		       const string & svmdata,
		       const string & brnndata,
		       const string & brnnlabel,
		       const string & brnnsize);
