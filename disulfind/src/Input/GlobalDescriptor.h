#include "utils.h"

#define NUMAMINOS 20
#define NO_BARS 5
#define AMINOS "ACDEFGHIKLMNPQRSTVWY"
#define MAXCYS 20

class GlobalDescriptor {

public:
  float m_aminoacid_frequency[NUMAMINOS];
  float m_rel_seq_length;
  float m_abs_cys_num;
  float m_rel_cys_num;
  int  m_parity; 
  float m_cyscon[NO_BARS];
  
  Matrix<double> toMatrix() const;
  void Write(ostream & out) const;
};

class GlobalDescriptorMaker {

  std::map<char,float> m_overall_aminoacid_frequency;
  float m_mean_sequence_length;

public:
  GlobalDescriptorMaker();
  GlobalDescriptorMaker(const char * infile);
  GlobalDescriptorMaker(istream & in);
  
  void Read(istream & in);
  void Write(ostream & out);
  void computeDescriptor(const char * infile, ostream& out) const;
  GlobalDescriptor computeDescriptor(const char * infile) const;
  GlobalDescriptor computeDescriptor(istream& in) const;  
  GlobalDescriptor computeDescriptor(Protein& info) const;
  
  
protected:
  void init();
};

