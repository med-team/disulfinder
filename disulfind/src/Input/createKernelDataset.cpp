#include <fstream>
#include "createKernelDataset.h"

void createDataKernel(Protein& info, int k, const string& outfile, const string& descstats, bool use_pssm)
{  
  ofstream out(outfile.c_str());
  Exception::Assert(out.good(), "Error: could not write temporary file %s", outfile.c_str());
  out.precision(PRECISION);
  
  GlobalDescriptorMaker desc_maker(descstats.c_str());

  createDataKernel(info, out, k, desc_maker, "", use_pssm);
}

void createDataKernel(Protein& info,
		      ostream& out,
		      unsigned int k,
		      const GlobalDescriptorMaker& desc_maker,
		      const string & labelfile, 
		      bool use_pssm)
{
  // read labels if available
  Matrix<int> L;
  int nocys = -1;
  if(labelfile != ""){
    ifstream inlabel(labelfile.c_str());
    Exception::Assert(inlabel.good(), "Error: could not open label file %s", labelfile.c_str());
    L.Read(inlabel);
    nocys = L.GetNoRows();
  }
  
  string sequence = info.GetSequence();
  int no_rows = sequence.length();
  Matrix<double> M(no_rows,22);    
  
  for(int r = 0; r < no_rows; r++){
    if(use_pssm)
      for( int c=0; c<20; c++ )
	M(r,c) = info.pssm[r][aa_codes[c]];
    else
      for( int c=0; c<20; c++ )
	M(r,c) = info.profile[r][aa_codes[c]];
    M(r,20) = info.entropy[r];
    M(r,21) = info.weight[r];
  }
  Matrix<double> W = makeWindow(M,k);
  
  // compute global descriptor
  Matrix<double> D = desc_maker.computeDescriptor(info).toMatrix();
  
  int c = 0;
  for(int r = 0; r < no_rows; r++){
    if(sequence[r] != 'C')
      continue;
    if(nocys != -1){
      Exception::Assert(nocys > c, "Error: found only %d labels in labelfile %s", nocys, labelfile.c_str());
      out << L(c,0) << " ";
      c++;
    }
    else
      out << 0 << " ";
    makeSparseVector(W.GetRow(r).joinByCol(D),out);
  }  
}

void createDataKernel(const string & psifile, 
		      ostream& out,
		      unsigned int k,
		      const GlobalDescriptorMaker& desc_maker,
		      const string & labelfile, 
		      bool use_pssm)
{  
  // parse psi2 file
  Protein info;
  ifstream inprofile(psifile.c_str());
  Exception::Assert(inprofile.good(), "Error: could not open profile file %s", psifile.c_str());
  info.ReadPsiBlast2(inprofile);
  inprofile.close();

  createDataKernel(info, out, k, desc_maker, labelfile, use_pssm);
}
  
void createKernelDataset(const string & chainlist, 			 
			 const string & profiledir,
			 const string & outfile,
			 const string & descstats,
			 unsigned int k,
			 const string & labeldir, 
			 bool use_pssm)
{
  ifstream in(chainlist.c_str());
  Exception::Assert(in.good(), "Error: could not open file %s", chainlist.c_str());
  ofstream out(outfile.c_str());
  Exception::Assert(out.good(), "Error: could not open file %s", outfile.c_str());
  out.precision(PRECISION);

  GlobalDescriptorMaker desc_maker(descstats.c_str());

  string chain;
  while(getline(in,chain)){
    string::size_type beg = chain.find_first_of(" \t");
    if(beg != string::npos && beg < chain.size()-1)
      chain = chain.substr(beg+1);
    string psifile = profiledir + "/" + chain;
    string labelfile = labeldir;
    if(labelfile != "")
      labelfile += "/" + chain;
    createDataKernel(psifile,out,k,desc_maker,labelfile,use_pssm);
  }
}
			 
#ifdef STANDALONE
int main(int argc, char ** argv){

  if(argc < 6){
    cerr << "Usage: " << basename(argv[0]) << " <chainlist> <profiledir> "
	 << "<outfile> <descstats> <k> [labeldir] [use_pssm]" << endl;
    exit(1);
  }  
  try{
      const string chainlist = argv[1];
      const string profiledir = argv[2];
      const string outfile = argv[3];
      const string descstats = argv[4];
      int k = atoi(argv[5]);
      const string labeldir = (argc > 6) ? argv[6] : "";
      bool use_pssm = (argc > 7) ? true : false;

      createKernelDataset(chainlist,profiledir,outfile,descstats,k,labeldir,use_pssm);  
  }
  catch(Exception *e) {
    cerr << e->GetMessage() << "\n" << endl;
    delete e;
    exit(1);
  }
}
#endif
