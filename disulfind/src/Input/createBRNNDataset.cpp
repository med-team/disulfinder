#include <fstream>
#include "createBRNNDataset.h"

const char * cysteinConservation(double cons)
{  
  if(cons == 0)
    return "0 0 0 0 0";
  if(cons <= 0.2)
    return "1 0 0 0 0";    
  if(cons <= 0.4)
    return "0 1 0 0 0";    
  if(cons <= 0.6)
    return "0 0 1 0 0";    
  if(cons <= 0.8)
    return "0 0 0 1 0";      

  return "0 0 0 0 1";    
}

const char * onehot(int x)
{
  if(x == -1)
    return "1 0";
  if(x == 1)
    return "0 1";
  if(x == 0)
    return "0 0";
  Exception::Assert(false, "Error: onehot takes binary values, found %d", x);
  return "0 0";
}

double sigmoid(double x)
{
  return 1.0 / (1 + exp(-x));
} 

void createDataBRNN(const string& psifile,
		    istream& insvmpred,
		    istream& insvmdata,
		    ofstream& outdata,
		    ofstream& outlabel,
		    ofstream& outsize)
{ 
  // parse psi2 file
  Protein info;
  ifstream inprofile(psifile.c_str());
  Exception::Assert(inprofile.good(), "Error: could not open profile file %s", psifile.c_str());
  info.ReadPsiBlast2(inprofile);
  inprofile.close();
  
  createDataBRNN(info, insvmpred, insvmdata, outdata, outlabel, outsize);
}

void createDataBRNN(Protein& info, 
		    const string& svmdata, 
		    const string& svmprediction, 
		    const string& brnndata, 
		    const string& brnnlabel, 
		    const string& brnnsize)
{
  ifstream insvmdata(svmdata.c_str());
  Exception::Assert(insvmdata.good(), "Error: could not open file %s", svmdata.c_str());
  ifstream insvmpred(svmprediction.c_str());
  Exception::Assert(insvmpred.good(), "Error: could not open file %s", svmprediction.c_str());
  ofstream outdata(brnndata.c_str());
  Exception::Assert(outdata.good(), "Error: could not open file %s", brnndata.c_str());
  ofstream outlabel(brnnlabel.c_str());
  Exception::Assert(outlabel.good(), "Error: could not open file %s", brnnlabel.c_str());
  ofstream outsize(brnnsize.c_str());
  Exception::Assert(outsize.good(), "Error: could not open file %s", brnnsize.c_str());
  
  createDataBRNN(info, insvmpred, insvmdata, outdata, outlabel, outsize);
}

void createDataBRNN(Protein& info,
		    istream& insvmpred,
		    istream& insvmdata,
		    ofstream& outdata,
		    ofstream& outlabel,
		    ofstream& outsize)
{ 
  unsigned int nocys = 0;
  vector<string> cyscon;
  string sequence = info.GetSequence();  
  string::size_type beg = 0,aa = 0;  
  while((aa = sequence.find('C',beg)) != string::npos){
    cyscon.push_back(cysteinConservation(info.profile[aa]['C'])); 
    if(aa == sequence.size()-1)
      break;
    beg = aa+1;
  } 
  nocys = cyscon.size();
  unsigned int parity = nocys % 2; 
  
  // write data 
  double svmpred;
  string buf;
  for(unsigned int c = 0; c < nocys; c++){
    insvmpred.peek();
    Exception::Assert(insvmpred.good(), "Error: found %d out of %d rows in svmpred file", c+1,nocys);
    insvmpred >> svmpred;
    getline(insvmpred,buf);
    outdata << sigmoid(svmpred) << " " << cyscon[c] << " " << parity << endl;
  }

  // write target 
  int svmlab;
  for(unsigned int c = 0; c < nocys; c++){
    insvmdata.peek();  
    Exception::Assert(insvmdata.good(), "Error: found %d out of %d rows in svmdata file", c+1,nocys);
    insvmdata >> svmlab;
    getline(insvmdata,buf);
    outlabel << onehot(svmlab) << endl;
  }
  // write size
  outsize << nocys << endl;  
}
  
void createBRNNDataset(const string & chainlist, 		       
		       const string & profiledir,
		       const string & svmprediction,
		       const string & svmdata,
		       const string & brnndata,
		       const string & brnnlabel,
		       const string & brnnsize)
{
  ifstream in(chainlist.c_str());
  Exception::Assert(in.good(), "Error: could not open file %s", chainlist.c_str());
  ifstream insvmpred(svmprediction.c_str());
  Exception::Assert(in.good(), "Error: could not open file %s", svmprediction.c_str());
  ifstream insvmdata(svmdata.c_str());
  Exception::Assert(in.good(), "Error: could not open file %s", svmdata.c_str());
  ofstream outdata(brnndata.c_str());
  Exception::Assert(outdata.good(), "Error: could not open file %s", brnndata.c_str());
  ofstream outlabel(brnnlabel.c_str());
  Exception::Assert(outlabel.good(), "Error: could not open file %s", brnnlabel.c_str());
  ofstream outsize(brnnsize.c_str());
  Exception::Assert(outsize.good(), "Error: could not open file %s", brnnsize.c_str());
  
  string chain;
  while(getline(in,chain)){
    string::size_type beg = chain.find_first_of(" \t");
    if(beg != string::npos && beg < chain.size()-1)
      chain = chain.substr(beg+1);
    string psifile = profiledir + "/" + chain;
    createDataBRNN(psifile,insvmpred,insvmdata,outdata,outlabel,outsize);
  }
}
			 
#ifdef STANDALONE
int main(int argc, char ** argv){

  if(argc < 8){
    cerr << "Usage: " << basename(argv[0]) << " <chainlist> <profiledir> "
	 << "<svmpred> <svmdata> <brnndata> <brnnlabel> <brnnsize>" << endl;
    exit(1);
  }  
  try{
    const string chainlist = argv[1];
    const string profiledir = argv[2];
    const string svmpred = argv[3];
    const string svmdata = argv[4];
    const string brnndata = argv[5];
    const string brnnlabel = argv[6];
    const string brnnsize = argv[7];    
    createBRNNDataset(chainlist,profiledir,svmpred,svmdata,brnndata,brnnlabel,brnnsize);
  }
  catch(Exception *e) {
    cerr << e->GetMessage() << "\n" << endl;
    delete e;
    exit(1);
  }
}
#endif
