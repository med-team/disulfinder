#include "utils.h"
#include "GlobalDescriptor.h"

#define PRECISION 4

void createDataKernel(Protein& info, 
		      int k, 
		      const string& outfile, 
		      const string& descstats,
		      bool use_pssm = false);

void createDataKernel(const string & psifile, 
                      ostream& out,
                      unsigned int k,
                      const GlobalDescriptorMaker& desc_maker,
                      const string & labelfile = "",
		      bool use_pssm = false);

void createDataKernel(Protein& info,
		      ostream& out,
		      unsigned int k,
		      const GlobalDescriptorMaker& desc_maker,
		      const string & labelfile = "",
		      bool use_pssm = false);

void createKernelDataset(const string & chainlist,                       
                         const string & profiledir,
                         const string & outfile,
                         const string & descstats,
                         unsigned int k,
                         const string & labeldir = "",
			 bool use_pssm = false);
