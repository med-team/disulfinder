#include <stdlib.h>
#include <time.h>
#include <dirent.h>
#include <fnmatch.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <sstream>
#include <map>
//#include "../Common/CommonMethods.h"
#include "../Common/Util.h"
#include "Protein.h"

#define NO_BARS 5

using namespace Util;

//------------------------------------------------------------------------------------------------
// Secondary Structure classes
//------------------------------------------------------------------------------------------------

const char* Protein::SSClassNames = "HECX";
Protein::SSClass Protein::SSCode(char ss_class)
{
  switch(ss_class) {
  case 'H':
    return SS_H;
  case 'E':
    return SS_E;
  case 'C':
    return SS_C;
  }
  return SS_UNKNOWN;
}


//------------------------------------------------------------------------------------------------
// Amino-acid single letter code
//------------------------------------------------------------------------------------------------

int Protein::AAnum(char aa)
{
  const char *pos = strchr(aa_codes,aa);
  return ((pos==NULL) ?AA_NONE :pos-aa_codes);
}

char Protein::AAcode(const char *aa) {
  for( int i=0; i<20; i++ ) {
    if( strncasecmp(aa_three_codes[i],aa,3)==0 ) {
      return aa_codes[i];
    }
  }
  return 'X';  
}

char Protein::CheckAA(char aa)
{
  if( aa=='B' ) // D N
    return 'N';
  if( aa=='Z' ) // Q E
    return 'Q';
  if( aa=='X' )
    return 'A';
  return aa;
}

//------------------------------------------------------------------------------------------------
// Secondary Structure allowed codes
//------------------------------------------------------------------------------------------------

char Protein::CheckSS(char ss) {
  switch(ss) {
  case 'H':
  case 'E':
    return ss;
    break;
  default:
    return 'C';
  }
}


//------------------------------------------------------------------------------------------------
// Protein container
//------------------------------------------------------------------------------------------------

const Protein& Protein::operator=(const Protein &src)
{
  this->name = src.name;
  this->sequence = src.sequence;
  this->sequence_num = src.sequence_num;
  this->sequence_num_str = src.sequence_num_str;
  this->chain_code = src.chain_code;
  this->aa_phi = src.aa_phi;	
  this->aa_psi = src.aa_psi;
  this->aa_x = src.aa_x;
  this->aa_y = src.aa_y;
  this->aa_z = src.aa_z;
  this->accessibility = src.accessibility;
  this->bonding_state = src.bonding_state;
  this->bonding_state_all = src.bonding_state_all;
  this->cys_coupled = src.cys_coupled;
  this->cys_position = src.cys_position;
  this->sec_structure = src.sec_structure;
  this->sec_structure_dssp = src.sec_structure_dssp;
  this->profile = src.profile;
  this->pssm = src.pssm;
  this->aligned_num= src.aligned_num;
  this->entropy = src.entropy;
  this->weight = src.weight;
  this->descriptors = src.descriptors;
  this->dsclass = src.dsclass;
  this->no_cys_total = src.no_cys_total;
  this->no_cys_linked = src.no_cys_linked;
  this->beta_partner1 = src.beta_partner1;
  this->beta_partner2 = src.beta_partner2;
  this->beta_id = src.beta_id;

  return *this;
}

void Protein::Clean()
{
  sequence.clear();
  sequence_num.clear();
  sequence_num_str.clear();
  chain_code.clear();
  aa_kappa.clear();
  aa_alpha.clear();
  aa_phi.clear();
  aa_psi.clear();
  aa_x.clear();
  aa_y.clear();
  aa_z.clear();	
  accessibility.clear();
  bonding_state.clear();
  bonding_state_all.clear();
  cys_coupled.clear();
  cys_position.clear();
  sec_structure.clear();
  sec_structure_dssp.clear();
  profile.clear();
  pssm.clear();
  aligned_num.clear();
  entropy.clear();
  weight.clear();
  beta_partner1.clear();
  beta_partner2.clear();
  beta_id.clear();
  descriptors.clear();  
  dsclass = DS_UNKNOWN;
  no_cys_total = 0;
  no_cys_linked = 0;
}


void Protein::AddDescriptors(vector<Protein> &infos)
{
  // retrieve global AA frequencies

  int no_res = 0;
  long sum_res_len = 0;
  map<char,int> globAAfreqs;
  for( unsigned int i=0; i<infos.size(); i++ ) {
    for( unsigned int c=0; c<infos[i].sequence.size(); c++ ) {
      if(globAAfreqs.find(infos[i].sequence[c]) == globAAfreqs.end())
	globAAfreqs.insert(make_pair(infos[i].sequence[c], 0));
      globAAfreqs[infos[i].sequence[c]]++;
    }
    sum_res_len += infos[i].sequence.size();
    no_res++;
  }
  if( no_res==0 )
    return;
  double avg_res_len = (double)sum_res_len/(double)no_res;

  // fill descriptors for each protein
  for( unsigned int i=0; i<infos.size(); i++ ) {
    if( infos[i].profile.size()==0 )
      continue;

    // retrieve local AA frequencies

    map<char,int> localAAfreqs;
    for( unsigned int c=0; c<infos[i].sequence.size(); c++ ) {
      if(localAAfreqs.find(infos[i].sequence[c]) == localAAfreqs.end())
	localAAfreqs.insert(make_pair(infos[i].sequence[c], 0));
      localAAfreqs[infos[i].sequence[c]]++;
    }

    // add descriptors
    
    for(map<char, int>::iterator it=globAAfreqs.begin(); it!=globAAfreqs.end(); ++it) {
      if((*it).second != 0) {
	if(localAAfreqs.find((*it).first) != localAAfreqs.end()) {
	  if(localAAfreqs[(*it).first] != 0)
	    infos[i].descriptors.push_back(log((double)localAAfreqs[(*it).first] / (double)(*it).second) / 10.0);
	  else
	    infos[i].descriptors.push_back(-1.0);
	}
	else
	  infos[i].descriptors.push_back(-1.0);
      }
      else
	infos[i].descriptors.push_back(-1.0);
    }

    infos[i].descriptors.push_back(log((double)infos[i].sequence.size() / avg_res_len));
    infos[i].descriptors.push_back((double)infos[i].no_cys_total/ 20.0);
    infos[i].descriptors.push_back((double)infos[i].no_cys_total / (double)infos[i].sequence.size());
    infos[i].descriptors.push_back(infos[i].no_cys_total % 2); // insert parity on number of cysteines

    // add evolutionary information
    if( infos[i].profile.size()==0 )
      continue;

    int bars[NO_BARS];
    for( int c=0; c<NO_BARS; c++ )	
      bars[c]=0;

    int no_cys = 0;
    //double bar_size = 1./(float)NO_BARS;
    for( unsigned int c=0; c<infos[i].sequence.size(); c++ )
      if( infos[i].sequence[c]=='C' ) {
	double prof = infos[i].profile[c]['C'];
	if( prof<=(1./(float)NO_BARS) )
	  bars[0]++;
	else if( prof<=(2./(float)NO_BARS) )
	  bars[1]++;
	else if( prof<=(3./(float)NO_BARS) )
	  bars[2]++;
	else if( prof<=(4./(float)NO_BARS) )
	  bars[3]++;
	else if( prof<=(5./(float)NO_BARS) )
	  bars[4]++;
	else
	  Exception::Assert(false,"error while creating evo descriptors for protein %s profile=%lf",infos[i].name.c_str(),prof);
	no_cys++;
      }

    for( int c=0; c<NO_BARS; c++ )
      infos[i].descriptors.push_back(( no_cys > 0 ) ?(double)bars[c]/(double)no_cys :0.);		
  }
}

void Protein::SkipLine(istream &fp) {
  char line[10002];
  fp.getline(line,10000);
  Exception::Assert(fp.good(),"EOF reached");
}

bool Protein::ReadCharLine(vector<char> &data, istream &fp, string &identifier, int linenum) {
  string::size_type pos;
  char line[10002];
  string str_line;

  //read a line of the file
  fp.getline(line,10000);
  if( fp.fail() )
    return false;

  // find the identifier

  str_line = line;
  pos = str_line.find_first_of(':');
  Exception::Assert(pos!=str_line.npos,"error at line %d - bad format: identifier not found", linenum);
  identifier = str_line.substr(0,pos);

  //copy the values

  Exception::Assert(str_line[pos+1]!='\0',"error at line %d - bad format: empty line", linenum);

  data.clear();

  string::size_type last;
  for( last=pos+1,pos=str_line.find_first_of(',',last);
       pos!=str_line.npos;
       last = pos+1, pos = str_line.find_first_of(',',last) ) {
    Exception::Assert(pos-last==1,"error at line %d - bad format: not a line of char", linenum);
    data.push_back(str_line[last]);
  }
  if( isprint(str_line[last]) )
    data.push_back(str_line[last]);
  return true;
}


void Protein::ReadCB513(istream &fp) {
  int linenum = 0;
  string ident;

  // read residues list
  Exception::Assert(ReadCharLine(sequence,fp,ident,linenum),"line %d - bad format: empty line", linenum);
  Exception::Assert(ident.compare("RES")==0,"line %d - bad format: unexpected identifier %s", linenum, ident.c_str());
  for( unsigned int i=0; i<sequence.size(); i++ ) {
    sequence[i] = sequence[i];
    if( sequence[i]=='C' )
      no_cys_total++;
  }
  linenum++;

  // read secondary structure
  Exception::Assert(ReadCharLine(sec_structure,fp,ident,linenum),"line %d - bad format: empty line", linenum);
  Exception::Assert(ident.compare("DSSP")==0,"line %d - bad format: unexpected identifier %s", linenum, ident.c_str());
  Exception::Assert(sequence.size()==sec_structure.size(),"line %d - bad format: wrong number of values in line",linenum);
  linenum++;

  for(unsigned int i=0; i<sec_structure.size(); i++ ) {
    if( sec_structure[i]!='H' && sec_structure[i]!='E' )
      sec_structure[i] = 'C';
  }

  SkipLine(fp);
  SkipLine(fp);
  SkipLine(fp);
  SkipLine(fp);
  linenum += 4;

  // creating profile
  profile.resize(sequence.size());
  for( unsigned int c=0; c<profile.size(); c++ )
    for( unsigned int i=0; i<20; i++ )
      profile[c][aa_codes[i]]=0;

  // read alignment
  
  int noaligns = 0;
  vector<char> align;
  do {
    if( !ReadCharLine(align,fp,ident,linenum) )
      break;
    Exception::Assert(sequence.size()==align.size(),"line %d - bad format: wrong number of values in line", linenum);
    noaligns++;

    for( unsigned int c=0; c<align.size(); c++ ) {
      if( align[c]=='X' ) {
	for( unsigned int i=0; i<20; i++ )
	  profile[c][aa_codes[i]]++;
      }
      else if( align[c]=='B' ) {
	  profile[c]['N']++;
	  profile[c]['D']++;
      }
      else if( align[c]=='Z' ) {
	  profile[c]['Q']++;
	  profile[c]['E']++;
      }
      else if( align[c]!='.' ) {
	Exception::Assert(AAnum(align[c])<20 && AAnum(align[c])>=0, "line %d - bad format: wrong value at position %d: %c", linenum, c, align[c]);
	profile[c][align[c]]++;
      }
    }
    linenum++;

    fp.peek(); //get last char without extracting
  }
  while( !fp.eof() );	// i suppose there's almost one align

  // normalize the profile

  for( unsigned int c=0; c<sequence.size(); c++ ) {
    double tot = 0.;
    for( int i=0; i<20; i++ )
      tot += profile[c][aa_codes[i]];
    for( int i=0; i<20; i++ )
      profile[c][aa_codes[i]] = profile[c][aa_codes[i]]/tot;
  }
}


string Protein::GetSequence() const {
  string s;
  
  s.resize(sequence.size());
  for( unsigned int i=0; i<sequence.size(); i++ )
    s[i]=sequence[i];
  return s;
}

int SelectCB513(const struct dirent* file) {
  return !fnmatch("*.all",file->d_name,0);
}


void Protein::ReadCB513Set(vector<Protein> &infos, const string &data_dir, bool output) {
  infos.clear();
  
  struct dirent **filelist;
  int nofiles = scandir(data_dir.c_str(),&filelist,SelectCB513,alphasort);
  for( int i=0; i<nofiles; i++ ) {
    if( output )
      OutProgress(infos.size(),10);

    // open protein files
    
    ifstream fp;
    string filename = data_dir+filelist[i]->d_name;
    fp.open(filename.c_str());
    Exception::Assert(fp.good(),"Error while opening protein file <%s>", filename.c_str());

    infos.push_back(Protein());
    *strstr(filelist[i]->d_name,".all") = '\0';
    infos.back().name = filelist[i]->d_name;
    try {
      infos.back().ReadCB513(fp);
    }
    catch(Exception *e) {
      Exception *ne = new Exception("Error while reading protein file <%s>\n\t%s",filename.c_str(),e->GetMessage());
      delete e;
      throw ne;
    }

    delete filelist[i];
    fp.close();
  }
  // Adding descriptors
  
  if( output )
    cerr<<"Adding descriptors\n";
  AddDescriptors(infos);

  if( nofiles>0 )
    delete []filelist;
}


void Protein::ReadHSSP(istream& fp, double prior) {
  char line[10002];

  //reading sequence and SS

  do {
    fp.getline(line,10000);
    Exception::Assert(fp.good(),"unexpected eof while looking for sequence infos in hssp file");
  }
  while(strncmp(line,"## ALIGNMENTS",13));

  no_cys_total = 0;
  sequence.clear();
  sec_structure.clear();
  fp.getline(line,10000); //skip header
  for(;;) {
    fp.getline(line,10000);
    if( !fp.good() )
      break;
    if( !strncmp(line,"##",2) )
      break;

    // 1---5	SeqNo
    // 7---12	PdbNo
    // 14		AACode
    // 17--25   SecStructure
 
    sequence.push_back(line[14]);
    if( line[14]=='C' )
      no_cys_total++;
    switch(line[17]) {
    case 'H':
    case 'E':
      sec_structure.push_back(line[17]);
      break;
    default:
      sec_structure.push_back('C');
    }
  }

  aligned_num.resize(sequence.size());
  entropy.resize(sequence.size());
  weight.resize(sequence.size());
  profile.resize(sequence.size());


  // reading profile
  bool all_zero = false;
  while(strncmp(line,"## SEQUENCE PROFILE AND ENTROPY",31)) {
    fp.getline(line,10000);
    if(!fp.good()){
      all_zero = true;
      break;
    }
  }
  
  // default profile if missing
  if(all_zero) {
    for(unsigned int c=0; c<sequence.size(); c++ ) {
      for(unsigned int i=0; i<20; ++i)
	profile[c][aa_codes[i]] = (1-prior)/20.;
      profile[c][sequence[c]] += prior;
    }
    return;
  }
  
  
  fp.getline(line,10000); //skip header
  
  unsigned int c;
  for( c=0; c<sequence.size(); c++ ) {
    fp.getline(line,10000);
    if( !fp.good() )
      break;
    if( !strncmp(line,"##",2) ) {
      c++;
      break;
    }
    for( int i=0; i<20; i++ )
      profile[c][aa_codes[i]] = atoi(line+4*i+12);
    aligned_num[c] = atof(line+95);
    entropy[c] = atof(line+109);
    weight[c] = atof(line+125);
  }
  Exception::Assert(c==sequence.size(),"Error while reading profile for position %d",c);

  // normalize the profile

  for( c=0; c<sequence.size(); c++ ) {
    double tot = 0.;
    for( int i=0; i<20; i++ )
      tot += profile[c][aa_codes[i]];
    for( int i=0; i<20; i++ )
      profile[c][aa_codes[i]] = profile[c][aa_codes[i]]/tot;
  }

}

int SelectHSSP(const struct dirent* file) {
  return !fnmatch("*.hssp",file->d_name,0);
}


void Protein::ReadHSSPSet(vector<Protein> &infos, const string &data_dir, bool output, double prior) {
  infos.clear();
  
  struct dirent **filelist;
  int nofiles = scandir(data_dir.c_str(),&filelist,SelectHSSP,alphasort);
  for( int i=0; i<nofiles; i++ ) {
    if( output )
      OutProgress(infos.size(),10);

    // open protein files
    
    ifstream fp;
    string filename = data_dir+filelist[i]->d_name;
    fp.open(filename.c_str());
    Exception::Assert(fp.good(),"Error while opening protein file <%s>", filename.c_str());
    
    infos.push_back(Protein());
    *strstr(filelist[i]->d_name,".hssp") = '\0';
    infos.back().name = filelist[i]->d_name;
    try {
      infos.back().ReadHSSP(fp, prior);
    }
    catch(Exception *e){
      Exception *ne = new Exception("Error while reading protein file <%s>\n\t%s",filename.c_str(),e->GetMessage());
      delete e;
      throw ne;
    }

    delete filelist[i];
    fp.close();
  }
  if( output )
    cerr<<"Adding descriptors\n";
  AddDescriptors(infos);

  if( nofiles>0 )
    delete []filelist;
}


void Protein::ReadSwissProt(istream &is) 
{
  no_cys_total = 0;
  no_cys_linked = 0;

  sequence.clear();
  sequence_num.clear();
  sequence_num_str.clear();
  bonding_state.clear();
  bonding_state_all.clear();
  cys_coupled.clear();
  cys_position.clear();

  string line;
  vector<int> bridge_start;
  vector<int> bridge_end;
  vector<int> cys_id;
  for(;;) {
    getline(is,line);
    if( !is.good() )
      break;

    // sequence data
    if( line.substr(0,2)=="  " ) {
      for( unsigned int i=0; i<line.length(); i++ ) {
	if( line[i]!=' ' ) {
	  sequence.push_back(line[i]);	
	  int num = sequence_num.size();
	  sequence_num.push_back(num);	  
	  sequence_num_str.push_back(cast<string>(num));	  
	  if( line[i]=='C' ) {
	    cys_id.push_back(cys_position.size());
	    cys_position.push_back(sequence_num.size()-1);
	    cys_coupled.push_back(-1);
	    bonding_state_all.push_back(-1);
	  }
	  else {
	    cys_id.push_back(-1);
	    bonding_state_all.push_back(0);
	  }
	}
      }
    }	
    
    // disulfide bridges
    if( line.substr(0,2)=="FT" && line.substr(5,8)=="DISULFID" ) {
      bridge_start.push_back(atoi(line.substr(14,6).c_str())-1);
      bridge_end.push_back(atoi(line.substr(21,6).c_str())-1);
    }	
  }

  for( unsigned int i=0; i<bridge_start.size(); i++ ) {
    if( bridge_start[i]==bridge_end[i] ) {
      cys_coupled[cys_id[bridge_start[i]]] = -3;      
      bonding_state_all[bridge_start[i]] = 2;
    }
    else {
      cys_coupled[cys_id[bridge_start[i]]] = bridge_end[i];     
      cys_coupled[cys_id[bridge_end[i]]] = bridge_start[i]; 
      bonding_state_all[bridge_start[i]] = 1;    
      bonding_state_all[bridge_end[i]] = 1;    
    }	
  }
}

void Protein::ReadDSSP(istream& fp, char chain, bool skip_unknown ) 
{
  string line;
  
  //look for alignment informations
  
  for(;;) {
    getline(fp,line);
    Exception::Assert(fp,"unexpected EOF while looking for protein\'s data");
    if( !strncmp(line.c_str(),"  #  RESIDUE",12) )
      break;
  }

  // check fields positions
  string::size_type acc_pos = 34;
  string::size_type kappa_pos = 91;
  string::size_type alpha_pos = 97;
  string::size_type phi_pos = 103;
  string::size_type psi_pos = 109;
  string::size_type x_pos = 115;
  string::size_type y_pos = 122;
  string::size_type z_pos = 129;     
  Exception::Assert((acc_pos=line.find("ACC")-1)!=line.npos-1,"Missing ACC");
  Exception::Assert((kappa_pos=line.find("KAPPA"))!=line.npos,"Missing KAPPA");
  Exception::Assert((alpha_pos=line.find("ALPHA")-1)!=line.npos-1,"Missing ALPHA");
  Exception::Assert((phi_pos=line.find("PHI")-2)!=line.npos-2,"Missing PHI");
  Exception::Assert((psi_pos=line.find("PSI")-2)!=line.npos-2,"Missing PSI");
  Exception::Assert((x_pos=line.find("X-CA")-3)!=line.npos-3,"Missing X-CA");
  Exception::Assert((y_pos=line.find("Y-CA")-3)!=line.npos-3,"Missing Y-CA");
  Exception::Assert((z_pos=line.find("Z-CA")-3)!=line.npos-3,"Missing Z-CA");

  no_cys_total = 0;
  no_cys_linked = 0;

  sequence.clear();
  sequence_num.clear();
  sequence_num_str.clear();
  chain_code.clear();
  aa_kappa.clear();
  aa_alpha.clear();
  aa_psi.clear();
  aa_phi.clear();
  aa_x.clear();
  aa_y.clear();
  aa_z.clear();
  accessibility.clear();
  sec_structure.clear();
  sec_structure_dssp.clear();
  bonding_state.clear();
  bonding_state_all.clear();
  cys_coupled.clear();
  cys_position.clear();
  beta_partner1.clear();
  beta_partner2.clear();
  beta_id.clear();

  vector<int> residue_num;
  for(;;) {
    getline(fp,line);
    if( !fp.good() )
      break;        
    // 1--4     Residue (in file)
    // 6--9     Residue (in protein)
    // 11       Chain code
    // 13       AA code
    // 16       Secondary Structure
    // 25--28   Beta partner 1
    // 29--32   Beta partner 2
    // 33       Beta id
    // 34--37   Solvent Accessibility
    // 39--44   NH->O offset
    // 46--49   NH->O energy
    // 50--55   O->NH offset
    // 57--60   O->NH energy
    // 61--66   NH->O offset
    // 68--71   NH->O energy
    // 72--77   O->NH offset
    // 79--82   O->NH energy
    // 103--108 Phi
    // 109--114 Psi
    // 115--121 X
    // 122--128 Y
    // 129--135 Z

    // check for chain termination
    if( line.find("!*")!=line.npos ) 
      continue;

    // read chain code, skip unwanted chains    
    char chaincode = line[11];
    if( chain!=0 && chain!='0' && toupper(chain)!=chaincode )
      continue;
    if( line[13]=='!' ) // skip chain breaks
      continue;
    if( line[13]=='X' && skip_unknown )
      continue;
    if( chaincode==' ' )
      chaincode = '0';
    chain_code.push_back(chaincode);

    // read overall residue number
    residue_num.push_back(atoi(line.substr(1,4).c_str()));
    sequence_num.push_back(atoi(line.substr(6,4).c_str()));   
    sequence_num_str.push_back(trim(line.substr(6,5)));   

    // read sequence and oxidation state
    if( islower(line[13]) ) {
      // bonded cystein
      sequence.push_back('C');
      bonding_state.push_back(true);
      bonding_state_all.push_back(1); // inter chain must be identified
      cys_coupled.push_back(-line[13]); // coupled cystein are identified by their negated label
      cys_position.push_back(sequence.size()-1);
      no_cys_total++;
      no_cys_linked++;
    }
    else {
      // whatever
      if( line[13]=='C' ) {
	// unbonded cystein
	bonding_state.push_back(false);
	bonding_state_all.push_back(-1);
	cys_coupled.push_back(-1);
	cys_position.push_back(sequence.size()-1);
	no_cys_total++;
      }
      else
	bonding_state_all.push_back(0);
      
      sequence.push_back(line[13]);	      
    }

    // read secondary structure
    sec_structure.push_back(CheckSS(line[16]));
    sec_structure_dssp.push_back((line[16]==' ') ?'C':line[16]);

    // read beta partners
    beta_partner1.push_back(atoi(line.substr(25,4).c_str()));
    beta_partner2.push_back(atoi(line.substr(29,4).c_str()));
    if( isalpha(line[33]) )	
      beta_id.push_back(line[33]);
    else
      beta_id.push_back('_');

    // read solvent accessibility
    accessibility.push_back(atoi(line.substr(acc_pos,4).c_str()));

    // read hydrogen bond energy

    // read angles    
    aa_kappa.push_back(atof(line.substr(kappa_pos,6).c_str()));
    aa_alpha.push_back(atof(line.substr(alpha_pos,6).c_str()));
    aa_phi.push_back(atof(line.substr(phi_pos,6).c_str()));
    aa_psi.push_back(atof(line.substr(psi_pos,6).c_str()));

    // read coordinates
    aa_x.push_back(atof(line.substr(x_pos,7).c_str()));
    aa_y.push_back(atof(line.substr(y_pos,7).c_str()));    
    aa_z.push_back(atof(line.substr(z_pos,7).c_str()));
  }

  // identify cys couples
  for(int i=0; i<no_cys_total; i++) {
    if( cys_coupled[i]<-3 ) { /// unidentified bridge
      bool found = false;
      for( int l=i+1; l<no_cys_total; l++ ) {
	if( cys_coupled[i]==cys_coupled[l] ) {
	  if( chain_code[cys_position[i]]!=chain_code[cys_position[l]] ) {
	    bonding_state_all[cys_position[i]] = 2;
	    bonding_state_all[cys_position[l]] = 2;
	  }
	  
	  // identified bridge
	  cys_coupled[i] = cys_position[l];
	  cys_coupled[l] = cys_position[i];
	  found = true;
	  break;
	}
      }
      if( !found ) {
	// interchain     
	cys_coupled[i] = -3; 
	bonding_state_all[cys_position[i]] = 2;	  
      }
    }    
  }

  // identify beta partners
  for(unsigned int i=0; i<beta_id.size(); i++ ) {
    // identify first beta partner
    if( beta_partner1[i]==0 )
      beta_partner1[i]=-1; // no partner
    else {
      bool intra_chain = false;
      for(unsigned int l=0; l<residue_num.size(); l++ ) {
	if( residue_num[l]==beta_partner1[i] ) {
	  beta_partner1[i]=l; // partner identified
	  intra_chain = true;
	  break;
	}
      }
      if( !intra_chain )
	beta_partner1[i] = -3; // inter-chain partner
    }

    // identify second beta partner
    if( beta_partner2[i]==0 )
      beta_partner2[i]=-1; // no partner
    else {
      bool intra_chain = false;
      for(unsigned int l=0; l<residue_num.size(); l++ ) {
	if( residue_num[l]==beta_partner2[i] ) {
	  beta_partner2[i]=l; // partner identified
	  intra_chain = true;
	  break;
	}
      }
      if( !intra_chain )
	beta_partner2[i] = -3; // inter-chain partner    
    }
  }

  // identifying bonding class
  if( no_cys_linked == no_cys_total )
    dsclass = DS_ALL;
  else if( no_cys_linked==0 )
    dsclass = DS_NONE;
  else
    dsclass = DS_MIXED;  
}


int SelectDSSP(const struct dirent* file) {
  return  !fnmatch("*.dssp*",file->d_name,0);
}

void Protein::ReadDSSPSet(vector<Protein> &infos, const string &dssp_dir, const string &hssp_dir, bool readprofile, bool output, double prior ) {
  infos.clear();

  struct dirent **filelist;
  int nofiles = scandir(dssp_dir.c_str(),&filelist,SelectDSSP,alphasort);
  for( int i=0; i<nofiles; i++ ) {
    if( output )
      OutProgress(infos.size(),10);

    // open protein files

    ifstream fp;
    string dsspname = dssp_dir + filelist[i]->d_name;
    fp.open(dsspname.c_str());
    Exception::Assert(fp.good(),"Error while opening protein file <%s>",dsspname.c_str());

    infos.push_back(Protein());
    *strstr(filelist[i]->d_name,".dssp") = '\0';
    infos.back().name = filelist[i]->d_name;

    ifstream fhssp;
    if( readprofile ) {
      string hsspname = hssp_dir + infos.back().name+".hssp";
      fhssp.open(hsspname.c_str());
      Exception::Assert(fp.good(),"Error while opening protein file <%s>", hsspname.c_str());
    }
    try {
      infos.back().ReadDSSP(fp);
      if( readprofile )
	infos.back().ReadHSSP(fhssp, prior);
    }
    catch(Exception *e) {
      //Exception *ne = new Exception("Error while reading protein <%s>\n\t%s",infos.back().name.c_str(),e->GetMessage());
      Exception *ne = new Exception("Error while reading protein <%s>\n\t%s",dsspname.c_str(),e->GetMessage());
      delete e;
      throw ne;
    }

    delete filelist[i];
    fp.close();
    if( readprofile )
      fhssp.close();
  }

  if( output )
    cerr<<"Adding descriptors`n";
  AddDescriptors(infos);

  if( nofiles>0 )
    delete []filelist;
}


void Protein::ReadStride(istream& fp, char chain) 
{
  string line;
  
  //look for alignment informations
  
  no_cys_total = 0;
  no_cys_linked = 0;

  sequence.clear();
  sequence_num.clear();
  sequence_num_str.clear();
  chain_code.clear();
  aa_kappa.clear();
  aa_alpha.clear();
  aa_psi.clear();
  aa_phi.clear();
  accessibility.clear();
  sec_structure.clear();
  sec_structure_dssp.clear();

  // disulfide and connectivity informations are in the header

  vector<int> residue_num;
  for(;;) {
    getline(fp,line);
    if( !fp.good() )
      break;  
    if( line.substr(0,3)!="ASG" )
      continue;
    
    // 5-7   AA 3 code
    // 9     chain code
    // 11-14 residue num in protein
    // 16-19 residue num in file
    // 24    SS code
    // 42-48 phi
    // 52-58 psi
    // 61-68 accessible surface

    // read chain code, skip unwanted chains    
    char chaincode = toupper(line[9]);
    if( chain!=0 && chain!='0' && toupper(chain)!=chaincode )
      continue;
    if( chaincode=='-' )
      chaincode = '0';
    chain_code.push_back(chaincode);

    // read residue
    sequence.push_back(AAcode(line.substr(5,3).c_str()));	      

    // read overall residue number
    residue_num.push_back(atoi(line.substr(16,4).c_str()));
    sequence_num.push_back(atoi(line.substr(11,4).c_str()));   
    sequence_num_str.push_back(trim(line.substr(11,4)));   

    // read secondary structure
    sec_structure.push_back(CheckSS(line[24]));
    sec_structure_dssp.push_back((line[24]==' ') ?'C':line[16]);

    // read solvent accessibility
    accessibility.push_back((int)atof(line.substr(61,8).c_str()));

    // read angles    
    aa_phi.push_back(atof(line.substr(42,7).c_str()));
    aa_psi.push_back(atof(line.substr(52,7).c_str()));
  }
}


int SelectAll(const struct dirent* file) {
  return  1;
}


void Protein::ReadSequence(istream &fp, bool treat_inter_as_bonded ) {
  no_cys_total = 0;
  no_cys_linked = 0;
  sequence.clear();
  bonding_state.clear();
  bonding_state_all.clear();
  
  string line;
  while(getline(fp, line)) {
    // First field is residue symbol, second and last
    // describes if it is a target and of which type.
    istringstream iss(line);

    char aa;
    int aa_type;
    iss >> aa >> aa_type;

    bonding_state_all.push_back(aa_type);

    if( aa=='C' ) {
      ++no_cys_total;
      if( aa_type == 1 || (treat_inter_as_bonded && aa_type==2)) {
	bonding_state.push_back(true);
	cys_coupled.push_back(-2); // unknown coupled
	++no_cys_linked;
      }
      else {
	bonding_state.push_back(false);
	cys_coupled.push_back(-1);
      }
      cys_position.push_back(sequence.size()-1);
    }
    sequence.push_back(aa);
  }

  if( !treat_inter_as_bonded )
    Exception::Assert((no_cys_linked%2)==0,"Error in number of SS bridges");

  // assign label to current protein: 'all', 'none', 'mix'
  if(no_cys_linked==no_cys_total)
    dsclass = DS_ALL;
  else if(no_cys_linked==0)
    dsclass = DS_NONE;
  else
    dsclass = DS_MIXED;
}

void Protein::ReadProfile(istream &fp) {
  // PSSM must have a number of rows equal to sequence length

  Exception::Assert(sequence.size()>0);

  string line;
  entropy.resize(sequence.size());
  profile.resize(sequence.size()*20);
  while(getline(fp, line)) {
    if(line.find("## SEQUENCE PROFILE AND ENTROPY") != string::npos) {
      getline(fp, line); // skip first line after profile indication
      int seq_num = 0;
      while(getline(fp, line) &&
	    line.find("##") == string::npos && // hssp file may contain other data beyond profile
	    line.find("//") == string::npos)   // hssp file ends with '//'
	{
	  
	// profile on each position begins at character 16
	  string portion_of_interest(line, 14);
	  istringstream iss(portion_of_interest);
	  
	  int index = 0;
	  float profile_info[26];
	  while(iss >> profile_info[index++]);
	  for( int i=0; i<20; i++ )
	    profile[seq_num][aa_codes[i]] = profile_info[i];
	  entropy[seq_num] = profile_info[24]/100.; //REL_ENT
	  ++seq_num;
	}
    }
  }
}


void Protein::ReadPsiBlast0(istream &fp) {
  // PSSM must have a number of rows equal to sequence length

  string line;
  entropy.clear();
  sequence.clear();
  profile.clear();
  while(getline(fp, line)) {
    if(line.find("## SEQUENCE PROFILE AND ENTROPY") != string::npos) {
      getline(fp, line); // skip first line after profile indication
      int seq_num = 0;
      while(getline(fp, line) &&
	    line.find("##") == string::npos && // hssp file may contain other data beyond profile
	    line.find("//") == string::npos)   // hssp file ends with '//' 
      {
	// profile on each position begins at character 16
	string portion_of_interest(line, 13);
	istringstream iss(portion_of_interest);
	
	// read profile

	int index = 0;
	float profile_info[26];
	while(iss >> profile_info[index++]);
	
	// save data
	 
	float prof_sum = 0.;
	sequence.push_back('?');
	profile.push_back(map<char,double>());	  
	for( int i=0; i<20; i++ ) {
	  profile[seq_num][aa_codes[i]] = profile_info[i];
	  prof_sum += profile_info[i];
	}
	if( prof_sum>0. ) {
	  for( int i=0; i<20; i++ )
	    profile[seq_num][aa_codes[i]] /= prof_sum;
	}
	else {
	  for( int i=0; i<20; i++ )
	    profile[seq_num][aa_codes[i]] = 0.05;
	}	
	entropy.push_back(profile_info[24]/100.); //REL_ENT
	
	++seq_num;
      }
    }
  }
}


void Protein::ReadPSIBLASTSet(vector<Protein> &infos, const string &seqdir, const string &profiledir, bool output, bool treat_inter_as_bonded) 
{
  infos.clear();
  struct dirent **filelist;

  //Read sequences

  int nofiles = scandir(seqdir.c_str(),&filelist,SelectAll,alphasort);
  for( int i=0; i<nofiles; i++ ) {
    if(!strcmp(filelist[i]->d_name, ".") || !strcmp(filelist[i]->d_name, ".."))
      continue;

    if( output )
      OutProgress(i,10);

    // open protein files

    ifstream fp;
    string file_name = seqdir + filelist[i]->d_name;
    fp.open(file_name.c_str());
    Exception::Assert(fp.good(),"Error while opening sequence file <%s>", file_name.c_str());

    infos.push_back(Protein());
    infos.back().name = filelist[i]->d_name;
    infos.back().name = infos.back().name.substr(0,5);

    ifstream fpsi;
    string profile_filename = profiledir + infos.back().name + ".psi0";
    fpsi.open(profile_filename.c_str());
    if( !fpsi.good() ) {
      cerr<<StringF("Error while opening profile file <%s>. Skipping...", profile_filename.c_str())<<endl;
      continue;
    }

    try {
      infos.back().ReadSequence(fp,treat_inter_as_bonded);
      if( infos.back().no_cys_total<=1 ) {
	infos.erase(infos.end());
	continue;
      }
      infos.back().ReadProfile(fpsi);
      
      if(infos.back().profile.size() == 0) {
	cerr << "Empty profile. Skipping..." << endl;
	continue;
      }
      Exception::Assert(infos.back().profile.size()!=infos.back().sequence.size(),
			"sequence length != # rows in profile");
    }
    catch(Exception *e) {
      Exception *ne = new Exception("Error while reading protein <%s>\n\t%s",infos.back().name.c_str(),e->GetMessage());
      delete e;
      throw ne;
    }

    delete filelist[i];
  }

  if( output )
    cerr<<"Adding descriptors\n";
  AddDescriptors(infos);
  
  if( nofiles>0 )
    delete []filelist;
}


void Protein::ReadPsiBlast2(istream& is, double prior) {

  string line;

  // First three lines aren't pssm data
  getline(is, line); 
  getline(is, line);
  getline(is, line);

  // get from current line the amino acids at given profile positions

  char psi2_aa_codes[20];
  istringstream iss(line);
  for(int i=0; i<20; ++i) 
    iss >> psi2_aa_codes[i]; 

  int c=0;
  sequence.clear();
  profile.clear();
  pssm.clear();
  entropy.clear();
  while(getline(is, line) && line.size()) {
    istringstream linestream(line);

    // read sequence position and amino acid char

    char aa;
    int sequence_pos; 
    linestream >> sequence_pos >> aa;    
    sequence.push_back(aa);
    profile.push_back(map<char,double>());
    pssm.push_back(map<char,double>());
    
    // read pssm for current amino acid position
    double value = 0;
    for(int i=0; i<20; ++i) {
      linestream >> value;
      pssm[c][psi2_aa_codes[i]] = value;
    }
      
    // Ok, now read profile for current amino acid position
    bool all_zero = true;
    for(int i=0; i<20; ++i) {
      linestream >> value;
      profile[c][psi2_aa_codes[i]] = value;
      if(value != 0)
	all_zero = false;
    }
    
    if(all_zero) {
      for(int i=0; i<20; ++i)
	profile[c][psi2_aa_codes[i]] = (1-prior)/20.;
      profile[c][sequence[c]] += prior;
    }
    else {	
      // and normalize it
      float norm_factor = 0.0;
      for(int i=0; i<20; ++i)
	norm_factor +=  profile[c][psi2_aa_codes[i]];
      for(int i=0; i<20; ++i)
	profile[c][psi2_aa_codes[i]] /= norm_factor;
    }
    
    // read entropy

    double ent;
    linestream >> ent;
    entropy.push_back(ent);

    // read conservation weight

    double w;
    linestream >> w;
    weight.push_back(w);

    // go to the next

    c++;
  }
}

void Protein::ReadTDB(istream& is) 
{
  char cval;
  int ival;
  double dval;  
  string line;
  
  sequence.clear();
  chain_code.clear();
  profile.clear();
  entropy.clear();
  sec_structure.clear();
  aa_kappa.clear();
  aa_alpha.clear();
  aa_psi.clear();
  aa_phi.clear();
  aa_x.clear();
  aa_y.clear();
  aa_z.clear();
  for(int c=0;;) {
    getline(is, line); 
    if( !is.good() )
      break;
    if( line.length()==0 && line[0]=='#' ) // skip header
      continue;    
    
    istringstream iss(line);
    iss >> ival; //residue number
    iss >> cval; sequence.push_back(cval); //aa code
    if( line[7]!=' ' ) {
      iss >> cval; 
      sec_structure.push_back(CheckSS(cval));
    }
    else
      sec_structure.push_back('C');
    iss >> dval; // Accessibility (% GGXGG pentapeptide in fully extended conformation)
    iss >> dval; aa_phi.push_back(dval); // phi angle
    iss >> dval; aa_psi.push_back(dval); // psi angle
    iss >> dval; // omega angle
    iss >> dval; // Nx
    iss >> dval; // Ny
    iss >> dval; // Nz 
    iss >> dval; aa_x.push_back(dval); // CAx
    iss >> dval; aa_y.push_back(dval); // CAy
    iss >> dval; aa_z.push_back(dval); // CAz 
    iss >> dval; // Cx
    iss >> dval; // Cy
    iss >> dval; // Cz 
    iss >> dval; // Ox
    iss >> dval; // Oy
    iss >> dval; // Oz 
    iss >> dval; // CBx
    iss >> dval; // CBy
    iss >> dval; // CBz 
    iss >> ival; // PDB Residue ID
    profile.push_back(map<char,double>());
    for( int i=0; i<20; i++ ) {
      iss >> dval; // profile
      profile[c][tdb_aa_codes[i]] = 1/(1+exp(-dval));
    }       
    c++;
  }
}
  
