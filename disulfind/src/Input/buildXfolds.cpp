#include <stdlib.h>
#include <vector>
#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <stdio.h>
using namespace std;

void buildXfolds(const char * example_file, unsigned int folds_number){

  std::ifstream in(example_file);
  assert(in.good());
  std::list<std::string> examples;
  std::vector<std::string> fold_examples[folds_number];
  std::list<std::string>::iterator iter;
  std::string buf;
  
  while(getline(in,buf))
    examples.push_back(buf);
  
  if(examples.size() < folds_number){
    cerr << "ERROR: only " << examples.size() << " examples for " << folds_number << " folds" << endl;
    exit(0);
  }

  srand(time(0));
  int examplesXfold = examples.size() / folds_number;
  for(unsigned int i = 0; i < folds_number; i++){
    int currfold_examples = examplesXfold;
    while (currfold_examples-- > 0){
      int ex = rand() % examples.size();      
      iter = examples.begin();
      for(int h = 0; h < ex; h++)
	iter++;
      fold_examples[i].push_back(*iter);
      examples.erase(iter);
    }
  }
  unsigned int i = 0; 
  for(iter = examples.begin(); iter != examples.end(); iter++)
    fold_examples[i++].push_back(*iter);
 
  for(unsigned int i = 0; i < folds_number; i++){
    buf = example_file;
    buf += "_train";
    char Buf[20];
    sprintf(Buf, "%d", i);
    buf += Buf;
    std::ofstream out_train(buf.c_str());
    assert(out_train.good());
    buf =  example_file;
    buf += "_test";
    buf += Buf;
    std::ofstream out_test(buf.c_str());
    assert(out_test.good());

    for(unsigned int j = 0; j < folds_number; j++){
      if (i == j)
	for (unsigned int h = 0; h < fold_examples[j].size(); h++)
	  out_test << fold_examples[j][h] << endl;
      else
	for (unsigned int h = 0; h < fold_examples[j].size(); h++)
	  out_train << fold_examples[j][h] << endl;
    }
  } 
}

#ifdef STANDALONE
int main(int argc, char ** argv){

  if (argc < 3){
    cerr << "Too few arguments, need: <examples_file> <folds_number>" << endl;
    exit(0);
  }
  
  buildXfolds(argv[1],(unsigned int)atoi(argv[2]));
}
#endif
