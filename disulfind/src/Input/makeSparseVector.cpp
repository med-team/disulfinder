#include "../Common/Matrix.h"

void makeSparseVector(const Matrix<double>& M, ostream& out){
  
  int rows = M.GetNoRows();
  int cols = M.GetNoCols();
  
  for(int r = 0; r < rows; r++){
    for(int c = 0; c < cols; c++)
      if(M(r,c) != 0)
	out << c+1 << ":" << M(r,c) << " ";
    out << endl;
  }  
}


#ifdef STANDALONE
int main(int argc, char ** argv){
  
  Matrix<double> M;
  M.Read(cin);
  makeSparseVector(M,cout);
}
#endif
