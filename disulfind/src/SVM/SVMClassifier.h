
#ifndef SVMCLASSIFIER_H
#define SVMCLASSIFIER_H

#include <math.h>
#include "Data.h"

float computeBalancedCost(const char* labelfile);

class SVMModel{

 public:
  typedef enum KernelType_
    {
      LINEAR = 0,
      POLYNOMIAL,
      RBF,
      SIGMOID
    } KernelType;
  double * m_alphas;
  int m_sv_number;
  Data * m_svs;
  double m_bias;
  KernelType m_kerneltype;
  double m_kernelpower;
  double m_kernelgamma;
  double m_kernelcoeff;
  double m_kerneloffset;
  
  SVMModel(){
    init();
  }
  ~SVMModel(){
    if(m_alphas != NULL)
      delete[] m_alphas; 
    if(m_svs != NULL)
      delete[] m_svs;
  }
  void init(){
    m_alphas = NULL;
    m_svs = NULL;
    m_sv_number = 0;
    m_bias = 0.;
  } 
  friend std::ostream & operator<<(std::ostream &out, const SVMModel& model){
    
    out << model.m_kerneltype << " " 
	<< model.m_kernelpower << " " 
	<< model.m_kernelgamma << " " 
	<< model.m_kernelcoeff << " " 
	<< model.m_kerneloffset << " " 
	<< model.m_bias << " " 
	<< model.m_sv_number << endl;

    for(int i = 0; i < model.m_sv_number; i++){
      out.precision(32);
      out << model.m_alphas[i] << " ";
      out.precision(8);
      out <<  model.m_svs[i];
    }
    return out;
  }
  friend std::istream & operator>>(std::istream &in, SVMModel& model){    

    int kerneltype;

    in >> kerneltype 
       >> model.m_kernelpower
       >> model.m_kernelgamma
       >> model.m_kernelcoeff
       >> model.m_kerneloffset
       >> model.m_bias
       >> model.m_sv_number
       >> ws;
      
    model.m_kerneltype = KernelType(kerneltype);
    
    model.m_alphas = new double[model.m_sv_number];
    model.m_svs = new Data[model.m_sv_number];
    for(int i = 0; i < model.m_sv_number; i++){
      in >> model.m_alphas[i] >> model.m_svs[i];
      if(model.m_kerneltype == RBF)
	model.m_svs[i].m_innerprod = model.innerProduct(model.m_svs[i],model.m_svs[i]);
    }
    return in;
  }
  double innerProduct(const Data& x, const Data& y) const{
    
    double v = 0;

    for(unsigned int i = 0, j = 0; i < x.m_features.size() && j < y.m_features.size();){
      if(x(i).first > y(j).first)
	j++;
      else if (x(i).first < y(j).first)
	i++;
      else{
	v += x(i).second * y(j).second;
	i++;
	j++;
      }
    }
    return v;
  }
  
  double Kernel(const Data& x, const Data& y) const{
  
    double v = innerProduct(x,y);

    switch(m_kerneltype)
      {
      case LINEAR:
	return v;
      case POLYNOMIAL:
	return pow(m_kernelcoeff*v+m_kerneloffset,m_kernelpower);
      case RBF:
	return exp(-m_kernelgamma*(x.m_innerprod+y.m_innerprod-2*v));
      case SIGMOID:
	return tanh(m_kernelcoeff*v+m_kerneloffset);
      default:
	return v;
      }
  }
  
  double Predict(Data x) const{            

    double f = 0.;
    for (int i=0;i<m_sv_number;i++)
      f += m_alphas[i]*Kernel(x, m_svs[i]);
    return f-m_bias;
  }
};

class SVMClassifier{

 private:
  SVMModel m_model;

public:

  SVMClassifier(std::istream& in);
  void Serialize(std::ostream& out) const;
  void DeSerialize(std::istream& in);
  void classify(const char * input, const char * output) const;
  
protected:
  void init();
};

#endif
