
#include <string>
#include <iostream>

using namespace std;

string getParam(istream& in)
{
  string param;
  string buf;
  in >> param;
  getline(in,buf);
  return param;
}

int main(int argc, char ** argv)
{
  string buf;
  // version header
  getline(cin,buf);
  // kernel params
  for(uint i = 0 ; i < 5; i++)
    cout << getParam(cin) << " ";
  // ignored params
  for(uint i = 0 ; i < 3; i++)
    getline(cin,buf);    
  int sv_number = atoi(getParam(cin).c_str())-1;
  // bias and sv_number
  cout << getParam(cin) << " " << sv_number << endl;
  // get support vectors
  while(getline(cin,buf)){
    int comment = buf.find("#");
    if(comment != string::npos)
      buf = buf.substr(0,comment);
    cout << buf << endl;
  }
}
