#ifndef DATA_H
#define DATA_H

#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

class Data{

 public:
  typedef vector< pair<int,float> > Features;
  Features m_features;
  double m_innerprod;

  Data() { m_innerprod = 0;}
  Data(const std::string& features_row){
    LoadFeatures(features_row);
  }
  const pair<int,float>& operator() (unsigned int i) const{
    return m_features[i];
  }

  friend std::ostream & operator<<(std::ostream &out, const Data& el){
    for(unsigned int i = 0; i < el.m_features.size(); i++)
      out << " " << el(i).first << ":" << el(i).second;
    out << endl;
    return out;
  }
  friend std::istream & operator>>(std::istream &in, Data& el){    
    string buf;
    getline(in, buf);
    el.LoadFeatures(buf);
    return in;
  }
  void LoadFeatures(const std::string& features_row){

    string::size_type offset = features_row.find_first_of(" ");
    if(offset == string::npos){
      cerr << "ERROR: incorrect feature row formatting" << endl;
      return;
    }
    while(true){
      string::size_type end = features_row.find_first_of(":",offset+1);
      if(end == string::npos)
	break;
      long index = atol(features_row.substr(offset+1,end-offset-1).c_str());
      offset = features_row.find_first_of(" ",end);
      if(offset == string::npos){
	m_features.push_back(make_pair(index,atof(features_row.substr(end+1).c_str())));
	break;
      }
      m_features.push_back(make_pair(index,atof(features_row.substr(end+1,offset-end-1).c_str())));
    }
  }

};

#endif








