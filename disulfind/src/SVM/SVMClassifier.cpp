
#include "SVMClassifier.h"
#include "../Common/Exception.h"
#include <fstream>
#include <cassert>

using namespace std;

float computeBalancedCost(const char* labelfile)
{
  ifstream in(labelfile);
  Exception::Assert(in.good(), "Error: could not open label file %s", labelfile);  
  string buf;
  int currlabel;
  int poslabels = 0;
  int neglabels = 0;
  
  while(in.good()){    
    in >> currlabel;
    getline(in,buf);
    Exception::Assert(currlabel == 1 || currlabel == -1, "Error: non binary label %d found", currlabel);  
    if(currlabel == -1)
      neglabels++;
    else
      poslabels++;
    in.peek();
  }
  return (float)neglabels / (float)poslabels;
}

SVMClassifier::SVMClassifier(istream& in){

  init();
  in >> m_model;
}

void SVMClassifier::Serialize(ostream& out) const
{
  out << m_model;
}

void SVMClassifier::DeSerialize(istream& in)
{	
  in >> m_model;
}


void SVMClassifier::init()
{

}

void SVMClassifier::classify(const char * input, const char * output) const
{
  ifstream in(input);
  ofstream out(output);
  assert(in.good() && out.good());
  int label;

  in.peek();
  while(in.good()){
    Data example;
    in >> label >> example;
    if(m_model.m_kerneltype == SVMModel::RBF)
      example.m_innerprod = m_model.innerProduct(example,example);
    out << m_model.Predict(example) << endl;
    in.peek();
  }
  in.close();
  out.close();
}



#ifdef SVMCLASSIFIER
int main(int argc, char* argv[]) {

  if (argc < 3){
    cerr << "Usage: " << argv[0]  << " <modelfile> <testfile> <resultsfile>" << endl;
    exit(0);
  }

  const char * modelfile = argv[1];
  const char * testfile = argv[2];
  const char * resultsfile = argv[3];
  
  ifstream in_model(modelfile);
  assert(in_model.good());

  SVMClassifier classifier(in_model);
  classifier.classify(testfile, resultsfile);
}
#endif
