#include <string.h>
#include <sstream>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <fstream>

#include "disultrainer.h"
#include "Input/utils.h"
#include "Input/createKernelDataset.h"
#include "Input/createBRNNDataset.h"
#include "SVM/SVMClassifier.h"
#include "FSA-Alignment/viterbi_aligner.h"
#include "BRNN/rnn_util.h"

#define SVMLEARN "svm_learn"
#define SVMCLASSIFY "svm_classify"
#define BRNNLEARN "brnn-train"
#define BRNNCLASSIFY "brnn-test"
#define DIRMASK 0777

void clearInput(const string& inputlist, 
		const string& psidir, 
		const string& outputlist)
{
  ifstream in(inputlist.c_str());
  Exception::Assert(in.good(), "Error: could not open input file %s", inputlist.c_str());
  ofstream out(outputlist.c_str());
  Exception::Assert(out.good(), "Error: could not open output file %s", outputlist.c_str());
  
  string chain;  
  while(getline(in,chain)){
    string::size_type beg = chain.find_first_of(" \t");
    if(beg != string::npos && beg < chain.size()-1)
      chain = chain.substr(beg+1);
    string psifile = psidir + "/" + chain;
    
    Protein info;
    ifstream inprofile(psifile.c_str());
    if(!inprofile.good()){
      cerr << "WARNING: missing profile file for chain " << chain << ". Skipped." << endl;
      continue;
    }
    info.ReadPsiBlast2(inprofile);
    inprofile.close();
    string sequence = info.GetSequence();
    if(sequence.find("C") == string::npos){
      cerr << "WARNING: no cysteines found in chain " << chain << ". Skipped." << endl;
      continue;
    }
    out << chain << endl;
  }
  in.close();
  out.close();
}

void writeFile(const string& input, ostream& out)
{
  ifstream in(input.c_str());
  Exception::Assert(in.good(), "Error: could not open input file %s", input.c_str());
  string buf;
  
  while(getline(in,buf))
    out << buf << endl;
  
  in.close();
}

void writeFile(const string& input,const string& output, ios_base::openmode mode)
{
  ifstream in(input.c_str());
  Exception::Assert(in.good(), "Error: could not open input file %s", input.c_str());
  ofstream out(output.c_str(),mode);
  Exception::Assert(out.good(), "Error: could not open output file %s", output.c_str());
  string buf;
  
  while(getline(in,buf))
    out << buf << endl;
  
  in.close();
  out.close();
}

void ViterbiAligner(const string& list, 
		    const string& brnnpred,
		    const string& brnnsize,
		    const string& viterbimodel,
		    const string& viterbiinputdir,
		    const string& viterbioutputdir,
		    const string& viterbioutputfile)
{
  ifstream inlist(list.c_str());
  Exception::Assert(inlist.good(), "Error: could not open input file %s", list.c_str());
  ifstream inpred(brnnpred.c_str());
  Exception::Assert(inpred.good(), "Error: could not open input file %s", brnnpred.c_str());
  ifstream insize(brnnsize.c_str());
  Exception::Assert(insize.good(), "Error: could not open input file %s", brnnsize.c_str());
  ofstream out(viterbioutputfile.c_str());
  Exception::Assert(out.good(), "Error: could not open output file %s", viterbioutputfile.c_str());

  string chain,buf,infile,outfile;
  unsigned int currsize;
  ofstream outbrnn,outviterbi;
  while(getline(inlist,chain)){
    // recover chain name
    string::size_type beg = chain.find_first_of(" \t");
    if(beg != string::npos && beg < chain.size()-1)
      chain = chain.substr(beg+1);
    // recover number of cysteines
    insize.peek();
    Exception::Assert(insize.good(), "Error: incorrect number of rows in file %s", brnnsize.c_str());
    insize >> currsize >> ws;
    // create input file
    infile = viterbiinputdir + "/" + chain;
    outbrnn.open(infile.c_str());
    Exception::Assert(outbrnn.good(), "Error: could not open output file %s", infile.c_str());
    for(unsigned int i = 0; i < currsize; i++){
      inpred.peek();
      Exception::Assert(inpred.good(), "Error: incorrect number of rows in file %s", brnnpred.c_str());
      getline(inpred,buf);
      outbrnn << buf << endl;	
    }
    outbrnn.close();
    // run viterbi aligner
    outfile = viterbioutputdir + "/" + chain;
    outviterbi.open(outfile.c_str());
    Exception::Assert(outviterbi.good(), "Error: could not open output file %s", outfile.c_str());
    int res = viterbi_aligner(viterbimodel.c_str(),infile.c_str(),outviterbi);
    outviterbi.close();
    if(res != 0)
      exit(res);
    writeFile(outfile,out);
  }
  inlist.close();
  inpred.close();
  insize.close();
  out.close();
}

void createViterbiAligner(const string& outfile)
{
  ofstream out(outfile.c_str());
  Exception::Assert(out.good(), "Error: could not open output file %s", outfile.c_str());
  out << "NoAutomata 1\n"
      << "BeginAutomata\n"
      << "NoOutputs 2\n"
      << "NoStates 4\n"
      << "StartStates 0\n"
      << "EndStates 0 3\n"
      << "Begin\n"
      << "T 0 0 0 1.\n"
      << "T 0 1 1 1.\n"
      << "T 1 2 0 1.\n"
      << "T 1 3 1 1.\n"
      << "T 2 2 0 1.\n"
      << "T 2 3 1 1.\n"
      << "T 3 0 0 1.\n"
      << "T 3 1 1 1.\n"
      << "End\n"
      << "EndAutomata\n";
  out.close();
}

void trainSVM(const string& kernelparams,
	      const string& inputfile,
	      const string& modelfile,
	      const string& outputfile,
	      const string& logfile)
{
  float j = computeBalancedCost(inputfile.c_str());
  ostringstream command;
  command << SVMLEARN << " -j " << j << " " << kernelparams
	  << " " << inputfile << " " << modelfile << " > " << logfile;
  cout << command.str() << endl;
  system(command.str().c_str());
}

void testSVM(const string& inputfile,
	     const string& modelfile,
	     const string& outputfile,
	     const string& logfile)
{
  ostringstream command;
  command << SVMCLASSIFY << " " << inputfile << " " 
	  << modelfile << " " << outputfile << " > " << logfile;
  cout << command.str() << endl;
  system(command.str().c_str());
}

void trainBRNN(const string& brnnparams,
	       const string& traindata,
	       const string& traintarget,
	       const string& trainsize,
	       const string& validatedata,
	       const string& validatetarget,
	       const string& validatesize,
	       const string& model,
	       const string& log)
{
  ostringstream command;
  command << BRNNLEARN << " " << brnnparams << " " 
	  << traintarget << " " << traindata << " " 
	  << trainsize << " " << model << " "
	  << validatetarget << " " << validatedata << " " 
	  << validatesize << " > " << log;
  cout << command.str() << endl;
  system(command.str().c_str());	  
}

double getAccuracy(const string& log)
{
  ifstream in(log.c_str());
  Exception::Assert(in.good(), "Error: could not open input file %s", log.c_str());
  string buf;
  double min;
  
  while(in.good()){
	in >> buf;
  	if(buf == "Minimum"){
		in >> buf >> min;
		in.close();
		return min;
	}
	getline(in,buf);
  }
  Exception::Assert(false, "Error: could not find 'Minimum' row while parsing file %s", log.c_str());
  return 0.;
}

void trainBRNNs(unsigned int trials,
		const string& brnnparams,
	       const string& traindata,
	       const string& traintarget,
	       const string& trainsize,
	       const string& validatedata,
	       const string& validatetarget,
	       const string& validatesize,
	       const string& modelpref,
	       const string& logpref)
{

  if(trials <= 1){
	trainBRNN(brnnparams,traindata,traintarget,trainsize,
			  validatedata,validatetarget,validatesize,
			  modelpref,logpref);
	return;
  }

  double bestacc = 0.;
  unsigned int bestmodel = 0;
  for(unsigned int i = 0; i < trials; i++){
  	ostringstream model,log;
	model << modelpref << "." << i;
	log << logpref << "." << i;
	trainBRNN(brnnparams,traindata,traintarget,trainsize,
			  validatedata,validatetarget,validatesize,
			  model.str(),log.str());
	double curracc = getAccuracy(log.str());
	if(curracc > bestacc){
			bestacc = curracc;
			bestmodel = i;
	}
  }
  // create symbolic link to best model
  ostringstream srcmodel;
  string dstmodel = modelpref;
  srcmodel << basename(modelpref.c_str()) << "." << bestmodel;
  if (brnnparams.find("-b") != string::npos){ // check if save on validation option used
	srcmodel << ".best";
  	dstmodel += ".best";
  }
  int res = symlink(srcmodel.str().c_str(),dstmodel.c_str());
  Exception::Assert(res == 0, "Error while creating symlimk from %s to %s:\n%s", 
				  srcmodel.str().c_str(),dstmodel.c_str(),strerror(errno));
}	   

void testBRNN(const string& data,
	      const string& target,
	      const string& size,
	      const string& model,
	      const string& output,
	      const string& log)
{
  ostringstream command;
  command << BRNNCLASSIFY << " " << target << " "
	  << data << " " << size << " " 
	  << model << " " << output << " > " << log;
  cout << command.str() << endl;
  system(command.str().c_str());	  
}	   

void test(const CommandLineOption& options)
{
  // make a copy of input cleared of incorrect chains
  string chainlist = options.wdir + "/chainlist";
  clearInput(options.input, options.psidir, chainlist);

  // create svm input data
  cout << "Creating input data for svm testing ... "; 
  cout.flush();
  string svmdir = options.wdir + "/SVM/";
  string svminput = svmdir + "/test";
  createKernelDataset(chainlist,                       
		      options.psidir,
		      svminput,
		      options.descstats,
		      options.w,
		      options.labeldir,
		      options.use_pssm);
  cout << "done" << endl;    
  // svm prediction
  cout << "Computing SVM prediction ... " << endl;
  string svmmodel = svmdir + "/model";
  string svmoutput = svmdir + "/test.prediction";
  string svmlog = svmdir + "/test.log";
  testSVM(svminput,svmmodel,svmoutput,svmlog);
  cout << "done" << endl;
  
  // create brnn input data 
  cout << "Creating input data for brnn testing ... "; 
  cout.flush();
  string brnndir = options.wdir + "/BRNN/";
  string brnndata = brnndir + "/test.data";
  string brnntarget = brnndir + "/test.target";
  string brnnsize = brnndir + "/test.size";
  createBRNNDataset(chainlist, 
		    options.psidir,
		    svmoutput,
		    svminput,
		    brnndata,
		    brnntarget,
		    brnnsize);
  cout << "done" << endl;  
  // brnn testing
  cout << "Computing BRNN prediction ... ";
  cout.flush();  
  string brnnmodel = brnndir + "/model.best";
  string brnnoutput = brnndir + "/test.prediction";
  string brnnlog = brnndir + "/test.log";
  testBRNN(brnndata,brnntarget,brnnsize,brnnmodel,brnnoutput,brnnlog);
  cout << "done" << endl;  

  // viterbi aligner adjusting non consistent predictions
  string viterbidir = options.wdir + "/Viterbi/";
  string viterbimodel = viterbidir + "/model";
  string viterbiinputdir = viterbidir + "/Input/";
  mkdir(viterbiinputdir.c_str(),DIRMASK);
  string viterbioutputdir = viterbidir + "/Output/";
  mkdir(viterbioutputdir.c_str(),DIRMASK);
  string viterbioutputfile = viterbidir + "/test.prediction";
  cout << "Computing Viterbi alignment ... ";
  cout.flush();
  ViterbiAligner(chainlist,
		 brnnoutput,
		 brnnsize,
		 viterbimodel,
		 viterbiinputdir,
		 viterbioutputdir,
		 viterbioutputfile);
  cout << "done" << endl;  
}

void kfoldcrossvalidationSVM(unsigned int numfolds,
			     const string& prefix,
			     const string& kernelparams,
			     const string& psidir,
			     const string& labeldir,
			     const string& descstats,
			     const string& workdir,
			     unsigned int w,
			     bool use_pssm)
{  
  string svmmodel,svminput,svmlog,svmoutput; 
  
  buildXfolds(prefix.c_str(),numfolds);

  for(unsigned int k = 0; k < numfolds; k++){
    ostringstream folddir,svmtrainlist,svmtestlist;
    folddir << workdir << k << "/";
    mkdir(folddir.str().c_str(),DIRMASK);
    svmtrainlist << prefix << "_train" << k;   
    svmtestlist << prefix << "_test" << k;   
    svmmodel = folddir.str() + "/model";
    svminput = folddir.str() + "/train";
    createKernelDataset(svmtrainlist.str(),psidir,svminput,descstats,w,labeldir,use_pssm);
    svmlog = folddir.str() + "/train.log";
    trainSVM(kernelparams,svminput,svmmodel,svmoutput,svmlog);  
    svminput = folddir.str() + "/test";
    createKernelDataset(svmtestlist.str(),psidir,svminput,descstats,w,labeldir,use_pssm);
    svmoutput = folddir.str() + "/test.predictions";
    svmlog = folddir.str() + "/test.log";
    testSVM(svminput,svmmodel,svmoutput,svmlog);
  }
}

void kfoldcrossvalidationSVM2BRNN(unsigned int numfolds,
				  const string& prefix,
				  const string& validationdir,
				  const string& brnntraininput,
				  const string& brnnvalidateinput,
				  const string& brnntrainsvmpred,	
				  const string& brnntrainsvmdata,
				  const string& brnnvalidatesvmpred,  
				  const string& brnnvalidatesvmdata)
{ 
  unsigned int k = 0;
  ostringstream traininput0,traindata0,trainpred0;
  traininput0 << prefix << "_test" << k;   
  traindata0 << validationdir << "/" << k << "/test";
  trainpred0 << validationdir << "/" << k << "/test.predictions";  
  writeFile(traininput0.str(),brnntraininput, ofstream::out | ofstream::trunc);
  writeFile(trainpred0.str(),brnntrainsvmpred, ofstream::out | ofstream::trunc);
  writeFile(traindata0.str(),brnntrainsvmdata, ofstream::out | ofstream::trunc);    
  for(k = 1; k < numfolds-1; k++){
    ostringstream traininput,traindata,trainpred;
    traininput << prefix << "_test" << k;   
    traindata << validationdir << "/" << k << "/test";
    trainpred << validationdir << "/" << k << "/test.predictions";  
    writeFile(traininput.str(),brnntraininput, ofstream::out | ofstream::app);
    writeFile(trainpred.str(),brnntrainsvmpred, ofstream::out | ofstream::app);
    writeFile(traindata.str(),brnntrainsvmdata, ofstream::out | ofstream::app);
  }
  ostringstream validateinput,validatedata,validatepred;
  validateinput << prefix << "_test" << k;   
  validatedata << validationdir << "/" << k << "/test";
  validatepred << validationdir << "/" << k << "/test.predictions";  
  writeFile(validateinput.str(),brnnvalidateinput, ofstream::out | ofstream::trunc);
  writeFile(validatepred.str(),brnnvalidatesvmpred, ofstream::out | ofstream::trunc);
  writeFile(validatedata.str(),brnnvalidatesvmdata, ofstream::out | ofstream::trunc);    
}    

void train(const CommandLineOption& options)
{
  mkdir(options.wdir.c_str(),DIRMASK);
  // make a copy of input cleared of incorrect chains
  string chainlist = options.wdir + "/chainlist";
  clearInput(options.input, options.psidir, chainlist);
  // create svm input data
  cout << "Creating input data for svm training ... "; 
  cout.flush();
  string svmdir = options.wdir + "/SVM/";
  mkdir(svmdir.c_str(),DIRMASK);
  string svminput = svmdir + "/train";
  createKernelDataset(chainlist,                       
		      options.psidir,
		      svminput,
		      options.descstats,
		      options.w,
		      options.labeldir,
		      options.use_pssm);
  cout << "done" << endl;    
  // SVM  training 
  cout << "Training SVM ... " << endl;
  string svmmodel = svmdir + "/model";
  string svmoutput = svmdir + "/train.prediction";
  string svmlog = svmdir + "/train.log";
  trainSVM(options.kernelparams,svminput,svmmodel,svmoutput,svmlog);  
  cout << "done" << endl;

  // kfold SVM for brnn inputs
  cout << "Performing k-fold SVM training for parameter estimation ... " << endl;
  string validationdir = options.wdir + "/Validation/";
  mkdir(validationdir.c_str(),DIRMASK);
  kfoldcrossvalidationSVM(options.folds,
			  chainlist,
			  options.kernelparams,
			  options.psidir,
			  options.labeldir,
			  options.descstats,
			  validationdir,
			  options.w,
			  options.use_pssm);
  cout << "done" << endl;

  // create brnn input data 
  cout << "Creating input data for brnn training ... "; 
  cout.flush();
  string brnndir = options.wdir + "/BRNN/";
  mkdir(brnndir.c_str(),DIRMASK);
  string brnntraininput = brnndir + "/train.list";
  string brnnvalidateinput = brnndir + "/validate.list";
  string brnntrainsvmpred = brnndir + "/train.svm.predictions";
  string brnntrainsvmdata = brnndir + "/train.svm.data";
  string brnnvalidatesvmpred = brnndir + "/validate.svm.predictions";
  string brnnvalidatesvmdata = brnndir + "/validate.svm.data";
  kfoldcrossvalidationSVM2BRNN(options.folds,
			       chainlist,
			       validationdir,
			       brnntraininput,
			       brnnvalidateinput,
			       brnntrainsvmpred,	
			       brnntrainsvmdata,
			       brnnvalidatesvmpred,  
			       brnnvalidatesvmdata);    
  string brnntraindata =  brnndir + "/train.data"; 
  string brnntraintarget = brnndir + "/train.target";
  string brnntrainsize = brnndir + "/train.size";
  createBRNNDataset(brnntraininput, 
		    options.psidir,
		    brnntrainsvmpred,
		    brnntrainsvmdata,       
		    brnntraindata,
		    brnntraintarget,
		    brnntrainsize);
  string brnnvalidatedata = brnndir + "/validate.data";
  string brnnvalidatetarget = brnndir + "/validate.target";
  string brnnvalidatesize = brnndir + "/validate.size";
  createBRNNDataset(brnnvalidateinput, 
		    options.psidir,
		    brnnvalidatesvmpred,
		    brnnvalidatesvmdata,
		    brnnvalidatedata,
		    brnnvalidatetarget,
		    brnnvalidatesize);
  cout << "done" << endl;  
  // brnn training
  cout << "Training BRNN network ... " << endl;
  string brnnmodel = brnndir + "/model";
  string brnnlog = brnndir + "/train.log";
  trainBRNNs(options.brnntrials,
	     options.brnnparams,
	     brnntraindata,
	     brnntraintarget,
	     brnntrainsize,
	     brnnvalidatedata,
	     brnnvalidatetarget,
	     brnnvalidatesize,
	     brnnmodel,
	     brnnlog);
  cout << "done" << endl;

  // viterbi aligner adjusting non consistent predictions
  cout << "Creating viterbi aligner ... " << endl;
  string viterbidir = options.wdir + "/Viterbi/";
  mkdir(viterbidir.c_str(),DIRMASK);
  string viterbimodel = viterbidir + "/model";
  createViterbiAligner(viterbimodel);
  cout << "done" << endl;  
}

int main(int argc, char ** argv){
  
  CommandLineOption options(argc,argv);
  
  try{
    Exception::Assert(options.train || options.test, "Please specify either -t or -T");

    if(options.train)
      train(options);
    if(options.test)
      test(options);
  }
  catch(Exception *e) {
    cerr << e->GetMessage() << "\n" << endl;
    delete e;
    exit(1);
  }
}
