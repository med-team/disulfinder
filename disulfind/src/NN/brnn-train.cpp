#include <time.h>
#include <getopt.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <values.h>
#include "../Common/Exception.h"
#include "brnn-model.h"
#include "brnn-train.h"

using namespace std;

Options::Options(int argc, char *argv[]) :
  targets_file(""),
  inputs_file(""),  
  sizes_file(""),
  model_file(""),
  validation_targets_file(""),
  validation_inputs_file(""),  
  validation_sizes_file(""),

  global_mode(0),

  output_mode(BiRecursiveNeuralNetwork::SOFTMAX),
  error_mode(BiRecursiveNeuralNetwork::ENTROPY),
  accuracy_mode(BiRecursiveNeuralNetwork::MULTICLASS),
  
  dim_hidden_layer(2),
  dim_recursive_layer(1),
  dim_out_recursive_layer(0),
  dim_win_out(0),
  dim_code(0),
  init_weight(1e-2),
  init_seed(0),

  batch(false),
  learning_rate(1e-2),  
  momentum(0.),
  weight_decay(0.),
  shuffle(false),

  max_epoch(100),  
  early_stop(0),
  autosave(0),
  savebest(false),
  load_net("")

{
  int c;
  int* opti = 0;
  char *progname = argv[0];
  
  // you must put ':' only after parameters that require an argument
  // so after 'a' and '?' we don't put ':'

  while ((c = getopt_long(argc, argv, "g:o:r:y:h:x:X:k:c:q:s:tl:m:n:fi:e:a:bL:?", optionNames, opti)) != -1) {
    switch(c) {
      
    case 'g':
      global_mode = atoi(optarg);
      break;

    case 'o':
      output_mode = atoi(optarg);
      break;
    case 'r':
      error_mode = atoi(optarg);
      break;
    case 'y':
      accuracy_mode = atoi(optarg);
      break;      

    case 'h':
      dim_hidden_layer = atoi(optarg);
      break;
    case 'x':
      dim_recursive_layer = atoi(optarg);
      break;
    case 'X':
      dim_out_recursive_layer = atoi(optarg);
      break;
    case 'k':
      dim_win_out = atoi(optarg);
      break;
    case 'c':
      dim_code = atoi(optarg);
      break;
    case 'q':
      init_weight = atof(optarg);
      break;
    case 's':
      init_seed = atoi(optarg);
      break;

    case 't':
      batch = true;
      break;
    case 'l':
      learning_rate = atof(optarg);
      break;
    case 'm':
      momentum = atof(optarg);
      break;
    case 'n':
      weight_decay = atof(optarg);
      break;
    case 'f':
      shuffle = true;
      break;

    case 'i':
      max_epoch = atoi(optarg);
      break;
    case 'e':
      early_stop = atoi(optarg);
      break;
    case 'a':
      autosave = atoi(optarg);
      break;
    case 'b':
      savebest = true;
      break;
    case 'L':
      load_net = optarg;
      break;
      

    case '?':
    default:
      usage(progname);      
    }
  }

if( optind < argc-7) {
    cerr << "Invalid parameters" << endl;
    usage(progname);
  }
  else if( optind > argc-4 || (optind<argc-4 && optind!=argc-7)) {
    cerr << "Missing parameters" << endl;
    usage(progname);
  }
  else {
    targets_file = argv[optind];
    inputs_file  = argv[optind+1];
    sizes_file   = argv[optind+2];
    model_file   = argv[optind+3];
    if( optind==argc-7 ) {
      validation_targets_file = argv[optind+4];
      validation_inputs_file = argv[optind+5];
      validation_sizes_file = argv[optind+6];
    }
  }  
}

void Options::usage(char* progname) 
{
  cerr << "usage: " << progname << " [options]  <targets> <inputs> <sizes> <model> [<validation targets> <validation inputs> <validation sizes>]" << endl;
  cerr << "Options:" << endl;
  cerr << "\t-g, --global_mode=value: set the kind of global learning to adopt:" << endl;
  cerr << "\t\t0: transduction (local only, default)" << endl;
  cerr << "\t\t1: supersource (global only)" << endl;

  cerr << endl;

  cerr << "\t-o, --output_mode=value: set the kind of output function:" << endl; 
  cerr << "\t\t0: softmax (default)" << endl;
  cerr << "\t\t1: sigmoid" << endl;
  cerr << "\t\t2: linear" << endl;
  cerr << "\t-r, --error_mode=value: set the kind of error function:" << endl; 
  cerr << "\t\t0: entropy (default)" << endl;
  cerr << "\t\t1: mean square error" << endl;
  cerr << "\t\t2: area under ROC curve" << endl;
  cerr << "\t-y, --accuracy_mode=value: set the kind of accuracy value:" << endl; 
  cerr << "\t\t0: multiclass (default)" << endl;
  cerr << "\t\t1: RMSD" << endl;
  cerr << "\t\t2: multilabel" << endl;
  cerr << "\t\t3: area under ROC curve" << endl;

  cerr << endl;
  
  cerr << "\t-h, --hidden_layer=value: number of neurons in hidden layer (default=2)" << endl;
  cerr << "\t-x, --recursive_layer=value: number of neurons in recursive layer (default=1)" << endl;
  cerr << "\t-X, --out_rec_layer=value: number of neurons in second recursive layer (default=0)" << endl;
  cerr << "\t-k, --win_out=value: dimension <k> of the output window <2k+1> (default=0)" << endl;
  cerr << "\t-c, --code=value: dimension of the code word used for adaptive coding (default=0, no coding)" << endl;
  cerr << "\t-q, --init_weight=value: maximum value for random initialization of weights (default=1e-2)" << endl;
  cerr << "\t-s, --init_seed=value: seed for init random number generator (default 0=use current time)" << endl;
  cerr << endl;

  cerr << "\t-t, --batch: batch training (default=false)" << endl;
  cerr << "\t-l, --learning_rate=value: learning rate for gradient descent (default=1e-2)" << endl;
  cerr << "\t-m, --momentum=value: momentum term for gradient descent (default=0.)" << endl;
  cerr << "\t-n, --weight_decay=value: weight decay term for gradient descent (default=0.)" << endl;
  cerr << "\t-f, --shuffle: shuffle the patterns at each iterations (default=false)" << endl;
  cerr << endl;
   
  cerr << "\t-i, --max_epoch=value: maximum number of epochs for training (default=100)" << endl;
  cerr << "\t-e, --early_stop=value: stop training if model has not improved on validation set in the last <value> epochs (default 0=no stop)" << endl;
  cerr << "\t-a, --autosave=value: save the model every <value> epochs (default 0=no autosave)" << endl;
  cerr << "\t-b, --savebest: always save the best model found so far as tested on validation set (default=false)" << endl;
  cerr << "\t-L, --load=file: load a model from file" << endl;

  cerr << endl;

  cerr << "\t-?, --help: this message" << endl;
  exit(0);
}

void shuffle(vector<int> &sequence) 
{
  int size = sequence.size();
  for( int ind1=0; ind1<size; ind1++ ) {
    int ind2 = rand()%size;
    int dummy = sequence[ind2];
    sequence[ind2] = sequence[ind1];
    sequence[ind1] = dummy;
  }
}

void PrintHeader( const Options &options, bool validate ) 
{ 
  printf("\t");
  printf("\tTrain\tTrain");
  if( validate ) 
    printf("\tValid\tValid");  
  printf("\n");
  
  printf("#\t||W||");
  printf("\tErr\tAcc");
  if( validate )
    printf("\tErr\tAcc");  
  printf("\n");
}

void ReadCollection(COLLECTIONA &collection, const string &filename) {
  ifstream ifs(filename.c_str());
  Exception::Assert(ifs.good(),"Error while opening <%s> for reading",filename.c_str()); 
  collection.Read(ifs);
  ifs.close();
}

void ReadIndexes(INDEXES &indexes, const string &filename) {
  ifstream ifs(filename.c_str());
  Exception::Assert(ifs.good(),"Error while opening <%s> for reading",filename.c_str()); 
  indexes.Read(ifs);
  ifs.close();
}

int brnnTrain(const Options& options)
{
  try
  {        
    // read training set
    
    COLLECTIONA targets;
    COLLECTIONA inputs;
    INDEXES     sizes;    

    // read targets
    cout << "Reading training-set" << endl;    
    ReadCollection(targets,options.targets_file);
    int nooutputs = targets.Cols();

    // read inputs
    ReadCollection(inputs,options.inputs_file);
    int noinputs = inputs.Cols();

    // read sizes
    ReadIndexes(sizes,options.sizes_file);
    int nosequences = sizes.Size();
    
    cout << "Read " << inputs.Rows() << " patterns, " << sizes.Size() << " sequences" << endl;
    cout << "NoInputs " << inputs.Cols() << " NoOutputs " << targets.Cols() << endl;
    cout << endl;

    // read validation set

    bool validate = false;
    COLLECTIONA validation_targets;
    COLLECTIONA validation_inputs;
    INDEXES     validation_sizes;        
    if( options.validation_targets_file!="" ) {
      validate = true;
      cout << "Reading validation-set" << endl;    

      // read targets
      ReadCollection(validation_targets,options.validation_targets_file);
      Exception::Assert( validation_targets.Cols()==nooutputs, 
			 "Invalid number of columns (%d) in validation targets file", validation_targets.Cols());
      
      // read inputs
      ReadCollection(validation_inputs,options.validation_inputs_file);
      Exception::Assert( validation_inputs.Cols()==noinputs, 
			 "Invalid number of columns (%d) in validation inputs file", validation_inputs.Cols());

      // read sizes
      ReadIndexes(validation_sizes,options.validation_sizes_file);

      cout << "Read " << validation_inputs.Rows() << " patterns, " << validation_sizes.Size() << " sequences" << endl;
      cout << endl;
    }

    // Initialize the network
    cout << "Initializing the network" << endl;
    BiRecursiveNeuralNetwork net;
    if( options.load_net!="" ) {
      ifstream imfs(options.load_net.c_str());
      Exception::Assert(imfs.good(),"Error while opening <%s> for reading",options.load_net.c_str()); 
      net.Read(imfs);
      imfs.close();      
    }
    else {
      if( options.init_seed>0 )
	srand(options.init_seed);
      else
	srand((unsigned int)time(NULL));
      net.Init(options.global_mode,
	       noinputs,options.dim_hidden_layer,
	       options.dim_recursive_layer,options.dim_out_recursive_layer,
	       nooutputs,options.dim_win_out,
	       options.dim_code,options.init_weight);
    }

    // set training parameters
    cout << "Setting Training Parameters" << endl;
    net.SetOutputMode((BiRecursiveNeuralNetwork::OUTPUT_MODE)options.output_mode);
    net.SetErrorMode((BiRecursiveNeuralNetwork::ERROR_MODE)options.error_mode);
    net.SetAccuracyMode((BiRecursiveNeuralNetwork::ACCURACY_MODE)options.accuracy_mode);
    net.SetLearningParameters(options.learning_rate,options.momentum,options.weight_decay);
    cout << endl;
    
    //--------
    // Training

    cout << "Training the network" << endl;
 
    COLLECTIONA predictions;        
    int save_countdown = options.autosave;

    // used for shuffling
    vector<int> sequence(nosequences);
    for(int i=0; i<nosequences; i++) 
      sequence[i]=i;    

    // print header
    
    PrintHeader(options,validate);
    
    // used for early stopping
    int  best_epoch = -1;
    REAL best_validation_acc = 0.;
    int  stop_countdown = options.early_stop;

    // iterations
    for( int i=0; i<options.max_epoch; i++ ) {
      
      // train the network
      printf("%-5d", i+1);
      fflush(stdout);
      
      // shuffling
      if( options.shuffle && !options.batch) 
	shuffle(sequence);

      // weights norm
      printf("\t%6.4f", net.Norm());
      fflush(stdout);
      
      // train the network
      if( options.batch )
	net.TrainBatch(targets,inputs,sizes);
      else
	net.TrainOnLine(targets,inputs,sizes,sequence);

      // test on training set
      REAL train_err = net.Test(targets,inputs,predictions,sizes);      
      printf("\t%6.4f", train_err );
      fflush(stdout);
      
      // accuracy on test set
      REAL train_acc = net.Accuracy(targets,predictions);
      printf("\t%6.4f", train_acc);
      fflush(stdout);
      
      if( validate ) {
	// test on validation set
	REAL validation_err = net.Test(validation_targets,validation_inputs,predictions,validation_sizes);
	printf("\t%6.4f", validation_err );
	fflush(stdout);
	
	// accuracy on validation set
	REAL validation_acc = net.Accuracy(validation_targets,predictions);
	printf("\t%6.4f", validation_acc);
	fflush(stdout);

	// early stopping
	if( validation_acc > best_validation_acc ) {
	  best_validation_acc = validation_acc;
	  stop_countdown = options.early_stop;
	  best_epoch = i;
	  
	  // save the best model
	  if( options.savebest ) {
	    printf("\tsaving best");
	    fflush(stdout);

	    string filename = options.model_file + ".best";
	    ofstream omfs(filename.c_str());
	    Exception::Assert(omfs.good(),"Error while opening <%s> for writing",filename.c_str()); 
	    net.Write(omfs);
	    omfs.close();
	  }
	}
	
	if( options.early_stop>0 ) {
	  if( stop_countdown==1 ) {
	    cout << endl;
	    break;
	  }
	  stop_countdown--;
	}
      }
	
      // autosave the model every N iterations
      if( save_countdown==1 ) {
	printf("\tautosaving");

	char filename[1000];
	sprintf( filename, "%s.%d", options.model_file.c_str(), i+1);
	ofstream omfs(filename);
	Exception::Assert(omfs.good(),"Error while opening <%s> for writing",filename); 
	net.Write(omfs);
	omfs.close();
	save_countdown = options.autosave;
      }
      else
	save_countdown = (save_countdown==0) ?0 :save_countdown-1;
      cout << endl;    
    }    
        
    // Save the last model
    
    cout << "Saving the model" << endl;
    ofstream omfs(options.model_file.c_str());
    Exception::Assert(omfs.good(),"Error while opening <%s> for writing",options.model_file.c_str()); 
    net.Write(omfs);
    omfs.close();

    cout << "Minimum is " << best_validation_acc << " reached at " << best_epoch << endl;
  }
  catch(Exception *e) {
    e->PrintMessage();
    delete e;
    return -1;
  }
  return 0;       
}

#ifdef STANDALONE
int main(int argc, char* argv[])
{
  Options options(argc,argv); 
  return brnnTrain(options);
}
#endif
