#include "brnn-model.h"

class BRNNTestOptions
{
private:

  typedef unsigned int uint;
  static const option optionNames[];

public:

  ////////////////////////////
  // Command line variables //
  ////////////////////////////

  string targets_file;
  string inputs_file;
  string sizes_file;
  string model_file;
  string outputs_file;
  
  int output_mode;
  int accuracy_mode;
  bool predict_only;

  // Constructor
  BRNNTestOptions(int argc = 0, char *argv[]=NULL);
  BRNNTestOptions(const string& targets,
		  const string& inputs,
		  const string& sizes,
		  const string& model,
		  const string& outputs,
		  int out_mode = BiRecursiveNeuralNetwork::SOFTMAX,
		  int acc_mode = BiRecursiveNeuralNetwork::MULTICLASS,
		  bool pred_only = false) :
    targets_file(targets),
    inputs_file(inputs),  
    sizes_file(sizes),  
    model_file(model),
    outputs_file(outputs),    
    output_mode(out_mode),
    accuracy_mode(acc_mode),
    predict_only(pred_only) {}
    
  void usage(char* progname);
};

int brnnTest(const BRNNTestOptions& options, ostream& out=cout);
 
