#ifndef __ARRAY_H
#define __ARRAY_H
#include <iostream>
#include <vector>
#include "multi-array.h"

struct VECTOR
{
  // data

  int size;
  REAL *buffer;
  
  // construction
  
  VECTOR(int dim=0);
  VECTOR(const VECTOR &src); 
  VECTOR(REAL *src, int size);
  VECTOR(istream &is);
  ~VECTOR();

  // operators
  
  REAL& operator [] (int i) { return buffer[i]; }
  VECTOR& operator = (const VECTOR &src);
  int MaxInd();
  
  // modification

  void Destroy();
  void Resize(int dim);
  void Fill(REAL value);
  VECTOR& Copy(const VECTOR &src);
  VECTOR& Copy(REAL *buffer, int buf_size);
  
  // serialization

  void Read(istream &is);
  void ReadFixedSize(istream &is);
  void Write(ostream &os);
};


struct COLLECTION
{
  // data
  
  vector<VECTOR> buffer;

  COLLECTION() {}
  COLLECTION(istream &is) { Read(is); }

  // operators

  VECTOR& operator [] (int i) { return buffer[i]; }

  // modification
  
  void Clear() { buffer.clear(); }
  void Add(VECTOR V) { buffer.push_back(V); };
  int Rows() { return buffer.size(); }
  int Cols() { return (buffer.size()>0 ?buffer[0].size :0); }
  
  // serialization
  
  void Read(istream &is);
  void Write(ostream &os);   
};

struct COLLECTIONA
{
  private:

  // data  
  int no_rows;
  int no_cols;
  vector<ARRAY1> buffer;
  
  public:
  
  COLLECTIONA(int norows=0, int nocols=0) { 
    no_rows = no_cols = 0;
    Resize(norows,nocols); 
  }
  COLLECTIONA(istream &is) { Read(is); }
  ~COLLECTIONA() { Clear(); }

  // operators
  ARRAY1& operator [] (int i) { return buffer[i]; }

  // modification  
  void Clear();
  void PushBack(ARRAY1 toadd, int nocols) { 
    no_cols = nocols;
    no_rows++;
    buffer.push_back(toadd);
  }
  void Resize(int norows, int nocols) {
    no_cols = nocols;
    no_rows = norows;
    
    buffer.clear();
    for( int r=0; r<no_rows; r++ ) 
      buffer.push_back(CreateArray(no_cols));
  }

  int Rows() { return no_rows; }
  int Cols() { return no_cols; }
  
  // serialization

  void Read(istream &is);
  void Write(ostream &os);   
};

struct INDEX 
{
  INDEX(int s, int e, int dim) { start=s; end=e; size=dim; }
  
  int start;
  int end;
  int size;  
};

struct INDEXES
{
  // data

  int last_index;
  vector<INDEX> buffer;
  
  // initialization

  INDEXES() {}
  INDEXES(istream &is) { Read(is); }

  // operators

  INDEX& operator [] (int i) { return buffer[i]; }

  // modification
  
  void Clear() { buffer.clear(); }
  int Size() { return buffer.size(); }
  
  // serialization

  void Read(istream &is);
};


#endif
