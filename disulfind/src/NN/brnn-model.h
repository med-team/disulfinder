#ifndef __BRNN_MODEL_H
#define __BRNN_MODEL_H
#include "array.h"

class BiRecursiveNeuralNetwork 
{
 public:

  enum OUTPUT_MODE   { SOFTMAX=0, SIGMOID=1, LINEAR=2 };
  enum ERROR_MODE    { ENTROPY=0, MSE=1, LAUC=2};
  enum ACCURACY_MODE { MULTICLASS=0, RMSD=1, MULTILABEL=2, AUC=3, CE=4, OAUC=5};

 private:

  // switches
  
  bool swap_delta;
  int  global_mode;
  int  output_mode;
  int  error_mode;
  int  accuracy_mode;

  // dimensions

  int no_codes;          // if no_codes==no_inputs there is no coding layer
  int no_inputs;

  bool use_hidden_layer;     // if hidden_layer==false: no_hiddens=no_inputs and output is directly connected to input
  int no_hiddens;         

  bool two_layers;       // if two_layers==false: no_recursives=no_out_recursives
  int no_recursives;     // first recursive layer
  int no_out_recursives; // second recursive layer

  int no_outputs;
  int no_globals;
  int win_out;

  // input/code weights
  ARRAY2 Wci, DWci1, DWci2, DWci, oDWci; // input -> code
  ARRAY1 Wct, DWct1, DWct2, DWct, oDWct; // code threshold
  
  // code layer
  ARRAY2 C, Cdelta; // code layer

  // code/states weights
  ARRAY2 Whc, DWhc1, DWhc2, DWhc, oDWhc; // code -> hidden
  ARRAY1 Wht, DWht1, DWht2, DWht, oDWht; // hidden threshold

  ARRAY2 Wfc, DWfc1, DWfc2, DWfc, oDWfc; // code -> forward
  ARRAY2 Wff, DWff1, DWff2, DWff, oDWff; // forward -> forward
  ARRAY1 Wft, DWft1, DWft2, DWft, oDWft; // forward threshold

  ARRAY2 Wbc, DWbc1, DWbc2, DWbc, oDWbc; // code -> backward
  ARRAY2 Wbb, DWbb1, DWbb2, DWbb, oDWbb; // backward -> backward
  ARRAY1 Wbt, DWbt1, DWbt2, DWbt, oDWbt; // backward threshold

  // hidden layer
  ARRAY2 H, Hdelta; // state no-recursive
  ARRAY2 F, Fdelta; // state forward
  ARRAY2 B, Bdelta; // state backward

  // states / output states weights  
  ARRAY2 W2ff, DW2ff1, DW2ff2, DW2ff, oDW2ff; // forward -> output forward
  ARRAY1 W2ft, DW2ft1, DW2ft2, DW2ft, oDW2ft; // output forward threshold

  ARRAY2 W2bb, DW2bb1, DW2bb2, DW2bb, oDW2bb; // forward -> output forward
  ARRAY1 W2bt, DW2bt1, DW2bt2, DW2bt, oDW2bt; // output forward threshold
  
  // output hidden layer
  ARRAY2 F2, F2delta; // output state forward
  ARRAY2 B2, B2delta; // output state backward

  // hidden/output weights 
  ARRAY3 Woh, DWoh1, DWoh2, DWoh, oDWoh; // hidden -> output
  ARRAY3 Wof, DWof1, DWof2, DWof, oDWof; // forward -> output
  ARRAY3 Wob, DWob1, DWob2, DWob, oDWob; // backward -> output
  ARRAY1 Wot, DWot1, DWot2, DWot, oDWot; // output threshold
  
  // forward/global weights
  ARRAY2 Wgf, DWgf1, DWgf2, DWgf, oDWgf; // forward -> global
  ARRAY2 Wgb, DWgb1, DWgb2, DWgb, oDWgb; // backward -> global
  ARRAY1 Wgt, DWgt1, DWgt2, DWgt, oDWgt; // global threshold
  
  // output layer
  ARRAY2 Y, Ydelta; // net output
  ARRAY2 O, Odelta; // net output

  // global layer
  ARRAY1 G, Gdelta;
  ARRAY1 OG, OGdelta;

  // learning parameters

  REAL epsilon; // minimum allowable delta
  REAL lambda; // learning rate
  REAL mu;     // momentum term
  REAL ni;     // weight decay term 

 public:
  
  // initialization
  
  BiRecursiveNeuralNetwork( int noinputs=2, int nohiddens=2, 
			    int norecursives=1, int nooutrecursives=0, 
			    int nooutputs=1, int noglobals=1, int winout=0, 
			    int code=0, REAL init_weight=0.01 );
  BiRecursiveNeuralNetwork( istream &is );
  ~BiRecursiveNeuralNetwork();
  void Init( int globalmode,
	     int noinputs, int nohiddens, int norecursives, int nooutrecursives, 
	     int nooutputs, int winout, int code, REAL init_weight );
  
  // set network parameters
  void SetOutputMode(OUTPUT_MODE mode);
  void SetErrorMode(ERROR_MODE mode);
  void SetAccuracyMode(ACCURACY_MODE mode);
  void SetEpsilon( REAL eps) { epsilon = eps; }
  void SetLearningParameters( REAL learning_rate, REAL momentum, REAL weight_decay );  

  // informations

  int GetNoInputs()        { return no_inputs; }
  int GetNoCodes()         { return no_codes; }
  int GetNoHiddens()       { return no_hiddens; }
  int GetNoRecursives()    { return no_recursives; }
  int GetNoOutRecursives() { return no_out_recursives; }
  int GetNoOutputs()       { return (global_mode==0) ?no_outputs :no_globals; }
  REAL Norm();
  
  // operations
 
  void TrainOnLine(COLLECTIONA &targets, COLLECTIONA &inputs, INDEXES &patterns, vector<int> &sequence);
  void TrainBatch(COLLECTIONA &targets, COLLECTIONA &inputs, INDEXES &patterns);
  REAL Test(COLLECTIONA &targets, COLLECTIONA &inputs, COLLECTIONA &predictions, INDEXES &patterns);
  void Predict(COLLECTIONA &inputs, COLLECTIONA &predictions, INDEXES &patterns);

  REAL Accuracy(COLLECTIONA &targets, COLLECTIONA &predictions);

  // serialization

  istream& Read(istream &is);
  ostream& Write(ostream &os);

 private:

  // initialization

  void DestroyMatrixes();
  void DestroyTempMatrixes(int pattern_size);

  void SizeMatrixes();
  void SizeTempMatrixes(int pattern_size);

  void EmptyMatrixes(bool both);
  static REAL RandWeight(REAL init_weight);
  void RandomizeWeights(REAL init_weight);

  void SwapMatrixes();

  // operation functions
   
  void Forward(COLLECTIONA &I, INDEX &pattern);
  REAL CalculateError(int r, COLLECTIONA &targets, COLLECTIONA &predictions, int num);  
  void CalculateDelta(int r, COLLECTIONA &targets, COLLECTIONA &predictions, ARRAY1 &delta, int num);
  REAL Error(COLLECTIONA &targets, COLLECTIONA &predictions, INDEXES &patterns, int r);
  void ErrorDelta(COLLECTIONA &targets, COLLECTIONA &predictions, INDEXES &patterns, int r);
  void getPredictions(COLLECTIONA &predictions, INDEXES &patterns, int t);
  void Backward(COLLECTIONA &I, INDEX &pattern); 
  void Update();   // update weights  
};
   
#endif
