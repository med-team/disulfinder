#include <string.h>
#include <vector>
#include <iostream>
#include <sstream>
#include "array.h"
#include "../Common/Exception.h"

VECTOR::VECTOR(int dim) 
{
  if( dim > 0 ) {
    size = dim;
    buffer = new REAL[dim];
  }
  else {
    size = 0;
    buffer = NULL;
  }	
}


VECTOR::VECTOR(const VECTOR &src)
{
 if( src.size > 0 ) {
   size = src.size;
   buffer = new REAL[src.size];
   memcpy(buffer,src.buffer,sizeof(REAL)*src.size);
 }
 else {
   size = 0;
   buffer = NULL;
 }	
} 

VECTOR::VECTOR(REAL *src, int _size)
{
  if( _size>0 ) {
    size = _size;
    buffer = new REAL[_size];
    memcpy(buffer,src,sizeof(REAL)*_size);
  }
  else {
    size = 0;
    buffer = NULL;
  }	
}


VECTOR::VECTOR(istream &is) 
{
  size = 0;
  buffer = NULL;
  Read(is);
}


VECTOR::~VECTOR() 
{
  if( buffer!=NULL ) 
    delete []buffer;
}

int VECTOR::MaxInd() 
{
  if( size==0 )
    return -1;
  int maxind = 0;
  REAL maxval = buffer[0];
  for( int i=1; i<size; i++ ) {
    if( buffer[i]>maxval ) {
      maxval = buffer[i];
      maxind = i;
    }
  }
  return maxind;
}

void VECTOR::Destroy() 
{
  if( buffer!=NULL ) {
    delete []buffer;
    buffer = NULL;
    size = 0;
  }
}

void VECTOR::Resize(int dim) 
{
  if( dim!=size ) {
    if( size>0 )
      delete []buffer;
    if( dim>0 ) {
      buffer = new REAL[dim];
      size = dim;
    }
    else {
      size = 0;
      buffer = NULL;
    }	
  }	
}

void VECTOR::Fill(REAL value) 
{
  for( int i=0; i<size; i++ )
    buffer[i] = value;
}

VECTOR& VECTOR::operator = (const VECTOR &src) 
{
  if( size!=src.size ) {
    if( size>0 )
      delete []buffer;
    if( src.size>0 ) {
      size = src.size;
      buffer = new REAL[src.size];
    }
    else {
      size = 0;
      buffer = NULL;
    }	
  }
  if( size>0 )
    memcpy(buffer,src.buffer,sizeof(REAL)*src.size);
  return *this;
}


VECTOR& VECTOR::Copy(const VECTOR &src) 
{
  if( size!=src.size ) {
    if( size>0 )
      delete []buffer;
    if( src.size>0 ) {
      size = src.size;
      buffer = new REAL[src.size];
    }
    else {
      size = 0;
      buffer = NULL;
    }	
  }
  if( size>0 )
    memcpy(buffer,src.buffer,sizeof(REAL)*src.size);
  return *this;
}


VECTOR& VECTOR::Copy(REAL *src_buffer, int buf_size)
{
  if( size!=buf_size ) {
    if( size>0 )
      delete []buffer;
    if( buf_size>0 ) {
      size = buf_size;
      buffer = new REAL[buf_size];
    }
    else {
      size = 0;
      buffer = NULL;
    }	
  }
  if( size>0 )
    memcpy(buffer,src_buffer,sizeof(REAL)*buf_size);
  return *this;
}

void VECTOR::Read(istream &is) 
{  
  REAL dval;
  vector<REAL> temp;
  while(is >> dval )
    temp.push_back(dval);

  Resize(temp.size());
  for( int i=0; i<size; i++ ) 
    buffer[i] = temp[i]; 
}

void VECTOR::ReadFixedSize(istream &is) 
{  
  for( int i=0; i<size; i++ ) 
    Exception::Assert(is>>buffer[i], "Unexpected EOF while reading vector"); 
}


void VECTOR::Write(ostream &os) 
{
  for( int i=0; i<size; i++ )
    os << buffer[i] << " ";
  os << endl;
}


void COLLECTION::Read(istream &is)
{
  Clear();

  REAL dummy;
  int ncols = 0;
  string line;
  VECTOR vtemp;
  while( getline(is,line) ) {   
    istringstream iss(line);
    if( ncols==0 ) {
      vtemp.Read(iss);
      ncols = vtemp.size;
    }
    else {
      vtemp.ReadFixedSize(iss);
      Exception::Assert(!(iss>>dummy), "Inconsistent number of columns");
    }
    buffer.push_back(vtemp);
  }
}



void COLLECTION::Write(ostream &os) 
{
  for( size_t r=0; r<buffer.size(); r++ )
    buffer[r].Write(os);  
}



void COLLECTIONA::Clear() 
{
  for( int r=0; r<no_rows; r++ )
    DestroyArray(buffer[r],no_cols);
  no_rows = no_cols = 0;
  buffer.clear();
}

void COLLECTIONA::Read(istream &is) 
{
  Clear();

  string line;
  while( getline(is,line) ) {
    ARRAY1 toadd = NULL;
    istringstream iss(line);
    if( no_cols==0 ) {
      REAL dval;
      vector<REAL> temp;
      while(iss >> dval ) 
	temp.push_back(dval);      
      no_cols = temp.size();
      
      toadd = CreateArray(no_cols);
      for( int i=0; i<no_cols; i++ ) 
	toadd[i] = temp[i]; 
    }
    else {
      toadd = CreateArray(no_cols);
      ReadArray(iss,toadd,no_cols);
    }
    buffer.push_back(toadd);
    no_rows++;
  }  
}

void COLLECTIONA::Write(ostream &os) 
{
  for( int r=0; r<no_rows; r++ ) {
    WriteArray(os,buffer[r],no_cols);
    os << endl;
  }  
}


void INDEXES::Read(istream &is) 
{
  Clear();
  
  int start = 0;
  int dummy;
  while( is >> dummy ) {
    Exception::Assert(dummy>0, "Inconsistent value <%d> of size", dummy );    
    buffer.push_back(INDEX(start,start+dummy-1,dummy));
    start += dummy;
  }  

  last_index = start;
}
