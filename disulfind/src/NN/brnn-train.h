

class Options
{
private:

  typedef unsigned int uint;
  static const option optionNames[];

public:

  ////////////////////////////
  // Command line variables //
  ////////////////////////////
  
  string targets_file;
  string inputs_file;
  string sizes_file;
  string model_file;  
  string validation_targets_file;
  string validation_inputs_file;
  string validation_sizes_file;

  bool global_mode;

  int output_mode;
  int error_mode;
  int accuracy_mode;

  int dim_hidden_layer;    
  int dim_recursive_layer;
  int dim_out_recursive_layer;
  int dim_win_out;
  int dim_code;
  REAL init_weight;
  int init_seed;

  bool batch;
  REAL learning_rate;
  REAL momentum;
  REAL weight_decay;
  bool shuffle;

  int max_epoch;
  int early_stop;
  int autosave;
  int savebest;
  string load_net;
  
  // Constructor
  Options(int argc = 0, char *argv[]=NULL);
  void usage(char* progname);
};


const option Options::optionNames[] =
{ 
  { "global_mode",   required_argument, 0, 'g'},

  { "output_mode",   required_argument, 0, 'o'}, 
  { "error_mode",    required_argument, 0, 'r'}, 
  { "accuracy_mode", required_argument, 0, 'y'}, 

  { "hidden_layer",     required_argument, 0, 'h'},
  { "recursive_layer",  required_argument, 0, 'x'},
  { "out_rec_layer",    required_argument, 0, 'X'},
  { "win_out",          required_argument, 0, 'k'},
  { "code",             required_argument, 0, 'c'},
  { "init_weight",      required_argument, 0, 'q'},
  { "init_seed",        required_argument, 0, 's'},

  { "batch",            required_argument, 0, 't'},
  { "learning_rate",    required_argument, 0, 'l'},
  { "momentum",         required_argument, 0, 'm'},
  { "weight_decay",     required_argument, 0, 'n'},
  { "shuffle",          no_argument,       0, 'f'},

  { "max_epoch",        required_argument, 0, 'i'},
  { "early_stop",       required_argument, 0, 'e'},
  { "autosave",         required_argument, 0, 'a'},
  { "savebest",         no_argument,       0, 'b'},
  { "load",             required_argument, 0, 'L'},

  { "help",             no_argument,       0, '?'},
  { 0,                  0,                 0, 0  }
};
  
