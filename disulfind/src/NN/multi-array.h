#ifndef __MULTI_ARRAY_H
#define __MULTI_ARRAY_H
#include <iostream>
#include "../Common/myreal.h"

using namespace std;

//typedef float REAL;
//#define REAL_SIZE 4
//#define REAL_LOG_SIZE 2

typedef REAL*   ARRAY1;
typedef ARRAY1* ARRAY2;
typedef ARRAY2* ARRAY3;
typedef ARRAY3* ARRAY4;

typedef ARRAY1*   DATA;


ARRAY1 CreateArray(int n1);
ARRAY2 CreateArray(int n1, int n2);
ARRAY3 CreateArray(int n1, int n2, int n3);
ARRAY4 CreateArray(int n1, int n2, int n3, int n4);

void FillArray(ARRAY1 &array, REAL value, int n1);
void FillArray(ARRAY2 &array, REAL value, int n1, int n2);
void FillArray(ARRAY3 &array, REAL value, int n1, int n2, int n3);
void FillArray(ARRAY4 &array, REAL value, int n1, int n2, int n3, int n4);

void EmptyArray(ARRAY1 &array, int n1);
void EmptyArray(ARRAY2 &array, int n1, int n2);
void EmptyArray(ARRAY3 &array, int n1, int n2, int n3);
void EmptyArray(ARRAY4 &array, int n1, int n2, int n3, int n4);

void CopyArray(ARRAY1 &dst, ARRAY1 &src, int n1);
void CopyArray(ARRAY2 &dst, ARRAY2 &src, int n1, int n2);
void CopyArray(ARRAY3 &dst, ARRAY3 &src, int n1, int n2, int n3);
void CopyArray(ARRAY4 &dst, ARRAY4 &src, int n1, int n2, int n3, int n4);

ARRAY1 CloneArray(ARRAY1 &src, int n1);
ARRAY2 CloneArray(ARRAY2 &src, int n1, int n2);
ARRAY3 CloneArray(ARRAY3 &src, int n1, int n2, int n3);
ARRAY4 CloneArray(ARRAY4 &src, int n1, int n2, int n3, int n4);

int MaxInd(ARRAY1 &array, int n1);

REAL norm1(ARRAY1 &array, int n1);
REAL norm1(ARRAY2 &array, int n1, int n2);
REAL norm1(ARRAY3 &array, int n1, int n2, int n3);
REAL norm1(ARRAY4 &array, int n1, int n2, int n3, int n4);

void DestroyArray(ARRAY1 &array, int n1);
void DestroyArray(ARRAY2 &array, int n1, int n2);
void DestroyArray(ARRAY3 &array, int n1, int n2, int n3);
void DestroyArray(ARRAY4 &array, int n1, int n2, int n3, int n4);

istream& ReadArray(istream &is, ARRAY1 &array, int n1);
istream& ReadArray(istream &is, ARRAY2 &array, int n1, int n2);
istream& ReadArray(istream &is, ARRAY3 &array, int n1, int n2, int n3);
istream& ReadArray(istream &is, ARRAY4 &array, int n1, int n2, int n3, int n4);

ostream& WriteArray(ostream &os, ARRAY1 &array, int n1);
ostream& WriteArray(ostream &os, ARRAY2 &array, int n1, int n2);
ostream& WriteArray(ostream &os, ARRAY3 &array, int n1, int n2, int n3);
ostream& WriteArray(ostream &os, ARRAY4 &array, int n1, int n2, int n3, int n4);

DATA CreateDataMatrix(int rows, int cols);
void DestroyDataMatrix(DATA &matrix, int rows, int cols);
ARRAY1 ReadArray(istream &is, int &size);
DATA ReadDataMatrix(istream &is, int &rows, int &cols);
ostream& WriteDataMatrix(ostream &os, DATA &matrix, int rows, int cols);

#endif
