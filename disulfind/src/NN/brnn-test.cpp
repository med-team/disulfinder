#include <stdlib.h>
#include <getopt.h>
#include <math.h>
#include <fstream>
#include "../Common/Accuracy.h"
#include "../Common/Exception.h"
#include "../Common/ConfusionMatrix.h"
#include "brnn-test.h"

const option BRNNTestOptions::optionNames[] =
{   
  { "output_mode",   required_argument, 0, 'o'}, 
  { "accuracy_mode", required_argument, 0, 'y'}, 
  { "predict_only",  no_argument,       0, 'p'},
  
  { "help",          no_argument,       0, '?'},
  { 0,               0,                 0, 0  }
};

BRNNTestOptions::BRNNTestOptions(int argc, char *argv[]) :
  targets_file(""),
  inputs_file(""),  
  sizes_file(""),  
  model_file(""),
  outputs_file(""),

  output_mode(BiRecursiveNeuralNetwork::SOFTMAX),
  accuracy_mode(BiRecursiveNeuralNetwork::MULTICLASS),
  predict_only(false)

{
  int c;
  int* opti = 0;
  char *progname = argv[0];
  
  // you must put ':' only after parameters that require an argument
  // so after 'a' and '?' we don't put ':'

  while ((c = getopt_long(argc, argv, "o:y:p?", optionNames, opti)) != -1) {
    switch(c) {
    
    case 'o':
      output_mode = atoi(optarg);
      break;
    case 'y':
      accuracy_mode = atoi(optarg);
      break;
    case 'p':
      predict_only = true;
      break;

    case '?':
    default:
      usage(progname);      
    }
  }

  if( optind < argc-5) {
    cerr << "Invalid parameters" << endl;
    usage(progname);
  }
  else if( optind > argc-5) {
    cerr << "Missing parameters" << endl;
    usage(progname);
  }
  else {
    targets_file = argv[optind];
    inputs_file = argv[optind+1];
    sizes_file = argv[optind+2];    
    model_file = argv[optind+3];    
    outputs_file = argv[optind+4];
  }  
}

void BRNNTestOptions::usage(char* progname) 
{
  cerr << "usage: " << progname << " [options] <target> <input> <sizes> <model> <output>" << endl;
  cerr << "target: file with targets (REQUIRED)" << endl;
  cerr << "input: file with inputs (REQUIRED)" << endl;
  cerr << "sizes: file with sequences size (REQUIRED)" << endl;
  cerr << "model: file with the model (REQUIRED)" << endl;
  cerr << "output: file to save predictions (REQUIRED)" << endl;

  cerr << endl;

  cerr << "Options:" << endl;
  cerr << "\t-o, --output_mode=value: set the kind of output function:" << endl; 
  cerr << "\t\t0: softmax (default)" << endl;
  cerr << "\t\t1: sigmoid" << endl;
  cerr << "\t\t2: linear" << endl;
  cerr << "\t-y, --accuracy_mode=value: set the kind of accuracy value:" << endl; 
  cerr << "\t\t0: multiclass (default)" << endl;
  cerr << "\t\t1: RMSD" << endl;
  cerr << "\t\t2: multilabel" << endl;
  cerr << "\t\t3: area under ROC curve" << endl;
  cerr << "\t-p, --predict_only: do not compute accuracy" << endl;

  cerr << endl;

  cerr << "\t-?, --help: this message" << endl;
  exit(0);
}

void ReadCollection(COLLECTIONA &collection, const string &filename) {
  ifstream ifs(filename.c_str());
  Exception::Assert(ifs.good(),"Error while opening <%s> for reading",filename.c_str()); 
  collection.Read(ifs);
  ifs.close();
}

void ReadIndexes(INDEXES &indexes, const string &filename) {
  ifstream ifs(filename.c_str());
  Exception::Assert(ifs.good(),"Error while opening <%s> for reading",filename.c_str()); 
  indexes.Read(ifs);
  ifs.close();
}

int brnnTest(const BRNNTestOptions& options, ostream& out)
{
  try {
    out << "Reading model" << endl;
    ifstream imfs(options.model_file.c_str());
    Exception::Assert(imfs.good(),"Error while opening <%s> for reading",options.model_file.c_str()); 
    BiRecursiveNeuralNetwork net(imfs);
    net.SetOutputMode((BiRecursiveNeuralNetwork::OUTPUT_MODE)options.output_mode);
    net.SetAccuracyMode((BiRecursiveNeuralNetwork::ACCURACY_MODE)options.accuracy_mode);
    imfs.close();

    //------------------
    // Reading test set
    
    COLLECTIONA targets;
    COLLECTIONA inputs;
    INDEXES     sizes;    

    // read targets
    out << "Reading test-set" << endl;    
    ReadCollection(targets,options.targets_file);
    Exception::Assert(targets.Cols()==net.GetNoOutputs(), 
		      "Invalid number of columns (%d!=%d) in targets file",targets.Cols(),net.GetNoOutputs());

    // read inputs
    ReadCollection(inputs,options.inputs_file);
    Exception::Assert(inputs.Cols()==net.GetNoInputs(), 
		      "Invalid number of columns (%d!=%d) in inputs file",inputs.Cols(),net.GetNoInputs());

    // read sizes
    ReadIndexes(sizes,options.sizes_file);

    out << "Read " << inputs.Rows() << " patterns, " << sizes.Size() << " sequences" << endl;
    out << endl;
    
    //------------------
    // Test

    out << "Testing..." << endl;
    COLLECTIONA predictions;
    net.Predict(inputs,predictions,sizes);

    // Compute performances
    if( !options.predict_only) {
      out << endl;
      if( options.accuracy_mode==BiRecursiveNeuralNetwork::MULTICLASS ) {
	ConfusionMatrix conf(net.GetNoOutputs());
	for( int r=0; r<targets.Rows(); r++ ) {
	  int pred_class = MaxInd(predictions[r],net.GetNoOutputs());
	  int target_class = MaxInd(targets[r],net.GetNoOutputs());
	  conf.Add(pred_class,target_class);
	}      
	conf.Output(out);  
      } 
      else if( options.accuracy_mode==BiRecursiveNeuralNetwork::MULTILABEL ) {
	  for( int o=0; o<net.GetNoOutputs(); o++ ) {
	    Accuracy acc;
	    for( int r=0; r<targets.Rows(); r++ ) {
	      int pred = ((predictions[r][o]>0.5) ?1 :-1);
	      int target = ((targets[r][o]>0.5) ?1 :-1);
	      acc.Add(target,pred);
	    }	
	    out << "Class " << o << endl;
	    acc.Output(out);
	    out << endl;
	  }      
      }
      else {
	out << "Accuracy: " << net.Accuracy(targets,predictions) << endl;;
      }	
    }
    // write predictions    
    ofstream ofs(options.outputs_file.c_str());
    Exception::Assert(ofs.good(),"Error while opening <%s> for writing",options.outputs_file.c_str()); 
    predictions.Write(ofs);
    ofs.close();

  }
  catch(Exception *e) {
    e->PrintMessage();
    delete e;
    return -1;
  }
  return 0;       
}

#ifdef STANDALONE
int main(int argc, char* argv[])
{
  BRNNTestOptions options(argc,argv); 
  return brnnTest(options);
}
#endif



