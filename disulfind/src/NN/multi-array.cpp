#include <string.h>
#include <vector>
#include <sstream>
#include "../Common/Exception.h"
#include "multi-array.h"
#include <math.h>


ARRAY1 CreateArray(int n1) 
{
  return new REAL[n1];
}

ARRAY2 CreateArray(int n1, int n2) 
{
  ARRAY2 array = new ARRAY1[n1];
  for( int i=0; i<n1; i++ )
    array[i] = CreateArray(n2);
  return array;
}

ARRAY3 CreateArray(int n1, int n2, int n3)
{
  ARRAY3 array = new ARRAY2[n1];
  for( int i=0; i<n1; i++ )
    array[i] = CreateArray(n2,n3);
  return array;
}


ARRAY4 CreateArray(int n1, int n2, int n3, int n4)
{
  ARRAY4 array = new ARRAY3[n1];
  for( int i=0; i<n1; i++ )
    array[i] = CreateArray(n2,n3,n4);
  return array;
}


void FillArray(ARRAY1 &array, REAL value, int n1) 
{
  for( int i=0; i<n1; i++ )
    array[i] = value;
}

void FillArray(ARRAY2 &array, REAL value, int n1, int n2)
{
  for( int i=0; i<n1; i++ )
    FillArray(array[i],value,n2);
}

void FillArray(ARRAY3 &array, REAL value, int n1, int n2, int n3)
{
  for( int i=0; i<n1; i++ )
    FillArray(array[i],value,n2,n3);
}

void FillArray(ARRAY4 &array, REAL value, int n1, int n2, int n3, int n4)
{
  for( int i=0; i<n1; i++ )
    FillArray(array[i],value,n2,n3,n4);
}

void EmptyArray(ARRAY1 &array, int n1)
{
  bzero(array,n1<<REAL_LOG_SIZE);
}

void EmptyArray(ARRAY2 &array, int n1, int n2)
{
  for( int i=0; i<n1; i++ )
    EmptyArray(array[i],n2);
}

void EmptyArray(ARRAY3 &array, int n1, int n2, int n3)
{
  for( int i=0; i<n1; i++ )
    EmptyArray(array[i],n2,n3);
}

void EmptyArray(ARRAY4 &array, int n1, int n2, int n3, int n4)
{
  for( int i=0; i<n1; i++ )
    EmptyArray(array[i],n2,n3,n4);
}

void CopyArray(ARRAY1 &dst, ARRAY1 &src, int n1)
{
  memcpy(dst,src,n1<<REAL_LOG_SIZE);
}

void CopyArray(ARRAY2 &dst, ARRAY2 &src, int n1, int n2)
{
  for( int i=0; i<n1; i++ )
    CopyArray(dst[i],src[i],n2);
}

void CopyArray(ARRAY3 &dst, ARRAY3 &src, int n1, int n2, int n3)
{
  for( int i=0; i<n1; i++ )
    CopyArray(dst[i],src[i],n2,n3);
}

void CopyArray(ARRAY4 &dst, ARRAY4 &src, int n1, int n2, int n3, int n4)
{
  for( int i=0; i<n1; i++ )
    CopyArray(dst[i],src[i],n2,n3,n4);
}

ARRAY1 CloneArray(ARRAY1 &src, int n1) 
{
  ARRAY1 dst = CreateArray(n1);
  CopyArray(dst,src,n1);
  return dst;
}

ARRAY2 CloneArray(ARRAY2 &src, int n1, int n2)
{
  ARRAY2 dst = CreateArray(n1,n2);
  CopyArray(dst,src,n1,n2);
  return dst;
}

ARRAY3 CloneArray(ARRAY3 &src, int n1, int n2, int n3)
{
  ARRAY3 dst = CreateArray(n1,n2,n3);
  CopyArray(dst,src,n1,n2,n3);
  return dst;
}

ARRAY4 CloneArray(ARRAY4 &src, int n1, int n2, int n3, int n4)
{
  ARRAY4 dst = CreateArray(n1,n2,n3,n4);
  CopyArray(dst,src,n1,n2,n3,n4);
  return dst;
}


int MaxInd(ARRAY1 &array, int n1) 
{
  if( n1==0 )
    return -1;

  int maxind = 0;
  REAL maxval = array[0];
  for( int i=1; i<n1; i++ ) {
    if( array[i]>maxval ) {
      maxval = array[i];
      maxind = i;
    }      
  }
  return maxind;
}

REAL norm1(ARRAY1 &array, int n1)
{
  REAL value = 0.;
  for( int i=0; i<n1; i++ )
    value += fabsf(array[i]);
  return value;
}

REAL norm1(ARRAY2 &array, int n1, int n2)
{
  REAL value = 0.;
  for( int i=0; i<n1; i++ )
    value += norm1(array[i],n2);
  return value;
}

REAL norm1(ARRAY3 &array, int n1, int n2, int n3)
{
  REAL value = 0.;
  for( int i=0; i<n1; i++ )
    value += norm1(array[i],n2,n3);
  return value;

}

REAL norm1(ARRAY4 &array, int n1, int n2, int n3, int n4)
{
  REAL value = 0.;
  for( int i=0; i<n1; i++ )
    value += norm1(array[i],n2,n3,n4);
  return value;
}


void DestroyArray(ARRAY1 &array, int n1) 
{
  delete []array;
}

void DestroyArray(ARRAY2 &array, int n1, int n2) 
{
  for( int i=0; i<n1; i++ )
    DestroyArray(array[i],n2);
  delete []array;
}

void DestroyArray(ARRAY3 &array, int n1, int n2, int n3)
{
  for( int i=0; i<n1; i++ )
    DestroyArray(array[i],n2,n3);
  delete []array;
}

void DestroyArray(ARRAY4 &array, int n1, int n2, int n3, int n4)
{
  for( int i=0; i<n1; i++ )
    DestroyArray(array[i],n2,n3,n4);
  delete []array;
}

istream& ReadArray(istream &is, ARRAY1 &array, int n1) 
{
  for( int i=0; i<n1; i++ ) 
    Exception::Assert((is >> array[i]), "Unexpected EOF while reading array");
  return is;
}

istream& ReadArray(istream &is, ARRAY2 &array, int n1, int n2) 
{
  for( int i=0; i<n1; i++ ) 
    ReadArray(is,array[i],n2);
  return is;
}

istream& ReadArray(istream &is, ARRAY3 &array, int n1, int n2, int n3) 
{
  for( int i=0; i<n1; i++ ) 
    ReadArray(is,array[i],n2,n3);
  return is;
}

istream& ReadArray(istream &is, ARRAY4 &array, int n1, int n2, int n3, int n4) 
{
  for( int i=0; i<n1; i++ ) 
    ReadArray(is,array[i],n2,n3,n4);
  return is;
}

ostream& WriteArray(ostream &os, ARRAY1 &array, int n1) 
{
  for( int i=0; i<n1; i++ )
    os << array[i] << " ";
  return os;
}

ostream& WriteArray(ostream &os, ARRAY2 &array, int n1, int n2) 
{
  for( int i=0; i<n1; i++ )
    WriteArray(os,array[i],n2);
  return os;
}

ostream& WriteArray(ostream &os, ARRAY3 &array, int n1, int n2, int n3)
{
  for( int i=0; i<n1; i++ )
    WriteArray(os,array[i],n2,n3);
  return os;
}

ostream& WriteArray(ostream &os, ARRAY4 &array, int n1, int n2, int n3, int n4)
{
  for( int i=0; i<n1; i++ )
    WriteArray(os,array[i],n2,n3,n4);
  return os;
}


DATA CreateDataMatrix(int rows, int cols)
{
  return CreateArray(rows,cols);
}

void DestroyDataMatrix(DATA &matrix, int rows, int cols)
{
  DestroyArray(matrix,rows,cols);
}


ARRAY1 ReadArray(istream &is, int &size) 
{  
  // read from stream
  REAL dval;
  vector<REAL> buffer;
  for(;;) {
    is >> dval;
    if( !is.good() )
      break;
    buffer.push_back(dval);
  }

  // create array
  size = buffer.size();
  ARRAY1 array = CreateArray(size);
  for( int i=0; i<size; i++ ) 
    array[i] = buffer[i]; 

  return array;
}

DATA ReadDataMatrix(istream &is, int &rows, int &cols)
{
  // read from stream
  cols = 0;
  string line;
  ARRAY1 vtemp;
  vector<ARRAY1> buffer;
  for(;;) {
    getline(is,line);
    if( !is.good() )
      break;

    istringstream iss(line);
    if( cols==0 ) 
      vtemp = ReadArray(iss,cols);      
    else {
      vtemp = CreateArray(cols);
      ReadArray(iss,vtemp,cols);
      // test for end of line
      REAL dummy; iss>>dummy; Exception::Assert(!iss.good(), "Inconsistent number of columns");
    }
    buffer.push_back(vtemp);
  }

  //create array
  rows = buffer.size();
  DATA matrix = new ARRAY1[rows];
  for( int i=0; i<rows; i++ )
    matrix[i] = buffer[i];
  return matrix;    
}

ostream& WriteDataMatrix(ostream &os, DATA &matrix, int rows, int cols)
{
  for( int i=0; i<rows; i++ ) {
    WriteArray(os,matrix[i],cols); 
    os << endl;
  }
  return os;
}
