#include <math.h>
#include <stdlib.h>
#include <map>
#include "../Common/Exception.h"
#include "brnn-model.h"

static bool output_forward;

//activation functions
#define SIGMOIDF(in) (1./(1.+exp(-(in))))
#define SIGMOIDB(out) ((out)*(1.-(out)))

#define TANHF(in)    (tanh(in))
#define TANHB(out)   (1.-(out)*(out))

#define USESIGMOID 0

#if USESIGMOID
#define PROPAGATEF(in)  SIGMOIDF(in)
#define PROPAGATEB(in)  SIGMOIDB(in)
#define ZEROPOINT       0.5
#else
#define PROPAGATEF(in)  TANHF(in)
#define PROPAGATEB(out) TANHB(out)
#define ZEROPOINT       0.
#endif

//----------------------------------------------------------------------------------------------------
//
//  NEURAL NETWORK CLASS
//
//----------------------------------------------------------------------------------------------------

BiRecursiveNeuralNetwork::BiRecursiveNeuralNetwork( int globalmode, int noinputs, int nohiddens, int norecursives, int nooutrecursives, int nooutputs, int winout, int code, REAL init_weight )
{
  output_forward=1;
  
  lambda = 1e-2;
  mu = 0.;
  ni = 0.;  

  output_mode = SOFTMAX;
  error_mode  = ENTROPY;
  accuracy_mode = CE;

  Init(globalmode,noinputs,nohiddens,norecursives,nooutrecursives,nooutputs,winout,code,init_weight);
}


BiRecursiveNeuralNetwork::BiRecursiveNeuralNetwork( istream &is )
{
  lambda = 1e-2;
  mu = 0.;
  ni = 0.;

  output_mode = SOFTMAX;
  error_mode  = ENTROPY;
  accuracy_mode = CE;

  Read(is);
}


BiRecursiveNeuralNetwork::~BiRecursiveNeuralNetwork()
{
  DestroyMatrixes();
}


void BiRecursiveNeuralNetwork::Init( int globalmode, int noinputs, int nohiddens, int norecursives, int nooutrecursives, int nooutputs, int winout, int code, REAL init_weight ) 
{
  global_mode = globalmode;

  no_inputs = noinputs;
  no_codes = (code>0) ?code :no_inputs;

  use_hidden_layer = (nohiddens>0);
  no_hiddens = (nohiddens>0) ?nohiddens :noinputs;  

  no_recursives = norecursives;
  no_out_recursives = (nooutrecursives>0) ?nooutrecursives :norecursives;
  two_layers = (nooutrecursives>0);

  if( global_mode==0 ) {
    no_outputs = nooutputs;  
    no_globals = 0;
  }
  else {
    no_outputs = 0;
    no_globals = nooutputs;
  }
  win_out = winout;

  // initialize matrixes
  
  SizeMatrixes();
  EmptyMatrixes(true);
  RandomizeWeights(init_weight);    
}

void BiRecursiveNeuralNetwork::SizeMatrixes()
{
  if( no_codes!=no_inputs ) {
     Wci  = CreateArray(no_codes,no_inputs);
    DWci1 = CreateArray(no_codes,no_inputs);
    DWci2 = CreateArray(no_codes,no_inputs);     
     Wct  = CreateArray(no_codes);
    DWct1 = CreateArray(no_codes);
    DWct2 = CreateArray(no_codes);
  }	

  if( use_hidden_layer ) {
    Whc  = CreateArray(no_hiddens,no_codes);
    DWhc1 = CreateArray(no_hiddens,no_codes);
    DWhc2 = CreateArray(no_hiddens,no_codes);
    Wht  = CreateArray(no_hiddens);
    DWht1 = CreateArray(no_hiddens);
    DWht2 = CreateArray(no_hiddens);
  }	

   Wfc  = CreateArray(no_recursives,no_codes);
  DWfc1 = CreateArray(no_recursives,no_codes);
  DWfc2 = CreateArray(no_recursives,no_codes);
   Wff  = CreateArray(no_recursives,no_out_recursives);
  DWff1 = CreateArray(no_recursives,no_out_recursives);
  DWff2 = CreateArray(no_recursives,no_out_recursives);
   Wft  = CreateArray(no_recursives);
  DWft1 = CreateArray(no_recursives);
  DWft2 = CreateArray(no_recursives);

   Wbc  = CreateArray(no_recursives,no_codes);
  DWbc1 = CreateArray(no_recursives,no_codes);
  DWbc2 = CreateArray(no_recursives,no_codes);
   Wbb  = CreateArray(no_recursives,no_out_recursives);
  DWbb1 = CreateArray(no_recursives,no_out_recursives);
  DWbb2 = CreateArray(no_recursives,no_out_recursives);
   Wbt  = CreateArray(no_recursives);
  DWbt1 = CreateArray(no_recursives);
  DWbt2 = CreateArray(no_recursives);

  if( two_layers ) {
     W2ff  = CreateArray(no_out_recursives,no_recursives);
    DW2ff1 = CreateArray(no_out_recursives,no_recursives);
    DW2ff2 = CreateArray(no_out_recursives,no_recursives);
     W2ft  = CreateArray(no_out_recursives);
    DW2ft1 = CreateArray(no_out_recursives);
    DW2ft2 = CreateArray(no_out_recursives);

     W2bb  = CreateArray(no_out_recursives,no_recursives);
    DW2bb1 = CreateArray(no_out_recursives,no_recursives);
    DW2bb2 = CreateArray(no_out_recursives,no_recursives);
     W2bt  = CreateArray(no_out_recursives);
    DW2bt1 = CreateArray(no_out_recursives);
    DW2bt2 = CreateArray(no_out_recursives);    
  }	

  if( no_outputs>0 ) {
     Woh  = CreateArray(no_outputs,2*win_out+1,no_hiddens);
    DWoh1 = CreateArray(no_outputs,2*win_out+1,no_hiddens);
    DWoh2 = CreateArray(no_outputs,2*win_out+1,no_hiddens);
     Wof  = CreateArray(no_outputs,2*win_out+1,no_out_recursives);
    DWof1 = CreateArray(no_outputs,2*win_out+1,no_out_recursives);
    DWof2 = CreateArray(no_outputs,2*win_out+1,no_out_recursives);
     Wob  = CreateArray(no_outputs,2*win_out+1,no_out_recursives);
    DWob1 = CreateArray(no_outputs,2*win_out+1,no_out_recursives);
    DWob2 = CreateArray(no_outputs,2*win_out+1,no_out_recursives);
     Wot  = CreateArray(no_outputs);
    DWot1 = CreateArray(no_outputs);
    DWot2 = CreateArray(no_outputs);
  }
   
  if( no_globals>0 ) {
     Wgf  = CreateArray(no_globals,no_out_recursives);
    DWgf1 = CreateArray(no_globals,no_out_recursives);
    DWgf2 = CreateArray(no_globals,no_out_recursives);
     Wgb  = CreateArray(no_globals,no_out_recursives);
    DWgb1 = CreateArray(no_globals,no_out_recursives);
    DWgb2 = CreateArray(no_globals,no_out_recursives);
     Wgt  = CreateArray(no_globals);
    DWgt1 = CreateArray(no_globals);
    DWgt2 = CreateArray(no_globals);  
  }
  
  swap_delta = true;
  SwapMatrixes();
}


void BiRecursiveNeuralNetwork::DestroyMatrixes()
{
  if( no_codes!=no_inputs ) {
    DestroyArray( Wci, no_codes,no_inputs);
    DestroyArray(DWci1,no_codes,no_inputs);
    DestroyArray(DWci2,no_codes,no_inputs);   
    DestroyArray( Wct, no_codes);
    DestroyArray(DWct1,no_codes);
    DestroyArray(DWct2,no_codes);
  }

  if( use_hidden_layer ) {
    DestroyArray( Whc, no_hiddens,no_codes);
    DestroyArray(DWhc1,no_hiddens,no_codes);
    DestroyArray(DWhc2,no_hiddens,no_codes);
    DestroyArray( Wht, no_hiddens);
    DestroyArray(DWht1,no_hiddens);
    DestroyArray(DWht2,no_hiddens);
  }

  DestroyArray( Wfc, no_recursives,no_codes);
  DestroyArray(DWfc1,no_recursives,no_codes);
  DestroyArray(DWfc2,no_recursives,no_codes);
  DestroyArray( Wff, no_recursives,no_out_recursives);
  DestroyArray(DWff1,no_recursives,no_out_recursives);
  DestroyArray(DWff2,no_recursives,no_out_recursives);
  DestroyArray( Wft ,no_recursives);
  DestroyArray(DWft1,no_recursives);
  DestroyArray(DWft2,no_recursives);

  DestroyArray( Wbc ,no_recursives,no_codes);
  DestroyArray(DWbc1,no_recursives,no_codes);
  DestroyArray(DWbc2,no_recursives,no_codes);
  DestroyArray( Wbb ,no_recursives,no_out_recursives);
  DestroyArray(DWbb1,no_recursives,no_out_recursives);
  DestroyArray(DWbb2,no_recursives,no_out_recursives);
  DestroyArray( Wbt ,no_recursives);
  DestroyArray(DWbt1,no_recursives);
  DestroyArray(DWbt2,no_recursives);

  if( two_layers ) {
    DestroyArray( W2ff, no_out_recursives,no_recursives);
    DestroyArray(DW2ff1,no_out_recursives,no_recursives);
    DestroyArray(DW2ff2,no_out_recursives,no_recursives);
    DestroyArray( W2ft ,no_out_recursives);
    DestroyArray(DW2ft1,no_out_recursives);
    DestroyArray(DW2ft2,no_out_recursives);

    DestroyArray( W2bb ,no_out_recursives,no_recursives);
    DestroyArray(DW2bb1,no_out_recursives,no_recursives);
    DestroyArray(DW2bb2,no_out_recursives,no_recursives);
    DestroyArray( W2bt ,no_out_recursives);
    DestroyArray(DW2bt1,no_out_recursives);
    DestroyArray(DW2bt2,no_out_recursives);
  }

  if( no_outputs>0 ) {
    DestroyArray( Woh ,no_outputs,2*win_out+1,no_hiddens);
    DestroyArray(DWoh1,no_outputs,2*win_out+1,no_hiddens);
    DestroyArray(DWoh2,no_outputs,2*win_out+1,no_hiddens);
    DestroyArray( Wof ,no_outputs,2*win_out+1,no_out_recursives);
    DestroyArray(DWof1,no_outputs,2*win_out+1,no_out_recursives);
    DestroyArray(DWof2,no_outputs,2*win_out+1,no_out_recursives);
    DestroyArray( Wob, no_outputs,2*win_out+1,no_out_recursives);
    DestroyArray(DWob1,no_outputs,2*win_out+1,no_out_recursives);
    DestroyArray(DWob2,no_outputs,2*win_out+1,no_out_recursives);
    DestroyArray( Wot ,no_outputs);
    DestroyArray(DWot1,no_outputs);
    DestroyArray(DWot2,no_outputs);
  }	
  
  if( no_globals>0 ) {
    DestroyArray( Wgf ,no_globals,no_out_recursives);
    DestroyArray(DWgf1,no_globals,no_out_recursives);
    DestroyArray(DWgf2,no_globals,no_out_recursives);
    DestroyArray( Wgb ,no_globals,no_out_recursives);
    DestroyArray(DWgb1,no_globals,no_out_recursives);
    DestroyArray(DWgb2,no_globals,no_out_recursives);
    DestroyArray( Wgt ,no_globals);
    DestroyArray(DWgt1,no_globals);
    DestroyArray(DWgt2,no_globals);
  }
}


void BiRecursiveNeuralNetwork::EmptyMatrixes( bool both )
{
  if( no_codes!=no_inputs ) {
    EmptyArray(DWci,no_codes,no_inputs);
    EmptyArray(DWct,no_codes);
  }
  
  if( use_hidden_layer ) {
    EmptyArray(DWhc,no_hiddens,no_codes);
    EmptyArray(DWht,no_hiddens);
  }	

  EmptyArray(DWfc,no_recursives,no_codes);
  EmptyArray(DWff,no_recursives,no_out_recursives);
  EmptyArray(DWft,no_recursives);
  
  EmptyArray(DWbc,no_recursives,no_codes);
  EmptyArray(DWbb,no_recursives,no_out_recursives);
  EmptyArray(DWbt,no_recursives);

  if( two_layers ) {
    EmptyArray(DW2ff,no_out_recursives,no_recursives);
    EmptyArray(DW2ft,no_out_recursives);
  
    EmptyArray(DW2bb,no_out_recursives,no_recursives);
    EmptyArray(DW2bt,no_out_recursives);
  }	
  
  if( no_outputs>0 ) {
    EmptyArray(DWoh,no_outputs,2*win_out+1,no_hiddens);
    EmptyArray(DWof,no_outputs,2*win_out+1,no_out_recursives);
    EmptyArray(DWob,no_outputs,2*win_out+1,no_out_recursives);
    EmptyArray(DWot,no_outputs);
  }
  
  if( no_globals>0 ) {
    EmptyArray(DWgf,no_globals,no_out_recursives);
    EmptyArray(DWgb,no_globals,no_out_recursives);
    EmptyArray(DWgt,no_globals);
  }

  if( both ) {
    if( no_codes!=no_inputs ) {
      EmptyArray(oDWci,no_codes,no_inputs);
      EmptyArray(oDWct,no_codes);
    }

    if( use_hidden_layer ) {
      EmptyArray(oDWhc,no_hiddens,no_codes);
      EmptyArray(oDWht,no_hiddens);
    }	

    EmptyArray(oDWfc,no_recursives,no_codes);
    EmptyArray(oDWff,no_recursives,no_out_recursives);
    EmptyArray(oDWft,no_recursives);
    
    EmptyArray(oDWbc,no_recursives,no_codes);
    EmptyArray(oDWbb,no_recursives,no_out_recursives);
    EmptyArray(oDWbt,no_recursives);
    
    if( two_layers ) {
      EmptyArray(oDW2ff,no_out_recursives,no_recursives);
      EmptyArray(oDW2ft,no_out_recursives);
      
      EmptyArray(oDW2bb,no_out_recursives,no_recursives);
      EmptyArray(oDW2bt,no_out_recursives);
    }	

    if( no_outputs>0 ) {
      EmptyArray(oDWoh,no_outputs,2*win_out+1,no_hiddens);
      EmptyArray(oDWof,no_outputs,2*win_out+1,no_out_recursives);
      EmptyArray(oDWob,no_outputs,2*win_out+1,no_out_recursives);
      EmptyArray(oDWot,no_outputs);
    }	

    if( no_globals>0 ) {
      EmptyArray(oDWgf,no_globals,no_out_recursives);
      EmptyArray(oDWgb,no_globals,no_out_recursives);
      EmptyArray(oDWgt,no_globals);
    }
  }	
}

void BiRecursiveNeuralNetwork::SwapMatrixes() 
{
  if( swap_delta ) {  
    if( no_codes!=no_inputs ) {
      DWci = DWci1; oDWci = DWci2;
      DWct = DWct1; oDWct = DWct2;
    }
    
    if( use_hidden_layer ) {
      DWhc = DWhc1; oDWhc = DWhc2;
      DWht = DWht1; oDWht = DWht2;
    }	

    DWfc = DWfc1; oDWfc = DWfc2;
    DWff = DWff1; oDWff = DWff2;
    DWft = DWft1; oDWft = DWft2;

    DWbc = DWbc1; oDWbc = DWbc2;
    DWbb = DWbb1; oDWbb = DWbb2;
    DWbt = DWbt1; oDWbt = DWbt2;

    if( two_layers ) {
      DW2ff = DW2ff1; oDW2ff = DW2ff2;
      DW2ft = DW2ft1; oDW2ft = DW2ft2;

      DW2bb = DW2bb1; oDW2bb = DW2bb2;
      DW2bt = DW2bt1; oDW2bt = DW2bt2;
    }	

    if( no_outputs>0 ) {
      DWoh = DWoh1; oDWoh = DWoh2;
      DWof = DWof1; oDWof = DWof2;
      DWob = DWob1; oDWob = DWob2;
      DWot = DWot1; oDWot = DWot2;
    }

    if( no_globals>0 ) {
      DWgf = DWgf1; oDWgf = DWgf2;
      DWgb = DWgb1; oDWgb = DWgb2;
      DWgt = DWgt1; oDWgt = DWgt2;
    }
  }	
  else {
    if( no_codes!=no_inputs ) {
      DWci = DWci; oDWci = DWci;
      DWct = DWct; oDWct = DWct;
    }
    
    if( use_hidden_layer ) {
      DWhc = DWhc2; oDWhc = DWhc1;
      DWht = DWht2; oDWht = DWht1;
    }

    DWfc = DWfc2; oDWfc = DWfc1;
    DWff = DWff2; oDWff = DWff1;
    DWft = DWft2; oDWft = DWft1;

    DWbc = DWbc2; oDWbc = DWbc1;
    DWbb = DWbb2; oDWbb = DWbb1;
    DWbt = DWbt2; oDWbt = DWbt1;
    
    if( two_layers ) {
      DW2ff = DW2ff2; oDW2ff = DW2ff1;
      DW2ft = DW2ft2; oDW2ft = DW2ft1;

      DW2bb = DW2bb2; oDW2bb = DW2bb1;
      DW2bt = DW2bt2; oDW2bt = DW2bt1;
    }

    if( no_outputs>0 ) {
      DWoh = DWoh2; oDWoh = DWoh1;
      DWof = DWof2; oDWof = DWof1;
      DWob = DWob2; oDWob = DWob1;
      DWot = DWot2; oDWot = DWot1;
    }	

    if( no_globals>0 ) {
      DWgf = DWgf2; oDWgf = DWgf1;
      DWgb = DWgb2; oDWgb = DWgb1;
      DWgt = DWgt2; oDWgt = DWgt1;
    }
  }
  
  swap_delta = !swap_delta;
}


void BiRecursiveNeuralNetwork::SizeTempMatrixes(int pattern_size)
{
  C      = CreateArray(pattern_size,no_codes);
  Cdelta = CreateArray(pattern_size,no_codes);

  H      = CreateArray(pattern_size,no_hiddens);
  Hdelta = CreateArray(pattern_size,no_hiddens);

  F      = CreateArray(pattern_size,no_recursives);
  Fdelta = CreateArray(pattern_size,no_recursives);
  if( two_layers ) {
    F2      = CreateArray(pattern_size,no_out_recursives);
    F2delta = CreateArray(pattern_size,no_out_recursives);
  }
  else {
    F2      = F;
    F2delta = Fdelta;    
  }

  B      = CreateArray(pattern_size,no_recursives);
  Bdelta = CreateArray(pattern_size,no_recursives);
  if( two_layers ) {
    B2      = CreateArray(pattern_size,no_out_recursives);
    B2delta = CreateArray(pattern_size,no_out_recursives);
  }
  else {
    B2      = B;
    B2delta = Bdelta;    
  }

  if( no_outputs>0 ) {
    Y      = CreateArray(pattern_size,no_outputs);
    Ydelta = CreateArray(pattern_size,no_outputs);
    O      = CreateArray(pattern_size,no_outputs);
    Odelta = CreateArray(pattern_size,no_outputs);
  }

  if( no_globals>0 ) {
    G       = CreateArray(no_globals);
    Gdelta  = CreateArray(no_globals);
    OG      = CreateArray(no_globals);
    OGdelta = CreateArray(no_globals);
  }
}

void BiRecursiveNeuralNetwork::DestroyTempMatrixes(int pattern_size)
{
  DestroyArray(C     ,pattern_size,no_codes);
  DestroyArray(Cdelta,pattern_size,no_codes);

  DestroyArray(H     ,pattern_size,no_hiddens);
  DestroyArray(Hdelta,pattern_size,no_hiddens);

  DestroyArray(F     ,pattern_size,no_recursives);
  DestroyArray(Fdelta,pattern_size,no_recursives);
  if( two_layers ) {
    DestroyArray(F2     ,pattern_size,no_out_recursives);
    DestroyArray(F2delta,pattern_size,no_out_recursives);
  }

  DestroyArray(B     ,pattern_size,no_recursives);
  DestroyArray(Bdelta,pattern_size,no_recursives);
  if( two_layers ) {
    DestroyArray(B2     ,pattern_size,no_out_recursives);
    DestroyArray(B2delta,pattern_size,no_out_recursives);
  }	

  if( no_outputs>0 ) {
    DestroyArray(Y     ,pattern_size,no_outputs);
    DestroyArray(Ydelta,pattern_size,no_outputs);
    DestroyArray(O     ,pattern_size,no_outputs);
    DestroyArray(Odelta,pattern_size,no_outputs);
  }

  if( no_globals>0 ) {
    DestroyArray(G      ,no_globals);
    DestroyArray(Gdelta ,no_globals);
    DestroyArray(OG     ,no_globals);
    DestroyArray(OGdelta,no_globals);
  }
}


REAL BiRecursiveNeuralNetwork::RandWeight(REAL init_weight)
{
  return (init_weight * (2.*(REAL)rand()/(REAL)RAND_MAX - 1.));
}


void BiRecursiveNeuralNetwork::RandomizeWeights(REAL init_weight) 
{
  // input -> code weights
  if( no_codes!=no_inputs ) {
    for( int c=0; c<no_codes; c++ ) {
      for( int i=0; i<no_inputs; i++ ) 
	Wci[c][i] = RandWeight(init_weight);
      Wct[c] = RandWeight(init_weight);
    }
  }	
  
  // code -> hidden weights
  if( use_hidden_layer ) {
    for( int h=0; h<no_hiddens; h++ ) {
      for( int c=0; c<no_codes; c++ ) 
	Whc[h][c] = RandWeight(init_weight);
      Wht[h] = RandWeight(init_weight);
    }
  }
  
  // code -> forward weights
  for( int f=0; f<no_recursives; f++ ) {
    for( int c=0; c<no_codes; c++ ) 
      Wfc[f][c] = RandWeight(init_weight);
    for( int r=0; r<no_out_recursives; r++ ) 
      Wff[f][r] = RandWeight(init_weight);
    Wft[f] = RandWeight(init_weight);
  }
  
  // code -> backward weights
  for( int b=0; b<no_recursives; b++ ) {
    for( int c=0; c<no_codes; c++ ) 
      Wbc[b][c] = RandWeight(init_weight);
    for( int r=0; r<no_out_recursives; r++ ) 
      Wbb[b][r] = RandWeight(init_weight);
    Wbt[b] = RandWeight(init_weight);      
  }

  if( two_layers ) {   
    // forward -> output forward weights
    for( int r=0; r<no_out_recursives; r++ ) {
      for( int f=0; f<no_recursives; f++ )
	W2ff[r][f] = RandWeight(init_weight);
      W2ft[r] = RandWeight(init_weight);
    }
    
    // backward -> output backward weights
    for( int r=0; r<no_out_recursives; r++ ) {
      for( int b=0; b<no_recursives; b++ )
	W2bb[r][b] = RandWeight(init_weight);
      W2bt[r] = RandWeight(init_weight);
    } 
  }

  // state -> output weights
  if( no_outputs>0 ) {
    for( int o=0; o<no_outputs; o++ ) {
      for( int w=-win_out,w1=0; w<=win_out; w++,w1++ ) {
	for( int h=0; h<no_hiddens; h++ ) 
	  Woh[o][w1][h] = RandWeight(init_weight);
	for( int r=0; r<no_out_recursives; r++ ) 
	  Wof[o][w1][r] = RandWeight(init_weight);
	for( int r=0; r<no_out_recursives; r++ ) 
	  Wob[o][w1][r] = RandWeight(init_weight);      
      }	
      Wot[o] = RandWeight(init_weight);      
    }
  }	

  // state -> global
  if( no_globals>0 ) {
    for( int g=0; g<no_globals; g++ ) {
      for( int r=0; r<no_out_recursives; r++ ) 
	Wgf[g][r] = RandWeight(init_weight);
      for( int r=0; r<no_out_recursives; r++ ) 
	Wgf[g][r] = RandWeight(init_weight);
      Wgt[g] = RandWeight(init_weight);
    }
  }
}	


REAL BiRecursiveNeuralNetwork::Norm() 
{
  REAL norm = 0.;
  int count = 0;
  
  // input -> code weights
  if( no_codes!=no_inputs ) {
    for( int c=0; c<no_codes; c++ ) {
      for( int i=0; i<no_inputs; i++ ) 
	norm += fabs(Wci[c][i]);
      norm += fabs(Wct[c]);
    }
    count += (no_codes*(no_inputs+1));
  }	

  // code -> hidden weights
  if( use_hidden_layer ) {
    for( int h=0; h<no_hiddens; h++ ) {
      for( int c=0; c<no_codes; c++ ) 
	norm += fabs(Whc[h][c]);
      norm += fabs(Wht[h]);
    }
    count += no_hiddens*(no_codes+1);
  }
  
  // code -> forward weights
  for( int f=0; f<no_recursives; f++ ) {
    for( int c=0; c<no_codes; c++ ) 
      norm += fabs(Wfc[f][c]);
    for( int r=0; r<no_out_recursives; r++ ) 
      norm += fabs(Wff[f][r]);
    norm += fabs(Wft[f]);
  }
  count += no_recursives*(no_codes+no_out_recursives+1);
  
  // code -> backward weights
  for( int b=0; b<no_recursives; b++ ) {
    for( int c=0; c<no_codes; c++ ) 
      norm += fabs(Wbc[b][c]);
    for( int r=0; r<no_out_recursives; r++ ) 
      norm += fabs(Wbb[b][r]);
    norm += fabs(Wbt[b]);
  }
  count += no_recursives*(no_codes+no_out_recursives+1);

  if( two_layers ) {   
    // forward -> output forward weights
    for( int r=0; r<no_out_recursives; r++ ) {
      for( int f=0; f<no_recursives; f++ )
	norm += fabs(W2ff[r][f]);
      norm += fabs(W2ft[r]);
    }
    count += no_out_recursives*(no_recursives+1);
    
    // backward -> output backward weights
    for( int r=0; r<no_out_recursives; r++ ) {
      for( int b=0; b<no_recursives; b++ )
	norm += fabs(W2bb[r][b]);
      norm += fabs(W2bt[r]);
    } 
    count += no_out_recursives*(no_recursives+1);
  }

  // state -> output weights
  if( no_outputs>0 ) {
    for( int o=0; o<no_outputs; o++ ) {
      for( int w=-win_out,w1=0; w<=win_out; w++,w1++ ) {
	for( int h=0; h<no_hiddens; h++ ) 
	  norm += fabs(Woh[o][w1][h]);
	for( int r=0; r<no_out_recursives; r++ ) 
	  norm += fabs(Wof[o][w1][r]);
	for( int r=0; r<no_out_recursives; r++ ) 
	  norm += fabs(Wob[o][w1][r]);
      }	
      norm += fabs(Wot[o]);
    }
    count += (no_outputs*((2*win_out+1)*(no_hiddens+2*no_out_recursives)+1));
  }	
    
  // state -> global
  if( no_globals>0 ) {
    for( int g=0; g<no_globals; g++ ) {
      for( int r=0; r<no_out_recursives; r++ ) 
	norm += fabs(Wgf[g][r]);
      for( int r=0; r<no_out_recursives; r++ ) 
	norm += fabs(Wgf[g][r]);
      norm += fabs(Wgt[g]);
    }
    count += (no_globals*(2*no_out_recursives+1));
  }

  return (norm/(REAL)count);
}	


void BiRecursiveNeuralNetwork::SetOutputMode(OUTPUT_MODE mode) 
{
  output_mode = mode; 
}

void BiRecursiveNeuralNetwork::SetErrorMode(ERROR_MODE mode) 
{
  error_mode = mode; 
}

void BiRecursiveNeuralNetwork::SetAccuracyMode(ACCURACY_MODE mode) {
  accuracy_mode = mode;
}

void BiRecursiveNeuralNetwork::SetLearningParameters( REAL learning_rate, REAL momentum, REAL weight_decay )
{
  Exception::Assert(learning_rate>0., "Invalid value for learning rate");
  Exception::Assert(momentum>=0., "Invalid value for momentum term");
  Exception::Assert(weight_decay>=0., "Invalid value for weight_decay term");

  lambda = learning_rate;
  mu = momentum;
  ni = weight_decay;
}

REAL BiRecursiveNeuralNetwork::Accuracy(COLLECTIONA &targets, COLLECTIONA &predictions)
{
  int dim = targets.Cols();
  int noexamples = targets.Rows();
  if( noexamples>0 ) {
    if( accuracy_mode==MULTICLASS ) {
      uint no_corrects = 0;
      for( int r=0; r<noexamples; r++ ) 
	if( MaxInd(predictions[r],dim)==MaxInd(targets[r],dim) )
	  no_corrects++;
      return ((REAL)no_corrects / (REAL)noexamples);
    }  
    if( accuracy_mode==RMSD ) {
      double rmsd = 0.;
      for( int r=0; r<noexamples; r++ ) {
	ARRAY1 &P = predictions[r];
	ARRAY1 &T = targets[r];
	for( int o=0; o<dim; o++ ) 
	  rmsd += (P[o]-T[o])*(P[o]-T[o]) / (REAL)dim;	
      }
      return sqrt(rmsd / (REAL)noexamples);
    }
    if( accuracy_mode==MULTILABEL ) {
      uint no_corrects = 0;
      for( int r=0; r<noexamples; r++ ) {
	ARRAY1 &P = predictions[r];
	ARRAY1 &T = targets[r];
	for( int o=0; o<dim; o++ ) {
	  if( (T[o]>0.5 && P[o]>0.5) || (T[o]<=0.5 && P[o]<=0.5) )
	    no_corrects++;	
	}
      }
      return ((REAL)no_corrects / (REAL)(noexamples*dim));
    }
    if( accuracy_mode==AUC ) {
      double auc = 0.;
      // for each pair of classes
      for( int i=0; i<dim; i++ ) {
	for( int j=0; j<dim; j++ ) {
	  if( i!=j ) {
	    multimap<double,uint> ranking;
	    
	    // rank examples
	    uint no_positives = 0;
	    uint no_negatives = 0;
	    for( int r=0; r<noexamples; r++ ) {
	      if( targets[r][i]>targets[r][j] ) { 
		// positive example
		ranking.insert(make_pair(predictions[r][i],1));
		no_positives++;
	      }
	      else if( targets[r][i]<targets[r][j] ) { 
		// negative example
		ranking.insert(make_pair(predictions[r][i],0));
		no_negatives++;
	      }
	    }

	    // compute WMW statistics
	    uint sum = 0;
	    uint rank = 0;
	    for( multimap<double,uint>::iterator r=ranking.begin(); r!=ranking.end(); r++, rank++ ) {
	      if( r->second==1 )
		sum += rank;
	    }
	    sum -= no_positives*(no_positives-1)/2;
	    
	    auc += (REAL)sum/(REAL)(no_positives*no_negatives);
	  }
	}
      }
      return auc/(REAL)(dim*(dim-1));
    }
    if( accuracy_mode==CE ) {
      double ce = 0.;
      for( int r=0; r<noexamples; r++ ) {
	ARRAY1 &P = predictions[r];
	ARRAY1 &T = targets[r];
	
	for( int o=0; o<dim; o++ ) {      
	  if( T[o]>0. ) 
	    ce += T[o]*log(T[o]/P[o]);	  
	  else if( T[o]<1. ) 
	    ce += (1-T[o])*log((1-T[o])/(1-P[o]));
	}	
      }
      return exp(-ce/(REAL)noexamples);
    }
    if( accuracy_mode==OAUC ) {
      double auc = 0.;
      // for each class
      for( int i=0; i<dim; i++ ) {
	multimap<double,uint> ranking;
	    
	// rank examples
	uint no_positives = 0;
	uint no_negatives = 0;
	for( int r=0; r<noexamples; r++ ) {
	  int true_class = MaxInd(targets[r],dim);
	  
	  // compute margin
	  REAL max = 0.;
	  for( int j=0; j<dim; j++ ) {
	    if( j!=true_class && predictions[r][j]>max )
	      max = predictions[r][j];
	  }
	  REAL margin = predictions[r][true_class] - max;

	  if( targets[r][i]>0. ) { 
	    // positive example
	    ranking.insert(make_pair(margin,1));
	    no_positives++;
	  }
	  else {
	    // negative example
	    ranking.insert(make_pair(-margin,0));
	    no_negatives++;
	  }
	}
	
	// compute WMW statistics
	uint sum = 0;
	uint rank = 0;
	for( multimap<double,uint>::iterator r=ranking.begin(); r!=ranking.end(); r++, rank++ ) {
	  if( r->second==1 )
	    sum += rank;
	}
	sum -= no_positives*(no_positives-1)/2;

	auc += (REAL)sum/(REAL)(no_positives*no_negatives);      		
      }
      return auc/(REAL)dim;
    }
    Exception::Throw("Wrong output mode");
  }
  return 0.;
}

 
void BiRecursiveNeuralNetwork::TrainOnLine(COLLECTIONA &targets, COLLECTIONA &inputs, INDEXES &patterns, vector<int> &sequence)
{ 
  Exception::Assert(error_mode!=LAUC, "AUC error is only available in batch mode");

  COLLECTIONA predictions(targets.Rows(),GetNoOutputs());

  int noexamples = patterns.Size();
  for( int i=0; i<noexamples; i++ ) {

    int r = (sequence.size()>0) ?sequence[i] :i;
    
    // allocate memory
    int seqsize = patterns[r].size;
    SizeTempMatrixes(seqsize);

    // train
    Forward(inputs,patterns[r]);                        // propagate inputs
    getPredictions(predictions,patterns,r);             // get output
    ErrorDelta(targets,predictions,patterns,r);         // compute error derivatives 
    EmptyMatrixes(false);                               // init weights deltas
    Backward(inputs,patterns[r]);                       // back propagate error
    Update();                                           // update weights

    // free memory
    DestroyTempMatrixes(seqsize);
  }  
}


void BiRecursiveNeuralNetwork::TrainBatch(COLLECTIONA &targets, COLLECTIONA &inputs, INDEXES &patterns)
{
  // collect predictions
  COLLECTIONA predictions(targets.Rows(),GetNoOutputs());

  int noexamples = patterns.Size();
  for( int r=0; r<noexamples; r++ ) {

    // allocate memory
    int seqsize = patterns[r].size;
    SizeTempMatrixes(seqsize);

    // train
    Forward(inputs,patterns[r]);            // propagate inputs
    getPredictions(predictions,patterns,r); // get output

    // free memory
    DestroyTempMatrixes(seqsize);
  }  

  // update weights
  EmptyMatrixes(false); // init weights deltas
  for( int r=0; r<noexamples; r++ ) {
    
    // allocate memory
    int seqsize = patterns[r].size;
    SizeTempMatrixes(seqsize);
  
    // train
    Forward(inputs,patterns[r]);            // propagate inputs
    Error(targets,predictions,patterns,r); // compute error derivatives     
    ErrorDelta(targets,predictions,patterns,r); // compute error derivatives     
    Backward(inputs,patterns[r]);               // back propagate error

    // free memory
    DestroyTempMatrixes(seqsize);
  }  
  Update(); // update weights
}


REAL BiRecursiveNeuralNetwork::Test(COLLECTIONA &targets, COLLECTIONA &inputs, COLLECTIONA &predictions, INDEXES &patterns) {  
  predictions.Resize(targets.Rows(),GetNoOutputs());

  // collect predictions
  int noexamples = patterns.Size();
  for( int r=0; r<noexamples; r++ ) {     

    int seqsize = patterns[r].size;
   
    SizeTempMatrixes(seqsize);

    Forward(inputs,patterns[r]);
    getPredictions(predictions,patterns,r);

    DestroyTempMatrixes(seqsize);
  }

  // compute error
  REAL sum_error = 0.;
  for( int r=0; r<noexamples; r++ )      
    sum_error += Error(targets,predictions,patterns,r); // compute error and derivatives    
  if( error_mode!=LAUC )
    sum_error /= (REAL)targets.Rows();  

  return sum_error;
}

void BiRecursiveNeuralNetwork::Predict(COLLECTIONA &inputs, COLLECTIONA &predictions, INDEXES &patterns) { 
  if( global_mode==0 ) 
    predictions.Resize(inputs.Rows(),GetNoOutputs());
  else
    predictions.Resize(patterns.Size(),GetNoOutputs());

  int noexamples = patterns.Size();
  for( int r=0; r<noexamples; r++ ) {     

    int seqsize = patterns[r].size;

    // save predictions 
    SizeTempMatrixes(seqsize);

    Forward(inputs,patterns[r]);
    getPredictions(predictions,patterns,r);

    DestroyTempMatrixes(seqsize);
  }
}

void BiRecursiveNeuralNetwork::getPredictions(COLLECTIONA &predictions, INDEXES &patterns, int r) {
  if( global_mode==0 ) {
    INDEX &pattern = patterns[r];
    for( int t=0,t1=pattern.start; t<pattern.size; t++, t1++ ) 
      CopyArray(predictions[t1],O[t],no_outputs);
  }
  else 
    CopyArray(predictions[r],OG,no_globals);
}

void BiRecursiveNeuralNetwork::Forward(COLLECTIONA &I, INDEX &pattern)
{
  int start = pattern.start;
  int end = pattern.end;
  int size  = pattern.size;
  int size1 = end-start; 

  //-------------------------------------
  // propagate from input to code layer 
  //-------------------------------------

  if( no_codes!=no_inputs ) {
    for( int t=0,t1=start; t<size; t++,t1++ ) {
      for( int c=0; c<no_codes; c++ ) {
	REAL net = Wct[c]; // bias
	for( int i=0; i<no_inputs; i++ )
	  net += Wci[c][i] * I[t1][i];
	C[t][c] = PROPAGATEF(net);
      }
    }
  }
  else {
    for( int t=0,t1=start; t<size; t++,t1++ )
      CopyArray(C[t],I[t1],no_inputs);
  }

  //-------------------------------------
  // propagate from code to state layer 
  //-------------------------------------

  // propagate forward state
  for( int t=0; t<size; t++ ) {
    
    // propagate to first layer
    for( int f=0; f<no_recursives; f++ ) {
      REAL net = Wft[f]; // bias
      for( int c=0; c<no_codes; c++ ) 
	net += Wfc[f][c] * C[t][c];
      if( t>0 ) { // first of the propagation is empty
	for( int r=0; r<no_out_recursives; r++ )
	  net += Wff[f][r] * F2[t-1][r];
      }
      else {
	for( int r=0; r<no_out_recursives; r++ )
	  net += Wff[f][r] * ZEROPOINT;
      }

      F[t][f] = PROPAGATEF(net);

    }

    // propagate to second layer
    if( two_layers ) {
      for( int r=0; r<no_out_recursives; r++ ) {
	REAL net = W2ft[r]; // bias
	for( int f=0; f<no_recursives; f++ )
	  net += W2ff[r][f] * F[t][f];
	F2[t][r] = PROPAGATEF(net);
      }      
    }	       
  }	

  // propagate backward state
  for( int t=size1; t>=0; t-- ) {

    // propagate to first layer
    for( int b=0; b<no_recursives; b++ ) {
      REAL net = Wbt[b]; // bias
      for( int c=0; c<no_codes; c++ )
	net += Wbc[b][c] * C[t][c];
      if( t<size1 ) { // first of the propagation is empty
	for( int r=0; r<no_out_recursives; r++ )
	  net += Wbb[b][r] * B2[t+1][r];
      }
      else {
	for( int r=0; r<no_out_recursives; r++ )
	  net += Wbb[b][r] * ZEROPOINT;
      }
      B[t][b] = PROPAGATEF(net);
    }

    // propagate to second layer
    if( two_layers ) {
      // propagate to output backward layer
      for( int r=0; r<no_out_recursives; r++ ) {
	REAL net = W2bt[r]; // bias
	for( int b=0; b<no_recursives; b++ )
	  net += W2bb[r][b] * B[t][b];
	B2[t][r] = PROPAGATEF(net);
      }      
    }	
  }	

  // propagate non-recursive state
  if( use_hidden_layer ) {
    for( int t=0; t<size; t++ ) {
      for( int h=0; h<no_hiddens; h++ ) {
	REAL net = Wht[h]; // bias
	for( int c=0; c<no_codes; c++ )
	  net += Whc[h][c] * C[t][c];
	H[t][h] = PROPAGATEF(net);
      }
    }
  }
  else {
    for( int t=0,t1=start; t<size; t++,t1++ )
      CopyArray(H[t],I[t1],no_inputs);
  }
    
  //-------------------------------------
  // propagate from state to output layer
  //-------------------------------------

  if( no_outputs>0 ) {
    for( int t=0; t<size; t++ ) {
      for( int o=0; o<no_outputs; o++ ) {
	Y[t][o] = Wot[o]; // bias
	for( int t1=t-win_out,w1=0; t1<=t+win_out; t1++,w1++ ) {
	  if( t1>=0 && t1<size ) {
	    for( int h=0; h<no_hiddens; h++ ) 
	      Y[t][o] += Woh[o][w1][h] * H[t1][h];     
	    for( int r=0; r<no_out_recursives; r++ ) 
	      Y[t][o] += Wof[o][w1][r] * F2[t1][r];     
	    for( int r=0; r<no_out_recursives; r++ ) 
	      Y[t][o] += Wob[o][w1][r] * B2[t1][r];   
	  }
	}
      }    
    }	
    output_forward = false;
  }	

  //-------------------------------------
  // propagate from state to global layer
  //-------------------------------------
  
  if( no_globals>0 ) {
    for( int g=0; g<no_globals; g++ ) {
      G[g] = Wgt[g]; // bias
      for( int r=0; r<no_out_recursives; r++ )
	G[g] += Wgf[g][r] * F2[size1][r];     
      for( int r=0; r<no_out_recursives; r++ )
	G[g] += Wgb[g][r] * B2[0][r];      
    }	
  }	

  //-------------------------------------
  // Apply output modification
  //-------------------------------------
  
  if( output_mode==SOFTMAX ) {        
    // apply softmax
    if( no_outputs>0 ) {
      for( int t=0; t<size; t++ ) {
	REAL sum = 0.;
	REAL max = Y[t][0];
	for( int o=1; o<no_outputs; o++ ) 
	  max = (Y[t][o]>max) ?Y[t][o] :max;      
	for( int o=0; o<no_outputs; o++ )
	  sum += (O[t][o] = exp(Y[t][o]-max));  
	for( int o=0; o<no_outputs; o++ )
	  O[t][o] /= sum;  
      }    
    }
    if( no_globals>0 ) {
      REAL sum = 0.;
      REAL max = G[0];
      for( int g=1; g<no_globals; g++ ) 
	max = (G[g]>max) ?G[g] :max;      
      for( int g=0; g<no_globals; g++ )
	sum += (OG[g] = exp(G[g]-max));  
      for( int g=0; g<no_globals; g++ )
	OG[g] /= sum;  
    }	
  }
  else if( output_mode==LINEAR ) {
    // copy activation
    if( no_outputs>0 ) {
      for( int t=0; t<size; t++ ) {
	for( int o=0; o<no_outputs; o++ )
	  O[t][o] = Y[t][o];
      }
    }
    if( no_globals>0 ) {
      for( int g=0; g<no_globals; g++ )
	OG[g] = G[g];
    }
  }
  else if( output_mode==SIGMOID ) {
    // apply sigmoid
    if( no_outputs>0 ) {
      for( int t=0; t<size; t++ ) {
	for( int o=0; o<no_outputs; o++ )
	  O[t][o] = SIGMOIDF(Y[t][o]);                     
      }	
    }
    if( no_globals>0 ) {
      for( int g=0; g<no_globals; g++ )
	OG[g] = SIGMOIDF(G[g]);         
    }      
  }
  else
    Exception::Throw("Wrong output mode" );
}


REAL BiRecursiveNeuralNetwork::CalculateError(int ind, COLLECTIONA &targets, COLLECTIONA &predictions, int dim) 
{
  // compute error and its derivatives, on-line version
  REAL sum_error = 0.;
  if( error_mode==ENTROPY ) {

    ARRAY1 &T = targets[ind];
    ARRAY1 &P = predictions[ind];
    for( int o=0; o<dim; o++ ) {      
      if( T[o]>0. ) 
    	sum_error += T[o]*log(T[o]/P[o]);
      else if( T[o]<1. && output_mode!=SOFTMAX) 
    	sum_error += (1-T[o])*log((1-T[o])/(1-P[o]));
    }
    return sum_error;
  }
  if( error_mode==MSE ) {  
    ARRAY1 &T = targets[ind];
    ARRAY1 &P = predictions[ind];
    for( int o=0; o<dim; o++ ) 
      sum_error += (P[o]-T[o]) * (P[o]-T[o]);
    return (sum_error / 2.);
  }
  if( error_mode==LAUC ) {
    double gamma = 0.1;

    // compute AUC error
    double sum_error = 0.;
    int noexamples = targets.Rows();
    for( int i=0; i<dim; i++ ) {
      for( int j=0; j<dim; j++ ) {
	if( i!=j ) {
	  if( targets[ind][i]!=targets[ind][j] ) { // positive or negative
	    double error_ij = 0.;
	    uint no_positives = 0;
	    uint no_negatives = 0;	    

	    double margin_x     = predictions[ind][i]; // multiclass margin
	    bool   ispositive_x = (targets[ind][i]>targets[ind][j]);	    
	    no_positives += ( ispositive_x) ?1 :0;
	    no_negatives += (!ispositive_x) ?1 :0;
	    
	    for( int r=0; r<noexamples; r++ ) {
	      if( r!=ind && targets[r][i]!=targets[r][j]) { // positive or negative, skip current
		double margin_y     = predictions[r][i]; // multiclass margin
		bool   ispositive_y = (targets[r][i]>targets[r][j]);
		no_positives += ( ispositive_y) ?1 :0;
		no_negatives += (!ispositive_y) ?1 :0;

		// compare margins
		if( ispositive_x!=ispositive_y ) {
		  if( ispositive_x ) {
		    if( (margin_x-margin_y)<gamma ) {
		      // positive margin is lower than negative, current is positive
		      double delta = (margin_x-margin_y-gamma);
		      error_ij += delta*delta/2.;
		    }
		  }
		  else {
		    if( (margin_y-margin_x)<gamma ) {
		      // positive margin is lower than negative, current is negative
		      double delta = (margin_y-margin_x-gamma);
		      error_ij += delta*delta/2.;		    
		    }
		  }
		}
	      }
	    }

	    // add contribution of this two classes
	    sum_error += (error_ij / (REAL)(no_positives*no_negatives));
	  }	
	}
      }
    }
    return (sum_error/(REAL)(dim*(dim-1)));
  }
  
  Exception::Throw("Wrong error mode");  
  return 0.;
}

void BiRecursiveNeuralNetwork::CalculateDelta(int ind, COLLECTIONA &targets, COLLECTIONA &predictions, ARRAY1 &delta, int dim) 
{
  // compute error and its derivatives, on-line version
  if( error_mode==ENTROPY ) {
    ARRAY1 &T = targets[ind];
    ARRAY1 &P = predictions[ind];
    for( int o=0; o<dim; o++ ) {      
      if( T[o]>0. ) 
	delta[o] = -T[o]/P[o];
      else if( T[o]<1. && output_mode!=SOFTMAX) 
	delta[o] = (1-T[o])/(1-P[o]);
      else
	delta[o] = 0.;      
    }
  }
  else if( error_mode==MSE ) { 
    ARRAY1 &T = targets[ind];
    ARRAY1 &P = predictions[ind]; 
    for( int o=0; o<dim; o++ )
      delta[o] = P[o]-T[o];
  }
  else if( error_mode==LAUC ) {
    double gamma = 0.1;

    // compute AUC derivative
    ARRAY1 delta_ij = CreateArray(dim);

    int noexamples = targets.Rows();
    for( int i=0; i<dim; i++ ) {
      for( int j=0; j<dim; j++ ) {
	if( i!=j ) {
	  EmptyArray(delta_ij,dim);

	  if( targets[ind][i]!=targets[ind][j] ) { // positive or negative
	    uint no_positives = 0;
	    uint no_negatives = 0;	    

	    double margin_x     = predictions[ind][i]; // multiclass margin
	    bool   ispositive_x = (targets[ind][i]>targets[ind][j]);	    
	    no_positives += ( ispositive_x) ?1 :0;
	    no_negatives += (!ispositive_x) ?1 :0;

	    for( int r=0; r<noexamples; r++ ) {
	      if( r!=ind && targets[r][i]!=targets[r][j]) { // positive or negative, skip current
		double margin_y     = predictions[r][i]; // multiclass margin
		bool   ispositive_y = (targets[r][i]>targets[r][j]);
		no_positives += ( ispositive_y) ?1 :0;
		no_negatives += (!ispositive_y) ?1 :0;

		// compare margins
		if( ispositive_x!=ispositive_y ) {
		  if( ispositive_x ) {
		    if( (margin_x-margin_y)<gamma ) {
		      // positive margin is lower than negative, current is positive
		      double delta = (margin_x-margin_y-gamma);
		      delta_ij[i] += delta; // increase positive class
		      delta_ij[j] -= delta; // decrease negative class
		    }
		  }
		  else {
		    if( (margin_y-margin_x)<gamma ) {
		      // positive margin is lower than negative, current is negative
		      double delta = (margin_y-margin_x-gamma);
		      delta_ij[i] -= delta; // decrease positive class
		      delta_ij[j] += delta; // increase negative class
		    }
		  }
		}
	      }
	    }

	    // normalize deltas
	    for( int o=0; o<dim; o++ ) 
	      delta[o] += delta_ij[o]/(REAL)(no_positives*no_negatives);
	  }	
	}
      }
    }

    DestroyArray(delta_ij,dim);
      
    // normalize deltas
    for( int o=0; o<dim; o++ ) 
      delta[o] /= (REAL)(dim*(dim-1));
  }
  else
    Exception::Throw("Wrong error mode");  
}

REAL BiRecursiveNeuralNetwork::Error(COLLECTIONA &targets, COLLECTIONA &predictions, INDEXES &patterns, int r)
{
  if( global_mode==0 ) {
    REAL sum_error = 0.;  
    INDEX &pattern = patterns[r];
    for( int t=0,t1=pattern.start; t<pattern.size; t++,t1++ ) 
      sum_error += CalculateError(t1,targets,predictions,no_outputs);

    return sum_error;
  }
  return CalculateError(r,targets,predictions,no_globals);
}

void BiRecursiveNeuralNetwork::ErrorDelta(COLLECTIONA &targets, COLLECTIONA &predictions, INDEXES &patterns, int r)
{
  if( global_mode==0 ) {
    INDEX &pattern = patterns[r];
    for( int t=0,t1=pattern.start; t<pattern.size; t++,t1++ ) 
      CalculateDelta(t1,targets,predictions,Odelta[t],no_outputs);
  }
  else 
    CalculateDelta(r,targets,predictions,OGdelta,no_globals);
}

void BiRecursiveNeuralNetwork::Backward(COLLECTIONA &I, INDEX &pattern) 
{  
  int start = pattern.start;
  int end = pattern.end;
  int size  = pattern.size;
  int size1 = end-start; 
 
  //-------------------------------------
  // back propagate through output function
  //-------------------------------------
  
  if( output_mode==SOFTMAX ) {    
    if( no_outputs>0 ) {
      for( int t=0; t<size; t++ ) {
	for( int o=0; o<no_outputs; o++ ) {
	  Ydelta[t][o] = Odelta[t][o]*O[t][o]*(1-O[t][o]);
	  for( int i=0; i<no_outputs; i++ ) {
	    if( i!=o )
	      Ydelta[t][o] -= Odelta[t][i]*O[t][o]*O[t][i];
	  }
	}
      }
    }
    if( no_globals>0 ) {      
      for( int g=0; g<no_globals; g++ ) {
	Gdelta[g] = OGdelta[g]*OG[g]*(1-OG[g]);
	for( int i=0; i<no_globals; i++ ) {
	  if( i!=g )
	    Gdelta[g] -= OGdelta[i]*OG[g]*OG[i];
	}
      }
    }
  }	
  else if( output_mode==LINEAR ) {
    if( no_outputs>0 ) {
      for( int t=0; t<size; t++ ) {
	for( int o=0; o<no_outputs; o++ )
	  Ydelta[t][o] = Odelta[t][o];
      }
    }
    if( no_globals>0 ) {
      for( int g=0; g<no_globals; g++ )
	Gdelta[g] = OGdelta[g];
    }  
  }
  else if( output_mode==SIGMOID ) {
    if( no_outputs>0 ) {
      for( int t=0; t<size; t++ ) {
	for( int o=0; o<no_outputs; o++ )
	  Ydelta[t][o] = Odelta[t][o]*SIGMOIDB(O[t][o]);
      }
    }
    if( no_globals>0 ) {
      for( int g=0; g<no_globals; g++ )
	Gdelta[g] = OGdelta[g]*SIGMOIDB(OG[g]);
    }  
  }
  else
    Exception::Throw("Wrong output mode" );
  
  //-------------------------------------
  // back propagate through output layer 
  //-------------------------------------

  if( no_outputs>0 ) {
    for( int t=0; t<size; t++ ) {          
      for( int o=0; o<no_outputs; o++ ) {
	for( int t1=t-win_out,w1=0; t1<=t+win_out; t1++,w1++ ) {
	  if( t1>=0 && t1<size ) {
	    for( int h=0; h<no_hiddens; h++ )
	      DWoh[o][w1][h] +=  Ydelta[t][o] * H[t1][h];  
	    for( int r=0; r<no_out_recursives; r++ )
	      DWof[o][w1][r] +=  Ydelta[t][o] * F2[t1][r];  
	    for( int r=0; r<no_out_recursives; r++ )
	      DWob[o][w1][r] +=  Ydelta[t][o] * B2[t1][r];  
	  }
	}	
	DWot[o] += Ydelta[t][o];  
      }
    }
  }

  if( no_globals>0 ) {
    for( int g=0; g<no_globals; g++ ) {
      for( int r=0; r<no_out_recursives; r++ )
	DWgf[g][r] += Gdelta[g] * F2[size1][r];
      for( int r=0; r<no_out_recursives; r++ )
	DWgb[g][r] += Gdelta[g] * B2[0][r];
      DWgt[g] += Gdelta[g];  
    }
  }
    
  // calculate states delta
  for( int t=0; t<size; t++ ) {    

    EmptyArray(Hdelta[t],no_hiddens);
    EmptyArray(F2delta[t],no_out_recursives);
    EmptyArray(B2delta[t],no_out_recursives);
    
    if( no_outputs>0 ) {
      for( int t1=t+win_out,w1=0; t1>=t-win_out; t1--,w1++ ) { 
	if( t1>=0 && t1<size ) {	
	  for( int o=0; o<no_outputs; o++ ) {
	    for( int h=0; h<no_hiddens; h++ )     
	      Hdelta[t][h] += Ydelta[t1][o] * Woh[o][w1][h];
	    for( int r=0; r<no_out_recursives; r++ )
	      F2delta[t][r] += Ydelta[t1][o] * Wof[o][w1][r];
	    for( int r=0; r<no_out_recursives; r++ ) 
	      B2delta[t][r] += Ydelta[t1][o] * Wob[o][w1][r];
	  }
	}	
      }	
    }	
    
    if( no_globals>0 ) {
      if( t==size1 ) {
	for( int g=0; g<no_globals; g++ ) {
	  for( int r=0; r<no_out_recursives; r++ )
	    F2delta[size1][r] += Gdelta[g] * Wgf[g][r];
	}	
      }
      if( t==0 ) {
	for( int g=0; g<no_globals; g++ ) {
	  for( int r=0; r<no_out_recursives; r++ ) 
	    B2delta[0][r] += Gdelta[g] * Wgb[g][r];
	}	
      }
    }	
      
    for( int h=0; h<no_hiddens; h++ )
      Hdelta[t][h] *= PROPAGATEB(H[t][h]);
    for( int r=0; r<no_out_recursives; r++ )
      F2delta[t][r] *= PROPAGATEB(F2[t][r]);
    for( int r=0; r<no_out_recursives; r++ )
      B2delta[t][r] *= PROPAGATEB(B2[t][r]);    
  }	

  //-------------------------------------
  // back propagate through state layer  
  //-------------------------------------

  // back-propagate forward delta
  for( int t=size1; t>=0; t-- ) {    
    if( two_layers ) {
      // back propagate through output layer
      for( int r=0; r<no_out_recursives; r++ ) {
	DW2ft[r] += F2delta[t][r];
	for( int f=0; f<no_recursives; f++ )
	  DW2ff[r][f] += F2delta[t][r] * F[t][f];
      }          
      for( int f=0; f<no_recursives; f++ ) {
	REAL delta = 0.;
	for( int r=0; r<no_out_recursives; r++ ) 
	  delta += F2delta[t][r] * W2ff[r][f];
	Fdelta[t][f] = delta * PROPAGATEB(F[t][f]);
      }      
    }

    // back propagate through hidden layer
    for( int f=0; f<no_recursives; f++ ) {
      DWft[f] += Fdelta[t][f];
      for( int c=0; c<no_codes; c++ ) 
	DWfc[f][c] += Fdelta[t][f] * C[t][c];
      if( t>0 ) { 
	for( int r=0; r<no_out_recursives; r++ )
	  DWff[f][r] += Fdelta[t][f] * F2[t-1][r];
      }
      else { 
	for( int r=0; r<no_out_recursives; r++ )
	  DWff[f][r] += Fdelta[t][f] * ZEROPOINT;
      }
    }    
    if( t>0 ) {
      for( int r=0; r<no_out_recursives; r++ ) {
	REAL delta = 0.;
	for( int f=0; f<no_recursives; f++ ) 
	  delta += Fdelta[t][f] * Wff[f][r];
	F2delta[t-1][r] += delta * PROPAGATEB(F2[t-1][r]);
      }    
    }
  }

  // back-propagate backward delta
  for( int t=0; t<size; t++ ) {    
    if( two_layers ) {
  
      // back propagate through output layer
      for( int r=0; r<no_out_recursives; r++ ) {
	DW2bt[r] += B2delta[t][r];
	for( int b=0; b<no_recursives; b++ )
	  DW2bb[r][b] += B2delta[t][r] * B[t][b];
      }          
      for( int b=0; b<no_recursives; b++ ) {
	REAL delta = 0.;
	for( int r=0; r<no_out_recursives; r++ ) 
	  delta += B2delta[t][r] * W2bb[r][b];
	Bdelta[t][b] = delta * PROPAGATEB(B[t][b]);
      }      
    }

    // back propagate through hidden layer
    for( int b=0; b<no_recursives; b++ ) {
      DWbt[b] += Bdelta[t][b];
      for( int c=0; c<no_codes; c++ ) 
	DWbc[b][c] += Bdelta[t][b] * C[t][c];
      if( t<size1 ) { 
	for( int r=0; r<no_out_recursives; r++ )
	  DWbb[b][r] += Bdelta[t][b] * B2[t+1][r];
      }
      else { 
	for( int r=0; r<no_out_recursives; r++ )
	  DWbb[b][r] += Bdelta[t][b] * ZEROPOINT;
      }
    }    
    if( t<size1 ) {
      for( int r=0; r<no_out_recursives; r++ ) {
	REAL delta = 0.;
	for( int b=0; b<no_recursives; b++ ) 
	  delta += Bdelta[t][b] * Wbb[b][r];
	B2delta[t+1][r] += delta * PROPAGATEB(B2[t+1][r]);
      }    
    }	
  }

  // back propagate through non-recursive states
  if( use_hidden_layer ) {
    for( int t=0; t<size; t++ ) { 
      for( int h=0; h<no_hiddens; h++ ) {
	for( int c=0; c<no_codes; c++ ) 
	  DWhc[h][c] += Hdelta[t][h] * C[t][c];
	DWht[h] += Hdelta[t][h];
      }    
    }
  }

  //-------------------------------------
  // back propagate through code layer
  //-------------------------------------

  if( no_codes!=no_inputs ) {    

    // back propagate delta
    for( int t=0; t<size; t++ ) {       
      for( int c=0; c<no_codes; c++ ) {
	Cdelta[t][c] = 0;
	for( int h=0; h<no_hiddens; h++ ) 
	  Cdelta[t][c] += Hdelta[t][h] * Whc[h][c];
	for( int f=0; f<no_recursives; f++ ) 
	  Cdelta[t][c] += Fdelta[t][f] * Wfc[f][c];
	for( int b=0; b<no_recursives; b++ ) 
	  Cdelta[t][c] += Bdelta[t][b] * Wbc[b][c];
	Cdelta[t][c] *= PROPAGATEB(C[t][c]);
      }       
    }	
    
    // back propagate through layer    
    for( int t=0,t1=start; t<size; t++,t1++ ) {       
      for( int c=0; c<no_codes; c++ ) {
	for( int i=0; i<no_inputs; i++ ) 
	  DWci[c][i] += Cdelta[t][c] * I[t1][i];
	DWct[c] += Cdelta[t][c];
      }    
    }
  }
}

void BiRecursiveNeuralNetwork::Update() 
{
  // apply weight-decay if necessary
  if( ni>0. ) {

    // state/outputs 
    if( no_outputs>0 ) {
      for( int o=0; o<no_outputs; o++ ) {
	for( int w=-win_out,w1=0; w<=win_out; w++,w1++ ) {      
	  for( int h=0; h<no_hiddens; h++ ) 
	    Woh[o][w1][h] -= Woh[o][w1][h]*ni;
	  for( int r=0; r<no_out_recursives; r++ ) 
	    Wof[o][w1][r] -= Wof[o][w1][r]*ni;
	  for( int r=0; r<no_out_recursives; r++ ) 
	    Wob[o][w1][r] -= Wob[o][w1][r]*ni;
	}
	Wot[o] -= Wot[o]*ni;
      }
    }	

    // state/global
    if( no_globals>0 ) {
      for( int g=0; g<no_globals; g++ ) {
	for( int r=0; r<no_out_recursives; r++ ) 
	  Wgf[g][r] -= Wgf[g][r]*ni;
	for( int r=0; r<no_out_recursives; r++ ) 
	  Wgb[g][r] -= Wgb[g][r]*ni;      
	Wgt[g] -= Wgt[g]*ni;
      }
    }	
    
    if( two_layers ) {
      // forward/out forward
      for( int r=0; r<no_out_recursives; r++ ) {
	for( int f=0; f<no_recursives; f++ ) 
	  W2ff[r][f] -= W2ff[r][f]*ni;
	W2ft[r] -= W2ft[r]*ni;
      }

      // backward/out backward
      for( int r=0; r<no_out_recursives; r++ ) {
	for( int b=0; b<no_recursives; b++ ) 
	  W2bb[r][b] -= W2bb[r][b]*ni;
	W2bt[r] -= W2bt[r]*ni;
      }
    }

    // code/hidden
    if( use_hidden_layer ) {
      for( int h=0; h<no_hiddens; h++ ) {
	for( int c=0; c<no_codes; c++ ) 
	  Whc[h][c] -= Whc[h][c]*ni;
	Wht[h] -= Wht[h]*ni;
      }
    }	

    // code/forward
    for( int f=0; f<no_recursives; f++ ) {
      for( int c=0; c<no_codes; c++ ) 
	Wfc[f][c] -= Wfc[f][c]*ni;
      for( int r=0; r<no_out_recursives; r++ ) 
	Wff[f][r] -= Wff[f][r]*ni;
      Wft[f] -= Wft[f]*ni;
    }

    // code/backward
    for( int b=0; b<no_recursives; b++ ) {
      for( int c=0; c<no_codes; c++ ) 
	Wbc[b][c] -= Wbc[b][c]*ni;
      for( int r=0; r<no_out_recursives; r++ ) 
	Wbb[b][r] -= Wbb[b][r]*ni;
      Wbt[b] -= Wbt[b]*ni;
    }
    
    // input/code
    if( no_codes!=no_inputs ) {
      for( int c=0; c<no_codes; c++ ) {
	for( int i=0; i<no_inputs; i++ ) 
	  Wci[c][i] -= Wci[c][i]*ni;
	Wct[c] -= Wct[c]*ni;
      }
    }
  }


  // update state/output weights
  if( no_outputs>0 ) {
    for( int o=0; o<no_outputs; o++ ) {
      for( int w=-win_out,w1=0; w<=win_out; w++,w1++ ) {      
	for( int h=0; h<no_hiddens; h++ ) 
	  Woh[o][w1][h] -= lambda * (DWoh[o][w1][h] + oDWoh[o][w1][h]*mu);
	for( int r=0; r<no_out_recursives; r++ ) 
	  Wof[o][w1][r] -= lambda * (DWof[o][w1][r] + oDWof[o][w1][r]*mu);
	for( int r=0; r<no_out_recursives; r++ ) 
	  Wob[o][w1][r] -= lambda * (DWob[o][w1][r] + oDWob[o][w1][r]*mu);
      }	
      Wot[o] -= lambda * (DWot[o] + oDWot[o]*mu);
    }
  }	
  
  // update state/global weights
  if( no_globals>0 ) {
    for( int g=0; g<no_globals; g++ ) {
      for( int r=0; r<no_out_recursives; r++ ) 
	Wgf[g][r] -= lambda * (DWgf[g][r] + oDWgf[g][r]*mu);
      for( int r=0; r<no_out_recursives; r++ ) 
	Wgb[g][r] -= lambda * (DWgb[g][r] + oDWgb[g][r]*mu);      	
      Wgt[g] -= lambda * (DWgt[g] + oDWgt[g]*mu);
    }
  }	

  if( two_layers ) {
    // update forward/out forward weights
    for( int r=0; r<no_out_recursives; r++ ) {
      for( int f=0; f<no_recursives; f++ ) 
	W2ff[r][f] -= lambda * (DW2ff[r][f] + oDW2ff[r][f]*mu);
      W2ft[r] -= lambda * (DW2ft[r] + oDW2ft[r]*mu);
    }

    // update backward/out backward weights
    for( int r=0; r<no_out_recursives; r++ ) {
      for( int b=0; b<no_recursives; b++ ) 
	W2bb[r][b] -= lambda * (DW2bb[r][b] + oDW2bb[r][b]*mu);
      W2bt[r] -= lambda * (DW2bt[r] + oDW2bt[r]*mu);
    }
  }	
  
  // update code/forward weights
  for( int f=0; f<no_recursives; f++ ) {
    for( int c=0; c<no_codes; c++ ) 
      Wfc[f][c] -= lambda * (DWfc[f][c] + oDWfc[f][c]*mu);
    for( int r=0; r<no_out_recursives; r++ ) 
      Wff[f][r] -= lambda * (DWff[f][r] + oDWff[f][r]*mu);
    Wft[f] -= lambda * (DWft[f] + oDWft[f]*mu);
  }

  // update code/backward weights
  for( int b=0; b<no_recursives; b++ ) {
    for( int c=0; c<no_codes; c++ ) 
      Wbc[b][c] -= lambda * (DWbc[b][c] + oDWbc[b][c]*mu);
    for( int r=0; r<no_out_recursives; r++ ) 
      Wbb[b][r] -= lambda * (DWbb[b][r] + oDWbb[b][r]*mu);
    Wbt[b] -= lambda * (DWbt[b] + oDWbt[b]*mu);
  }

  // update code/hidden weights
  if( use_hidden_layer ) {
    for( int h=0; h<no_hiddens; h++ ) {
      for( int c=0; c<no_codes; c++ ) 
	Whc[h][c] -= lambda * (DWhc[h][c] + oDWhc[h][c]*mu);
      Wht[h] -= lambda * (DWht[h] + oDWht[h]*mu);
    }
  }	

  if( no_codes!=no_inputs ) {
    for( int c=0; c<no_codes; c++ ) {
      for( int i=0; i<no_inputs; i++ ) 
	Wci[c][i] -= lambda * (DWci[c][i] + oDWci[c][i]*mu);
      Wct[c] -= lambda * (DWct[c] + oDWct[c]*mu);
    }
  }  
  
  SwapMatrixes();
}


// serialization

void AssertTag(istream &is, string exp_tag) {
  string tag;
  is >> tag;
  Exception::Assert(tag==exp_tag, "Unexpected tag <%s> in model file while looking for <%s>", tag.c_str(), exp_tag.c_str());  
}
 
istream& BiRecursiveNeuralNetwork::Read(istream &is) 
{
  string tag;
  
  // read dimensions

  AssertTag(is,"NoInputs");        is >> no_inputs;
  AssertTag(is,"NoCodes");         is >> no_codes;
  AssertTag(is,"UseHiddenLayer");  is >> use_hidden_layer;
  AssertTag(is,"NoHiddens");       is >> no_hiddens;
  AssertTag(is,"TwoLayers");       is >> two_layers;
  AssertTag(is,"NoRecursives");    is >> no_recursives;
  AssertTag(is,"NoOutRecursives"); is >> no_out_recursives;
  AssertTag(is,"NoOutputs");       is >> no_outputs;
  AssertTag(is,"WinOut");          is >> win_out;
  AssertTag(is,"NoGlobals");       is >> no_globals;
 
  global_mode = ( no_outputs>0 ) ?0 :1;
  
  // initialize

  SizeMatrixes();
  EmptyMatrixes(true);

  // read weights

  if( no_codes!=no_inputs ) {
    AssertTag(is,"InputCodeWeights");       ReadArray(is,Wci,no_codes,no_inputs);
    AssertTag(is,"ThresholdCodeWeights");   ReadArray(is,Wct,no_codes);
  }	
  if( use_hidden_layer ) {
    AssertTag(is,"CodeHiddenWeights");        ReadArray(is,Whc,no_hiddens,no_codes);
    AssertTag(is,"ThresholdHiddenWeights");   ReadArray(is,Wht,no_hiddens);
  }
  AssertTag(is,"CodeForwardWeights");       ReadArray(is,Wfc,no_recursives,no_codes);
  AssertTag(is,"ForwardForwardWeights");    ReadArray(is,Wff,no_recursives,no_out_recursives);
  AssertTag(is,"ThresholdForwardWeights");  ReadArray(is,Wft,no_recursives);
  AssertTag(is,"CodeBackwardWeights");      ReadArray(is,Wbc,no_recursives,no_codes);
  AssertTag(is,"BackwardBackwardWeights");  ReadArray(is,Wbb,no_recursives,no_out_recursives);
  AssertTag(is,"ThresholdBackwardWeights"); ReadArray(is,Wbt,no_recursives);
  if( two_layers ) {
    AssertTag(is,"ForwardOutForwardWeights");    ReadArray(is,W2ff,no_out_recursives,no_recursives);
    AssertTag(is,"ThresholdOutForwardWeights");  ReadArray(is,W2ft,no_out_recursives);
    AssertTag(is,"BackwardOutBackwardWeights");  ReadArray(is,W2bb,no_out_recursives,no_recursives);
    AssertTag(is,"ThresholdOutBackwardWeights"); ReadArray(is,W2bt,no_out_recursives);
  }
  if( no_outputs>0 ) {
    AssertTag(is,"HiddenOutputWeights");      ReadArray(is,Woh,no_outputs,2*win_out+1,no_hiddens);
    AssertTag(is,"ForwardOutputWeights");     ReadArray(is,Wof,no_outputs,2*win_out+1,no_out_recursives);
    AssertTag(is,"BackwardOutputWeights");    ReadArray(is,Wob,no_outputs,2*win_out+1,no_out_recursives);
    AssertTag(is,"ThresholdOutputWeights");   ReadArray(is,Wot,no_outputs);
  }
  if( no_globals>0 ) {
    AssertTag(is,"ForwardGlobalWeights");     ReadArray(is,Wgf,no_globals,no_out_recursives);
    AssertTag(is,"BackwardGlobalWeights");    ReadArray(is,Wgb,no_globals,no_out_recursives);
    AssertTag(is,"ThresholdGlobalWeights");   ReadArray(is,Wgt,no_globals);
  }

  return is;
}	

ostream& BiRecursiveNeuralNetwork::Write(ostream &os) 
{
  // write dimensions

  os << "NoInputs "        << no_inputs         << endl;
  os << "NoCodes "         << no_codes          << endl;
  os << "UseHiddenLayer "  << use_hidden_layer  << endl;
  os << "NoHiddens "       << no_hiddens        << endl;
  os << "TwoLayers "       << two_layers        << endl;
  os << "NoRecursives "    << no_recursives     << endl;
  os << "NoOutRecursives " << no_out_recursives << endl;
  os << "NoOutputs "       << no_outputs        << endl;  
  os << "WinOut "          << win_out           << endl;
  os << "NoGlobals "       << no_globals        << endl;
  
  // write weights

  if( no_codes!=no_inputs ) {
    os << "InputCodeWeights ";       WriteArray(os,Wci,no_codes,no_inputs);                   os << endl;
    os << "ThresholdCodeWeights ";   WriteArray(os,Wct,no_codes);                             os << endl;
  }
  if( use_hidden_layer ) {
    os << "CodeHiddenWeights ";        WriteArray(os,Whc,no_hiddens,no_codes);                  os << endl;
    os << "ThresholdHiddenWeights ";   WriteArray(os,Wht,no_hiddens);                           os << endl;
  }
  os << "CodeForwardWeights ";       WriteArray(os,Wfc,no_recursives,no_codes);               os << endl;
  os << "ForwardForwardWeights ";    WriteArray(os,Wff,no_recursives,no_out_recursives);      os << endl;
  os << "ThresholdForwardWeights ";  WriteArray(os,Wft,no_recursives);                        os << endl;
  os << "CodeBackwardWeights ";      WriteArray(os,Wbc,no_recursives,no_codes);               os << endl;
  os << "BackwardBackwardWeights ";  WriteArray(os,Wbb,no_recursives,no_out_recursives);      os << endl;
  os << "ThresholdBackwardWeights "; WriteArray(os,Wbt,no_recursives);                        os << endl;
  if( two_layers ) {
    os << "ForwardOutForwardWeights ";    WriteArray(os,W2ff,no_out_recursives,no_recursives); os << endl;
    os << "ThresholdOutForwardWeights ";  WriteArray(os,W2ft,no_out_recursives);               os << endl;
    os << "BackwardOutBackwardWeights ";  WriteArray(os,W2bb,no_out_recursives,no_recursives); os << endl;
    os << "ThresholdOutBackwardWeights "; WriteArray(os,W2bt,no_out_recursives);               os << endl;    
  }
  if( no_outputs>0 ) {
    os << "HiddenOutputWeights ";      WriteArray(os,Woh,no_outputs,2*win_out+1,no_hiddens);        os << endl;
    os << "ForwardOutputWeights ";     WriteArray(os,Wof,no_outputs,2*win_out+1,no_out_recursives); os << endl;
    os << "BackwardOutputWeights ";    WriteArray(os,Wob,no_outputs,2*win_out+1,no_out_recursives); os << endl;
    os << "ThresholdOutputWeights ";   WriteArray(os,Wot,no_outputs);                               os << endl;
  }
  if( no_globals>0 ) {
    os << "ForwardGlobalWeights ";     WriteArray(os,Wgf,no_globals,no_out_recursives); os << endl;
    os << "BackwardGlobalWeights ";    WriteArray(os,Wgb,no_globals,no_out_recursives); os << endl;
    os << "ThresholdGlobalWeights ";   WriteArray(os,Wgt,no_globals);                   os << endl;
  }	

  return os;
}
  
