#include <iostream>
#include <string>
#include <string.h>
#include <getopt.h>
#include <wordexp.h>
#include <stdlib.h>

using namespace std;

class CommandLineOption
{
private:

  typedef unsigned int uint;
  static const option optionNames[];

public:

  ////////////////////////////
  // Command line variables //
  ////////////////////////////

  string fasta;          // -f --fasta: fasta dir or file
  string output;         // -o --output: output dir (default=PWD)
  int alternatives;      // -a --alternatives: alternative connectivity patterns (default=3)
  string format;         // -F --format: format type (ascii,html, default=ascii)
  string psi2;           // -p --psi2: psi2 dir or file
  bool use_pssm;         // -P --usepssm: use pssm instead of counts for profiles (default=false)
  string input;          // -i --input: file containing list of input (psi2/fasta) files
  string rootdir;        // -r --rootdir: root predictor directory (default=PWD)
  string pkgdatadir;     // -k --pkgdatadir: package data directory containing Models (default=PWD)
  string blastdb;        // -d --blastdb: blastpgp -d option (default=<pkgdatadir>/Models/PsiBlast/sp+trembl)
  bool cleanpred;        // -c --cleanpred: cleanup intermediate prediction files (default=false) 
  bool known_bs;         // -C --knownbondingstate: assume bonding state is known 
                         //      (one file for each chain in directory <rootdir>/Predictions/Viterbi)  
  string version;        // -v --version: disulfinder version
  string wdir;
  string modeldir;
  string preddir;
  string psidir;

  // Constructor
  CommandLineOption(int argc = 0, char *argv[]=NULL);
  void usage(char* progname);
  

 private:

  void parse_args(int argc, char *argv[]);

};


const option CommandLineOption::optionNames[] =
{ 
  { "fasta",         required_argument, 0, 'f'},
  { "output",        required_argument, 0, 'o'},
  { "alternatives",  required_argument, 0, 'a'},
  { "format",        required_argument, 0, 'F'},
  { "psi2",          required_argument, 0, 'p'},
  { "usepssm",       no_argument,       0, 'P'},
  { "input",         required_argument, 0, 'i'},
  { "rootdir",       required_argument, 0, 'r'},
  { "pkgdatadir",    required_argument, 0, 'k'},
  { "blastdb",       required_argument, 0, 'd'},
  { "known_bs",      no_argument,       0, 'C'},
  { "version",       no_argument,       0, 'v'},
  { "help",          no_argument,       0, '?'},
  { 0,              0,                 0, 0  }
};
  
  
#ifndef DEFAULT_ROOTDIR
#define DEFAULT_ROOTDIR "~/disulfinder" // this really is the workdir with stuff like ./Predictions ./tmp
#endif
#ifndef DEFAULT_PKGDATADIR
#define DEFAULT_PKGDATADIR "/usr/share/disulfinder"
#endif

CommandLineOption::CommandLineOption(int argc, char *argv[]) :
     fasta(""),     
     output(getenv("PWD")),
     alternatives(3),
     format("ascii"),
     psi2(""),
     use_pssm(false),
     input(""),
     rootdir(DEFAULT_ROOTDIR),
     pkgdatadir(DEFAULT_PKGDATADIR),
     blastdb(""),
     cleanpred(false),
     known_bs(false),
     version("1.2")
{
  int c;
  int* opti = 0;
  
  // you must put ':' only after parameters that require an argument
  // so after 'a' and '?' we don't put ':'

  while ((c = getopt_long(argc, argv, "f:o:a:F:p:i:r:k:d:cvPC?", optionNames, opti)) != -1) {
    switch(c) {
    case 'f':
      fasta = optarg;
      break;
    case 'o':
      output = optarg;
      break;
    case 'a':
      alternatives = atoi(optarg);
      break;
    case 'F':
      format = optarg;
      break;
    case 'p':
      psi2 = optarg;
      break;   
    case 'P':
      use_pssm = true;
      break;
    case 'i':
      input = optarg;
      break;
    case 'r':
      rootdir = optarg;
      break;
    case 'k':
      pkgdatadir = optarg;
      break;
    case 'd':
      blastdb = optarg;
      break;
    case 'c':
      cleanpred = true;
      break;
    case 'C':
      known_bs = true;
      break;
    case 'v':
      cerr << "disulfinder version " << version << endl; 
      exit(0);
    case '?':
    default:
      usage(*argv);      
    }
  }

  // expand possible ~ in rootdir:
  wordexp_t p;
  if( wordexp( rootdir.c_str(), &p, WRDE_NOCMD ) == 0 )
  {
    if( p.we_wordc > 0 )
    {
      rootdir = p.we_wordv[0];
    }
    wordfree(&p);
  }
  char mkdircmd[1024];
  snprintf( mkdircmd, 1024, "mkdir -p '%s/tmp' '%s/Predictions/All' '%s/Predictions/Bondstate/BRNN' '%s/Predictions/Bondstate/Viterbi' '%s/Predictions/Connectivity' '%s/Predictions/Pssm'", rootdir.c_str(), rootdir.c_str(), rootdir.c_str(), rootdir.c_str(), rootdir.c_str(), rootdir.c_str() );
  if( system( mkdircmd ) != 0 ) { cerr << "command '" << mkdircmd << "' failed"; exit(1); }

  // set environmental variables 
  wdir = rootdir + "/tmp/";
  modeldir = pkgdatadir + "/Models/";
  preddir = rootdir + "/Predictions/";
  psidir = rootdir + "/Predictions/Pssm/";

  if(blastdb == "") blastdb = "/data/sp+trembl";
}

void CommandLineOption::usage(char* progname) {
  cerr << "usage: " << basename(progname) << " ..." << endl;
  cerr << "\t-f --fasta: input in fasta format, either as single file or directory" << endl;
  cerr << "\t-p --psi2: input in psi2 format, either a single file or a directory" << endl;
  cerr << "\t-i --input: file containing list of input files from directory\n"
       << "\t            (assumes -p or -f specify a directory)" << endl;
  cerr << "\t-a --alternatives: alternative connectivity patterns (default=3)" << endl;
  cerr << "\t-F --format: output format type (ascii or html default=ascii)" << endl;
  cerr << "\t-o --output: output dir where predictions will be saved (default=PWD)" << endl;
  cerr << "\t-r --rootdir: predictor working directory (default=" << DEFAULT_ROOTDIR << ")" << endl;
  cerr << "\t-k --pkgdatadir: predictor data directory (default=" << DEFAULT_PKGDATADIR << ")" << endl;
  cerr << "\t-d --blastdb: blastpgp -d option (default=/data/sp+trembl)" << endl;
  cerr << "\t-c --cleanpred: cleanup intermediate prediction files (default=false)" << endl; 
  cerr << "\t-P --usepssm: use pssm instead of counts for profiles (default=false)" << endl;
  cerr << "\t-C --knownbondingstate: assume bonding state is known\n" 
       << "\t     (one file for each chain in directory <rootdir>/Predictions/Bondstate/Viterbi)" 
       << "\t     (default=false)" << endl;  
  cerr << "\t-v --version: disulfinder version" << endl;
  cerr << "\t-?, --help: this message" << endl;
  exit(0);
}

// vim:ai:ts=2:et:
