#include <time.h>
#include <dirent.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fstream>
#include "Input/utils.h"
#include "Input/createKernelDataset.h"
#include "Input/createBRNNDataset.h"
#include "Output/utils.h"
#include "SVM/SVMClassifier.h"
#include "FSA-Alignment/viterbi_aligner.h"
#include "BRNN/rnn_util.h"
#include "disulfinder.h"
#include "NN/brnn-test.h"

#define MAXBRIDGES 5
#define CHAIN "chain"
#define LICENSEKEY "LICENSE.KEY"

#ifdef COMMERCIAL
#define LICENSEDURATION 380*24*3600 // 365 days + 15 trial days
#define LICENSEXOR 531892909 // long int

void checkLicense(const string& keyfile){

	ifstream in(keyfile.c_str());
  	Exception::Assert(in.good(), "Error: could not open license key file %s", keyfile.c_str());  

	time_t xortime;
	in >> xortime;
	in.close();
	time_t currtime = time(NULL);
//	srand48(currtime);
//	long int LICENSEXOR = lrand48();
	time_t basetime = xortime ^ LICENSEXOR;

	double elapsedtime = difftime(currtime,basetime);
	
  	Exception::Assert(elapsedtime <= LICENSEDURATION, "Error: your license key is expired! please get a new one"); 
	
//	cerr << basetime << " " << currtime <<  " " << elapsedtime << " " << LICENSEXOR << endl;
}
#else
void checkLicense(const string& keyfile){}
#endif
  

string getFastaID(const string& header, unsigned int chainnum){
  
  string buf = header.substr(1);

  string::size_type k = buf.find_first_of(" \t");
  if(k != string::npos)
    buf = buf.substr(0,k-1);
  
  k = buf.rfind("|");
  if(k != string::npos)
    buf = buf.substr(k+1);
  
  if(buf == ""){
    ostringstream os;
    os << CHAIN << "_" << chainnum;
    buf = os.str();
  }
  return buf;
}

void saveFasta(const string& header,
	       const string& sequence,
	       const string& fastafile){
  
  ofstream out(fastafile.c_str());
  Exception::Assert(out.good(), "Error: could not write temporary file %s", fastafile.c_str());  
  out << header << "\n" << sequence << endl;
  out.close();
}

void clean(const char * dir_){
  
  DIR * dir;
  struct dirent *dirent_p;
  struct stat st;
  
  assert((dir = opendir(dir_)));  
  while((dirent_p = readdir(dir))){
    if (!strcmp(dirent_p->d_name,".") || !strcmp(dirent_p->d_name, ".."))
      continue;
    
    string filename = (string)dir_ + "/" + dirent_p->d_name;
    stat(filename.c_str(), &st);
    if (S_ISREG (st.st_mode)){
      if (remove(filename.c_str()) != 0)
	cerr << "ERROR could not remove file " << filename << endl;  
    }
    else
      clean(filename.c_str());
  }
  closedir(dir);
}

void defaultOutput(const string& sequence, const char* idseq, ostream& out, const string& __format = "ascii" )
{
		if( __format == "html" ) out << "<html><head><title>Cysteines Disulfide Bonding State and Connectivity Predictor</title></head><body><pre>\n";
  out << "-------------------------------------------------------------------------------\n"
      << "          Cysteines Disulfide Bonding State and Connectivity Predictor         \n"
      << "-------------------------------------------------------------------------------\n"
      << "\n\n"    
      << "Chain identifier: " << idseq << "\n\n\n"    
      << sequence << "\n\n"    
      << "The sequence contains no cysteine !!\n\n"    
      << "-------------------------------------------------------------------------------\n"
      << "Please cite:\n\n"
      << "A. Ceroni, A. Passerini, A. Vullo and P. Frasconi.\n"
      << "\"DISULFIND: a Disulfide Bonding State and Cysteine Connectivity Prediction Server\"\n" 
      << "Nucleic Acids Research, 34(Web Server issue):W177--W181, 2006.\n\n"
    
      << "Questions and comments are very appreciated.\n"
      << "Please, send email to: cystein@dsi.unifi.it\n\n"
    
      << "Copyright 2006, Alessio Ceroni, Andrea Passerini, Alessandro Vullo\n\n"
      << "-------------------------------------------------------------------------------\n";  
		if( __format == "html" ) out << "</pre></body></html>\n";
}

void runBRNN(const string& data,
	     const string& target,
	     const string& size,
	     const string& model,
	     const string& output,
	     const string& logfile)
{
  ofstream out(logfile.c_str());
  Exception::Assert(out.good(), "Error: could not  write temporary file %s", logfile.c_str());
  BRNNTestOptions options(target,data,size,model,output);
  brnnTest(options,out);
}	   

void SvmPredict(const char * model, const char * input, const char * output){
   
  ifstream in_model(model);
  Exception::Assert(in_model.good(), "Error: could not read model file %s", model);
  
  SVMClassifier classifier(in_model);
  classifier.classify(input,output);
}

void ViterbiAligner(const char * model, const char * input, const char * output){

  ofstream out(output);
  Exception::Assert(out.good(), "Error: could not write temporary file %s", output);
  int res = viterbi_aligner(model,input,out);  
  Exception::Assert(res == 0, "viterbi_aligner returned %d", res );
}

void ConnectivityPrediction(const char * idseq,
			    const string & psifile,
			    const string & modeldir, 			    
			    const string & viterbioutput, 
			    const string & rnninput,
			    const string & rnnoutput,
			    int alternatives)
{			    
  // computing number of bridges
  int bridges = number_of_disulphides(viterbioutput.c_str());
  
  if(bridges != 0){
    if(bridges <= MAXBRIDGES){

      if(bridges == 1){
	cerr << "Computing trivial connectivity prediction ... "; 
	ofstream out(rnnoutput.c_str());
	Exception::Assert(out.good(), "Error: could not write temporary file %s", rnnoutput.c_str());
	out << 1 << "\n" << 1 << " " << 2 << endl;
	out.close();
	cerr << "done" << endl;
      }
      else{ 
	cerr << "Creating input data for connectivity prediction ... ";  
	create_connectivity_input_data(viterbioutput, psifile, 5, bridges, rnninput);
	cerr << "done" << endl;

	// predict connectivity 
	ostringstream rnnoss; 
	rnnoss << modeldir << "/conn/data/" << bridges << '/';	
	string rnnconfig = rnnoss.str() + "conf",
	  rnnmodel = rnnoss.str() + "net",
	  rnntraindir =  modeldir + "/conn/data/graphs",
	  rnntrain = rnnoss.str() + "train";
	
	cerr << "Predicting disulphide connectivity ... ";	
	rnn_predict_disulphides(rnnconfig, rnnmodel, rnntraindir, rnntrain, 
				rnninput, rnnoutput, alternatives);  
	cerr << "done" << endl;     
      }
    }
    else{
      cerr << "too many bridges" << endl;
    }
  }
}

void AssembleOutput(const char * viterbiprediction,
		    const char * brnnprediction,
		    const char * connprediction,
		    const char * allprediction)
  {
    unsigned int label;      
    ofstream out(allprediction);
    Exception::Assert(out.good(), "Error: could not write temporary file %s", allprediction);
    
    // output bond information
    out << "#bonds" << endl;    
    ifstream inviterbi(viterbiprediction);
    ifstream inbrnn(brnnprediction);
    assert(inviterbi.good());
    
    if(inbrnn.good()){
      double posprob, negprob;
      while(inviterbi.good() && inbrnn.good()){
	inviterbi >> label >> ws;
	inbrnn >> negprob >> posprob >> ws;
	out << label << " " << negprob << " " << posprob << endl;
	inviterbi.peek();
	inbrnn.peek();
      }
      inviterbi.close();
      inbrnn.close();
    }
    else{ // no confidence for bonding state predictions
      while(inviterbi.good()){
	inviterbi >> label >> ws;
	out << label << " " << 1-label << " " << label << endl;
	inviterbi.peek();
      }
      inviterbi.close();      
    }
    // output connectivity information if any
    ifstream inconn(connprediction);
    if (inconn.good()){
      string buf;
      out << "#bridges" << endl;
      while(getline(inconn, buf))
	if (buf != "")
	  out << buf << endl;
      inconn.close();          
    }
    out.close();
  }


void disulfind(const CommandLineOption& options,
	       const char* idseq,
	       const string& psifile,
	       const char* outfile)
{
  int alternatives = options.alternatives;
  string format = options.format;
  string wdir = options.wdir;
  string modeldir = options.modeldir;
  string pdir = options.preddir;


  // open output file
  ofstream outstream(outfile);
  Exception::Assert(outstream.good(), "Error: could not write output file %s", outfile);
  
  // getting protein sequence
  Protein info;
  double profile_prior = 1.; // warning: default would be 0.5!! 
  ifstream inprofile(psifile.c_str());
  Exception::Assert(inprofile.good(), "Error: could not open profile file %s", psifile.c_str());
  info.ReadPsiBlast2(inprofile,profile_prior);
  string sequence=info.GetSequence();
  
  // check if there is any cysteine, otherwise exit now
  if(sequence.find('C') == string::npos){
    defaultOutput(sequence, idseq, outstream, format);
    outstream.flush();
    return;
  }

  // Predict bonding state 
  string wfile = wdir + idseq;
  string brnnoutput = "";
  string viterbidir = pdir + "Bondstate/Viterbi/";   
  string viterbioutput = viterbidir + idseq;
  if(!options.known_bs){
    // create svm input data
    cerr << "Creating input data for svm prediction ... "; 
    string svminput = wfile + ".kernel.input";
    string descstats = modeldir + "descriptor.stats";
    createDataKernel(info, 7, svminput, descstats, options.use_pssm);
    cerr << "done" << endl;  
    // svm prediction
    cerr << "Computing SVM prediction ... ";
    string svmmodel = modeldir + "kernel.model";
    string svmprediction = wfile + ".kernel.prediction";
    SvmPredict(svmmodel.c_str(),svminput.c_str(),svmprediction.c_str());
    cerr << "done" << endl;
    
    // create brnn input data 
    cerr << "Creating input data for brnn prediction ... "; 
    string brnndata = wfile + ".brnn.data";
    string brnnlabel = wfile + ".brnn.label";
    string brnnsize = wfile + ".brnn.size";
    createDataBRNN(info, svminput, svmprediction, brnndata, brnnlabel, brnnsize);
    cerr << "done" << endl;  
    // brnn prediction
    cerr << "Computing BRNN prediction ... " << endl;
    string brnnmodel = modeldir + "brnn.model";
    string brnnlog = wfile + ".brnn.log";
    brnnoutput = pdir + "Bondstate/BRNN/" + idseq;
    runBRNN(brnndata,brnnlabel,brnnsize,brnnmodel,brnnoutput,brnnlog);
    cerr << "done" << endl;
    
    // viterbi aligner adjusting non consistent predictions
    string viterbimodel = modeldir + "automa.model";
    cerr << "Computing Viterbi alignment ... ";
    ViterbiAligner(viterbimodel.c_str(),brnnoutput.c_str(),viterbioutput.c_str());
    cerr << "done" << endl;
  }
  // predict connectivity
  setenv("OPTIONTYPE", "Default", 1); // default to standard option
  string conninput = wfile + ".conn.input";
  string connoutput = pdir + "Connectivity/" + idseq;
  ConnectivityPrediction(idseq, psifile, modeldir, viterbioutput, conninput, connoutput, alternatives);

  // assemble output
  string prediction = pdir + "All/" + idseq;
  AssembleOutput(viterbioutput.c_str(), brnnoutput.c_str(), connoutput.c_str(), prediction.c_str());
  // write output
  if (format == "html")
    makeHTMLOutput(sequence, prediction.c_str(), alternatives, idseq, outstream);
  else
    makeASCIIOutput(sequence, prediction.c_str(), alternatives, idseq, outstream);

  // cleanup
  cerr << "Cleaning up temporary files" << endl;
  clean(wdir.c_str());
  if (options.cleanpred){
    cerr << "Cleaning up prediction files" << endl;
    clean(pdir.c_str());
  }
}

void computeProfile(const CommandLineOption& options,
		    const string& idseq,
		    const string& fasta,
		    const string& psifile)
{
  string chk = options.wdir + idseq + ".chk";
  string align = options.wdir + idseq + ".align";
  
  string command = (string)"blastpgp -d " + options.blastdb 
    + " -i " + fasta + " -j 2 -a 1 -C " 
    + chk + " -Q " + psifile + " > "+ align;
  
  int ret = system(command.c_str());
  
  Exception::Assert(ret != -1, "ERROR in executing blastpgp ... exiting");    
}

void processFasta(const CommandLineOption& options,
		  unsigned int chainnum, 
		  const string& header, 
		  const string& sequence)
{    
  string idseq = getFastaID(header,chainnum);
  cerr << "Processing chain " << idseq << endl;
  string fastafile = options.wdir + idseq + ".fasta";
  saveFasta(header,sequence,fastafile);
  cerr << "Computing multiple alignment profile ... ";
  string psifile = options.psidir + "/" + idseq;	  
  computeProfile(options,idseq,fastafile,psifile);
  cerr << "done" << endl;
  string outfile = options.output + "/" + idseq;
  cerr << __FILE__ << ":" << __LINE__ << " Running predictor\n";
  disulfind(options,idseq.c_str(),psifile.c_str(),outfile.c_str());      
  cerr << "Prediction completed" << endl;	  
}

void processFasta(const CommandLineOption& options)
{
  struct stat st;  
  const char * fasta = options.fasta.c_str();

  stat(fasta, &st);

  if(S_ISDIR (st.st_mode)){
    Exception::Assert(options.input != "", "Error: list file (option -i) expected when option -f specifies a directory");
    ifstream in(options.input.c_str());
    Exception::Assert(in.good(), "Error: could not open input file %s", options.input.c_str());
    
    string idseq;
    while(getline(in,idseq)){
      cerr << "Processing chain " << idseq << endl;
      string fastafile = options.fasta + "/" + idseq;
      string psifile = options.psidir + idseq;
      cerr << "Computing multiple alignment profile ... ";
      computeProfile(options,idseq,fastafile,psifile);
      cerr << "done" << endl;
      string outfile = options.output + "/" + idseq;      
      cerr << __FILE__ << ":" << __LINE__ << " Running predictor\n";
      disulfind(options,idseq.c_str(),psifile.c_str(),outfile.c_str());      
      cerr << "Prediction completed" << endl;
    }  
  }
  else if (S_ISREG(st.st_mode)){
    ifstream in(fasta);
    Exception::Assert(in.good(), "Error: could not open input file %s", fasta);
    string buf;
    string header;
    string sequence;
    unsigned int chainnum = 0;
    while(getline(in,buf)){
      if(buf[0] == '>'){
	if(chainnum != 0)
	  processFasta(options, chainnum, header, sequence);
	header = buf;
	chainnum++;
	sequence.clear();
      } 
      else
	sequence += buf;
    }
    if(chainnum != 0)
      processFasta(options, chainnum, header, sequence);
    in.close();
  }
  else
    Exception::Assert(false, "Please specificy either regular file or directory with -f option");
}

void processPSI2(const CommandLineOption& options)
{
  struct stat st;  
  const char * psi2 = options.psi2.c_str();

  stat(psi2, &st);

  if(S_ISDIR (st.st_mode)){    
    Exception::Assert(options.input != "", "Error: list file (option -i) expected when option -p specifies a directory");
    ifstream in(options.input.c_str());
    Exception::Assert(in.good(), "Error: could not open input file %s", options.input.c_str());
    
    string idseq;
    while(getline(in,idseq)){
      cerr << "Processing chain " << idseq << endl;
      string psifile = options.psi2 + "/" + idseq;
      string outfile = options.output + "/" + idseq;
      cerr << __FILE__ << ":" << __LINE__ << " Running predictor\n";
      disulfind(options,idseq.c_str(),psifile.c_str(),outfile.c_str());      
      cerr << "Prediction completed" << endl;
    }
  }
  else if (S_ISREG(st.st_mode)){
    const char * idseq = basename(psi2);
    cerr << "Processing chain " << idseq << endl;
    string outfile = options.output + "/" + idseq;
    cerr << __FILE__ << ":" << __LINE__ << " Running predictor > '" << outfile << "'\n";
    disulfind(options,idseq,psi2,outfile.c_str());      
    cerr << "Prediction completed" << endl;
  }
  else
    Exception::Assert(false, "Please specificy either regular file or directory with -p option");
}



int main(int argc, char ** argv){

  // popularity contest
  //system("pp_popcon_cnt -p disulfinder");

  CommandLineOption options(argc,argv);
  
  try{
    //checkLicense(options.rootdir + "/" + LICENSEKEY);
    
    Exception::Assert(options.fasta != "" || options.psi2 != "",
		      "Please specify either -p or -f option");
    if(options.fasta != "")
      processFasta(options);  
    
    if(options.psi2 != "")
      processPSI2(options);    
  }
  catch(Exception *e) {
    cerr << e->GetMessage() << "\n" << endl;
    delete e;
    options.usage(*argv);
    return 1;
  }
}
