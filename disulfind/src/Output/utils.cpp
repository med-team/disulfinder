#include <sstream>
#include <math.h>
#include "utils.h"

using namespace std;

double read_bridges(vector< pair<int,int> >& bridges, unsigned int nobonded, istream& in) 
{    
  double bridgesconfidence = 0;
  unsigned int nobridges = nobonded/2;
  string buf;
  
  // check if more patterns are available
  in.peek();
  if(!in.good())
    return bridgesconfidence;
  
  // skip white lines
  while(getline(in, buf))
    if (buf != "")
      break;
  
  // get confidence
  bridgesconfidence = atof(buf.c_str());
  
  // get connections
  int first,second;
  for(unsigned int i = 0; i < nobridges; i++){
    in >> first >> second >> ws;
    bridges.push_back(make_pair(first,second));
  }
  return bridgesconfidence;
}

vector<string> make_rulers(const vector< pair<int,int> >& bridges,
			   unsigned int noaa,  
			   const vector<int>& bonded_cysteines) 
{
  unsigned int nobridges = bridges.size();
  
  //read bridges (<index first cysteine> <index second cysteine>)
  vector< pair<int,int> > ind_bridges;
  for(unsigned int i=0; i<nobridges; i++ ) 
    ind_bridges.push_back(make_pair(bonded_cysteines[(bridges[i].first)-1],bonded_cysteines[(bridges[i].second)-1]));
  
  return make_bridges(nobridges, noaa, ind_bridges);
}

vector<string> make_bridges(unsigned int nobridges, 
			    unsigned int noaa, 
			    const vector< pair<int,int> >& ind_bridges)
{  
  vector<string> bridges;

  if( nobridges==0 )
    return bridges;
  
  unsigned int start_bridge[nobridges], end_bridge[nobridges];

  for(unsigned int i=0; i < nobridges; i++){
    start_bridge[i] = ind_bridges[i].first;
    end_bridge[i] = ind_bridges[i].second;
  }
  
  // count maximum number of crossing bridges
  unsigned int nocrossings = 0;
  unsigned int noactivebridges = 0;
  for(unsigned int i=0; i<noaa; i++ ) {
    // look for starting bridges
    for(unsigned int j=0; j<nobridges; j++ ) 
      if( start_bridge[j]==i )
	noactivebridges++;
    // look for ending bridges
    for(unsigned int j=0; j<nobridges; j++ ) 
      if( end_bridge[j]==i )
	noactivebridges--;
    if( noactivebridges>nocrossings ) 
      nocrossings = noactivebridges;
  }

  // make bridges representations
  bridges.resize(nocrossings+1);
  unsigned int activebridges[nobridges];
  for(unsigned int i=0; i<nocrossings; i++ ) {
    activebridges[i] = 0;
    bridges[i] = "";
  }
  bridges[nocrossings] = "";

  for(unsigned int i=0; i<noaa; i++ ) {
    unsigned int vertical = 0;

    // look for starting bridges
    for(unsigned int j=0; j<nobridges; j++ ) 
      if( start_bridge[j]==i ) // occupy a line
	for(unsigned int l=0; l<nocrossings; l++ ) 
	  if( activebridges[l]==0 ) {
	    activebridges[l] = j+1;
	    vertical = l+1;
	    break;
	  }        
    // look for ending bridges
    for(unsigned int j=0; j<nobridges; j++ ) 
      if( end_bridge[j]==i ) // free a line
	for(unsigned int l=0; l<nocrossings; l++ ) 
	  if( activebridges[l]==(j+1) ) {
	    activebridges[l] = 0;
	    vertical = l+1;
	    break;
	  }           
    // look for busy lines
    if( vertical>0 )
      bridges[0] += '|' ;
    
    else
      bridges[0] += ' ';

    for(unsigned int l=1; l<=nocrossings; l++ )
      if( l==vertical ) 
	bridges[l] += '+';      
      else if( l<vertical ) 
	bridges[l] += '|';	      
      else if( activebridges[l-1]==0 ) 
	bridges[l] += ' ';	    
      else if( activebridges[l-1]>0 ) 
	bridges[l] += '-';	
  }

  return bridges;
}

void print_bridges(unsigned int i, unsigned int noaa, 
		   const vector<string>& bridges_rulers, 
		   ostream& out) 
{
  // print bridges
  for(int l= bridges_rulers.size()-1; l>=0; l-- ){
    out << "         ";
    for(unsigned int j=0, c=i; j<79 && c<noaa; j++,c++) 	  
      out <<  bridges_rulers[l][c];
    out << endl;
  }	
        
  // print ruler  
  out << "         ";
  int wait = 0;
  for(unsigned int j=0, c=i; j<79 && c<noaa; j++,c++){	  
    if( wait>0 )
      wait--;
    int ca = c+1;
    if( ca%10==0 ) {
      out << ca;
      wait=1;
      if( ca>=10 ) 
	wait++;
      if( ca>=100 ) 
	wait++;
      if( ca>=1000 ) 
	wait++;
    }
    else if( wait==0 ) 
      out << ".";
  }  
  out << endl;
}


unsigned int readBondingState(vector<bool> & bondstate, 
			      vector<bool> & bondflipped,
			      vector<int> & bondconfidence,
			      istream & in)
{
  string buf;
  unsigned int nobonded = 0;

  while(getline(in,buf)){

    if( buf == "#bonds" )
      continue;
    if( buf == "#bridges" ) 
      break;
    
    bool label;
    double negprob,posprob;
    istringstream inbuf(buf);
    inbuf >> label >> negprob >> posprob >> ws;
    bondstate.push_back(label);
    nobonded += label;
    
    int confidence = (int)(fabs(posprob - 0.5)*20);
    confidence = (confidence < 10) ? confidence : 9; // single digit of confidence
    bondconfidence.push_back(confidence);
    bool flipped = ((2*label-1)*(posprob-0.5) > 0 || (posprob-0.5  == 0 && label == 1)) ? 0 : 1;
    bondflipped.push_back(flipped);
  }
  return nobonded;
}

void findBondedCysteines(const vector<bool> & bondstate,
			 const string & sequence,
			 unsigned int noaa,
			 vector<int> & bonded_cysteines)
{
  for(unsigned int c=0, b=0; c<noaa; c++){
    if(sequence[c] == 'C' ) {
      if(bondstate[b]==1 ) 
	bonded_cysteines.push_back(c);      
      b++;    
    }
  }  
}

void printASCIIConnectivitySynthesis(const vector< pair<int,int> >& bridges,
				     const vector<int> & bonded_cysteines,
				     ostream& out)
{  
  unsigned int nobridges = bridges.size();

  if(nobridges == 0)
    return;
  
  for(unsigned int i=0; i<nobridges; i++ ) 
    out << "DB_bond   bond(" 
	<< bonded_cysteines[bridges[i].first-1]+1 << ","
	<< bonded_cysteines[bridges[i].second-1]+1 << ")\n";
  out << endl;
}

void printHTMLConnectivitySynthesis(const vector< pair<int,int> >& bridges,
				    const vector<int> & bonded_cysteines,
				    ostream& out)
{  
  unsigned int nobridges = bridges.size();

  if(nobridges == 0)
    return;
  
  for(unsigned int i=0; i<nobridges; i++ ) 
    out << "<a href=\"#DB_bond\">DB_bond</a>   bond(" 
	<< bonded_cysteines[bridges[i].first-1]+1 << ","
	<< bonded_cysteines[bridges[i].second-1]+1 << ")\n";
  out << endl;
}

void printConnectivityTable(const vector< pair<int,int> >& bridges,
			    const vector<int> & bonded_cysteines,
			    ostream& out)
{  
  unsigned int nobridges = bridges.size();

  if(nobridges == 0)
    return;
  
  out << "Connectivity pattern: summary table\n\n"
      << "# bridge | first cys | second cys\n"
      <<  "---------------------------------\n";
  
  for(unsigned int i=0; i<nobridges; i++ ) {
    out << spaces(i+1,4) << i+1 << "    |";
    unsigned int first = bonded_cysteines[bridges[i].first-1]+1;
    out << spaces(first,6) << first << "    |"; 
    unsigned int second = bonded_cysteines[bridges[i].second-1]+1;
    out << spaces(second,6) << second << "    \n"; 
  }
  out << endl;
}

string spaces(unsigned int totnum, unsigned int totspaces)
{
  unsigned int spaces = totspaces;
  float num = totnum;
  string space = "";
  
  for(unsigned int i = 0; i < totspaces; i++,spaces--){
    num /= 10;
    if(num < 1.)
      break;
  }
  for(unsigned int i = 0; i < spaces; i++)
    space += " ";
  return space;
}
