#ifndef __OUTPUTUTILS_H
#define __OUTPUTUTILS_H

#include <stdlib.h>
#include <vector>
#include <iostream>
#include <string>

double read_bridges(std::vector< std::pair<int,int> >& bridges, unsigned int nobonded, std::istream& in);

std::vector<std::string> make_rulers(const std::vector< std::pair<int,int> >& bridges, 
				     unsigned int noaa, 
				     const std::vector<int>& bonded_cysteines); 

std::vector<std::string> make_bridges(unsigned int nobridges, 
				      unsigned int noaa, 
				      const std::vector< std::pair<int,int> >& ind_bridges);

void print_bridges(unsigned int i, unsigned int noaa, 
		   const std::vector<std::string>& bridges_rulers, 
		   std::ostream& out); 

unsigned int  readBondingState(std::vector<bool> & bondstate, 
			       std::vector<bool> & bondflipped,
			       std::vector<int> & bondconfidence,
			       std::istream & in);

void findBondedCysteines(const std::vector<bool> & bondstate,
			 const std::string & sequence,
			 unsigned int noaa,
			 std::vector<int> & bonded_cysteines);

void print_html_footer(bool hasconfidence, bool hasbridges, std::ostream& out);

void makeHTMLOutput(std::string sequence, 
		     const char * predictions, 
		     int alternatives, 
		     std::string idseq = "", 
		     std::ostream& out = std::cout);

void print_ascii_footer(bool hasconfidence, bool hasbridges, std::ostream& out);

void makeASCIIOutput(std::string sequence, 
		     const char * predictions, 
		     int alternatives, 
		     std::string idseq = "", 
		     std::ostream& out = std::cout);

std::string spaces(unsigned int totnum, unsigned int totspaces);

void printConnectivityTable(const std::vector< std::pair<int,int> >& bridges,
			    const std::vector<int> & bonded_cysteines,
			    std::ostream& out);

void printASCIIConnectivitySynthesis(const std::vector< std::pair<int,int> >& bridges,
				     const std::vector<int> & bonded_cysteines,
				     std::ostream& out);

void printHTMLConnectivitySynthesis(const std::vector< std::pair<int,int> >& bridges,
				    const std::vector<int> & bonded_cysteines,
				    std::ostream& out);

#endif
