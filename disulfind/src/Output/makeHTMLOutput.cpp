#include <fstream>
#include <sstream>
#include <assert.h>
#include "utils.h"

using namespace std;

// font colors
const char * GREY[10] = {"#9f9e9e",
			 "#8f8e8e",
			 "#7f7e7e",
			 "#6f6e6e",
			 "#5f5e5e",
			 "#4f4e4e",
			 "#3f3e3e",
			 "#2f2e2e",
			 "#1f1e1e",
			 "#0f0e0e"};

const char * RED[10] = {"#ffd7d7",
			"#ffbfbf",
			"#ffa1a1",
			"#ff8686",
			"#ff6b6b",
			"#ff5353",
			"#ff3c3c",
			"#ff2929",
			"#ff1515",
			"#ff0000"};


void print_html_footer(bool hasconfidence, bool hasbridges, ostream& out){

  out << "</PRE></div></td></tr><br><br>\n"
      << "<tr><td><UL>\n"
      << "<LI><b>Please cite:</b>\n"
      << "<UL>\n"
      << "<LI>A. Ceroni, A. Passerini, A. Vullo and P. Frasconi.\n"
      << "<i>DISULFIND: a Disulfide Bonding State and Cysteine Connectivity Prediction Server</i>,\n"
      << "Nucleic Acids Research, 34(Web Server issue):W177--W181, 2006.\n"
      << "</UL>\n"
      << "<LI><b>Contact information:</b>\n"
      << "<UL>\n"
      << "<LI>Questions and comments are very appreciated. Please, send email to: "
      << "<A href=\"mailto:cystein@dsi.unifi.it\">cystein@dsi.unifi.it</a>\n"
      << "</UL>\n"
      << "<LI><b>Abbreviations used:</b>\n"
      << "<UL>\n"
      << "<LI><b><a NAME=\"DISULFIND_AA\">AA</a></b>\n"
      << "amino acid sequence\n"
      << "<LI><b><a NAME=\"DISULFIND_DB_state\">DB_state</a></b>\n"
      << "predicted disulfide bonding state (1=disulfide bonded, 0=not disulfide bonded)\n";
    if(hasconfidence){
      out << "<LI><b><a NAME=\"DISULFIND_DB_conf\">DB_conf</a></b>\n"
	  << "confidence of disulfide bonding state "
	  << "prediction (0=low to 9=high)<br> A red colour means that the viterbi "
	  << "aligner overruled the most likely predition for that residue in order "
	  << "to achieve a consistent prediction at a protein level (even number of "
	  << "disulfide bonded cysteines, as interchain bonds are ignored). See papers for details.\n";
    }
    if (hasbridges){
      out << "<LI><b><a NAME=\"DB_bond\">DB_bond</a></b>\n"
	  << "position in sequence of a pair of cysteines predicted to be forming a disulfide bridge.\n";
      out << "<LI><b><a NAME=\"DISULFIND_Conn_conf\">Conn_conf</a></b>\n"
	  << "confidence of connectivity assignment given the predicted disulfide bonding state (real value in [0,1])\n";
    }
    out << "<LI>Copyright 2006, Alessio Ceroni, Andrea Passerini, Alessandro Vullo.\n"
    	<< "</UL>\n"
	<< "</UL></td></tr>\n"
    	<< "</table></body></html>\n";
}

void makeHTMLOutput(string sequence, const char * predictions, int alternatives, string idseq, ostream& out){

  unsigned int noaa = sequence.length();

  // open prediction file
  ifstream in(predictions);
  assert(in.good());

  // read bonding state
  vector<bool> bondstate,bondflipped;
  vector<int> bondconfidence;
  unsigned int nobonded = readBondingState(bondstate, bondflipped, bondconfidence, in);

  // find bonded cysteines in protein
  vector<int> bonded_cysteines;
  findBondedCysteines(bondstate, sequence, noaa, bonded_cysteines);

  // make output

  // print header
  out << "<html><body><table>" << endl;  
  // print identifier if any
  if( idseq != ""  ) 
    out << "<tr><td><br><font size=5>Results for <b>" << idseq << "</b></font>\n</td></tr>" << endl;    
  out << "<br><br><tr><td><div style=\"FONT-FAMILY: Courier\"><PRE>" << endl;


  // read bridges 
  vector< pair<int,int> > bridges;
  double bridgesconfidence = read_bridges(bridges, nobonded, in);
  vector<string> bridges_rulers = make_rulers(bridges, noaa, bonded_cysteines);
  
  unsigned int i,c,j,b = 0;
  
  for(i=0; i<noaa; ) {

    // print bridges
    print_bridges(i,noaa,bridges_rulers, out);
  
    // print sequence  
    out << "<a href=\"#DISULFIND_AA\">AA</a>       ";
    for(j=0, c=i; j<79 && c<noaa; j++,c++)
      out << sequence[c];
    out << endl;
    
    // print bonds
    out << "<a href=\"#DISULFIND_DB_state\">DB_state</a> ";
    unsigned int bb;
    for(j=0,c=i,bb=b; j<79 && c<noaa; j++,c++) {
      if( sequence[c] == 'C' ) {
	out << bondstate[bb];
	bb++;
      }
      else
	out << " ";
    }  
    out << endl;

    if(bondconfidence.size() > 0){
      // print confidence
      out << "<a href=\"#DISULFIND_DB_conf\">DB_conf</a>  ";
      for(j=0,c=i,bb=b; j<79 && c<noaa; j++,c++) {
	if( sequence[c] == 'C' ) {
	  if(bondflipped[bb] == 0)
	    out << "<FONT style=\"color:" << GREY[bondconfidence[bb]] << "\">";
	  else
	    //out << "<FONT style=\"color:" << RED[bondconfidence[bb]] << "\">"; // unreadable for small confidence values
	    out << "<FONT style=\"color:" << RED[9] << "\">";
	  out << bondconfidence[bb];
	  out << "</FONT>";
	  bb++;
	}
	else
	  out << " ";
      }  
      out << endl;    
    }    
    out << endl;
    b = bb;
    i = c;
  }

  if( bridges.size() > 0 ){
    printHTMLConnectivitySynthesis(bridges, bonded_cysteines, out);
    out << "<a href=\"#DISULFIND_Conn_conf\">Conn_conf</a> " << bridgesconfidence << "<br>\n" << endl;
  }

  // print connectivity pattern alternatives 
  int alt = 0;
  char * altname[2] = {"Second","Third"};

  while(bridges.size() > 0){
    
    if(alt+1 >= alternatives)
	break;
    
    bridges.clear();
    bridgesconfidence = read_bridges(bridges, nobonded, in);
    if(bridges.size() == 0)
      break;
    
    out <<  "</PRE></div></td></tr>\n"
	<< "<tr><td><br><font size=5><b>" << altname[alt] 
	<< "</b> best ranking connectivity pattern</font>\n</td></tr>\n"
	<<  "<br><br><br><tr><td><div style=\"FONT-FAMILY: Courier\"><PRE>" << endl;    
    alt++;

    // make rulers 
    bridges_rulers = make_rulers(bridges, noaa, bonded_cysteines);
    
    for( i=0; i<noaa; ) {
	
      // print bridges
      print_bridges(i,noaa,bridges_rulers, out);
	
      // print sequence  
      out << "<a href=\"#DISULFIND_AA\">AA</a>       ";
      for( j=0,c=i; j<79 && c<noaa; j++,c++)
	out << sequence[c];
      out << "\n" << endl;
      
      i = c;
    }
    printHTMLConnectivitySynthesis(bridges, bonded_cysteines, out);
    out << "<a href=\"#DISULFIND_Conn_conf\">Conn_conf</a> " << bridgesconfidence << "<br>" << endl;
  }

  in.close();
  
  if( nobonded > 10)
    out << "Connectivity Pattern can be predicted for up to 5 bonds.\n" << endl;

 print_html_footer((bondconfidence.size() > 0),(bridges.size() > 0), out);

}

#ifdef STANDALONE
int main(int argc, char ** argv){

  if( argc < 4 ) {
    cerr << "\nUsage:\n\t" << argv[0] << " <sequence> <pred_file> <alternatives> [<id>]\n" << endl;
    exit(-1);
  }

  string sequence = argv[1];
  const char * predictions = argv[2];
  int alternatives = atoi(argv[3]);
  string idseq = (argc > 4) ? argv[4] : "";
  
  makeHTMLOutput(sequence, predictions, alternatives, idseq, cout);
}
#endif
