#include <fstream>
#include <sstream>
#include <assert.h>
#include "utils.h"

using namespace std;

void print_ascii_footer(bool hasconfidence, bool hasbridges, ostream& out){

  out << "----------------------------------------------------------------------------------------\n"
      << "\nAbbreviations used:\n\n"
      << "AA        = amino acid sequence\n"
      << "DB_state  = predicted disulfide bonding state\n" 
      << "            (1=disulfide bonded, 0=not disulfide bonded)\n";
  if(hasconfidence){
    out << "DB_conf   = confidence of disulfide bonding state prediction (0=low to 9=high)\n"
	<< "DB_flip   = an asterisk (*) indicates that the viterbi aligner overruled the\n"
	<< "            most likely predition for that residue in order to achieve a\n"
	<< "            consistent prediction at a protein level (even number of disulfide\n"
	<< "            bonded cysteines, as interchain bonds are ignored).\n";
  }
  if( hasbridges ) {
    out << "DB_bond   = position in sequence of a pair of cysteines predicted to be forming\n"
	<< "            a disulfide bridge.\n";
    out << "Conn_conf = confidence of connectivity assignment given the predicted disulfide\n"
	<< "            bonding state (real value in [0,1])\n\n";
  }
  out << "----------------------------------------------------------------------------------------\n"
      << "\nPlease cite:\n\n"
      << "A. Ceroni, A. Passerini, A. Vullo and P. Frasconi.\n"
      << "\"DISULFIND: a Disulfide Bonding State and Cysteine Connectivity Prediction Server\"\n"
      << "Nucleic Acids Research, 34(Web Server issue):W177--W181, 2006.\n\n"
      << "----------------------------------------------------------------------------------------\n"
      << "\nQuestions and comments are very appreciated.\n"
      << "Please, send email to: cystein@dsi.unifi.it\n\n"
    
      << "Copyright 2006, Alessio Ceroni, Andrea Passerini, Alessandro Vullo\n\n"
    
      << "----------------------------------------------------------------------------------------\n";
}

void makeASCIIOutput(string sequence, const char * predictions, int alternatives, string idseq, ostream& out){

  unsigned int noaa = sequence.length();

  // open prediction file
  ifstream in(predictions);
  assert(in.good());

  // read bonding state
  vector<bool> bondstate,bondflipped;
  vector<int> bondconfidence;
  unsigned int nobonded = readBondingState(bondstate, bondflipped, bondconfidence, in);

  // find bonded cysteines in protein
  vector<int> bonded_cysteines;
  findBondedCysteines(bondstate, sequence, noaa, bonded_cysteines);

  // make output

  // print header
  out << "----------------------------------------------------------------------------------------\n"
      << "                                    DISULFIND                                  \n"
      << "              Cysteines Disulfide Bonding State and Connectivity Predictor     \n"
      << "----------------------------------------------------------------------------------------\n"
      << "\n" << endl;

  // print identifier if any
  if( idseq != ""  ) 
    out << "Query Name: " << idseq << "\n\n" << endl;


  // read bridges 
  vector< pair<int,int> > bridges;
  double bridgesconfidence = read_bridges(bridges, nobonded, in);
  vector<string> bridges_rulers = make_rulers(bridges, noaa, bonded_cysteines);
  
  unsigned int i,c,j,b = 0;
  
  for(i=0; i<noaa; ) {

    // print bridges
    print_bridges(i,noaa,bridges_rulers, out);
  
    // print sequence  
    out << "AA       ";
    for(j=0, c=i; j<79 && c<noaa; j++,c++)
      out << sequence[c];
    out << endl;
    
    // print bonds
    out << "DB_state ";
    unsigned int bb;
    for(j=0,c=i,bb=b; j<79 && c<noaa; j++,c++) {
      if( sequence[c] == 'C' ) {
	out << bondstate[bb];
	bb++;
      }
      else
	out << " ";
    }  
    out << endl;

    if(bondconfidence.size() > 0){
      // print confidence
      out << "DB_conf  ";
      for(j=0,c=i,bb=b; j<79 && c<noaa; j++,c++) {
	if( sequence[c] == 'C' ) {
	  out << bondconfidence[bb];
	  bb++;
	}
	else
	  out << " ";
      }  
      out << endl;
    
      // print flipping
      out << "DB_flip  ";
      for(j=0,c=i,bb=b; j<79 && c<noaa; j++,c++) {
	if( sequence[c] == 'C' ) {
	  if(bondflipped[bb] == 1)
	    out << "*";
	  else
	    out << " ";
	  bb++;
	}
	else
	  out << " ";
      }  
      out << endl;
    }    
    out << endl;
    b = bb;
    i = c;
  }

  if( bridges.size() > 0 ){
    printASCIIConnectivitySynthesis(bridges, bonded_cysteines, out);
    out << "Conn_conf " << bridgesconfidence << "\n" << endl;
  }

  // print connectivity pattern alternatives 
  int alt = 0;
  char * altname[2] = {"SECOND","THIRD"};

  while(bridges.size() > 0){
    
    if(alt+1 >= alternatives)
	break;
    
    bridges.clear();
    bridgesconfidence = read_bridges(bridges, nobonded, in);
    if(bridges.size() == 0)
      break;
    
    out << "----------------------------------------------------------------------------------------\n"
	<< "\n" << altname[alt] << " best ranking connectivity pattern\n\n" << endl;
    alt++;

    // make rulers 
    bridges_rulers = make_rulers(bridges, noaa, bonded_cysteines);
    
    for( i=0; i<noaa; ) {
	
      // print bridges
      print_bridges(i,noaa,bridges_rulers, out);
	
      // print sequence  
      out << "AA       ";
      for( j=0,c=i; j<79 && c<noaa; j++,c++)
	out << sequence[c];
      out << "\n" << endl;
      
      i = c;
    }
    printASCIIConnectivitySynthesis(bridges, bonded_cysteines, out);
    out << "Conn_conf " << bridgesconfidence << "\n" << endl;
  }

  in.close();
  
  if( nobonded > 10)
    out << "Connectivity Pattern can be predicted for up to 5 bonds.\n" << endl;

 print_ascii_footer((bondconfidence.size() > 0),(bridges.size() > 0), out);

}

#ifdef STANDALONE
int main(int argc, char ** argv){

  if( argc < 4 ) {
    cerr << "\nUsage:\n\t" << argv[0] << " <sequence> <pred_file> <alternatives> [<id>]\n" << endl;
    exit(-1);
  }

  string sequence = argv[1];
  const char * predictions = argv[2];
  int alternatives = atoi(argv[3]);
  string idseq = (argc > 4) ? argv[4] : "";
  
  makeASCIIOutput(sequence, predictions, alternatives, idseq, cout);
}
#endif
