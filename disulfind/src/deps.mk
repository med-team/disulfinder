GlobalDescriptor.o: Input/GlobalDescriptor.cpp Input/utils.h \
 Input/../Common/Matrix.h Input/../Common/Exception.h \
 Input/../Common/SparseMatrix.h Input/../Common/Matrix.h Input/Protein.h \
 Input/GlobalDescriptor.h
createBRNNDataset.o: Input/createBRNNDataset.cpp \
 Input/createBRNNDataset.h Input/utils.h Input/../Common/Matrix.h \
 Input/../Common/Exception.h Input/../Common/SparseMatrix.h \
 Input/../Common/Matrix.h Input/Protein.h
createKernelDataset.o: Input/createKernelDataset.cpp \
 Input/createKernelDataset.h Input/utils.h Input/../Common/Matrix.h \
 Input/../Common/Exception.h Input/../Common/SparseMatrix.h \
 Input/../Common/Matrix.h Input/Protein.h Input/GlobalDescriptor.h
makeWindow.o: Input/makeWindow.cpp Input/../Common/Matrix.h \
 Input/../Common/Exception.h Input/../Common/SparseMatrix.h \
 Input/../Common/Matrix.h
makeSparseVector.o: Input/makeSparseVector.cpp Input/../Common/Matrix.h \
 Input/../Common/Exception.h Input/../Common/SparseMatrix.h \
 Input/../Common/Matrix.h
Protein.o: Input/Protein.cpp Input/../Common/Util.h \
 Input/../Common/../Common/Exception.h Input/Protein.h
buildXfolds.o: Input/buildXfolds.cpp
SVMClassifier.o: SVM/SVMClassifier.cpp SVM/SVMClassifier.h SVM/Data.h \
 SVM/../Common/Exception.h
makeHTMLOutput.o: Output/makeHTMLOutput.cpp Output/utils.h
utils.o: Output/utils.cpp Output/utils.h
makeASCIIOutput.o: Output/makeASCIIOutput.cpp Output/utils.h
fsa.o: FSA-Alignment/fsa.cpp FSA-Alignment/fsa.h \
 FSA-Alignment/../Common/CommonMethods.h FSA-Alignment/../Common/coords.h \
 FSA-Alignment/../Common/myreal.h FSA-Alignment/../Common/Matrix.h \
 FSA-Alignment/../Common/Exception.h \
 FSA-Alignment/../Common/SparseMatrix.h \
 FSA-Alignment/../Common/Accuracy.h \
 FSA-Alignment/../Common/ConfusionMatrix.h \
 FSA-Alignment/../Common/Statistics.h FSA-Alignment/../Common/Utility.h
viterbi_aligner.o: FSA-Alignment/viterbi_aligner.cpp \
 FSA-Alignment/viterbi_aligner.h FSA-Alignment/../Common/CommonMethods.h \
 FSA-Alignment/../Common/coords.h FSA-Alignment/../Common/myreal.h \
 FSA-Alignment/../Common/Matrix.h FSA-Alignment/../Common/Exception.h \
 FSA-Alignment/../Common/SparseMatrix.h \
 FSA-Alignment/../Common/Accuracy.h \
 FSA-Alignment/../Common/ConfusionMatrix.h \
 FSA-Alignment/../Common/Statistics.h FSA-Alignment/../Common/Utility.h \
 FSA-Alignment/fsa.h
multi-array.o: NN/multi-array.cpp NN/../Common/Exception.h \
 NN/multi-array.h NN/../Common/myreal.h
array.o: NN/array.cpp NN/array.h NN/multi-array.h NN/../Common/myreal.h \
 NN/../Common/Exception.h
brnn-model.o: NN/brnn-model.cpp NN/../Common/Exception.h NN/brnn-model.h \
 NN/array.h NN/multi-array.h NN/../Common/myreal.h
brnn-test.o: NN/brnn-test.cpp NN/../Common/Accuracy.h \
 NN/../Common/Exception.h NN/../Common/ConfusionMatrix.h NN/brnn-test.h \
 NN/brnn-model.h NN/array.h NN/multi-array.h NN/../Common/myreal.h
Node.o: BRNN/RNNs/Node.cpp BRNN/RNNs/require.h BRNN/RNNs/Node.h \
 BRNN/RNNs/Options.h
StructureSearch.o: BRNN/RNNs/StructureSearch.cpp BRNN/RNNs/util.h \
 BRNN/RNNs/StructureSearch.h BRNN/RNNs/require.h BRNN/RNNs/DPAG.h \
 BRNN/RNNs/Node.h BRNN/RNNs/Options.h
DataSet.o: BRNN/RNNs/DataSet.cpp BRNN/RNNs/DataSet.h BRNN/RNNs/require.h \
 BRNN/RNNs/util.h BRNN/RNNs/Options.h BRNN/RNNs/DPAG.h BRNN/RNNs/Node.h
DPAG.o: BRNN/RNNs/DPAG.cpp BRNN/RNNs/require.h BRNN/RNNs/Options.h \
 BRNN/RNNs/DPAG.h
Options.o: BRNN/RNNs/Options.cpp BRNN/RNNs/require.h BRNN/RNNs/Options.h
protein_util.o: BRNN/protein_util.cpp BRNN/protein_util.h
blast_util.o: BRNN/blast_util.cpp BRNN/blast_util.h
rnn_util.o: BRNN/rnn_util.cpp BRNN/rnn_util.h BRNN/RNNs/Options.h \
 BRNN/RNNs/DPAG.h BRNN/RNNs/DataSet.h BRNN/RNNs/require.h \
 BRNN/RNNs/util.h BRNN/RNNs/Options.h BRNN/RNNs/DPAG.h BRNN/RNNs/Node.h \
 BRNN/RNNs/RecursiveNN.h BRNN/RNNs/ActivationFunctions.h \
 BRNN/RNNs/ErrorMinimizationProcedure.h BRNN/RNNs/DataSet.h \
 BRNN/RNNs/StructureSearch.h BRNN/protein_util.h BRNN/blast_util.h
disulfinder.o: disulfinder.cpp Input/utils.h Input/../Common/Matrix.h \
 Input/../Common/Exception.h Input/../Common/SparseMatrix.h \
 Input/../Common/Matrix.h Input/Protein.h Input/createKernelDataset.h \
 Input/utils.h Input/GlobalDescriptor.h Input/createBRNNDataset.h \
 Output/utils.h SVM/SVMClassifier.h SVM/Data.h \
 FSA-Alignment/viterbi_aligner.h FSA-Alignment/../Common/CommonMethods.h \
 FSA-Alignment/../Common/coords.h FSA-Alignment/../Common/myreal.h \
 FSA-Alignment/../Common/Matrix.h FSA-Alignment/../Common/Exception.h \
 FSA-Alignment/../Common/Accuracy.h \
 FSA-Alignment/../Common/ConfusionMatrix.h \
 FSA-Alignment/../Common/Statistics.h FSA-Alignment/../Common/Utility.h \
 FSA-Alignment/fsa.h BRNN/rnn_util.h disulfinder.h NN/brnn-test.h \
 NN/brnn-model.h NN/array.h NN/multi-array.h NN/../Common/myreal.h
