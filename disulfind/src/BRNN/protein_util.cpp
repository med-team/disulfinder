#include "protein_util.h"
#include <cassert>
#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;

#define PR(x) cout << #x << " " << x << endl;
#define RADIAN 57.29578

float dotProduct(const SSegment& s1, const SSegment& s2) {
  // Determine segment vectors from the origin and
  // accumulate dot product component
  float dotproduct = .0;
  for(int i=0; i<3; ++i)
    dotproduct +=  
      (s1.last[i] - s1.first[i]) * 
      (s2.last[i] - s2.first[i]);

  return dotproduct;
}

float dist(const SSegment& s1, const SSegment& s2) {
  double distance = .0;
  for(int i=0; i<3; ++i) {
    float component = (s2.median[i] - s1.median[i]);
    distance += component * component;
  }
  return float(sqrt(distance));

  // double df = .0, dm = .0, dl = .0;
//   for(int i=0; i<3; ++i) {
//     float cm = (s2.median[i] - s1.median[i]),
//       cf = (s2.first[i] - s1.first[i]),
//       cl = (s2.last[i] - s1.last[i]);
//     dm+=cm*cm; df+=cf*cf; dl+=cl*cl;
//   }
//   return float((sqrt(dm)+sqrt(df)+sqrt(dl))/3.0);
}

// angle is always from 0 to PI
float angle(const SSegment& s1, const SSegment& s2) {
  return float(acos(double(dotProduct(s1, s2) / (s1.norm() * s2.norm()))));
}

float Atan2(float y, float x) {
  double z;

  if (x != 0.0) z = atan(y / x);
  else if (y > 0.0) z = M_PI_2;
  else if (y < 0.0) z = -M_PI_2;
  else z = 2*M_PI;

  if (x >= 0.0) return z;
  if (y > 0.0) z += M_PI;
  else z -= M_PI;
  return z;
}  /* Atan2 */

float dihedralAngle(const SSegment& s1, const SSegment& s2) {
    /*CALCULATES TORSION ANGLE OF A SET OF 4 ATOMS V1-V2-V3-V4.
    DIHEDRALANGLE IS THE ANGLE BETWEEN THE PROJECTION OF
    V1-V2 AND THE PROJECTION OF V4-V3 ONTO A PLANE NORMAL TO
    BOND V2-V3.*/
  /***/

  //Vector v12, v43, x, y, z, p;
  float v12[3], v43[3], x[3], y[3], z[3], p[3];
  
  /* Diff(v1, v2, v12);
     Diff(v4, v3, v43);
     Diff(v2, v3, z); */
  v12[0] = s1.first[0] - s1.last[0];
  v12[1] = s1.first[1] - s1.last[1];
  v12[2] = s1.first[2] - s1.last[2];

  v43[0] = s2.last[0] - s2.first[0];
  v43[1] = s2.last[1] - s2.first[1];
  v43[2] = s2.last[2] - s2.first[2];

  z[0] = s1.last[0] - s2.first[0];
  z[1] = s1.last[1] - s2.first[1];
  z[2] = s1.last[2] - s2.first[2];

  /* Cross(z, v12, p);
     Cross(z, v43, x);
     Cross(z, x, y); */
  p[0] = z[1] * v12[2] - v12[1] * z[2];
  p[1] = z[2] * v12[0] - v12[2] * z[0];
  p[2] = z[0] * v12[1] - v12[0] * z[1];

  x[0] = z[1] * v43[2] - v43[1] * z[2];
  x[1] = z[2] * v43[0] - v43[2] * z[0];
  x[2] = z[0] * v43[1] - v43[0] * z[1];

  y[0] = z[1] * x[2] - x[1] * z[2];
  y[1] = z[2] * x[0] - x[2] * z[0];
  y[2] = z[0] * x[1] - x[0] * z[1];

  /* u = Dot(x, x);
     v = Dot(y, y); */
  float u = x[0]*x[0] + x[1]*x[1] + x[2]*x[2];
  float v = y[0]*y[0] + y[1]*y[1] + y[2]*y[2];

  float Result = 360.0;
  if (u <= 0.0 || v <= 0.0)
    return Result;
  u = (p[0]*x[0]+p[1]*x[1]+p[2]*x[2]) / sqrt(u);
  v = (p[0]*y[0]+p[1]*y[1]+p[2]*y[2]) / sqrt(v);

  if (u != 0.0 || v != 0.0)
    return (Atan2(v, u) * RADIAN);
  return Result;
}

void SSegment::computeReprVector() {
  // get the list of residues
  vector<Residue> residues(begin(), end());
  // get indexes of first and last segment residues
  int i = 0, j = residues.size()-1;

  // compute beginning and end points of 
  // helix or strand representative vector
  if(_symbol == 'H') {
    first[0] = (.74*residues[i]._coord._x + residues[i+1]._coord._x + residues[i+2]._coord._x + .74*residues[i+3]._coord._x) / 3.48;
    first[1] = (.74*residues[i]._coord._y + residues[i+1]._coord._y + residues[i+2]._coord._y + .74*residues[i+3]._coord._y) / 3.48;
    first[2] = (.74*residues[i]._coord._z + residues[i+1]._coord._z + residues[i+2]._coord._z + .74*residues[i+3]._coord._z) / 3.48;
	  
    last[0] = (.74*residues[j]._coord._x + residues[j-1]._coord._x + residues[j-2]._coord._x + .74*residues[j-3]._coord._x) / 3.48;
    last[1] = (.74*residues[j]._coord._y + residues[j-1]._coord._y + residues[j-2]._coord._y + .74*residues[j-3]._coord._y) / 3.48;
    last[2] = (.74*residues[j]._coord._z + residues[j-1]._coord._z + residues[j-2]._coord._z + .74*residues[j-3]._coord._z) / 3.48;
	  
  } else { // for loops it hasn't much sense to compute straight line segment...
    if(numResidues() <= 2) {
      first[0] = residues[i]._coord._x;
      first[1] = residues[i]._coord._y;
      first[2] = residues[i]._coord._z;

      last[0] = residues[j]._coord._x;
      last[1] = residues[j]._coord._y;
      last[2] = residues[j]._coord._z;
    } else {
      first[0] = (residues[i]._coord._x + residues[i+1]._coord._x) / 2.0;
      first[1] = (residues[i]._coord._y + residues[i+1]._coord._y) / 2.0;
      first[2] = (residues[i]._coord._z + residues[i+1]._coord._z) / 2.0;
	    
      last[0] = (residues[j]._coord._x + residues[j-1]._coord._x) / 2.0;
      last[1] = (residues[j]._coord._y + residues[j-1]._coord._y) / 2.0;
      last[2] = (residues[j]._coord._z + residues[j-1]._coord._z) / 2.0;
    }
  }

  // compute segment line median point
  median[0] = (first[0] + last[0]) / 2.0;
  median[1] = (first[1] + last[1]) / 2.0;
  median[2] = (first[2] + last[2]) / 2.0;
}

Coordinate3D SSegment::computeBaricentre() const {
  float x = 0.0, y = 0.0, z = 0.0;
  for(const_iterator it=begin(); it!=end(); ++it) {
    x += (*it)._coord._x;
    y += (*it)._coord._y;
    z += (*it)._coord._z;
  }
  x /= size();
  y /= size();
  z /= size();
  return Coordinate3D(x, y, z);
}

// TO COMPLETE!
int SSegment::getSheetPairingWith(const SSegment& s) const {
  if(_symbol != 'E' || s.getSSymbol() != 'E') return 0;

  // get the set of sheets in which this segment and s are involved
  // if these sets are disjoints return 0
  set<char> bs1, bs2;
  for(const_iterator it=begin(); it!=end(); ++it)
    if(it->_sheet != ' ') bs1.insert(it->_sheet);
  for(const_iterator it=s.begin(); it!=s.end(); ++it)
    if(it->_sheet != ' ') bs2.insert(it->_sheet);
  vector<char> sp;
  set_intersection(bs1.begin(), bs1.end(), 
		   bs2.begin(), bs2.end(), 
		   back_inserter(sp));
  if(!sp.size()) return 0;

  // number of antiparallel and parallel bridges
  // between these two strands
  int numapb = 0, numpb = 0;

  for(const_iterator it1=begin(); it1!=end(); ++it1) {
    for(const_iterator it2=s.begin(); it2!=s.end(); ++it2) {
      if(it1->_bp1 == it2->_index) {
	if(!(it1->_index == it2->_bp1 || it1->_index == it2->_bp2 ||
	     it1->_bl1 == it2->_bl1 || it1->_bl1 == it2->_bl2)) 
	  throw Protein::BadRequest("Error in strand pairing");
	if(isupper(it1->_bl1)) ++numapb;
	else ++numpb;
      } else if(it1->_bp2 == it2->_index) {
	if(!(it1->_index == it2->_bp1 || it1->_index == it2->_bp2 ||
	     it1->_bl2 == it2->_bl1 || it1->_bl2 == it2->_bl2))
	  throw Protein::BadRequest("Error in strand pairing");
	if(isupper(it1->_bl2)) ++numapb;
	else ++numpb;
      }
    }
  }

  if(numapb && !numpb) return -1;
  else if(numpb && !numapb) return 1;
  else if(!numapb && !numpb) return 0; // same sheet but not bridged 
  else throw Protein::BadRequest("Error in strand pairing");
}

Protein::Protein(const char* filename) {
  parseDsspFile(filename);
}

void Protein::parseDsspFile(const char* filename) {
  ifstream ifs(filename);
  if(!ifs) {
    cerr << "Cannot open file " << filename << ". Aborting" << endl;
    exit(1);
  }

  string line;
  while(getline(ifs, line) && 
	line.find("RESIDUE AA ") == string::npos) {
    // parse header information if it is useful...
  }
  while(!ifs.eof()) {
    push_back(Chain());
    back().parseDsspFile(ifs);
    back().setSShydrophobicity();
    //back().setSheetTopology();
  }  
}

void Chain::parseDsspFile(istream& ifs) {
  char prev_chain_id = '\0';
  bool switch_seg = true;
  int aaindex = 0; // aa sequential index

  // SS CASP assigments...
  map<char, char> ss_map; ss_map['H'] = 'H'; ss_map['E'] = 'E';
  HydrophobicityScale hScale;
  
  map<char, vector<int> > ssm;

  string residue_line; bool first_residue = true;
  while(getline(ifs, residue_line)) {
    if(residue_line.find("!*") != string::npos) {
      // last segment have uninitialized baricentre
      back().setBaricentre(); back().computeReprVector();
      return;
    }
    // skip lines containing '!' char (chain breaks)
    if(residue_line.find ("!") != string::npos) {
      _chainbreak = true;
      continue;
    }
    // get current residue, segment, c_alpha coordinates and so on...
    char res_symb = (islower(residue_line[13])?'C':residue_line[13]);
    char seg_symb = residue_line[16];

    _id = residue_line[11];
    if(prev_chain_id != '\0') assert(_id == prev_chain_id);

    switch(res_symb) {
    case 'B': res_symb = 'N';
      break;
    case 'Z': res_symb = 'Q';
      break;
    case 'X': res_symb = 'A';
      break;
    default:
      break;
    }
	
    if(ss_map.find(seg_symb) == ss_map.end()) seg_symb = 'C';
    else seg_symb = ss_map[seg_symb];

    int index; // dssp index of parsed amino acid
    istringstream indexss(residue_line.substr(0, 6)); indexss >> index;
    if(first_residue) { 
      _fres_index = index; // store index of first residue
      first_residue = false;
    }
    float x, y, z, acc;
    // C_alpha coords begin at character 119
    istringstream calphass(residue_line.substr(117)); calphass >> x >> y >> z;
    istringstream accss(residue_line.substr(34, 5)); accss >> acc;

    if(size() && seg_symb != back().getSSymbol()) {
      back().setBaricentre();  back().computeReprVector();
      switch_seg = true;
    } else if(size())
      switch_seg = false;

    if(switch_seg) push_back(SSegment(seg_symb));

    back().addResidue(index, ++aaindex, res_symb, x, y, z, 
		      hScale[res_symb], acc, 
		      (islower(residue_line[13])?residue_line[13]:'0'));
    if(islower(residue_line[13])) {
      if(ssm.find(residue_line[13]) == ssm.end())
	ssm.insert(make_pair(residue_line[13], vector<int>()));
      ssm[residue_line[13]].push_back(aaindex);
    }

    if(seg_symb == 'E') {
      back().back()._bl1 = residue_line[23]; back().back()._bl2 = residue_line[24];
      back().back()._sheet = residue_line[33];
      istringstream bp1ss(residue_line.substr(25, 4)); bp1ss >> back().back()._bp1;
      istringstream bp2ss(residue_line.substr(29, 4)); bp2ss >> back().back()._bp2;

      if(back().back()._bl1 != ' ') assert(back().back()._bp1 != 0);
      if(back().back()._bl2 != ' ') assert(back().back()._bp2 != 0);
    }
    prev_chain_id = _id;
  }
  // last segment have uninitialized baricentre
  back().setBaricentre();
  back().computeReprVector();

  // set disulfide bonds partners information
  for(iterator cit=begin(); cit!=end(); ++cit)
    for(SSegment::iterator sit=cit->begin(); sit!=cit->end(); ++sit) {
      char ssb = sit->_ssbridge;
      if(ssb != '0') {
	assert(sit->_symbol == 'C');
	map<char, vector<int> >::iterator mit=ssm.find(ssb);
	assert(mit != ssm.end() && (mit->second.size() == 1 || mit->second.size() == 2));
	if(mit->second.size() == 1) {
	  assert((mit->second)[0] == sit->_sindex);
	  sit->_ssp = -1; // interchain ss bond
	} else if(mit->second.size() == 2) { // intrachain ss bond
	  if(sit->_sindex == (mit->second)[0]) sit->_ssp = (mit->second)[1];
	  else if(sit->_sindex == (mit->second)[1]) sit->_ssp = (mit->second)[0];
	  else { cerr << "Error in disulfide bond " << ssb << endl; exit(1); }
	} else { cerr << "Error in disulfide bond " << ssb << endl; exit(1); }
      }
    }
}

int Chain::numResidues() const {
  int num_tot_res = 0;
  for(const_iterator it=begin(); it!=end(); ++it)
    num_tot_res += (*it).numResidues();
  return num_tot_res;
}

void Chain::setSShydrophobicity() {
  // store K-D hydrophobicity values of chain residues
  vector<float> resHydrScales;
  for(iterator c_it=begin(); c_it!=end(); ++c_it)
    for(SSegment::iterator s_it=(*c_it).begin(); s_it!=(*c_it).end(); ++s_it)
      resHydrScales.push_back((*s_it)._hydr);
  assert(resHydrScales.size() == numResidues());

  int seq_pos = 0;
  for(iterator c_it=begin(); c_it!=end(); ++c_it) {
    // for each residue in current segment
    // store its local window average hydrophobicity value
    vector<float> hAvg;

    // Compute current segment hydrophobicity value by moving 
    // a 7-res window at each residue position, computing the 
    // mean value at each position and then averaging the final results.
    for(SSegment::iterator s_it=(*c_it).begin(); 
	s_it!=(*c_it).end(); ++s_it, ++seq_pos) {
      int l_i = seq_pos-3, r_i = seq_pos+3;
      if(l_i <= 0) l_i = 0;
      if(r_i >= resHydrScales.size()) r_i = resHydrScales.size()-1;
      float point_avg = 0.0;
      for(int j=l_i; j<=r_i; ++j)
	point_avg += resHydrScales[j];
      hAvg.push_back(point_avg / (r_i - l_i + 1));
    }
    assert(hAvg.size() == (*c_it).numResidues());

    // Now compute current segment hydropatic tendency by
    // averaging over all window positions
    float avg = 0.0, min = FLT_MAX, max = -FLT_MAX;
    for(int i=0; i<hAvg.size(); ++i) {
      avg += hAvg[i];
      if(min > hAvg[i]) min = hAvg[i];
      if(max < hAvg[i]) max = hAvg[i];
    }
    avg /= hAvg.size();

    (*c_it).setHydrophobicity(avg, min, max);
  }

  assert(seq_pos == resHydrScales.size());
  
  // for(iterator c_it=begin(); c_it!=end(); ++c_it) {
//     float avg = 0.0, min = FLT_MAX, max = -FLT_MAX;
//     for(SSegment::iterator s_it=(*c_it).begin(); 
// 	s_it!=(*c_it).end(); ++s_it) {
//       float h_res = (*s_it)._hydr;
//       avg += h_res;
//       if(min > h_res) min = h_res;
//       if(max < h_res) max = h_res;
//     }
//     avg /= c_it->numResidues();
//     c_it->setHydrophobicity(avg, min, max);
//   }
}

string Chain::getPrimaryStruct() const {
  string seq;
  for(const_iterator c_it=begin(); c_it!=end(); ++c_it)
    for(SSegment::const_iterator s_it=(*c_it).begin(); 
	s_it!=(*c_it).end(); ++s_it) {
      seq += (*s_it)._symbol;
    }
  return seq;
}

string Chain::getSecondaryStruct() const {
  string seq;
  for(const_iterator c_it=begin(); c_it!=end(); ++c_it) {
    int num_res = (*c_it).numResidues();
    char ss_type = (*c_it).getSSymbol();
    for(int i=1; i<=num_res; ++i)
      seq += ss_type;
  }
  return seq;
}

Chain Protein::getChain(char id) const throw(Protein::BadRequest) {
  id = char(toupper(char(id)));
  if(id == '_' || id == '-')
    id = ' ';
  for(const_iterator it=begin(); it!=end(); ++it) {
    if((*it).getId() == id)
      return (*it);
  }
  //char* message;
  string message = "chain not found";
  //sprintf(message, "chain %c not found", id);
  throw BadRequest(message.c_str());
}


