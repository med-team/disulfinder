#include "require.h"
#include "Options.h"
#include "StructureSearch.h"
#include "protein_util.h"
#include "blast_util.h"
#include <cstdlib>
#include <dirent.h>
#include <map>
#include <set>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

static int one(const struct dirent* unused) {
  return 1;
}

int debugLevel;

// Note: Need only profile file, since it reports the sequence
// Need to able to compute data only on a subset of cysteins,
// because the bonding state predictor could say 'Mix'

int main(int argc, char* argv[]) {
  if(argc != 5) {
    cerr << "Usage: seq2conngraphs queryname predir pssmdir winsize\n\n";
    exit(1);
  }
  
  string predir(argv[2]);
  if(predir[predir.length()-1] != '/') predir += '/';
  string pssmdir(argv[3]);
  if(pssmdir[pssmdir.length()-1] != '/') pssmdir += '/';
//   string seqdir(argv[4]);
//   if(seqdir[seqdir.length()-1] != '/') seqdir += '/';
//   string outdir(argv[5]);
//   if(outdir[outdir.length()-1] != '/') outdir += '/';

  setenv("OPTIONTYPE", "Default", 1);
  try {
    Options::instance()->parse_args(argc,argv);
      
//     struct dirent** filelist;
//     int n = scandir(seqdir.c_str(), &filelist, one, alphasort);
//     cout << n << endl << flush;
//     if(n>0) {

//       int max_seq_length = -1;
//       for(int cnt=0; cnt<n; ++cnt) {
// 	if(!strcmp(filelist[cnt]->d_name, ".") || 
// 	   !strcmp(filelist[cnt]->d_name, ".."))
// 	  continue;
// 	if(!(cnt%100)) cout << '.' << flush;
// 	string chain_name(filelist[cnt]->d_name);
// 	string chain_filename = seqdir + filelist[cnt]->d_name;
// 	ifstream chainfs(chain_filename.c_str());
// 	if(!chainfs) {
// 	  cerr << "Cannot open " << chain_filename << " for input. Skipping..." << endl;
// 	  continue;
// 	}

// 	int num_cys, seq_length;
// 	// first line contains number of bonded cysteines and sequence length
// 	chainfs >> num_cys >> seq_length;
// 	// skip if number of bridges is == 1 or > 5
// 	if(num_cys < 4 || num_cys > 10)
// 	  continue;

// 	// eventually update maximum sequence length
// 	if(max_seq_length < seq_length)
// 	  max_seq_length = seq_length;
//       }
      
      int max_seq_length = 500; // TODo: measure this
      
      // Get chain profile
      string pssmfilename = pssmdir + argv[1];
      PSSM pssm;
      pssm.parsePsiBlastReport(pssmfilename.c_str());

      // Read prediction and get bonded cystein indices
      string predfilename = predir + argv[1];
      ifstream predfs(predfilename.c_str());
      if(!predfs) { cerr << "Unable to open prediction file. Aborting...\n"; exit(1); }

      // Register profile bonded cystein positions
      set<int> bcyspos;
      int bondstate; 
      for(int i=0; i<pssm.size(); ++i)
	if(pssm[i].AA == 'C') {
	  if(predfs >> bondstate) { 
	    assert(bondstate == 0 || bondstate == 1);
	    if(bondstate) bcyspos.insert(i+1);
	  } else { 
	    cerr << "Predicted cystein are less than those in profile";
	    exit(1);
	  }
	}
      assert(!(predfs >> bondstate)); // more pred cystein than those in profile
      int numbcys = bcyspos.size();
      if(numbcys%2) { cerr << "Wrong number of bonded cysteins\n"; exit(1); }

      // skip if number of bridges is == 1 or > 5
      if(numbcys < 4) {
	cerr << "Trivial Prediction! Exiting..." << endl;
	exit(1);
      } else if(numbcys > 10) {
	cerr << "Sorry. Connectivity can be computed only for up to 5 bonds." << endl;
	exit(1);
      }

      // target DPAG representing disulfide connectivity matrix
      // NOTE: it's dummy, useful only for generating data
      DPAG targetDPAG(numbcys);
      
      map<int, vector<float> > nodesInputs;

      int win_size_2 = atoi(argv[6]) / 2, 
	cyscounter = 0, 
	seq_length = pssm.size();
      for(set<int>::iterator it=bcyspos.begin();
	  it!=bcyspos.end(); ++it, ++cyscounter) {
	if(!(cyscounter%2))
	  boost::add_edge(cyscounter, cyscounter+1, targetDPAG);

	nodesInputs.insert(make_pair(cyscounter, vector<float>()));
	int central_pos = *it - 1;
	require(pssm[central_pos].AA == 'C', "Error in AA position"); // re-check
	
	for(int i=central_pos-win_size_2; 
	    i<=central_pos+win_size_2; ++i) {
	  if(i >=0 && i < pssm.size())
	    for(int j=0; j<20; ++j)
	      nodesInputs[cyscounter].push_back(pssm[i].profile[j]);
	  else
	    for(int j=0; j<20; ++j)
	      nodesInputs[cyscounter].push_back(0.0);
	}
	// add information on sequence length and sequence fraction spanned
	nodesInputs[cyscounter].push_back(float(*it)/float(seq_length));
	nodesInputs[cyscounter].push_back(float(pssm.size())/float(max_seq_length));
      }
	  	
      // Final check on target graph. Vertices cannot have 
      // indegree or outdegree > 1
      // (i.e. they can't be connected with more than one vertex)
      VertexId vertex_id = boost::get(boost::vertex_index, targetDPAG);
      vertexIt v_i, v_end;
      for(boost::tie(v_i, v_end) = boost::vertices(targetDPAG); v_i!=v_end; ++v_i)
	if(boost::in_degree(*v_i, targetDPAG) > 1 && 
	   boost::out_degree(*v_i, targetDPAG) > 1) {
	  cerr << "Error in connectivity graph for " << vertex_id[*v_i] << endl;
	  exit(1);
	  }
      
      // Initialize a SearchStructure object
      StructureSearch search_simulation;
      
      // UPDATE: generate B!! alternative patterns on stdout
      search_simulation.ConnectivityPatternSearch(cout, argv[1], 
 						  nodesInputs, 
						  targetDPAG, 
						  PRMeasure);

//       string outfilename = outdir + argv[1];
//       // Simulate the search passing filename where to append generated graphs
//       search_simulation.ConnectivityPatternSearch(outfilename.c_str(), 
						  //argv[1],
 						  //nodesInputs, 
						  //targetDPAG, 
						  //PRMeasure);
      //}
  } catch(Options::BadOptionSetting e) {
    cerr << e.explainError() << endl;
    exit(-1);
  }
}
