/****************************************************************
 * predictPattern: given a train set and a test set (usually    *
 * composed of one instance, predict the disulfide connectivity *
 * pattern for instances in test set.                           *
 *                                                              *
 * Author: Alessandro Vullo                                     *
 * Email:  vullo@dsi.unifi.it                                   *
 ****************************************************************/

#include "require.h"
#include "Options.h"
#include "DataSet.h"
#include "RecursiveNN.h"
#include "StructureSearch.h"
#include <cstdlib>
#include <cfloat>
#include <ctime>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

#define PR(x) cout << #x << ": " << x << endl
int debugLevel;

void predictPattern(const char* netname, 
		    DataSet* trainingSet, 
		    DataSet* testSet) {
  if(!trainingSet || !trainingSet->size()) {
    cout << "Need some train data to go on, use option -d with a non empty directory and --train-set option\n";
    return;
  }
  if(!testSet || !testSet->size()) {
    cout << "Need some test data to go on, use option -d with a non empty directory and --test-set option\n";
    return;
  }
  
  int B = -1;

  // Cycle through train instances and determine 
  // the set of candidate connectivity patterns
  // over which to search the right one
  vector<DPAG> candidates;
  for(DataSet::iterator it=trainingSet->begin(); 
      it!=trainingSet->end(); ++it) {
    if(it.currentIsGoal()) {
      // Warning! Dataset contains modified graph with added edges
      // between adjacent nodes whenever they are not present in the
      // true target graph. Need to remove these dummy edges in order
      // to create a perfect matching.
      DPAG candidate((*(it.currentDPAGs())).first);
      
      VertexId vertex_id = boost::get(boost::vertex_index, candidate);
      vertexIt v_i, v_end;
      EdgeId edge_id = boost::get(boost::edge_index, candidate);
      for(boost::tie(v_i, v_end) = boost::vertices(candidate); v_i!=v_end; ++v_i) {
	vertexIt v_i1=v_i+1;
	pair<Edge_d, bool> edge_pair = boost::edge(*v_i, *v_i1, candidate);
	if(edge_pair.second == true && edge_id[edge_pair.first] == 0)
	  boost::remove_edge(*v_i, *v_i1, candidate);
      }
      if(B < 0)	B = boost::num_edges(candidate);
      else {
	require(B == boost::num_edges(candidate) && 
		B >= 2 && B <= 5, "Different number of bridges in training set"
		" or wrong number of bridges");
      }
      candidates.push_back(candidate);
    }
  }

  /*** Read network in if it exists ***/
  RecursiveNN<TanH, Sigmoid, MGradientDescent>* 
    rnn = new RecursiveNN<TanH, Sigmoid, MGradientDescent>(netname);
  
  for(DataSet::iterator it=testSet->begin(); it!=testSet->end(); ++it) {
    if(it.currentIsGoal()) {
      // Warning! Dataset contains modified graph with added edges
      // between adjacent nodes whenever they are not present in the
      // true target graph. Need to remove these dummy edges in order
      // to create a perfect matching.
      DPAG target((*(it.currentDPAGs())).first);
      VertexId vertex_id = boost::get(boost::vertex_index, target);
      vertexIt v_i, v_end;
      EdgeId edge_id = boost::get(boost::edge_index, target);
      for(boost::tie(v_i, v_end) = boost::vertices(target); 
	  v_i!=v_end; ++v_i) {
	vertexIt v_i1=v_i+1;
	pair<Edge_d, bool> edge_pair = boost::edge(*v_i, *v_i1, target);
	if(edge_pair.second == true && edge_id[edge_pair.first] == 0)
	  boost::remove_edge(*v_i, *v_i1, target);
      }

      require(B == boost::num_vertices(target) / 2, 
	      "Train instances and test istance have"
	      " different number of disulfide bridges");

      vector<vector<float> > nodesInputs;
      for(vector<Node*>::iterator v_it=(*(it.currenTONodes())).begin();
	  v_it!=(*(it.currenTONodes())).end(); ++v_it) {
	nodesInputs.push_back((*v_it)->_encodedInput);
      }

//       // Search for target connectivity pattern
//             StructureSearch topologySearcherTest(nodesInputs);
//       DPAG predicted = 
// 	topologySearcherTest.ConnectivityPatternSearch(rnn, candidates);
	  
//       require(B == boost::num_edges(predicted) &&
// 	      B == boost::num_vertices(predicted) / 2, 
// 	      "Error in predicted number of disulfide bridges");

      //      string chain_id = it.getId();
//       // Insert here code to output predicted pattern for current chain
//       //cout << "> predicted" << endl;
//       vertex_id = boost::get(boost::vertex_index, predicted);
//       outIter out_i, out_end;  
//       for(boost::tie(v_i, v_end) = boost::vertices(predicted); 
// 	  v_i!=v_end; ++v_i) {
// 	boost::tie(out_i, out_end)=boost::out_edges(*v_i, predicted);
// 	if(out_i != out_end) cout << (vertex_id[*v_i]+1) << '\t' 
// 				  << (vertex_id[boost::target(*out_i, predicted)]+1) << endl;
// 	// cout << vertex_id[*v_i] << " --> ";
// // 	for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, predicted);
// // 	    out_i!=out_end; ++out_i)
// // 	  cout <<  << " ";
// // 	cout << endl;
//       }

      
      // Predict most likely configurations among the patterns
      vector<float> bestScores;
      // Initialise search for target connectivity pattern
      StructureSearch topologySearcherTest(nodesInputs);

//       for(int j=0; j<candidates.size(); ++j) {
// 	vertex_id = boost::get(boost::vertex_index, candidates[j]);
// 	outIter out_i, out_end;  
// 	for(boost::tie(v_i, v_end) = boost::vertices(candidates[j]); 
// 	    v_i!=v_end; ++v_i) {
// 	  boost::tie(out_i, out_end)=boost::out_edges(*v_i, candidates[j]);
// 	  if(out_i != out_end) 
// 	    cout << (vertex_id[*v_i]+1) << '\t' 
// 		 << (vertex_id[boost::target(*out_i, candidates[j])]+1) 
// 		 << endl;
// 	}
// 	cout << "---" << endl;
//       }
//       exit(1);

      vector<DPAG> bestPatterns = 
	topologySearcherTest.ConnectivityPatternSearch(rnn, 
						       candidates, 
						       bestScores);

      // output only the first THREE most likely configurations
      for(int j=0; j<10 && j<bestPatterns.size(); ++j) {
 	require(B == boost::num_edges(bestPatterns[j]) &&
 		B == boost::num_vertices(bestPatterns[j]) / 2, 
 		"Error in predicted number of disulfide bridges");

	// first output score (confidence?)
	cout << bestScores[j] << endl;
	vertex_id = boost::get(boost::vertex_index, bestPatterns[j]);
	outIter out_i, out_end;  
	for(boost::tie(v_i, v_end) = boost::vertices(bestPatterns[j]); 
	    v_i!=v_end; ++v_i) {
	  boost::tie(out_i, out_end)=boost::out_edges(*v_i, bestPatterns[j]);
	  if(out_i != out_end) 
	    cout << (vertex_id[*v_i]+1) << '\t' 
		 << (vertex_id[boost::target(*out_i, bestPatterns[j])]+1) 
		 << endl;
	}
      }
      break;
    }
  }

  // Deallocate Recursive Network structure
  delete rnn; rnn = 0;
}

int main(int argc, char* argv[]) {
  debugLevel=0;
  setenv("OPTIONTYPE", "RNNTrainingOptions", 1);

  try {
    Options::instance()->parse_args(argc,argv);
    
    string netname = Options::instance()->getParameter("netname");
    if(!netname.length()) {
      cerr << "Must specify a network file" << endl << endl;
      throw Options::BadOptionSetting(Options::instance()->getUsageString());
    }

    DataSet *trainingSet = NULL, *testSet = NULL;

    string datasetdir = Options::instance()->getParameter("datasetdir");
    if(datasetdir.length()) {
      string train_set_list = Options::instance()->getParameter("train_set");
      if(train_set_list.length()) {
	trainingSet = new DataSet(datasetdir.c_str(), train_set_list.c_str());
      } else { 
	cerr << "Train list not specified. Skipping..." << endl;
	exit(1);
      }

      string test_set_list = Options::instance()->getParameter("test_set");
      if(test_set_list.length()) {
	testSet = new DataSet(datasetdir.c_str(), test_set_list.c_str());
      } else {
	cerr << "Test list not specified. Skipping testing..." << endl;
	exit(1);
      }
    } else {
      cerr << "Must specify directory of data" << endl << endl;
      throw Options::BadOptionSetting(Options::instance()->getUsageString());
    }

    /*** Train the network and save results ***/
    if(trainingSet && testSet)
      predictPattern(netname.c_str(), trainingSet, testSet);
      
    // Deallocate training set
    if(trainingSet)
      delete trainingSet;

    // Deallocate test set
    if(testSet)
      delete testSet;
    
    exit(0);
  } catch(Options::BadOptionSetting e) {
    cerr << e.explainError() << endl;
    exit(1);
  }
}
