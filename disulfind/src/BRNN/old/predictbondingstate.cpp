#include "require.h"
#include "Options.h"
#include "DataSet.h"
#include "RecursiveNN.h"
#include <cstdlib>
#include <cfloat>
#include <ctime>
#include <vector>
#include <fstream>
#include <iostream>
using namespace std;

#define PR(x) cout << #x << ": " << x << endl
int debugLevel;

void predict_bonding_state(const char* netname, DataSet* testSet) {
  /*** Read network in if it exists ***/
  RecursiveNN<TanH, Sigmoid, MGradientDescent>* 
    rnn = new RecursiveNN<TanH, Sigmoid, MGradientDescent>(netname);
  
  for(DataSet::iterator it=testSet->begin(); it!=testSet->end(); ++it) {
    vector<Node*> *nodes = it.currenTONodes();
    std::pair<DPAG, DPAG> *dpags = it.currentDPAGs();

    rnn->propagateStructuredInput(*nodes, *dpags);
    for(std::vector<Node*>::iterator nit=nodes->begin(); 
	nit!=nodes->end(); ++nit)
      cout << (*nit)->_h_layers_activations[(*nit)->_s-1][0] << endl;
  }
  cout << endl;

  // Deallocate Recursive Network structure
  delete rnn; rnn = 0;
}

int main(int argc, char* argv[]) {
  debugLevel = 0;
  setenv("OPTIONTYPE", "RNNTrainingOptions", 1);

  try {
    Options::instance()->parse_args(argc,argv);
    
    string netname = Options::instance()->getParameter("netname");
    if(!netname.length()) {
      cerr << "Must specify a network file" << endl << endl;
      throw Options::BadOptionSetting(Options::instance()->getUsageString());
    }

    DataSet *testSet = NULL;

    string datasetdir = Options::instance()->getParameter("datasetdir");
    if(datasetdir.length()) {
      string test_set_list = Options::instance()->getParameter("test_set");
      if(test_set_list.length()) {
	testSet = new DataSet(datasetdir.c_str(), test_set_list.c_str());
      } else {
	cerr << "Test list not specified. Skipping testing..." << endl;
	exit(1);
      }
    } else {
      cerr << "Must specify directory of data" << endl << endl;
      throw Options::BadOptionSetting(Options::instance()->getUsageString());
    }

    if(testSet)
      predict_bonding_state(netname.c_str(), testSet);
    
    // Deallocate test set
    if(testSet)
      delete testSet;
    
    exit(0);
  } catch(Options::BadOptionSetting e) {
    cerr << e.explainError() << endl;
    exit(1);
  }
}
