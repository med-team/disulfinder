#include <string.h>
#include <cassert>
#include <libgen.h>
#include <cstdlib>

//#include "Protein.h"
//#include "utils.h"
//#include "SVM/SVMClassifier.h"
//#include "FSA-Alignment/viterbi_aligner.h"

#include "rnn_util.h"
#include <sstream>
#include <fstream>
#include <iostream>
using namespace std;

void defaultOutput(const string& sequence, const char* qfile, ostream& out){
  
  out << "-------------------------------------------------------------------------------\n"
      << "          Cysteines Disulfide Bonding State and Connectivity Predictor         \n"
      << "-------------------------------------------------------------------------------\n"
      << "\n\n"    
      << "Chain identifier: " << qfile << "\n\n\n"    
      << sequence << "\n\n"    
      << "The sequence contains no cysteine !!\n\n"    
      << "-------------------------------------------------------------------------------\n"
      << "Please cite:\n\n"
      << "P. Frasconi, A. Passerini, and A. Vullo.\n"
      << "\"A Two-Stage SVM Architecture for Predicting the Disulfide Bonding State of Cysteines\"\n"
      << "Proc. IEEE Workshop on Neural Networks for Signal Processing, pp. 25-34, 2002.\n\n"
      << "A. Vullo and P. Frasconi.\n"
      << "\"Disulfide Connectivity Prediction using Recursive Neural Networks and Evolutionary Information\"\n"
      << "Bioinformatics, 20, 653-659, 2004.\n\n"
    
      << "Questions and comments are very appreciated.\n"
      << "Please, send email to: cystein@dsi.unifi.it\n\n"
    
      << "Created by members of the Machine Learning and\n"
      << "Neural Networks Group, Universita' di Firenze\n\n"
      << "-------------------------------------------------------------------------------\n";  
}

int main(int argc, char ** argv){

  if(argc < 6){
    cerr << "\nUsage:\n\t" << basename(argv[0]) 
	 << " <chain id> <profile file>"
	 << " <output file> <connectivity alternatives> <ascii|html>\n"
	 << "\t<chain id>                   = chain identifier.\n"
	 << "\t<profile file>               = file containing chain profile as psi2 file.\n"
	 << "\t<output file>                = file where predictions will be saved.\n"
	 << "\t<connectivity alternatives>  = number of alternative connectivity patterns to report (0 to 3).\n"
	 << "\t<ascii|html>                 = output format.\n" << endl;
    exit(1);
  }
  
  
  // get command line arguments
  const char* qfile=argv[1];
  const char* psifile=argv[2];
  const char* outfile=argv[3];
  int alternatives = atoi(argv[4]); // CHECK user given input
  const char* format=argv[5];
    
  char charbuf[strlen(psifile)+1];
  strcpy(charbuf,psifile);
  const char* psidir=dirname(charbuf);

  // get environmental arguments
  const char * rootdir = getenv("PWD");
  string modeldir = (string)rootdir + "/Models";
  string pdir = (string)rootdir + "/Predictions";
  string wdir = (string)rootdir + "/tmp";

  /*** DECOMMENT ***/
  
  // open output file
  //ofstream outstream(outfile);
  //if (!outstream.good()){
  // cerr << "ERROR: could not open output file " << outfile << endl
  // << "Exiting..." << endl;
  //exit(1);
  //}

  // getting protein sequence
//   Protein info;
//   double profile_prior = 1.; // warning: default would be 0.5!! 
//   ifstream inprofile(psifile);
//   if (!inprofile.good()){
//     cerr << "ERROR: could not open profile file " << psifile << endl
// 	 << "Exiting..." << endl;
//     exit(1);
//   }
//   info.ReadPsiBlast2(inprofile,profile_prior);
//   string sequence=info.GetSequence();
  
//   // check if there is any cysteine, otherwise exit now
//   if(sequence.find('C') == string::npos){
//     defaultOutput(sequence, qfile, outstream);
//     exit(0);
//   }

//   /*** Predict bonding state ***/

//   // create svm input data
//   cout << "Creating input data for svm prediction ... "; 
//   string wfile = wdir + qfile;
//   string input = wfile + ".kernel.input";
//   createDataKernel(info, 7, input.c_str());
//   cout << "done" << endl;  
//   // svm prediction
//   cout << "Computing SVM prediction ... " << endl;
//   string model = modeldir + "/kernel.model";
//   string prediction = wfile + ".kernel.prediction";
//   SvmPredict(model.c_str(),input.c_str(),prediction.c_str());
//   cout << "done" << endl;

  /****************/  
  
  /********* Alessandro: Bonding State Prediction ********/

  setenv("OPTIONTYPE", "Default", 1); // default to standard option
  // create brnn input data
  cout << "Creating input data for BRNN prediction ... "; 
  string graph = (string)qfile + ".graph";
  string brnninput = wdir + "/" + graph; // wdir==Models???!!!

  /*** DECOMMENT ***/

  // TODO: check output file path (wdir==Models?)
  //createDataBRNN(qfile, info, model.c_str(), 
  //		 prediction.c_str(), brnninput.c_str());

  /*****************/

  cout << "done" << endl;  

  cout << "Computing BRNN prediction ... ";
  string brnnconfig = modeldir + "/brnn.config",
    brnnmodel = modeldir + "/brnn.model",
    brnnoutput = pdir + "/Bondstate/BRNN/" + qfile;

  /*** DECOMMENT ***/

  //rnn_predict_bonding_state(brnnconfig, brnnmodel, 
  //			    brnninput, brnnoutput);

  /*****************/

  cout << "done" << endl;

  /*******************************************************/

  /*** DECOMMENT ***/

  // TODO: check file paths

  // viterbi aligner adjusting non consistent predictions
  //model = modeldir + "/automa.model";
  string viterbiprediction = pdir + "/Bondstate/Viterbi/" + qfile;
  cout << "Computing Viterbi alignment ... ";
  
  //ViterbiAligner(model.c_str(), brnnoutput.c_str(), 
  //viterbiprediction.c_str());

  cout << "done" << endl;

  /*****************/

  /***
      Here code to create trivial pattern (B==1)
      or rejection (B==5)
  ***/
  int B = number_of_disulphides(viterbiprediction.c_str());

  // TODO
  if(B == 0); // STOP HERE
  if(B == 1); // assemble trivial output
  if(B > 5); // assemble rejection output
  
  /*** Alessandro: RNNs Predict Connectivity *************/

  // assume viterbi filter out is in "viterbiprediction"

  // create RNN data
  cout << "Creating RNN data for connectivity prediction ... ";
  string rnninput = modeldir + "/conn/data/graphs/" + qfile;

  create_connectivity_input_data(viterbiprediction, psifile, 5, B, rnninput);

  cout << "done" << endl;

  // predict connectivity 
  ostringstream rnnoss; 
  rnnoss << modeldir << "/conn/data/" 
	 << number_of_disulphides(viterbiprediction.c_str()) 
	 << '/';
  
  string rnnconfig = rnnoss.str() + "conf",
    rnnmodel = rnnoss.str() + "net",
    rnntraindir =  modeldir + "/conn/data/graphs",
    rnntrain = rnnoss.str() + "train",
    rnnoutput = pdir + "/Connectivity/" + qfile;
  
  cout << "Predicting disulphide connectivity ... ";

  rnn_predict_disulphides(rnnconfig, rnnmodel, rnntraindir, rnntrain, 
			  rnninput, rnnoutput, alternatives);
  
  cout << "done" << endl;
  
  /*******************************************************/
  
  // Predicted disulphides are stored in Predictions/Connectivity/qfile
   
}

