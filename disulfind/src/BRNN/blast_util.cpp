#include "blast_util.h"
#include <cstdio>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;

void PSSM::parsePsiBlastReport(const char* psi_outfile) throw(PSSM::BadRequest) {
  ifstream ifs(psi_outfile); 
  if(!ifs) {
    string message("Error opening profile file "); message += psi_outfile;
    throw BadRequest(message.c_str());
  }
  //if(!ifs) { cerr << "Error opening " << psi_outfile << endl; exit(1); }

  string line;
  // First three lines aren't pssm data
  getline(ifs, line); 
  getline(ifs, line);
  getline(ifs, line);
  // get from current line the amino acids at given profile positions
  char aa;
  istringstream iss(line);
  for(int i=0; i<20; ++i) {
    iss >> aa; 
    _aa_at_profile_pos.insert(make_pair(aa, i));
  }

  while(getline(ifs, line) && line.size()) {
    istringstream linestream(line);
    // read sequence position and amino acid char
    int sequence_pos; 
    Profile prof;
    push_back(Profile());
    linestream >> sequence_pos >> back().AA;
    
    // I dont't know what next 20 numbers are.
    for(int i=1; i<=20; ++i)
      linestream >> sequence_pos;
    
    // Ok, now read profile for current amino acid position
    bool all_zero = true;
    for(int i=0; i<20; ++i) {
      linestream >> back().profile[i];
      if(back().profile[i] != 0.0)
	all_zero = false;
    }

    if(all_zero)
      for(int i=0; i<20; ++i)
	back().profile[i] = .05;
	
    // and normalize it
    float norm_factor = 0.0;
    for(int i=0; i<20; ++i)
      norm_factor +=  back().profile[i];
    for(int i=0; i<20; ++i)
      back().profile[i] /= norm_factor;
    
  }
  
}
