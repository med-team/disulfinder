#ifndef _PROTEIN_UTIL_H_
#define _PROTEIN_UTIL_H_

#include <stdlib.h>
#include <cfloat>
#include <cmath>
#include <exception>
#include <list>
#include <map>
#include <set>
#include <string>
#include <iostream>


// Kyte-Doolittle hydrophobicity scale
class HydrophobicityScale: public std::map<char, float> {
 public:
  HydrophobicityScale() {
    this->insert(std::make_pair('A', 1.8)); this->insert(std::make_pair('R', -4.5));
    this->insert(std::make_pair('N', -3.5)); this->insert(std::make_pair('D', -3.5));
    this->insert(std::make_pair('C', 2.5)); this->insert(std::make_pair('Q', -3.5));
    this->insert(std::make_pair('E', -3.5)); this->insert(std::make_pair('G', -.4));
    this->insert(std::make_pair('H', -3.2)); this->insert(std::make_pair('I', 4.5));
    this->insert(std::make_pair('L', 3.8)); this->insert(std::make_pair('K', -3.9));
    this->insert(std::make_pair('M', 1.9)); this->insert(std::make_pair('F', 2.8));
    this->insert(std::make_pair('P', -1.6)); this->insert(std::make_pair('S', -.8));
    this->insert(std::make_pair('T', -.7)); this->insert(std::make_pair('W', -.9));
    this->insert(std::make_pair('Y', -1.3)); this->insert(std::make_pair('V', 4.2));
    //this->insert(std::make_pair('B', -3.5)); this->insert(std::make_pair('Z', -3.5));
    //this->insert(std::make_pair('X', 0.0));
    this->insert(std::make_pair(' ', 4.2));
  }
};
/*                                                                          *
*  +----+----+----+----+----+----+----+----+----+----+----+----+           *
*  |  A |  B |  C |  D |  E |  F |  G |  H |  I |  K |  L |  M |           *
*  | 106| 160| 135| 163| 194| 197|  84| 184| 169| 205| 164| 188|           *
*  +----+----+----+----+----+----+----+----+----+----+----+----+           *
*  |  N |  P |  Q |  R |  S |  T |  V |  W |  X |  Y |  Z |                *
*  | 157| 136| 198| 248| 130| 142| 142| 227| 180| 222| 196|                *
*  +----+----+----+----+----+----+----+----+----+----+----+                */

class MaxSolventAcc: public std::map<char, float> {
 public:
  MaxSolventAcc() {
    this->insert(std::make_pair('A', 106)); this->insert(std::make_pair('R', 248));
    this->insert(std::make_pair('N', 157)); this->insert(std::make_pair('D', 163));
    this->insert(std::make_pair('C', 135)); this->insert(std::make_pair('Q', 198));
    this->insert(std::make_pair('E', 194)); this->insert(std::make_pair('G', 84));
    this->insert(std::make_pair('H', 184)); this->insert(std::make_pair('I', 169));
    this->insert(std::make_pair('L', 164)); this->insert(std::make_pair('K', 205));
    this->insert(std::make_pair('M', 188)); this->insert(std::make_pair('F', 197));
    this->insert(std::make_pair('P', 136)); this->insert(std::make_pair('S', 130));
    this->insert(std::make_pair('T', 142)); this->insert(std::make_pair('W', 227));
    this->insert(std::make_pair('Y', 222)); this->insert(std::make_pair('V', 142));
  }
};

// Contact Potentials 
class ContactPotentials: public std::map<char, std::map<char, float> > {
 public:
  ContactPotentials() {
    insert(std::make_pair('A', std::map<char, float>()));
    insert(std::make_pair('C', std::map<char, float>()));
    insert(std::make_pair('D', std::map<char, float>()));
    insert(std::make_pair('E', std::map<char, float>()));
    insert(std::make_pair('F', std::map<char, float>()));
    insert(std::make_pair('G', std::map<char, float>()));
    insert(std::make_pair('H', std::map<char, float>()));
    insert(std::make_pair('I', std::map<char, float>()));
    insert(std::make_pair('K', std::map<char, float>()));
    insert(std::make_pair('L', std::map<char, float>()));
    insert(std::make_pair('M', std::map<char, float>()));
    insert(std::make_pair('N', std::map<char, float>()));
    insert(std::make_pair('P', std::map<char, float>()));
    insert(std::make_pair('Q', std::map<char, float>()));
    insert(std::make_pair('R', std::map<char, float>()));
    insert(std::make_pair('S', std::map<char, float>()));
    insert(std::make_pair('T', std::map<char, float>()));
    insert(std::make_pair('V', std::map<char, float>()));
    insert(std::make_pair('W', std::map<char, float>()));
    insert(std::make_pair('Y', std::map<char, float>()));

    // values are derived with Monte-Carlo simulations
    (*this)['A']['A'] = 1.077; (*this)['A']['C'] = .892; (*this)['A']['D'] = 1.073; 
    (*this)['A']['E'] = 1.064; (*this)['A']['F'] = 1.155; (*this)['A']['G'] = 1.111; 
    (*this)['A']['H'] = 1.097; (*this)['A']['I'] = 1.018; (*this)['A']['K'] = 1.143; 
    (*this)['A']['L'] = 1.244; (*this)['A']['M'] = 1.344; (*this)['A']['N'] = 1.180; 
    (*this)['A']['P'] = .882; (*this)['A']['Q'] = 1.081; (*this)['A']['R'] = 1.087; 
    (*this)['A']['S'] = 1.107; (*this)['A']['T'] = .897; (*this)['A']['V'] = 1.122; 
    (*this)['A']['W'] = 1.202; (*this)['A']['Y'] = .840; 

    (*this)['C']['C'] = .566; (*this)['C']['D'] = .807; (*this)['C']['E'] = .765; 
    (*this)['C']['F'] = .619; (*this)['C']['G'] = .798; (*this)['C']['H'] = .949; 
    (*this)['C']['I'] = .757; (*this)['C']['K'] = .971; (*this)['C']['L'] = .811; 
    (*this)['C']['M'] = .570; (*this)['C']['N'] = .914; (*this)['C']['P'] = .905; 
    (*this)['C']['Q'] = .780; (*this)['C']['R'] = .758; (*this)['C']['S'] = .759; 
    (*this)['C']['T'] = .926; (*this)['C']['V'] = .845; (*this)['C']['W'] = .907; 
    (*this)['C']['Y'] = .641; 

    (*this)['D']['D'] = 1.028; (*this)['D']['E'] = .876; (*this)['D']['F'] = .828; 
    (*this)['D']['G'] = .889; (*this)['D']['H'] = .716; (*this)['D']['I'] = .990; 
    (*this)['D']['K'] = .858; (*this)['D']['L'] = 1.054; (*this)['D']['M'] = 1.205; 
    (*this)['D']['N'] = .936; (*this)['D']['P'] = .865; (*this)['D']['Q'] = 1.145; 
    (*this)['D']['R'] = .761; (*this)['D']['S'] = 1.318; (*this)['D']['T'] = .998; 
    (*this)['D']['V'] = 1.019; (*this)['D']['W'] = 1.129; (*this)['D']['Y'] = 1.008; 

    (*this)['E']['E'] = .998; (*this)['E']['F'] = 1.034; (*this)['E']['G'] = .944; 
    (*this)['E']['H'] = .539; (*this)['E']['I'] = .945; (*this)['E']['K'] = 1.007; 
    (*this)['E']['L'] = 1.171; (*this)['E']['M'] = 1.193; (*this)['E']['N'] = .941; 
    (*this)['E']['P'] = .737; (*this)['E']['Q'] = 1.123; (*this)['E']['R'] = 1.106; 
    (*this)['E']['S'] = .956; (*this)['E']['T'] = .950; (*this)['E']['V'] = 1.024;  
    (*this)['E']['W'] = 1.130; (*this)['E']['Y'] = .751;

    (*this)['F']['F'] = 1.195; (*this)['F']['G'] = .783; (*this)['F']['H'] = .962; 
    (*this)['F']['I'] = .787; (*this)['F']['K'] = .959; (*this)['F']['L'] = 1.308; 
    (*this)['F']['M'] = 1.359; (*this)['F']['N'] = .868; (*this)['F']['P'] = .960; 
    (*this)['F']['Q'] = 1.082; (*this)['F']['R'] = 1.287; (*this)['F']['S'] = 1.031; 
    (*this)['F']['T'] = .888; (*this)['F']['V'] = .969; (*this)['F']['W'] = 1.545; 
    (*this)['F']['Y'] = .879;

    (*this)['G']['G'] = 1.304; (*this)['G']['H'] = 1.433; (*this)['G']['I'] = .845; 
    (*this)['G']['K'] = .877; (*this)['G']['L'] = 1.013; (*this)['G']['M'] = .856; 
    (*this)['G']['N'] = .822; (*this)['G']['P'] = .937; (*this)['G']['Q'] = .948; 
    (*this)['G']['R'] = .956; (*this)['G']['S'] = 1.097; (*this)['G']['T'] = 1.052; 
    (*this)['G']['V'] = 1.035; (*this)['G']['W'] = 1.043; (*this)['G']['Y'] = .910;

    (*this)['H']['H'] = 1.568; (*this)['H']['I'] = .870; (*this)['H']['K'] = .764; 
    (*this)['H']['L'] = 1.336; (*this)['H']['M'] = 1.164; (*this)['H']['N'] = .797; 
    (*this)['H']['P'] = 1.107; (*this)['H']['Q'] = .636; (*this)['H']['R'] = .686; 
    (*this)['H']['S'] = .859; (*this)['H']['T'] = 1.055; (*this)['H']['V'] = 1.103;  
    (*this)['H']['W'] = 2.015; (*this)['H']['Y'] = 1.496;

    (*this)['I']['I'] = .992; (*this)['I']['K'] = .948; (*this)['I']['L'] = 1.132; 
    (*this)['I']['M'] = 1.277; (*this)['I']['N'] = 1.117; (*this)['I']['P'] = .982; 
    (*this)['I']['Q'] = 1.016; (*this)['I']['R'] = .828; (*this)['I']['S'] = .863; 
    (*this)['I']['T'] = 1.025; (*this)['I']['V'] = .838; (*this)['I']['W'] = 1.188; 
    (*this)['I']['Y'] = .856;

    (*this)['K']['K'] = 1.017; (*this)['K']['L'] = 1.144; (*this)['K']['M'] = .880; 
    (*this)['K']['N'] = 1.141; (*this)['K']['P'] = 1.050; (*this)['K']['Q'] = 1.094; 
    (*this)['K']['R'] = .906; (*this)['K']['S'] = 1.061; (*this)['K']['T'] = .910; 
    (*this)['K']['V'] = 1.139; (*this)['K']['W'] = 1.012; (*this)['K']['Y'] = 1.047;

    (*this)['L']['L'] = 1.233; (*this)['L']['M'] = .781; (*this)['L']['N'] = 1.051; 
    (*this)['L']['P'] = .932; (*this)['L']['Q'] = 1.126; (*this)['L']['R'] = 1.119; 
    (*this)['L']['S'] = 1.024; (*this)['L']['T'] = 1.220; (*this)['L']['V'] = 1.074;  
    (*this)['L']['W'] = 1.118; (*this)['L']['Y'] = 1.260;

    (*this)['M']['M'] = .715; (*this)['M']['N'] = 1.295; (*this)['M']['P'] = 1.003; 
    (*this)['M']['Q'] = .924; (*this)['M']['R'] = 1.150; (*this)['M']['S'] = 1.149; 
    (*this)['M']['T'] = 1.004; (*this)['M']['V'] = 1.280; (*this)['M']['W'] = 1.334; 
    (*this)['M']['Y'] = 1.005;

    (*this)['N']['N'] = 1.008; (*this)['N']['P'] = .898; (*this)['N']['Q'] = .852; 
    (*this)['N']['R'] = .906; (*this)['N']['S'] = 1.176; (*this)['N']['T'] = 1.013; 
    (*this)['N']['V'] = 1.290; (*this)['N']['W'] = .706; (*this)['N']['Y'] = 1.054;

    (*this)['P']['P'] = 1.037; (*this)['P']['Q'] = 1.140; (*this)['P']['R'] = .933; 
    (*this)['P']['S'] = .943; (*this)['P']['T'] = 1.069; (*this)['P']['V'] = 1.061;  
    (*this)['P']['W'] = 1.133; (*this)['P']['Y'] = 1.005;

    (*this)['Q']['Q'] = 1.149; (*this)['Q']['R'] = 1.003; (*this)['Q']['S'] = 1.052; 
    (*this)['Q']['T'] = 1.296; (*this)['Q']['V'] = 1.327; (*this)['Q']['W'] = 1.070; 
    (*this)['Q']['Y'] = 1.115;

    (*this)['R']['R'] = .897; (*this)['R']['S'] = 1.115; (*this)['R']['T'] = 1.216; 
    (*this)['R']['V'] = 1.050; (*this)['R']['W'] = 1.011; (*this)['R']['Y'] = .999;

    (*this)['S']['S'] = 1.111; (*this)['S']['T'] = 1.111; (*this)['S']['V'] = 1.080;  
    (*this)['S']['W'] = 1.723; (*this)['S']['Y'] = 1.032;

    (*this)['T']['T'] = .843; (*this)['T']['V'] = 1.153;  (*this)['T']['W'] = 1.093; 
    (*this)['T']['Y'] = .969;

    (*this)['V']['V'] = 1.302;  (*this)['V']['W'] = 1.097; (*this)['V']['Y'] = 1.016;

    (*this)['W']['W'] = .857; (*this)['W']['Y'] = 1.115;

    (*this)['Y']['Y'] = .870;
  }
};

class Coordinate3D {
 public:
  float _x, _y, _z;
  
  Coordinate3D(float x=0.0, float y=0.0,  float z=0.0): _x(x), _y(y), _z(z) {}
  Coordinate3D(const Coordinate3D& c): _x(c._x), _y(c._y), _z(c._z) {}

  float euclDistFrom(const Coordinate3D& c) const {
    return 
      static_cast<float>(sqrt((_x - c._x) * (_x - c._x) + 
			      (_y - c._y) * (_y - c._y) +
			      (_z - c._z) * (_z - c._z)));
  }

  friend std::ostream& operator<<(std::ostream& os, const Coordinate3D& c) {
    os << c._x << ' ' << c._y << ' ' << c._z;
    return os;
  }
};

class Residue {
 public:
  int _index, _sindex, _ssp; // DSSP index, sequential index and disulfide bond partner
  char _symbol, _ssbridge; // one char standard amino acid symbol and ss bond indicator
  float _hydr, _acc; // residue hydrophobicity and acc. values
  Coordinate3D _coord; // residue representative coordinates
  char _bl1, _bl2, _sheet; // bridge labels and sheets
  int _bp1, _bp2; // bridge partners

  Residue(int index, int sindex, char symbol, float x, float y, float z, float hydr, float acc, char ssbridge):
    _index(index), _sindex(sindex), _symbol(symbol), _coord(x, y, z), 
    _hydr(hydr), _acc(acc), _ssbridge(ssbridge) {
    _bl1 = _bl2 = _sheet = ' '; _bp1 = _bp2 = 0;
    _ssp = 0;
  }
  Residue(const Residue& r): 
    _index(r._index), _sindex(r._sindex), _symbol(r._symbol), _coord(r._coord), 
    _hydr(r._hydr), _acc(r._acc), _ssbridge(r._ssbridge),
    _bl1(r._bl1), _bl2(r._bl2), _sheet(r._sheet), _bp1(r._bp1), _bp2(r._bp2), _ssp(r._ssp) {}

  char getAASymbol() const { return _symbol; }
  int getDisulfidePartner() const { return _ssp; }
};

class SSegment: public std::list<Residue> {
  char _symbol;
  std::string bsheets;
  std::string bbridges;

  typedef struct Hydrophobicity {
    float avg, min, max;
    Hydrophobicity(): avg(0.0), min(FLT_MAX), max(-FLT_MAX) {}
   
  } Hydrophobicity;

  Hydrophobicity _hydr; // hydropatic tendency (?!) of the segment

  Coordinate3D _baricentre; // baricentrum of C_alpha atoms set

 public:
  SSegment() {}
  SSegment(char symbol):
    _symbol(symbol), _baricentre(), _hydr() {}

  float first[3], last[3], median[3];

  void addResidue(int index, int sindex, char symbol, float x, float y,  float z, float hydr, float acc, char ssb) {
    push_back(Residue(index, sindex, symbol, x, y, z, hydr, acc, ssb));
  }
  void addResidue(const Residue& r) { push_back(r); }
  void addBSheet(char s) { bsheets += s; }
  void addBBridge(char b) { bbridges += b; }

  char getSSymbol() const { return _symbol; }
  std::string getBSheets() const { return bsheets; }
  std::string getBBridges() const { return bbridges; }
  int getSheetPairingWith(const SSegment& s) const;
  int numResidues() const { return size(); }

  Coordinate3D computeBaricentre() const;
  Coordinate3D getBaricentre() const { return _baricentre; }
  void setBaricentre() { _baricentre = computeBaricentre(); }
  void computeReprVector();

  void setHydrophobicity(float avg, float min, float max) {
    _hydr.avg = avg;
    _hydr.min = min;
    _hydr.max = max;
  }

  float getHydrophobicity(std::string type) const { 
    if(type == "avg")
      return _hydr.avg;
    if(type == "min")
      return _hydr.min;
    if(type == "max")
      return _hydr.max;
    std::cerr << "Unknown request" << std::endl;
    exit(1);
  }

  double norm() const {
    double n = .0;
    for(int i=0; i<3; ++i)
      n += (last[i] - first[i]) * (last[i] - first[i]);
    return sqrt(n);
  }
};

class Chain: public std::list<SSegment> {
  char _id;
  bool _chainbreak;
  int _fres_index; // index of first residue
 public:
  Chain(): _chainbreak(false) {}
  Chain(char id): _id(id), _chainbreak(false) {}

  void parseDsspFile(std::istream&);
  int numResidues() const;
  int numSSegments() const { return size(); }
  char getId() const { return _id; }
  bool HasChainBreaks() const { return _chainbreak; }
  int getStartIndex() const { return _fres_index; }
  std::string getPrimaryStruct() const;
  std::string getSecondaryStruct() const;

  void setSShydrophobicity();
};

class Protein: public std::list<Chain> {
  //std::string _id;
  void parseDsspFile(const char*);

 public:
  class BadRequest: public std::exception {
    std::string _reason;
  public:
    BadRequest(std::string reason): _reason(reason) {}
    ~BadRequest() throw() {}

    const char* explain() const {
      return _reason.c_str();
    }
  };

  Protein(const char*);
  
  int numChains() const { return size(); }
  int numResidues() const;

  Chain getChain(char id) const
    throw(BadRequest);
};


float dotProduct(const SSegment&, const SSegment&);
float dist(const SSegment&, const SSegment&);
float angle(const SSegment&, const SSegment&);
float dihedralAngle(const SSegment&, const SSegment&);


typedef struct SegmentInfo {
  char _symbol; // symbol of the segment (H, E or C)
  int _length; // its length in residues

  Coordinate3D _baricentre; // coordinates of its baricentre
  float _hydr, _min_hydr, _max_hydr; // hydropatic tendence of the segment
  std::map<char, double> resFreqs;

  SegmentInfo(char s, int len, const Coordinate3D& b, 
	      float hydr, float min_h, float max_h):
    _symbol(s), _length(len), _baricentre(b), _hydr(hydr), _min_hydr(min_h), _max_hydr(max_h) {
    resFreqs['A'] = 0;  resFreqs['R'] = 0;  resFreqs['N'] = 0; resFreqs['D'] = 0;   
    resFreqs['C'] = 0;  resFreqs['Q'] = 0;  resFreqs['E'] = 0; resFreqs['G'] = 0;  
    resFreqs['H'] = 0;  resFreqs['I'] = 0;  resFreqs['L'] = 0; resFreqs['K'] = 0;
    resFreqs['M'] = 0;  resFreqs['F'] = 0;  resFreqs['P'] = 0; resFreqs['S'] = 0;
    resFreqs['T'] = 0;  resFreqs['W'] = 0;  resFreqs['Y'] = 0; resFreqs['V'] = 0;
    //resFreqs['B'] = 0;  resFreqs['Z'] = 0;  resFreqs['X'] = 0;
  }

} SegmentInfo;

#endif // _PROTEIN_UTIL_H_
