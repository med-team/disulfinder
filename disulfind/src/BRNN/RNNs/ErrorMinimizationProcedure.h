/*
  Templatized Strategy Pattern implementation
  of different Error Minimization Procedures.
*/

#ifndef _ERROR_MINIMIZATION_PROCEDURE_H
#define _ERROR_MINIMIZATION_PROCEDURE_H

#include <iostream>
#include <string.h>
#include <vector>
using std::cout;
using std::endl;

/* Simple Gradient Descent */
class GradientDescent {
  // Simply update networks weights in the opposite direction 
  // of the current gradient stored in the net and with a 
  // distance defined by learning rate.
  bool _process_dr, _ss_tr, _ios_tr;
  int _n, _v, _m, _r, _s;
  std::vector<int> _lnunits;

  public:
  template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
    void setInternals(RNN<T1, T2, T3>* const rnn);

  template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
    void updateWeights(RNN<T1, T2, T3>* const rnn, float, float = 0.0);
};

template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
  void GradientDescent::setInternals(RNN<T1, T2, T3>* const rnn) {
    // Simply synchronize private member with those of recursive network
    _process_dr = rnn->_process_dr;
    _ss_tr = rnn->_ss_tr;
    _ios_tr = rnn->_ios_tr;
    _n = rnn->_n, _v = rnn->_v, _m = rnn->_m, _r = rnn->_r, _s = rnn->_s;
    _lnunits = rnn->_lnunits;
}

template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN> 
void GradientDescent::updateWeights(RNN<T1, T2, T3>* const rnn, float _learning_rate, float) {
  // rnn (the recursive network) store gradient components, so to obtain
  // weight update rule change the sign of these components and multiply
  // by the learning rate
  
  if(_ss_tr) {
    // begin with layers of the MLP output function
    for(int i=0; i<_m; i++) {
      for(int j=0; j<_lnunits[_r]; j++) {
	// Save parameters in case we make a bad move.
	rnn->_prev_g_layers_w[0][i][j] = rnn->_g_layers_w[0][i][j];
	rnn->_g_layers_w[0][i][j] += -_learning_rate * rnn->_g_layers_gradient_w[0][i][j];
      }
    }
    if(_process_dr) {
      for(int i=_m; i<2*_m; i++) {
	for(int j=0; j<_lnunits[_r]; j++) {
	  rnn->_prev_g_layers_w[0][i][j] = rnn->_g_layers_w[0][i][j];
	  rnn->_g_layers_w[0][i][j] += -_learning_rate * rnn->_g_layers_gradient_w[0][i][j];
	}
      }
    }
    for(int j=0; j<_lnunits[_r]; j++) {
      rnn->_prev_g_layers_w[0][2*_m][j] = rnn->_g_layers_w[0][2*_m][j];
      rnn->_g_layers_w[0][2*_m][j] += -_learning_rate * rnn->_g_layers_gradient_w[0][2*_m][j];
    }
    for(int k=1; k<_s; k++) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	for(int j=0; j<_lnunits[_r+k]; j++) {
	  rnn->_prev_g_layers_w[k][i][j] = rnn->_g_layers_w[k][i][j];
	  rnn->_g_layers_w[k][i][j] += -_learning_rate * rnn->_g_layers_gradient_w[k][i][j];
	}
      }
    }
  }

  if(_ios_tr) {
    // proceed with layers of the h map
    for(int i=0; i<_m; i++) {
      for(int j=0; j<_lnunits[_r]; j++) {
	// Save parameters in case we make a bad move.
	rnn->_prev_h_layers_w[0][i][j] = rnn->_h_layers_w[0][i][j];
	rnn->_h_layers_w[0][i][j] += -_learning_rate * rnn->_h_layers_gradient_w[0][i][j];
      }
    }
    if(_process_dr) {
      for(int i=_m; i<2*_m; i++) {
	for(int j=0; j<_lnunits[_r]; j++) {
	  rnn->_prev_h_layers_w[0][i][j] = rnn->_h_layers_w[0][i][j];
	  rnn->_h_layers_w[0][i][j] += -_learning_rate * rnn->_h_layers_gradient_w[0][i][j];
	}
      }
    }
    for(int i=2*_m; i<2*_m+_n; i++) {
      for(int j=0; j<_lnunits[_r]; j++) {
	// Save parameters in case we make a bad move.
	rnn->_prev_h_layers_w[0][i][j] = rnn->_h_layers_w[0][i][j];
	rnn->_h_layers_w[0][i][j] += -_learning_rate * rnn->_h_layers_gradient_w[0][i][j];
      }
    }
    for(int j=0; j<_lnunits[_r]; j++) {
      rnn->_prev_h_layers_w[0][2*_m + _n][j] = rnn->_h_layers_w[0][2*_m + _n][j];
      rnn->_h_layers_w[0][2*_m + _n][j] += -_learning_rate * rnn->_h_layers_gradient_w[0][2*_m + _n][j];
    }
    for(int k=1; k<_s; k++) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	for(int j=0; j<_lnunits[_r+k]; j++) {
	  rnn->_prev_h_layers_w[k][i][j] = rnn->_h_layers_w[k][i][j];
	  rnn->_h_layers_w[k][i][j] += -_learning_rate * rnn->_h_layers_gradient_w[k][i][j];
	}
      }
    }
  }


  // proceed with casual part folding layers
  // (eventually the non-casual ones).
  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    for(int j=0; j<_lnunits[0]; j++) {
      rnn->_prev_f_layers_w[0][i][j] = rnn->_f_layers_w[0][i][j];
      rnn->_f_layers_w[0][i][j] += -_learning_rate * rnn->_f_layers_gradient_w[0][i][j];
      if(_process_dr) {
	rnn->_prev_b_layers_w[0][i][j] = rnn->_b_layers_w[0][i][j];
	rnn->_b_layers_w[0][i][j] += -_learning_rate * rnn->_b_layers_gradient_w[0][i][j];
      }
    }
  }
    
  for(int k=1; k<_r; k++) {
    for(int i=0; i<_lnunits[k-1]+1; i++) {
      for(int j=0; j<_lnunits[k]; j++) {
	rnn->_prev_f_layers_w[k][i][j] = rnn->_f_layers_w[k][i][j];
	rnn->_f_layers_w[k][i][j] += -_learning_rate * rnn->_f_layers_gradient_w[k][i][j];
	if(_process_dr) {
	  rnn->_prev_b_layers_w[k][i][j] = rnn->_b_layers_w[k][i][j];
	  rnn->_b_layers_w[k][i][j] += -_learning_rate * rnn->_b_layers_gradient_w[k][i][j];
	}
      }
    }
  } 
}

/* Gradient Descent with momentum */
class MGradientDescent {
  // Store previuos step weights delta values and update
  // weights with net current gradient and these values.
  float*** _f_layers_old_deltas_w;
  float*** _b_layers_old_deltas_w;
  float*** _g_layers_old_deltas_w;
  float*** _h_layers_old_deltas_w;

  bool _process_dr, _ss_tr, _ios_tr;
  int _n, _v, _m, _r, _s;
  std::vector<int> _lnunits;
  
 public:
  ~MGradientDescent();

  template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
    void setInternals(RNN<T1, T2, T3>* const rnn);

  template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
    void updateWeights(RNN<T1, T2, T3>* const rnn, float = 0.0, float = 0.0, float = 0.0);
};

MGradientDescent::~MGradientDescent() {
#ifdef DEBUG
  cout << "Deallocating MGD (f) old delta matrix at layer 1" << endl;
#endif
  if(_f_layers_old_deltas_w[0]) {
    for(int i=0; i<(_n+_v*_m) + 1; i++) {
      if(_f_layers_old_deltas_w[0][i])
	delete[] _f_layers_old_deltas_w[0][i];
      _f_layers_old_deltas_w[0][i] = 0;
    }
    delete[] _f_layers_old_deltas_w[0];
    _f_layers_old_deltas_w[0] = 0;
  }

  for(int k=1; k<_r; k++) {
#ifdef DEBUG
    cout << "Deallocating MGD (f) old delta matrix at layer: " << k+1 << endl;
#endif
    if(_f_layers_old_deltas_w[k]) {
      for(int i=0; i<_lnunits[k-1]+1; i++) {
	if(_f_layers_old_deltas_w[k][i])
	  delete[] _f_layers_old_deltas_w[k][i];
	_f_layers_old_deltas_w[k][i] = 0;
      }
      delete[] _f_layers_old_deltas_w[k];
      _f_layers_old_deltas_w[k] = 0;
    }
    
  }
  
  delete[] _f_layers_old_deltas_w;
  _f_layers_old_deltas_w = 0;

  if(_process_dr) {
#ifdef DEBUG
    cout << "Deallocating MGD (b) old delta matrix at layer 1" << endl;
#endif
    if(_b_layers_old_deltas_w[0]) {
      for(int i=0; i<(_n+_v*_m) + 1; i++) {
	if(_b_layers_old_deltas_w[0][i])
	  delete[] _b_layers_old_deltas_w[0][i];
	_b_layers_old_deltas_w[0][i] = 0;
      }
      delete[] _b_layers_old_deltas_w[0];
      _b_layers_old_deltas_w[0] = 0;
    }

    for(int k=1; k<_r; k++) {
#ifdef DEBUG
      cout << "Deallocating MGD (b) old delta matrix at layer: " << k+1 << endl;
#endif
      if(_b_layers_old_deltas_w[k]) {
	for(int i=0; i<_lnunits[k-1]+1; i++) {
	  if(_b_layers_old_deltas_w[k][i])
	    delete[] _b_layers_old_deltas_w[k][i];
	  _b_layers_old_deltas_w[k][i] = 0;
	}
	delete[] _b_layers_old_deltas_w[k];
	_b_layers_old_deltas_w[k] = 0;
      }
    }
  
    delete[] _b_layers_old_deltas_w;
    _b_layers_old_deltas_w = 0;
  }
    
  if(_ss_tr) {
#ifdef DEBUG
    cout << "Deallocating MGD (g) old delta matrix at layer 1" << endl;
#endif
    if(_g_layers_old_deltas_w[0]) {
      for(int i=0; i<2*_m + 1; i++) {
	if(_g_layers_old_deltas_w[0][i])
	  delete[] _g_layers_old_deltas_w[0][i];
	_g_layers_old_deltas_w[0][i] = 0;
      }
      delete[] _g_layers_old_deltas_w[0];
      _g_layers_old_deltas_w[0] = 0;
    }

    for(int k=1; k<_s; k++) {
#ifdef DEBUG
      cout << "Deallocating MGD (g) old delta matrix at layer: " << k+1 << endl;
#endif
      if(_g_layers_old_deltas_w[k]) {
	for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	  if(_g_layers_old_deltas_w[k][i])
	    delete[] _g_layers_old_deltas_w[k][i];
	  _g_layers_old_deltas_w[k][i] = 0;
	}
	delete[] _g_layers_old_deltas_w[k];
	_g_layers_old_deltas_w[k] = 0;
      }
    }
  
    delete[] _g_layers_old_deltas_w;
    _g_layers_old_deltas_w = 0;
  }

  if(_ios_tr) {
#ifdef DEBUG
    cout << "Deallocating MGD (h) old delta matrix at layer 1" << endl;
#endif
    if(_h_layers_old_deltas_w[0]) {
      for(int i=0; i<2*_m + _n + 1; i++) {
	if(_h_layers_old_deltas_w[0][i])
	  delete[] _h_layers_old_deltas_w[0][i];
	_h_layers_old_deltas_w[0][i] = 0;
      }
      delete[] _h_layers_old_deltas_w[0];
      _h_layers_old_deltas_w[0] = 0;
    }

    for(int k=1; k<_s; k++) {
#ifdef DEBUG
      cout << "Deallocating MGD (h) old delta matrix at layer: " << k+1 << endl;
#endif
      if(_h_layers_old_deltas_w[k]) {
	for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	  if(_h_layers_old_deltas_w[k][i])
	    delete[] _h_layers_old_deltas_w[k][i];
	  _h_layers_old_deltas_w[k][i] = 0;
	}
	delete[] _h_layers_old_deltas_w[k];
	_h_layers_old_deltas_w[k] = 0;
      }
    }
  
    delete[] _h_layers_old_deltas_w;
    _h_layers_old_deltas_w = 0;
  }
}


typedef	float*	floatref;
typedef	float**	floatrefref;


template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
								    void MGradientDescent::setInternals(RNN<T1, T2, T3>* const rnn) {
  // Assume rnn constructor has initialized 
  // required dimension quantities.
  _process_dr = rnn->_process_dr;
  _ss_tr = rnn->_ss_tr; _ios_tr = rnn->_ios_tr;
  _n = rnn->_n, _v = rnn->_v, _m = rnn->_m, _r = rnn->_r, _s = rnn->_s;
  _lnunits = rnn->_lnunits;

  _f_layers_old_deltas_w = new floatrefref[_r];
  
  // Allocate weights and gradient components matrixes
  // for f folding part. Connections from input layer
  // have special dimensions.
#ifdef DEBUG
  cout << endl << endl 
       << "MGD (f) Layer 1: (" << (_n+_v*_m) + 1 << "," << _lnunits[0] << ")" 
       << endl;
#endif
  _f_layers_old_deltas_w[0] = new floatref[(_n + _v * _m) + 1];

  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    _f_layers_old_deltas_w[0][i] = new float[_lnunits[0]];

    // Reset gradient for corresponding weights
    memset(_f_layers_old_deltas_w[0][i], 0, _lnunits[0] * sizeof(float));
  }
  
  // Allocate f weights&gradient matrixes for f folding part.
  for(int k=1; k<_r; k++) {
    // Allocate space for weight&delta matrix between layer i-1 and i.
    // Automatically include space for threshold unit in layer i-1
#ifdef DEBUG
    cout << endl << endl
	 << "MGD (f) Layer " << k+1 << ": (" << _lnunits[k-1] + 1  << "," << _lnunits[k] << ")" 
	 << endl;
#endif
    _f_layers_old_deltas_w[k] = new floatref[_lnunits[k-1] + 1];

    for(int i=0; i<_lnunits[k-1]+1; i++) {
      _f_layers_old_deltas_w[k][i] = new float[_lnunits[k]];

      // Reset f gradient components for corresponding weights
      memset(_f_layers_old_deltas_w[k][i], 0, (_lnunits[k])*sizeof(float));
    }
    
  }

  // eventually allocate space for b layers gradient structures.
  if(_process_dr) {
    _b_layers_old_deltas_w = new floatrefref[_r];
  
    // Allocate weights and gradient components matrixes
    // for f folding part. Connections from input layer
    // have special dimensions.
#ifdef DEBUG
    cout << endl << endl 
	 << "MGD (b) Layer 1: (" << (_n+_v*_m) + 1 << "," << _lnunits[0] << ")" 
	 << endl;
#endif
    _b_layers_old_deltas_w[0] = new floatref[(_n + _v * _m) + 1];

    for(int i=0; i<(_n+_v*_m) + 1; i++) {
      _b_layers_old_deltas_w[0][i] = new float[_lnunits[0]];

      // Reset gradient for corresponding weights
      memset(_b_layers_old_deltas_w[0][i], 0, _lnunits[0] * sizeof(float));
    }
  
    // Allocate weights&gradient matrixes for b folding part.
    for(int k=1; k<_r; k++) {
      // Allocate space for weight&delta matrix between layer i-1 and i.
      // Automatically include space for threshold unit in layer i-1
#ifdef DEBUG
      cout << endl << endl
	   << "MGD (b) Layer " << k+1 << ": (" << _lnunits[k-1] + 1  << "," << _lnunits[k] << ")" 
	   << endl;
#endif
      _b_layers_old_deltas_w[k] = new floatref[_lnunits[k-1] + 1];

      for(int i=0; i<_lnunits[k-1]+1; i++) {
	_b_layers_old_deltas_w[k][i] = new float[_lnunits[k]];

	// Reset f gradient components for corresponding weights
	memset(_b_layers_old_deltas_w[k][i], 0, (_lnunits[k])*sizeof(float));
      }
    }
  }

  // Finally allocate space for g and or h gradient structures
  if(_ss_tr) {
    _g_layers_old_deltas_w = new floatrefref[_s];
    
    // Allocate weights and gradient components matrixes
    // for g transforming part. Connections from input layers
    // have special dimensions.
#ifdef DEBUG
    cout << endl << endl 
	 << "MGD (g) Layer 1: (" << (2*_m) + 1 << "," << _lnunits[_r] << ")" 
	 << endl;
#endif
    _g_layers_old_deltas_w[0] = new floatref[2*_m + 1];

    for(int i=0; i<2*_m + 1; i++) {
      _g_layers_old_deltas_w[0][i] = new float[_lnunits[_r]];

      // Reset gradient for corresponding weights
      memset(_g_layers_old_deltas_w[0][i], 0, _lnunits[_r] * sizeof(float));
    }
  
    // Allocate weights&gradient matrixes for g transforming part
    for(int k=1; k<_s; k++) {
      // Allocate space for weight&gradient matrixes between layer i-1 and i.
      // Automatically include space for threshold unit in layer i-1
#ifdef DEBUG
      cout << endl << endl
	   << "MGD (g) Layer " << k+1 << ": (" << _lnunits[_r+k-1] + 1  << "," << _lnunits[_r+k] << ")" 
	   << endl;
#endif
      _g_layers_old_deltas_w[k] = new floatref[_lnunits[_r+k-1] + 1];

      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	_g_layers_old_deltas_w[k][i] = new float[_lnunits[_r+k]];

	// Reset g gradient components for corresponding weights
	memset(_g_layers_old_deltas_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));
      }
    }
  }

  if(_ios_tr) {
    _h_layers_old_deltas_w = new floatrefref[_s];
    
    // Allocate weights and gradient components matrixes
    // for h map. Connections from input layers have special dimensions.
#ifdef DEBUG
    cout << endl << endl 
	 << "MGD (h) Layer 1: (" << (2*_m+_n) + 1 << "," << _lnunits[_r] << ")" 
	 << endl;
#endif
    _h_layers_old_deltas_w[0] = new floatref[2*_m + _n + 1];

    for(int i=0; i<2*_m + _n + 1; i++) {
      _h_layers_old_deltas_w[0][i] = new float[_lnunits[_r]];

      // Reset gradient for corresponding weights
      memset(_h_layers_old_deltas_w[0][i], 0, _lnunits[_r] * sizeof(float));
    }
  
    // Allocate weights&gradient matrixes for h map
    for(int k=1; k<_s; k++) {
      // Allocate space for weight&gradient matrixes between layer i-1 and i.
      // Automatically include space for threshold unit in layer i-1
#ifdef DEBUG
      cout << endl << endl
	   << "MGD (h) Layer " << k+1 << ": (" << _lnunits[_r+k-1] + 1  << "," << _lnunits[_r+k] << ")" 
	   << endl;
#endif
      _h_layers_old_deltas_w[k] = new floatref[_lnunits[_r+k-1] + 1];

      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	_h_layers_old_deltas_w[k][i] = new float[_lnunits[_r+k]];

	// Reset h gradient components for corresponding weights
	memset(_h_layers_old_deltas_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));
      }
    }
  }
}

template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
												    void MGradientDescent::updateWeights(RNN<T1, T2, T3>* const rnn, float _learning_rate, float momentum_term, float ni) {
  // rnn (the recursive network) store gradient components, so to obtain
  // weight update rule change the sign of this components, multiply
  // by the learning rate and add multiplication of momentum_term
  // with old weights deltas.
    
  float new_delta_w;
  if(_ss_tr) { // begin with layers of the MLP output function
    new_delta_w = 0.0;
    for(int i=0; i<_m; i++) {
      for(int j=0; j<_lnunits[_r]; j++) {
	rnn->_prev_g_layers_w[0][i][j] = rnn->_g_layers_w[0][i][j];
	new_delta_w = 
	  -_learning_rate * rnn->_g_layers_gradient_w[0][i][j] +
	  (momentum_term * _g_layers_old_deltas_w[0][i][j]) -
	  (ni * rnn->_g_layers_w[0][i][j]);

	rnn->_g_layers_w[0][i][j] += new_delta_w;
	_g_layers_old_deltas_w[0][i][j] = new_delta_w;
      }
    }
    
    if(_process_dr) {
      for(int i=_m; i<2*_m; i++) {
	for(int j=0; j<_lnunits[_r]; j++) {
	  rnn->_prev_g_layers_w[0][i][j] = rnn->_g_layers_w[0][i][j];
	  new_delta_w = 
	    -_learning_rate * rnn->_g_layers_gradient_w[0][i][j] +
	    (momentum_term * _g_layers_old_deltas_w[0][i][j]) - 
	    (ni * rnn->_g_layers_w[0][i][j]);

	  rnn->_g_layers_w[0][i][j] += new_delta_w;
	  _g_layers_old_deltas_w[0][i][j] = new_delta_w;
	}
      }
    }
    
    for(int j=0; j<_lnunits[_r]; j++) {
      rnn->_prev_g_layers_w[0][2*_m][j] = rnn->_g_layers_w[0][2*_m][j];
      new_delta_w = 
	-_learning_rate * rnn->_g_layers_gradient_w[0][2*_m][j] +
	(momentum_term * _g_layers_old_deltas_w[0][2*_m][j]) - 
	(ni * rnn->_g_layers_w[0][2*_m][j]);
    
      rnn->_g_layers_w[0][2*_m][j] += new_delta_w;
      _g_layers_old_deltas_w[0][2*_m][j] = new_delta_w;
    }

    for(int k=1; k<_s; k++) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	for(int j=0; j<_lnunits[_r+k]; j++) {
	  rnn->_prev_g_layers_w[k][i][j] = rnn->_g_layers_w[k][i][j];
	  new_delta_w = 
	    -_learning_rate * rnn->_g_layers_gradient_w[k][i][j] +
	    (momentum_term * _g_layers_old_deltas_w[k][i][j]) - 
	    (ni * rnn->_g_layers_w[k][i][j]);
	  
	  rnn->_g_layers_w[k][i][j] += new_delta_w;
	  _g_layers_old_deltas_w[k][i][j] = new_delta_w;
	}
      }
    }
  }

  if(_ios_tr) { // proceed with layers of the MLP h map
    new_delta_w = 0.0;
    for(int i=0; i<_m; i++) {
      for(int j=0; j<_lnunits[_r]; j++) {
	rnn->_prev_h_layers_w[0][i][j] = rnn->_h_layers_w[0][i][j];
	new_delta_w = 
	  -_learning_rate * rnn->_h_layers_gradient_w[0][i][j] +
	  (momentum_term * _h_layers_old_deltas_w[0][i][j]) -
	  (ni * rnn->_h_layers_w[0][i][j]);

	rnn->_h_layers_w[0][i][j] += new_delta_w;
	_h_layers_old_deltas_w[0][i][j] = new_delta_w;
      }
    }
    
    if(_process_dr) {
      for(int i=_m; i<2*_m; i++) {
	for(int j=0; j<_lnunits[_r]; j++) {
	  rnn->_prev_h_layers_w[0][i][j] = rnn->_h_layers_w[0][i][j];
	  new_delta_w = 
	    -_learning_rate * rnn->_h_layers_gradient_w[0][i][j] +
	    (momentum_term * _h_layers_old_deltas_w[0][i][j]) -
	    (ni * rnn->_h_layers_w[0][i][j]);

	  rnn->_h_layers_w[0][i][j] += new_delta_w;
	  _h_layers_old_deltas_w[0][i][j] = new_delta_w;
	}
      }
    }
    
    for(int i=2*_m; i<2*_m + _n; i++) {
      for(int j=0; j<_lnunits[_r]; j++) {
	rnn->_prev_h_layers_w[0][i][j] = rnn->_h_layers_w[0][i][j];
	new_delta_w = 
	  -_learning_rate * rnn->_h_layers_gradient_w[0][i][j] +
	  (momentum_term * _h_layers_old_deltas_w[0][i][j]) -
	  (ni * rnn->_h_layers_w[0][i][j]);

	rnn->_h_layers_w[0][i][j] += new_delta_w;
	_h_layers_old_deltas_w[0][i][j] = new_delta_w;
      }
    }

    for(int j=0; j<_lnunits[_r]; j++) {
      rnn->_prev_h_layers_w[0][2*_m + _n][j] = rnn->_h_layers_w[0][2*_m + _n][j];
      new_delta_w = 
	-_learning_rate * rnn->_h_layers_gradient_w[0][2*_m + _n][j] +
	(momentum_term * _h_layers_old_deltas_w[0][2*_m + _n][j]) -
	(ni * rnn->_h_layers_w[0][2*_m + _n][j]);
    
      rnn->_h_layers_w[0][2*_m + _n][j] += new_delta_w;
      _h_layers_old_deltas_w[0][2*_m + _n][j] = new_delta_w;
    }

    for(int k=1; k<_s; k++) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	for(int j=0; j<_lnunits[_r+k]; j++) {
	  rnn->_prev_h_layers_w[k][i][j] = rnn->_h_layers_w[k][i][j];
	  new_delta_w = 
	    -_learning_rate * rnn->_h_layers_gradient_w[k][i][j] +
	    (momentum_term * _h_layers_old_deltas_w[k][i][j]) -
	    (ni * rnn->_h_layers_w[k][i][j]);
	  
	  rnn->_h_layers_w[k][i][j] += new_delta_w;
	  _h_layers_old_deltas_w[k][i][j] = new_delta_w;
	}
      }
    }
  }

  // proceed with casual part folding layers (eventually the non-casual ones).
  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    for(int j=0; j<_lnunits[0]; j++) {
      rnn->_prev_f_layers_w[0][i][j] = rnn->_f_layers_w[0][i][j];
      new_delta_w = 
	-_learning_rate * rnn->_f_layers_gradient_w[0][i][j] +
	(momentum_term * _f_layers_old_deltas_w[0][i][j]) -
	(ni * rnn->_f_layers_w[0][i][j]);

      rnn->_f_layers_w[0][i][j] += new_delta_w;
      _f_layers_old_deltas_w[0][i][j] = new_delta_w;

      if(_process_dr) {
	rnn->_prev_b_layers_w[0][i][j] = rnn->_b_layers_w[0][i][j];
	new_delta_w = 
	  -_learning_rate * rnn->_b_layers_gradient_w[0][i][j] +
	  (momentum_term * _b_layers_old_deltas_w[0][i][j]) -
	  (ni * rnn->_b_layers_w[0][i][j]);
	  
	rnn->_b_layers_w[0][i][j] += new_delta_w;
	_b_layers_old_deltas_w[0][i][j] = new_delta_w;
      }
    }
  }
    
  for(int k=1; k<_r; k++) {
    for(int i=0; i<_lnunits[k-1]+1; i++) {
      for(int j=0; j<_lnunits[k]; j++) {
	rnn->_prev_f_layers_w[k][i][j] = rnn->_f_layers_w[k][i][j];
	new_delta_w = 
	  -_learning_rate * rnn->_f_layers_gradient_w[k][i][j] +
	  (momentum_term * _f_layers_old_deltas_w[k][i][j]) -
	  (ni * rnn->_f_layers_w[k][i][j]);

	rnn->_f_layers_w[k][i][j] += new_delta_w;
	_f_layers_old_deltas_w[k][i][j] = new_delta_w;

	if(_process_dr) {
	  rnn->_prev_b_layers_w[k][i][j] = rnn->_b_layers_w[k][i][j];
	  new_delta_w = 
	    -_learning_rate * rnn->_b_layers_gradient_w[k][i][j] +
	    (momentum_term * _b_layers_old_deltas_w[k][i][j]) -
	    (ni * rnn->_b_layers_w[k][i][j]);
	  
	  rnn->_b_layers_w[k][i][j] += new_delta_w;
	  _b_layers_old_deltas_w[k][i][j] = new_delta_w;
	}
      }
    }
  }
    
}

/* Conjugate Gradient minimization procedure */

class ConjugateGradient {
  // Update network weights in a direction that is
  // a compromise between current gradient (stored in network)
  // and old gradient components stored in this class.
  float*** _f_layers_old_gradient_w;
  float*** _b_layers_old_gradient_w;
  float*** _g_layers_old_gradient_w;
  float*** _h_layers_old_gradient_w;

  bool _process_dr, _ss_tr, _ios_tr;
  int _n, _v, _m, _r, _s;
  std::vector<int> _lnunits;
  
 public:
  ~ConjugateGradient() {
#ifdef DEBUG
    cout << "Deallocating CGD (f) old gradient matrix at layer 1" << endl;
#endif
    if(_f_layers_old_gradient_w[0]) {
      for(int i=0; i<(_n+_v*_m) + 1; i++) {
	if(_f_layers_old_gradient_w[0][i])
	  delete[] _f_layers_old_gradient_w[0][i];
	_f_layers_old_gradient_w[0][i] = 0;
      }
      delete[] _f_layers_old_gradient_w[0];
      _f_layers_old_gradient_w[0] = 0;
    }

    for(int k=1; k<_r; k++) {
#ifdef DEBUG
      cout << "Deallocating CGD (f) old gradient matrix at layer: " << k+1 << endl;
#endif
      if(_f_layers_old_gradient_w[k]) {
	for(int i=0; i<_lnunits[k-1]+1; i++) {
	  if(_f_layers_old_gradient_w[k][i])
	    delete[] _f_layers_old_gradient_w[k][i];
	  _f_layers_old_gradient_w[k][i] = 0;
	}
	delete[] _f_layers_old_gradient_w[k];
	_f_layers_old_gradient_w[k] = 0;
      }
    
    }
  
    delete[] _f_layers_old_gradient_w;
    _f_layers_old_gradient_w = 0;

    if(_process_dr) {
#ifdef DEBUG
      cout << "Deallocating CGD (b) old gradient matrix at layer 1" << endl;
#endif
      if(_b_layers_old_gradient_w[0]) {
	for(int i=0; i<(_n+_v*_m) + 1; i++) {
	  if(_b_layers_old_gradient_w[0][i])
	    delete[] _b_layers_old_gradient_w[0][i];
	  _b_layers_old_gradient_w[0][i] = 0;
	}
	delete[] _b_layers_old_gradient_w[0];
	_b_layers_old_gradient_w[0] = 0;
      }

      for(int k=1; k<_r; k++) {
#ifdef DEBUG
	cout << "Deallocating CGD (b) old gradient matrix at layer: " << k+1 << endl;
#endif
	if(_b_layers_old_gradient_w[k]) {
	  for(int i=0; i<_lnunits[k-1]+1; i++) {
	    if(_b_layers_old_gradient_w[k][i])
	      delete[] _b_layers_old_gradient_w[k][i];
	    _b_layers_old_gradient_w[k][i] = 0;
	  }
	  delete[] _b_layers_old_gradient_w[k];
	  _b_layers_old_gradient_w[k] = 0;
	}
    
      }
  
      delete[] _b_layers_old_gradient_w;
      _b_layers_old_gradient_w = 0;
    }

    if(_ss_tr) {
#ifdef DEBUG
      cout << "Deallocating CGD (g) old gradient matrix at layer 1" << endl;
#endif
      if(_g_layers_old_gradient_w[0]) {
	for(int i=0; i<2*_m + 1; i++) {
	  if(_g_layers_old_gradient_w[0][i])
	    delete[] _g_layers_old_gradient_w[0][i];
	  _g_layers_old_gradient_w[0][i] = 0;
	}
	delete[] _g_layers_old_gradient_w[0];
	_g_layers_old_gradient_w[0] = 0;
      }

      for(int k=1; k<_s; k++) {
#ifdef DEBUG
	cout << "Deallocating CGD (g) old gradient matrix at layer: " << k+1 << endl;
#endif
	if(_g_layers_old_gradient_w[k]) {
	  for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	    if(_g_layers_old_gradient_w[k][i])
	      delete[] _g_layers_old_gradient_w[k][i];
	    _g_layers_old_gradient_w[k][i] = 0;
	  }
	  delete[] _g_layers_old_gradient_w[k];
	  _g_layers_old_gradient_w[k] = 0;
	}
      }
  
      delete[] _g_layers_old_gradient_w;
      _g_layers_old_gradient_w = 0;
    }

    if(_ios_tr) {
#ifdef DEBUG
      cout << "Deallocating CGD (h) old gradient matrix at layer 1" << endl;
#endif
      if(_h_layers_old_gradient_w[0]) {
	for(int i=0; i<2*_m + _n + 1; i++) {
	  if(_h_layers_old_gradient_w[0][i])
	    delete[] _h_layers_old_gradient_w[0][i];
	  _h_layers_old_gradient_w[0][i] = 0;
	}
	delete[] _h_layers_old_gradient_w[0];
	_h_layers_old_gradient_w[0] = 0;
      }

      for(int k=1; k<_s; k++) {
#ifdef DEBUG
	cout << "Deallocating CGD (h) old gradient matrix at layer: " << k+1 << endl;
#endif
	if(_h_layers_old_gradient_w[k]) {
	  for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	    if(_h_layers_old_gradient_w[k][i])
	      delete[] _h_layers_old_gradient_w[k][i];
	    _h_layers_old_gradient_w[k][i] = 0;
	  }
	  delete[] _h_layers_old_gradient_w[k];
	  _h_layers_old_gradient_w[k] = 0;
	}
      }
  
      delete[] _h_layers_old_gradient_w;
      _h_layers_old_gradient_w = 0;
    }
  }

  template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
    void setInternals(RNN<T1, T2, T3>* const rnn) {
    // Assume rnn constructor has initialized required dimension quantities.
    _process_dr = rnn->_process_dr;
    _ss_tr = rnn->_ss_tr; _ios_tr = rnn->_ios_tr;
    _n = rnn->_n, _v = rnn->_v, _m = rnn->_m, _r = rnn->_r, _s = rnn->_s;
    _lnunits = rnn->_lnunits;

    _f_layers_old_gradient_w = new floatrefref[_r];
  
    // Allocate weights and gradient components matrixes
    // for f folding part. Connections from input layer
    // have special dimensions.
#ifdef DEBUG
    cout << endl << endl 
       << "CGD (f) Layer 1: (" << (_n+_v*_m) + 1 << "," << _lnunits[0] << ")" 
       << endl;
#endif
    _f_layers_old_gradient_w[0] = new floatref[(_n + _v * _m) + 1];

    for(int i=0; i<(_n+_v*_m) + 1; i++) {
      _f_layers_old_gradient_w[0][i] = new float[_lnunits[0]];

      // Reset gradient for corresponding weights
      memset(_f_layers_old_gradient_w[0][i], 0, _lnunits[0] * sizeof(float));
    }
  
    // Allocate f weights&gradient matrixes for f folding part.
    for(int k=1; k<_r; k++) {
      // Allocate space for weight&delta matrix between layer i-1 and i.
      // Automatically include space for threshold unit in layer i-1
#ifdef DEBUG
      cout << endl << endl
	 << "CGD (f) Layer " << k+1 << ": (" << _lnunits[k-1] + 1  << "," << _lnunits[k] << ")" 
	 << endl;
#endif
      _f_layers_old_gradient_w[k] = new floatref[_lnunits[k-1] + 1];

      for(int i=0; i<_lnunits[k-1]+1; i++) {
	_f_layers_old_gradient_w[k][i] = new float[_lnunits[k]];

	// Reset f gradient components for corresponding weights
	memset(_f_layers_old_gradient_w[k][i], 0, (_lnunits[k])*sizeof(float));
      }
    
    }

    // eventually allocate space for b layers gradient structures.
    if(_process_dr) {
      _b_layers_old_gradient_w = new floatrefref[_r];
  
      // Allocate weights and gradient components matrixes
      // for f folding part. Connections from input layer
      // have special dimensions.
#ifdef DEBUG
      cout << endl << endl 
       << "CGD (b) Layer 1: (" << (_n+_v*_m) + 1 << "," << _lnunits[0] << ")" 
       << endl;
#endif
      _b_layers_old_gradient_w[0] = new floatref[(_n + _v * _m) + 1];

      for(int i=0; i<(_n+_v*_m) + 1; i++) {
	_b_layers_old_gradient_w[0][i] = new float[_lnunits[0]];

	// Reset gradient for corresponding weights
	memset(_b_layers_old_gradient_w[0][i], 0, _lnunits[0] * sizeof(float));
      }
  
      // Allocate f weights&gradient matrixes for f folding part.
      for(int k=1; k<_r; k++) {
	// Allocate space for weight&delta matrix between layer i-1 and i.
	// Automatically include space for threshold unit in layer i-1
#ifdef DEBUG
	cout << endl << endl
	 << "CGD (b) Layer " << k+1 << ": (" << _lnunits[k-1] + 1  << "," << _lnunits[k] << ")" 
	 << endl;
#endif
	_b_layers_old_gradient_w[k] = new floatref[_lnunits[k-1] + 1];

	for(int i=0; i<_lnunits[k-1]+1; i++) {
	  _b_layers_old_gradient_w[k][i] = new float[_lnunits[k]];

	  // Reset f gradient components for corresponding weights
	  memset(_b_layers_old_gradient_w[k][i], 0, (_lnunits[k])*sizeof(float));
	}
      }
    }

    // Finally allocate space for g and/or h gradient structures
    if(_ss_tr) {
      _g_layers_old_gradient_w = new floatrefref[_s];
    
      // Allocate weights and gradient components matrixes
      // for g transforming part. Connections from input layers
      // have special dimensions.
#ifdef DEBUG
      cout << endl << endl 
	   << "CGD (g) Layer 1: (" << (2*_m) + 1 << "," << _lnunits[_r] << ")" 
	   << endl;
#endif
      _g_layers_old_gradient_w[0] = new floatref[2*_m + 1];

      for(int i=0; i<2*_m + 1; i++) {
	_g_layers_old_gradient_w[0][i] = new float[_lnunits[_r]];

	// Reset gradient for corresponding weights
	memset(_g_layers_old_gradient_w[0][i], 0, _lnunits[_r] * sizeof(float));
      }
  
      // Allocate weights&gradient matrixes for g transforming part
      for(int k=1; k<_s; k++) {
	// Allocate space for weight&gradient matrixes between layer i-1 and i.
	// Automatically include space for threshold unit in layer i-1
#ifdef DEBUG
	cout << endl << endl
	     << "CGD (g) Layer " << k+1 << ": (" << _lnunits[_r+k-1] + 1  << "," << _lnunits[_r+k] << ")" 
	     << endl;
#endif
	_g_layers_old_gradient_w[k] = new floatref[_lnunits[_r+k-1] + 1];

	for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	  _g_layers_old_gradient_w[k][i] = new float[_lnunits[_r+k]];

	  // Reset g gradient components for corresponding weights
	  memset(_g_layers_old_gradient_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));
	}
      }
    }

    if(_ios_tr) {
      _h_layers_old_gradient_w = new floatrefref[_s];
    
      // Allocate weights and gradient components matrixes
      // for h map. Connections from input layers have special dimensions.
#ifdef DEBUG
      cout << endl << endl 
	   << "CGD (h) Layer 1: (" << (2*_m+_n) + 1 << "," << _lnunits[_r] << ")" 
	   << endl;
#endif
      _h_layers_old_gradient_w[0] = new floatref[2*_m + _n + 1];

      for(int i=0; i<2*_m + _n + 1; i++) {
	_h_layers_old_gradient_w[0][i] = new float[_lnunits[_r]];

	// Reset gradient for corresponding weights
	memset(_h_layers_old_gradient_w[0][i], 0, _lnunits[_r] * sizeof(float));
      }
  
      // Allocate weights&gradient matrixes for h map
      for(int k=1; k<_s; k++) {
	// Allocate space for weight&gradient matrixes between layer i-1 and i.
	// Automatically include space for threshold unit in layer i-1
#ifdef DEBUG
	cout << endl << endl
	     << "CGD (h) Layer " << k+1 << ": (" << _lnunits[_r+k-1] + 1  << "," << _lnunits[_r+k] << ")" 
	     << endl;
#endif
	_h_layers_old_gradient_w[k] = new floatref[_lnunits[_r+k-1] + 1];

	for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	  _h_layers_old_gradient_w[k][i] = new float[_lnunits[_r+k]];

	  // Reset g gradient components for corresponding weights
	  memset(_h_layers_old_gradient_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));
	}
      }
    }
  }

  template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
    void updateWeights(RNN<T1, T2, T3>* const rnn, float = 0.0, float = 0.0) {
    require(0, "Have to wait for a good method!");
  }
};



/*
  This is polymorphic version of Strategy pattern,
  but it does not work because RNN is templatized and
  we cannot create virtual member template.

class ErrMinProcedure {
 protected:
  ErrMinProcedure();

 public:
  virtual ~ErrMinProcedure() {}

  
  //  Parameters of weight update method are:
  //  - matrixes of weights to update
  //  - matrixes of weight components of current gradient
  //  - array of layers dimensions
  
  virtual void 
    updateWeights(float***, float***, const std::vector<int>&) = 0;
};

class GradientDescent: public ErrMinProcedure {
  // Standard gradient descent needs only
  // to know learning rate and current gradient
  // to update the weights.
  float _learning_rate;
 public:
  GradientDescent(float learning_rate = .05): _learning_rate(learning_rate) {}
  
  virtual void updateWeights(float***, float***, const std::vector<int>&);
};

class MomentGradientDescent: public ErrMinProcedure {
  // Momentum version of gradient descent needs to
  // know learning rate, momentum term current gradient
  // (from rnn) and previous values of weights update.
  float _learning_rate, _momentum;
  float*** _layers_old_delta_w;

 public:
  MomentGradientDescent(const std::vector<int>&, float = .05, float = .3);
  ~MomentGradientDescent();

  virtual void updateWeights(float***, float***, const std::vector<int>&);
};

class ConjugateGradient {
  // Conjugate gradient needs to know current (from rnn)
  // and old value of the gradient 
  float*** _layers_old_gradient;
 public:
  ConjugateGradient(const std::vector<int>&);
  ~ConjugateGradient();

  virtual void updateWeights(float***, float***, const std::vector<int>&);
};

*/

#endif // _ERROR_MINIMIZATION_PROCEDURE_H
