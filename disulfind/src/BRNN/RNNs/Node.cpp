#include <iostream>
#include <string.h>
#include "require.h"
#include "Node.h"
using namespace std;

//#define WFP int z; cin >> z
typedef	float *floatref;

/*** Private functions ***/

/* Allocation routines */
void Node::allocFoutputStruct() {
  // We assume _lnunits[0.._r-1] vector contains number of
  // output units for each layer in f folding part.
  _f_layers_activations = new floatref[_r];
  
  for(unsigned int k=0; k<_r; k++) {
    _f_layers_activations[k] = new float[_lnunits[k]];
    memset(_f_layers_activations[k], 0, _lnunits[k]*sizeof(float));
  }

  // r is representation layer, whose dimension is m
  _f_delta_lr = new float[_lnunits[_r-1]];
  memset(_f_delta_lr, 0, (_lnunits[_r-1])*sizeof(float));
}

void Node::allocBoutputStruct() {
  // We assume _lnunits[0.._r-1] vector contains number of
  // output units for each layer in b folding part.
  _b_layers_activations = new floatref[_r];
    
  for(unsigned int k=0; k<_r; k++) {
    _b_layers_activations[k] = new float[_lnunits[k]];
    memset(_b_layers_activations[k], 0, _lnunits[k]*sizeof(float));
  }

  // r is representation layer, whose dimension is m
  _b_delta_lr = new float[_lnunits[_r-1]];
  memset(_b_delta_lr, 0, (_lnunits[_r-1])*sizeof(float));
}

void Node::allocHoutputStruct() {
  // We assume _lnunits[_r.._r+_s-1] vector contains number of
  // output units for each layer in h map.
  _h_layers_activations = new floatref[_s];
  
  for(unsigned int k=0; k<_s; k++) {
    _h_layers_activations[k] = new float[_lnunits[_r+k]];
    memset(_h_layers_activations[k], 0, _lnunits[_r+k]*sizeof(float));
  }

  _h_delta = new float[_lnunits[_r+_s-1]];
  memset(_h_delta, 0, _lnunits[_r+_s-1]*sizeof(float));
}

/* Deallocation routines */
void Node::deallocFoutputStruct() {
  for(unsigned int k=0; k<_r; k++) {
    delete[] _f_layers_activations[k];
    _f_layers_activations[k] = 0;
  }

  delete[] _f_layers_activations;
  delete[] _f_delta_lr;
  _f_layers_activations = 0; _f_delta_lr = 0;
}

void Node::deallocBoutputStruct() {
  for(unsigned int k=0; k<_r; k++) {
    delete[] _b_layers_activations[k];
    _b_layers_activations[k] = 0;
  }

  delete[] _b_layers_activations;
  delete[] _b_delta_lr;
  _b_layers_activations = 0; _b_delta_lr = 0;
}

void Node::deallocHoutputStruct() {
  for(unsigned int k=0; k<_s; k++) {
    delete[] _h_layers_activations[k];
    _h_layers_activations[k] = 0;
  }

  delete[] _h_layers_activations; _h_layers_activations = 0;
  delete[] _h_delta; _h_delta = 0;
}

/* Constructor */
Node::Node(const vector<float>& ei): _process_dr(Options::instance()->getProcessingFlag()), _v(Options::instance()->getDomainOutDegree()), _encodedInput(ei), _lnunits(Options::instance()->getLayersUnitsNumber()) {

  // Get other fundamental parameters from Options class
  pair<int, int> indexes = Options::instance()->getLayersIndexes();
  _r = indexes.first; _s = indexes.second;
  require(_lnunits.size() == _r + _s, "Node Dim.error");

  //_ios_tr = (trasd & 1)?true:false;
  _ios_tr = (Options::instance()->getTrasductionType() & 1)?true:false;
  
  allocFoutputStruct();

  if(_process_dr)
    allocBoutputStruct();

  if(_ios_tr) 
    allocHoutputStruct();
}

/* Copy Constructor */
Node::Node(const Node& n):
  _process_dr(n._process_dr), _ios_tr(n._ios_tr), _v(n._v), _r(n._r), _s(n._s),
  _encodedInput(n._encodedInput), _otargets(n._otargets), _lnunits(n._lnunits) {

  require(_lnunits.size() == _r + _s, "Node Dim.error");
  if(_otargets.size())
    require(_lnunits[_r+_s-1] == static_cast<int>(_otargets.size()), "Node Dim.Error");

  allocFoutputStruct();

  if(_process_dr)
    allocBoutputStruct();

  if(_ios_tr)
    allocHoutputStruct();
}

Node::~Node() {
  deallocFoutputStruct();

  if(_process_dr)
    deallocBoutputStruct();

  if(_ios_tr)
    deallocHoutputStruct();
}

void Node::resetValues() {
  for(unsigned int k=0; k<_r; k++) {
    memset(_f_layers_activations[k], 0, _lnunits[k]*sizeof(float));
  }
  memset(_f_delta_lr, 0, (_lnunits[_r-1])*sizeof(float));

  if(_process_dr) {
    for(size_t k=0; k<_r; k++) {
      memset(_b_layers_activations[k], 0, _lnunits[k]*sizeof(float));
    }
    memset(_b_delta_lr, 0, (_lnunits[_r-1])*sizeof(float));
  }

  if(_ios_tr) {
    for(size_t k=0; k<_s; k++) {
      memset(_h_layers_activations[k], 0, _lnunits[_r+k]*sizeof(float));
    }
    memset(_h_delta, 0, _lnunits[_r+_s-1]*sizeof(float));
  }
}
