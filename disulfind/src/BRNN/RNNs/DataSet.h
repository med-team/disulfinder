#ifndef _DATA_SET_H
#define _DATA_SET_H

#include "require.h"
#include "util.h"
#include "Options.h"
#include "DPAG.h"
#include "Node.h"
#include <list>
/** Using directive in header files is really bad design ***/
//using namespace std;

typedef std::vector<std::pair<DPAG, DPAG> >::iterator DPAGsIter;
typedef std::vector<std::vector<float> >::iterator DPAGsTargetsIter;
typedef std::vector<std::vector<std::vector<float> > >::iterator NodesTargetsIter;
typedef std::vector<bool>::iterator GoalMaskIter;
typedef std::vector<bool>::iterator PropMaskIter;

typedef struct StructuredInstanceTemplate {
  std::string _id;
  std::vector<Node*> _toNodes;
  std::vector<std::vector<std::vector<float> > > _nodes_targets;

  std::vector<std::pair<DPAG, DPAG> > _dpags;
  std::vector<std::vector<float> > _dpags_targets;
  std::vector<bool> _goal_mask;
  std::vector<bool> _prop_mask;

  StructuredInstanceTemplate(const std::string&, const std::vector<std::vector<float> >&, const std::vector<std::vector<std::vector<float> > >&, const std::vector<std::pair<DPAG, DPAG> >&, const std::vector<std::vector<float> >&);

  ~StructuredInstanceTemplate() {
    for(std::vector<Node*>::iterator it=_toNodes.begin();
	it!=_toNodes.end(); ++it) {
      delete *it;
    }
  }
};

//typedef std::list<StructuredInstanceTemplate*> DataSet;
typedef std::vector<StructuredInstanceTemplate*> StructuredInstancesSet;
typedef StructuredInstancesSet::iterator StructuredInstancesSetIter;

class DataSet {
  int _n, _v;
  int g_counter;
  std::vector<int> _lnunits;
  TrasdType _trasd;
  bool _process_dr;
  StructuredInstancesSet _template_instances;

  /* Private functions */
  // to parse a file that defines a set of graphs and inputs
  // and to load resulting instances into current list
  bool loadInstances(const char*);

 public:
  DataSet() {}
  DataSet(const char*);
  DataSet(const char*, const char*);
  ~DataSet();

  // Nested iterator class,
  // Abstract the complicated process of iterating
  // through instances of a data set
  class iterator;
  friend class iterator;
  class iterator {
    DataSet& _ds;
    StructuredInstancesSetIter _si_it;
    DPAGsIter _dpags_iter;
    DPAGsTargetsIter _dpags_targets_iter;
    NodesTargetsIter _nodes_targets_iter;
    GoalMaskIter _goal_mask_iter;
    PropMaskIter _prop_mask_iter;

    bool not_switched;
  public:
    iterator(DataSet& ds): _ds(ds), _si_it(_ds._template_instances.begin()), _dpags_iter((*_si_it)->_dpags.begin()), _dpags_targets_iter((*_si_it)->_dpags_targets.begin()), _nodes_targets_iter((*_si_it)->_nodes_targets.begin()), _goal_mask_iter((*_si_it)->_goal_mask.begin()), _prop_mask_iter((*_si_it)->_prop_mask.begin()) {
      //cout << _ds._template_instances.size() << " "
      //   << (*_si_it)->_dpags.size() << " "
      //   << (*_si_it)->_dpags_targets.size() << " "
      //   << (*_si_it)->_nodes_targets.size() << endl;
      
      // load into sequence of nodes corresponding output label
      int num_nodes = (*_si_it)->_toNodes.size();
      require(num_nodes == (*_nodes_targets_iter).size(), 
	      "Error! Inconsistency in nodes structures dimensions\n"
	      "in call to bool DataSet::iterator::iterator(DataSet&)\n");
      for(int i=0; i<num_nodes; i++)
	(*_si_it)->_toNodes[i]->loadTarget((*_nodes_targets_iter)[i]);

      not_switched = true;
    }

    iterator(const iterator& dsi): _ds(dsi._ds), _si_it(dsi._si_it), _dpags_iter(dsi._dpags_iter), _dpags_targets_iter(dsi._dpags_targets_iter), _nodes_targets_iter(dsi._nodes_targets_iter), _goal_mask_iter(dsi._goal_mask_iter), _prop_mask_iter(dsi._prop_mask_iter), not_switched(dsi.not_switched) {}

    // end sentinel iterator
    iterator(DataSet& ds, bool): _ds(ds), _si_it(0) {} 

    bool operator++() {
      if(_si_it != _ds._template_instances.end()) {
	_dpags_iter++;
	_dpags_targets_iter++;
	_nodes_targets_iter++;
	_goal_mask_iter++;
	_prop_mask_iter++;

	if(_dpags_iter != (*_si_it)->_dpags.end() && 
	   _dpags_targets_iter != (*_si_it)->_dpags_targets.end() &&
	   _nodes_targets_iter != (*_si_it)->_nodes_targets.end() &&
	   _goal_mask_iter != (*_si_it)->_goal_mask.end() &&
	   _prop_mask_iter != (*_si_it)->_prop_mask.end()) {

	  // load into sequence of nodes corresponding output label
	  int num_nodes = (*_si_it)->_toNodes.size();
	  require(num_nodes == (*_nodes_targets_iter).size(), 
		  "Error! Inconsistency in nodes structures dimensions\n"
		  "in call to bool DataSet::iterator::operator++()\n");
	  for(int i=0; i<num_nodes; i++)
	    (*_si_it)->_toNodes[i]->loadTarget((*_nodes_targets_iter)[i]);

	  not_switched = true;
	  return true;
	} else {
	  _si_it++;
	  if(_si_it != _ds._template_instances.end()) {
	    _dpags_iter = (*_si_it)->_dpags.begin();
	    _dpags_targets_iter = (*_si_it)->_dpags_targets.begin();
	    _nodes_targets_iter = (*_si_it)->_nodes_targets.begin();
	    _goal_mask_iter = (*_si_it)->_goal_mask.begin();
	    _prop_mask_iter = (*_si_it)->_prop_mask.begin();
	    
	    // load into sequence of nodes corresponding output label
	    int num_nodes = (*_si_it)->_toNodes.size();
	    require(num_nodes == (*_nodes_targets_iter).size(), 
		    "Error! Inconsistency in nodes structures dimensions\n"
		    "in call to bool DataSet::iterator::operator++()\n");
 	    for(int i=0; i<num_nodes; i++)
	      (*_si_it)->_toNodes[i]->loadTarget((*_nodes_targets_iter)[i]);
	    
	    not_switched = false;
	    return true;
	  } else {
	    // Must signal end of sequence
	    _si_it = _ds._template_instances.end(); //0;//_si_it = 0;
	    not_switched = false;
	    return false;
	  }
	}
      } else {
	// Must signal end of sequence
	_si_it = _ds._template_instances.end(); //0;_si_it = 0;
	not_switched = false;
	return false;
      }
    }

    bool operator++(int) { return operator++(); }

/*     // Comparison operators to test for end: */
/*     bool operator==(const iterator&) { */
/*       return _si_it == 0; */
/*     } */
    
/*     bool operator!=(const iterator&) { */
/*       return _si_it != 0; */
/*     } */

 // Comparison operators to test for end:
    bool operator==(const iterator&) {
      return _si_it == _ds._template_instances.end(); //0;
    }
    
    bool operator!=(const iterator&) {
      return _si_it != _ds._template_instances.end(); //0;
    }

    std::vector<Node*>* currenTONodes() const {
      //if(!_si_it) return 0;
      return &((*_si_it)->_toNodes);
    }
    
    std::pair<DPAG, DPAG>* currentDPAGs() const {
      //if(!_si_it) return 0;
      //return &((*_si_it)->_dpags);
      return &(*_dpags_iter);
    }
    
    std::vector<float>* currentDPAGsTargets() const {
      //if(!_si_it) return 0;
      //return &((*_si_it)->_dpags_targets);
      return &(*_dpags_targets_iter);
    }
    
    bool currentIsGoal() const {
      return *_goal_mask_iter;
    }

    bool propagate() const {
      return *_prop_mask_iter;
    }

    void setPropagate(bool flag) {
      *_prop_mask_iter = flag;
    }

    bool hasNotSwitched() const { return not_switched; }

    std::string getId() const {
      return (*_si_it)->_id;
    }
  };
  
  iterator begin() { return iterator(*this); }
  iterator end() { return iterator(*this, true); }

  int size();
  void shuffle() {   
    CustomRandom crand;
    std::random_shuffle(_template_instances.begin(), _template_instances.end(), crand);
  }
  int getNodesInputDim() { return _n; }
  int getDomainValence() { return _v; }
};

#endif // _DATA_SET_H
