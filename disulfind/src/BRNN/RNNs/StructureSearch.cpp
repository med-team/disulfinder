#include "util.h"
#include "StructureSearch.h"
//#include <climits>
#include <cmath>
//#include <boost/graph/graphviz.hpp>
//#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;

extern int debugLevel;

long int factorial(long int n) {
  if(n == 1 || n == 0)
    return 1;
  long int value = n * factorial(n-1);
  if(!value) {
    cerr << "Exceeded maximum value!" << endl;
  }
  return value;
}

///////////////////////////////////////////////////////////////////////////////////

// A recursive method to generate all possible graph connectivity patterns
void connectivityPatterns(const std::set<int>& S, const std::set<pair<int, int> >& P, 
			  std::vector<std::set<std::pair<int, int> > >& connPatterns) {
  std::set<pair<int, int> > C;
  std::set<int>::iterator it=S.begin(); ++it;
  for(; it!=S.end(); ++it) C.insert(make_pair(*(S.begin()), *it));

  if(C.size() == 1) {
    std::set<std::pair<int, int> > P_final(P); P_final.insert(*(C.begin()));
    connPatterns.push_back(P_final);
    return;
  }

  for(std::set<std::pair<int, int> >::iterator it=C.begin(); it!=C.end(); ++it) {
    int a[] = {(*it).first, (*it).second };
    std::set<int> PairSet(a, a+2);
    std::set<int> X_new; 
    std::set<pair<int, int> > P_new(P); P_new.insert(*it);

    std::set_difference(S.begin(), S.end(), PairSet.begin(), PairSet.end(), 
			std::inserter(X_new, X_new.begin()));

    if(X_new.size() != 0)
      connectivityPatterns(X_new, P_new, connPatterns);
  }
}

///////////////////////////////////////////////////////////////////////////////////////

/*** Search Simulation ***/
// void StructureSearch::BeamSearch(const char* outfilename, const string& id, const DPAG& target, float (*measure)(const DPAG&, const DPAG&, vector<float>&), int beamsize, bool print_generated_data) {
//   // Initialize random number generator with current time seed,
//   // so the choice of what good node to expand is random
//   srand(time(0));
//   cout.precision(10);

//   /* determine some informations to print graphs */
//   // Assume all nodes have the same input dimension
//   int nodes_input_dim = _nodesInputs[0].size();
//   // Warning!!!
//   // Node output dim is fixed to 1, because SearchTreeNodes and graphs
//   // measures assume nodes output dimension == 1
//   int nodes_output_dim = 1;
//   // Same observation for graphs global output dimension
//   int graphs_output_dim = 2;

//   // Open file to write graphs found during the simulation process
//   ofstream os;
//   if(print_generated_data) {
//     os.open(outfilename);
//     assure(os, outfilename);
//     os << "id " << id << endl
//        << "global_target_dim " << graphs_output_dim << endl
//        << "local_target_dim " << nodes_output_dim << endl
//        << "nodes_input_dim " << nodes_input_dim << endl << endl;

//     // print inputs to the set of nodes
//     for(int i=0; i<_nodesInputs.size(); i++) {
//       os << "node ";
//       copy(_nodesInputs[i].begin(), _nodesInputs[i].end(), ostream_iterator<float>(os, " "));
//       os << endl;
//     }
//     os << endl;
//   }

//   int num_nodes = boost::num_vertices(target);
//   //int beamsize = atoi((Options::instance()->getParameter("beamsize")).c_str()); //(num_nodes*(num_nodes-1)) / 2;
//   if(!beamsize)
//     beamsize = 1;
  
//   // With target graph we can deduce optimality through its score
//   vector<float> dummyScores;
//   float maximal_score = measure(target, target, dummyScores);
//   //cout << "Target score: " << maximal_score << endl;

//   // Initialize the search tree using the 0-edges graph
//   list<SearchTreeNode*> frontier;

//   // Instead of printing expanded nodes in sequence (so as to bias
//   // network online training towards low score graphs) we store
//   // these nodes and when the search procedure stops, perfom a 
//   // random shuffle of the order of the elements
//   vector<int> expanded_nodes_indexes;
//   vector<SearchTreeNode*> expanded_nodes;
//   // Initially reserve to this vector a size that is 
//   // a rough optimistic estimation of the number of graphs 
//   // generated, to avoid unnecessarily vector reallocation
//   expanded_nodes.reserve((num_nodes*(num_nodes-1)) / 2);
//   /************************************/

//   // Start state is the graph which connects all the node
//   // adjacent in sequence --> this is almost true in practice,
//   // adjacent secondary structure segments are near in 3-d space
//   DPAG initgraph(num_nodes);
//   //for(int i=1; i<num_nodes; ++i)
//   //boost::add_edge(i-1, i, EdgeProperty(0), initgraph);

//   vector<float> localInitScores;
//   float initscore = measure(initgraph, target, localInitScores);
//   float prevFScore = PRMeasure(initgraph, target, localInitScores);
//   int depth = 0;
//   frontier.push_back(new SearchTreeNode(initgraph, depth, initscore, localInitScores));

//   SearchTreeNode* bestSubOptimal;
//   if(!print_generated_data)
//     bestSubOptimal = new SearchTreeNode(initgraph, depth, initscore);
  
//   while(!frontier.empty()) {
//     SearchTreeNode* to_expand;
    
//     // Mantains a list of the beamsize best candidates
//     vector<SearchTreeNode*> newCandidates;
//     //priority_queue<SearchTreeNode*, vector<SearchTreeNode*>, LT_SNode> newCandidates;
//     //set<SearchTreeNode*, GT_SNode> newCandidates;

//     //newCandidates.reserve(beamsize * frontier.size());
  
//     // Iterates through candidates and generate successors
//     for(list<SearchTreeNode*>::iterator iter=frontier.begin(); iter!=frontier.end(); iter++) {
//       to_expand = *iter;
//       depth = to_expand->_depth + 1;
      
//       // Store current expanded node to print it lately
//       expanded_nodes.push_back(new SearchTreeNode(*to_expand));
//       int index = expanded_nodes_indexes.size();
//       expanded_nodes_indexes.push_back(index);

//       // Goal check!
//       if(to_expand->_score == maximal_score) {
// 	cout << endl << "GOAL!" << endl;

// 	// expanded_nodes.push_back(new SearchTreeNode(*to_expand));
// // 	int index = expanded_nodes_indexes.size();
// // 	expanded_nodes_indexes.push_back(index);

// 	//break;
// 	continue;
//       }
  
//       /* 
// 	 Expand current node. Successors are derived in the following way:
// 	 repeat
// 	   1. choose at random one vertex in current graph
// 	   2. if its score is not the optimal one and its indegree and outdegree
// 	      are less than the maximal ones (estimated from the available datasets, then
// 	      generate successors each one corresponding to the graph with one edge from
// 	      a predecessor to the vertex or one edge from the vertex to a successor (in
// 	      the sequence)
// 	 until 2 is not satisfied
//       */
//       VertexId vertex_id = boost::get(boost::vertex_index, to_expand->_dpag);
//       vertexIt v_open, v_begin, v_i1, v_end;
//       outIter out_i, out_end;

//       // get start and end guards iterators
//       boost::tie(v_begin, v_end) = boost::vertices(to_expand->_dpag);

//       // Find first "open" node (for which there is margin of gain
//       // and that has in/out degree less than the maximum
//       int num_attempts = 0;
//       while(num_attempts++ < 1000) {
// 	int i, R = rand() % boost::num_vertices(to_expand->_dpag);
// 	vertexIt v_i;
// 	for(i=0, v_i=v_begin; v_i!=v_end; ++v_i, ++i) {
// 	  if(i == R) 
// 	    break;
// 	}
// 	if(fabs(to_expand->_localScores[vertex_id[*v_i]] - 1) > 0.0) {
// 	  v_open = v_i;
// 	  break;
// 	}
//       }
//       if(num_attempts >= 1000)
// 	continue;

//       // Generate all successors by adding one 
//       // edge linking current popped node
//       // First try edges to current node from its predecessors
//       for(v_i1=v_begin; v_i1!=v_open; ++v_i1) {
// 	DPAG successor(to_expand->_dpag);
// 	bool found = false;
// 	for(boost::tie(out_i, out_end)=boost::out_edges(*v_i1, to_expand->_dpag);
// 	    out_i!=out_end; ++out_i) {
// 	  if(boost::target(*out_i, to_expand->_dpag) == *v_open) {
// 	    found = true;
// 	    break;
// 	  }
// 	}
// 	if(!found) {
// 	  boost::add_edge(vertex_id[*v_i1], vertex_id[*v_open], EdgeProperty(0), successor);
// 	  float succ_score;
// 	  vector<float> localScores;
// 	  succ_score = measure(successor, target, localScores);
// 	  VERBOSE(3, "Successor score: " << succ_score);
// 	  if(newCandidates.size() < beamsize) {
// 	    // Add this successor to the list of new candidates.
// 	    // Do not add if it is a duplicate.
// 	    bool duplicate = false;
	    
// 	    vector<SearchTreeNode*>::iterator iter = newCandidates.begin();
// 	    while(iter != newCandidates.end()) {
// 	      if(equal(successor, (*iter)->_dpag)) {
// 		duplicate = true;
// 		break;
// 	      }
// 	      ++iter;
// 	    }
	    
// 	    if(!duplicate) {
// 	      // Insert successor into list of new candidates only if
// 	      // it is not a duplicate. Nodes are automatically sorted into
// 	      // descending order.
// 	      newCandidates.push_back(new SearchTreeNode(successor, depth, succ_score, localScores));
// 	      VERBOSE(3, " Inserted (room in beam)" << endl);
// 	      sort(newCandidates.begin(), newCandidates.end(), GT_SNode());
// 	    } else {
// 	      VERBOSE(3, " Rejected (duplicate)" << endl);
// 	    }
// 	  } else {
// 	    if(succ_score >= newCandidates[beamsize-1]->_score) {
// 	      // Add this successor to the list of new candidates.
// 	      // Do not add if it is a duplicate.
// 	      bool duplicate = false;
// 	      vector<SearchTreeNode*>::iterator iter = newCandidates.begin();
// 	      while(iter != newCandidates.end()) {
// 		if(equal(successor, (*iter)->_dpag)) {
// 		  duplicate = true;
// 		  break;
// 		}
// 		++iter;
// 	      }
	      
// 	      if(!duplicate) {
// 		// Insert successor into list of new candidates only if
// 		// it is not a duplicate. Nodes are automatically sorted into
// 		// descending order.
// 		newCandidates.push_back(new SearchTreeNode(successor, depth, succ_score, localScores));
// 		VERBOSE(3, " Inserted! (better than " 
// 			<< newCandidates[beamsize-1]->_score << ")" << endl);
		
// 		sort(newCandidates.begin(), newCandidates.end(), GT_SNode());
// 	      } else {
// 		VERBOSE(3, " Rejected (duplicate)" << endl);
// 	      }
// 	    } else {
// 	      VERBOSE(3, " Rejected (beam full and no better)" << endl);
// 	    }
// 	  }
// 	}
//       }
      
//       // Then try edges from current node to its successors
//       for(v_i1=v_open+1; v_i1!=v_end; ++v_i1) {
// 	DPAG successor(to_expand->_dpag);
// 	bool found = false;
// 	for(boost::tie(out_i, out_end)=boost::out_edges(*v_open, to_expand->_dpag);
// 	    out_i!=out_end; ++out_i) {
// 	  if(boost::target(*out_i, to_expand->_dpag) == *v_i1) {
// 	    found = true;
// 	    break;
// 	  }
// 	}
// 	if(!found) {
// 	  boost::add_edge(vertex_id[*v_open], vertex_id[*v_i1], EdgeProperty(0), successor);
// 	  float succ_score;
// 	  vector<float> localScores;
// 	  succ_score = measure(successor, target, localScores);
// 	  VERBOSE(3, "Successor score: " << succ_score);
// 	  if(newCandidates.size() < beamsize) {
// 	    // Add this successor to the list of new candidates.
// 	    // Do not add if it is a duplicate.
// 	    bool duplicate = false;
// 	    //unsigned int successor_coding = getDPAGcoding(successor);
// 	    vector<SearchTreeNode*>::iterator iter = newCandidates.begin();
// 	    while(iter != newCandidates.end()) {
// 	      if(equal(successor, (*iter)->_dpag)) {
// 		duplicate = true;
// 		break;
// 	      }
// 	      ++iter;
// 	    }
	      
// 	    if(!duplicate) {
// 	      // Insert successor into list of new candidates only if
// 	      // it is not a duplicate. Nodes are automatically sorted into
// 	      // descending order.
// 	      newCandidates.push_back(new SearchTreeNode(successor, depth, succ_score, localScores));
// 	      VERBOSE(3, " Inserted (room in beam)" << endl);
// 	      sort(newCandidates.begin(), newCandidates.end(), GT_SNode());
// 	    } else {
// 	      VERBOSE(3, " Rejected (duplicate)" << endl);
// 	    }
// 	  } else {
// 	    if(succ_score >= newCandidates[beamsize-1]->_score) {
// 	      // Add this successor to the list of new candidates.
// 	      // Do not add if it is a duplicate.
// 	      bool duplicate = false;
// 	      vector<SearchTreeNode*>::iterator iter = newCandidates.begin();
// 	      while(iter != newCandidates.end()) {
// 		if(equal(successor, (*iter)->_dpag)) {
// 		  duplicate = true;
// 		  break;
// 		}
// 		++iter;
// 	      }
	      
// 	      if(!duplicate) {
// 		// Insert successor into list of new candidates only if
// 		// it is not a duplicate. Nodes are automatically sorted into
// 		// descending order.
// 		newCandidates.push_back(new SearchTreeNode(successor, depth, succ_score, localScores));
// 		VERBOSE(3, " Inserted! (better than " 
// 			<< newCandidates[beamsize-1]->_score << ")" << endl);
		
// 		sort(newCandidates.begin(), newCandidates.end(), GT_SNode());
// 	      } else {
// 		VERBOSE(3, " Rejected (duplicate)" << endl);
// 	      }
// 	    } else {
// 	      VERBOSE(3, " Rejected (beam full and no better)" << endl);
// 	    }
// 	  }
// 	}
//       }
      
//     }

//     if(!print_generated_data) {
//       vector<float> dummy;
//       float fScore = PRMeasure(bestSubOptimal->_dpag, target, dummy);
//       cout << endl << "(" << bestSubOptimal->_score << ", " << fScore 
// 	   << ", " << (bestSubOptimal->_score-fScore) << ", " << (fScore-prevFScore) << ")" << endl;
//       prevFScore = fScore;
//       cout << "[ ";
//       for(int i=0; i<newCandidates.size(); i++)
// 	cout << newCandidates[i]->_score << " ";
//       cout << "]" << endl;
	
//       // Update highest score suboptimal graph
//       if(newCandidates.size() && bestSubOptimal->_score < (*(newCandidates.begin()))->_score) {
// 	delete bestSubOptimal;
// 	// Copy construct because pointer to nodes are deallocated
// 	bestSubOptimal = new SearchTreeNode(*(*(newCandidates.begin())));
//       } else {
// 	cout << "\tGOAL " << bestSubOptimal->_score << " "
// 	     << fScore;
// 	break;
//       }
//     }

//     // Update frontier with the best candidates.
//     // First remove current elements...
//     for(list<SearchTreeNode*>::iterator iter=frontier.begin(); iter!=frontier.end(); iter++) {
//       if(*iter) {
// 	delete *iter;
// 	*iter = 0;
//       }
//     }
//     frontier.clear(); // Remove previous elements

//     // Then insert beamsize best new candidates after resorting
//     stable_sort(newCandidates.begin(), newCandidates.end(), GT_SNode());
//     vector<SearchTreeNode*>::iterator it = newCandidates.begin();
//     int i = 1;
//     while(it != newCandidates.end()) {
//       if(i++<=beamsize)
// 	frontier.push_back(*it);
//       else {
// 	delete *it;
// 	*it = 0;
//       }
//       ++it;
//     }
//     newCandidates.clear();

//     cout << depth << ' ' << flush; 
//   }

//   // this is to clean up memory after goal
//   for(list<SearchTreeNode*>::iterator iter=frontier.begin(); 
//       iter!=frontier.end(); iter++) {
//     if (*iter) {
//       delete *iter;
//       *iter = 0;
//     }
//   }
//   frontier.clear(); // Remove previous elements
  
//   if(print_generated_data) {
//     //ofstream ofs("f-corr.dat", ios::app);
    
//     // Ok, now we have to randomly shuffle the graphs and print
//     // them in the resulting order. Of course, perfoms shuffling
//     // on the array of indexes...
//     CustomRandom crand;
//     random_shuffle(expanded_nodes_indexes.begin(), expanded_nodes_indexes.end(), crand);

//     for(vector<int>::iterator it=expanded_nodes_indexes.begin();
// 	it!=expanded_nodes_indexes.end(); ++it) {
//       SearchTreeNode* expanded = expanded_nodes[*it];
    
//       // follow declarations to iterate through graph 
//       // vertices and print informations.
//       VertexId vertex_id = boost::get(boost::vertex_index, expanded->_dpag);
//       vertexIt v_i, v_end;
//       outIter out_i, out_end;
//       vector<float> localScores;
//       ConfusionTable ct = getPredictionIndexes(expanded->_dpag, target, localScores);
//       os << "graph (" << boost::num_vertices(expanded->_dpag) << ") " << ct.getPrecision() << ' ' << ct.getRecall() << endl << endl;

//       //int a, b, c;
//       //pair<float, float> global_pr = globalPRMeasure(expanded->_dpag, target, a, b, c);
//       //ofs << ((2*global_pr.first*global_pr.second)/(global_pr.first+global_pr.second)) << '\t' << expanded->_score << endl;
  
//       for(boost::tie(v_i, v_end) = boost::vertices(expanded->_dpag); 
// 	  v_i!=v_end; ++v_i) {
// 	os << vertex_id[*v_i] << ' '
// 	   << expanded->_localScores[vertex_id[*v_i]] << ' ';
// 	//copy((_nodesInputs[vertex_id[*v_i]]).begin(), (_nodesInputs[vertex_id[*v_i]]).end(),
// 	//   ostream_iterator<float>(os, " "));
// 	for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, expanded->_dpag);
// 	    out_i!=out_end; ++out_i) {
// 	  os << boost::target(*out_i, expanded->_dpag) << ' ';
// 	}
// 	os << endl;
//       }
//       os << endl << endl;

//     }
//   }

//   // Purge content of the expanded node array
//   for(vector<SearchTreeNode*>::iterator iter=expanded_nodes.begin(); 
//       iter!=expanded_nodes.end(); iter++) {
//     if(*iter) {
//       delete *iter;
//       *iter = 0;
//     }
//   }
// }

// ///////////////////////////////////////////////////////////////////////////////////////
// void StructureSearch::simpleSearch(const char* outfilename, const string& id, 
// 				   const DPAG& target) {
//   int nodes_input_dim = _nodesInputs[0].size();
//   int nodes_output_dim = 1, graphs_output_dim = 1; 

//   // Open file to write graphs found during the simulation process
//   ofstream os(outfilename);
//   assure(os, outfilename);
//   os << "id " << id << endl
//      << "global_target_dim " << graphs_output_dim << endl
//      << "local_target_dim " << nodes_output_dim << endl
//      << "nodes_input_dim " << nodes_input_dim << endl << endl;
  
//   // print inputs to the set of nodes
//   for(int i=0; i<_nodesInputs.size(); i++) {
//     os << "node ";
//     copy(_nodesInputs[i].begin(), _nodesInputs[i].end(), ostream_iterator<float>(os, " "));
//     os << endl;
//   }
//   os << endl;

//   int num_nodes = _nodesInputs.size();
//   DPAG currentDpag(num_nodes);
//   VertexId vertex_id = boost::get(boost::vertex_index, target);
//   vertexIt v_i, v_end;
//   if(boost::num_edges(target) == 0) {
//     vector<float> localScores;
//     ConfusionTable ct = getPredictionIndexes(currentDpag, target, localScores);
//     float prec = ct.getPrecision(), rec = ct.getRecall();
//     os << "graph (" << boost::num_vertices(currentDpag) << ") " 
//        << (2*prec*rec)/(prec+rec) << endl << endl;

//     for(boost::tie(v_i, v_end) = boost::vertices(target); 
//       v_i!=v_end; ++v_i) {
//       os << vertex_id[*v_i] << ' ' << 1 << endl;
//     }
//     os << endl;
//     return;
//   }
//   for(boost::tie(v_i, v_end) = boost::vertices(target); 
//       v_i!=v_end; ++v_i) {
//     for(vertexIt v_current=v_i+1; v_current!=v_end; ++v_current) {
//       pair<Edge_d, bool> edge_pair = boost::edge(*v_i, *v_current, target);
//       if(edge_pair.second == true) {
// 	boost::add_edge(vertex_id[*v_i], vertex_id[*v_current], currentDpag);
// 	vector<float> localScores;
// 	ConfusionTable ct = getPredictionIndexes(currentDpag, target, localScores);
// 	float prec = ct.getPrecision(), rec = ct.getRecall();
// 	os << "graph (" << boost::num_vertices(currentDpag) << ") " 
// 	   << (2*prec*rec)/(prec+rec) << endl << endl;
  
// 	vertexIt v_i1, v_end1;
// 	outIter out_i, out_end;
// 	for(boost::tie(v_i1, v_end1) = boost::vertices(currentDpag); 
// 	    v_i1!=v_end1; ++v_i1) {
// 	  os << vertex_id[*v_i1] << " 1 ";
	  
// 	  for(boost::tie(out_i, out_end)=boost::out_edges(*v_i1, currentDpag);
// 	      out_i!=out_end; ++out_i) {
// 	    os << boost::target(*out_i, currentDpag) << ' ';
// 	  }
// 	  os << endl;
// 	}
// 	os << endl << endl;
//       }
//     }
//   }
  
// }

///////////////////////////////////////////////////////////////////////////////////////

// void StructureSearch::ConnectivityPatternSearch(const char* outfilename, const string& id, const map<int, vector<float> >& nodesInputs, const DPAG& target, float (*measure)(const DPAG&, const DPAG&, std::vector<float>&)) {
//   /* determine some informations to print graphs */
//   // Assume all nodes have the same input dimension
//   int nodes_input_dim = (*nodesInputs.begin()).second.size();
//   // Warning!!!
//   // Node output dim is fixed to 1, because SearchTreeNodes and graphs
//   // measures assume nodes output dimension == 1
//   int nodes_output_dim = 1;
//   // Same observation for graphs global output dimension
//   int graphs_output_dim = 1;

//   // Open file to write graphs found during the simulation process
//   ofstream os;
//   os.open(outfilename);
//   assure(os, outfilename);

void StructureSearch::ConnectivityPatternSearch(ostream& os,
						const string& id,
						const map<int, vector<float> >& nodesInputs, 
						const DPAG& target, 
						float (*measure)(const DPAG&, 
								 const DPAG&, 
								 std::vector<float>&)) {
  /* determine some informations to print graphs */
  // Assume all nodes have the same input dimension
  int nodes_input_dim = (*nodesInputs.begin()).second.size();
  // Warning!!!
  // Node output dim is fixed to 1, because SearchTreeNodes and graphs
  // measures assume nodes output dimension == 1
  int nodes_output_dim = 1;
  // Same observation for graphs global output dimension
  int graphs_output_dim = 1;

  os << "id " << id << endl 
     << "global_target_dim " << graphs_output_dim << endl
     << "local_target_dim " << nodes_output_dim << endl
     << "nodes_input_dim " << nodes_input_dim << endl << endl;
  
  // print inputs to the set of nodes
  for(map<int, vector<float> >::const_iterator it=nodesInputs.begin(); it!=nodesInputs.end(); ++it) {
    os << "node ";
    copy((*it).second.begin(), (*it).second.end(), ostream_iterator<float>(os, " "));
    os << endl;
  }
  os << endl;

  int num_nodes = boost::num_vertices(target);
  // Instead of printing expanded nodes in sequence (so as to bias
  // network online training towards low score graphs) we store
  // these nodes and when the search procedure stops, perfom a 
  // random shuffle of the order of the elements
  vector<int> expanded_nodes_indexes;
  vector<SearchTreeNode*> expanded_nodes;

  expanded_nodes.reserve((num_nodes*(num_nodes-1)) / 2);

  vector<float> localScores;
  float maximal_score = measure(target, target, localScores);

  vector<int> V;
  for(int i=0; i<num_nodes; ++i)
    V.push_back(i);
  set<int> S(V.begin(), V.end());

  vector<set<pair<int, int> > > connPatterns;
  connectivityPatterns(S, set<pair<int, int> >(), connPatterns);
  require(connPatterns.size() > 0, "Error in connectivity patterns generation");
  
  vector<set<pair<int, int> > >::iterator v_it = connPatterns.begin();
  cout << connPatterns.size() << endl;
  while(v_it != connPatterns.end()) {
    DPAG candidate(num_nodes);
    for(set<pair<int, int> >::iterator s_it=(*v_it).begin(); s_it!=(*v_it).end(); ++s_it)
      boost::add_edge((*s_it).first, (*s_it).second, candidate); 

    
    float candidate_score = 
      measure(candidate, target, localScores); //(equal(candidate, target)?1.0:0.0);
    
    // Goal check!
    if(candidate_score == maximal_score)
      cout << endl << "GOAL!" << endl;

    expanded_nodes.push_back(new SearchTreeNode(candidate, 0, candidate_score, localScores));
    int index = expanded_nodes_indexes.size();
    expanded_nodes_indexes.push_back(index);

    ++v_it;
  }
    
  // Ok, now we have to randomly shuffle the graphs and print
  // them in the resulting order. Of course, perfoms shuffling
  // on the array of indexes...
  //CustomRandom crand;
  //random_shuffle(expanded_nodes_indexes.begin(), expanded_nodes_indexes.end(), crand);

  for(vector<int>::iterator it=expanded_nodes_indexes.begin();
      it!=expanded_nodes_indexes.end(); ++it) {
    SearchTreeNode* expanded = expanded_nodes[*it];
    
    // follow declarations to iterate through graph 
    // vertices and print informations.
    VertexId vertex_id = boost::get(boost::vertex_index, expanded->_dpag);
    vertexIt v_i, v_end;
    outIter out_i, out_end;
    
    os << "graph (" << boost::num_vertices(expanded->_dpag) << ") " 
       << expanded->_score << endl << endl;
  
    for(boost::tie(v_i, v_end) = boost::vertices(expanded->_dpag); 
	v_i!=v_end; ++v_i) {
      os << vertex_id[*v_i] << ' '
	 << expanded->_localScores[vertex_id[*v_i]] << ' ';

      for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, expanded->_dpag);
	  out_i!=out_end; ++out_i) {
	os << boost::target(*out_i, expanded->_dpag) << ' ';
      }
      os << endl;
    }
    os << endl << endl;

  }
  
  // Purge content of the expanded node array
  for(vector<SearchTreeNode*>::iterator iter=expanded_nodes.begin(); 
      iter!=expanded_nodes.end(); iter++) {
    if(*iter) {
      delete *iter;
      *iter = 0;
    }
  }
}

///////////////////////////////////////////////////////////////////////////////////////

/*** Search Simulation ***/
// void StructureSearch::CombinatorialSearch(const char* outfilename, const DPAG& target, float (*measure)(const DPAG&, const DPAG&, vector<float>&), bool print_generated_data) {

//   // Initialize random number generator with current time seed
//   srand(time(0));
//   int rand_seed = rand();
//   rand_seed = (rand_seed)?rand_seed:1;
//   base_generator_type generator(rand_seed);
  
//   // Assume all nodes have the same input dimension
//   int nodes_input_dim = _nodesInputs[0].size();
//   int nodes_output_dim = 1;
//   int graphs_output_dim = 1;

//   // Open file to write graphs found during the simulation process
//   ofstream os;
//   if(print_generated_data) {
//     os.open(outfilename);
//     assure(os, outfilename);
//     os << "global_target_dim " << graphs_output_dim << endl
//        << "local_target_dim " << nodes_output_dim << endl
//        << "nodes_input_dim " << nodes_input_dim << endl << endl;

//     // print inputs to the set of nodes
//     for(int i=0; i<_nodesInputs.size(); i++) {
//       os << "node ";
//       copy(_nodesInputs[i].begin(), _nodesInputs[i].end(), ostream_iterator<float>(os, " "));
//       os << endl;
//     }
//     os << endl;
//   }
  
//   int num_nodes = boost::num_vertices(target);
//   DPAG initgraph(num_nodes);
//   for(int i=1; i<boost::num_vertices(initgraph); ++i)
//     boost::add_edge(i-1, i, EdgeProperty(0), initgraph);

//   vector<float> localScores;
//   float score = measure(initgraph, target, localScores);
//   os << "graph (" << num_nodes << ") " << score << endl << endl;

//   vertexIt v_i, v_end;
//   outIter out_i, out_end;
//   VertexId vertex_id = boost::get(boost::vertex_index, initgraph);

//   for(boost::tie(v_i, v_end) = boost::vertices(initgraph); 
//       v_i!=v_end; ++v_i) {
//     os << vertex_id[*v_i] << ' '
//        << localScores[vertex_id[*v_i]] << ' ';

//     for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, initgraph);
// 	out_i!=out_end; ++out_i) {
//       os << boost::target(*out_i, initgraph) << ' ';
//     }
//     os << endl;
//   }
//   os << endl << endl;

//   boost::uniform_int<base_generator_type> cluster_indegree_distr(generator, 1, 4);
  
//   ieIter in_i, in_end;

//   int counter = 0;
//   while(counter++ < 100 && num_nodes > 1) {
//     DPAG g(initgraph);
//     vertex_id = boost::get(boost::vertex_index, g);

//     for(boost::tie(v_i, v_end)=boost::vertices(g); v_i!=v_end; ++v_i) {
//       if(vertex_id[*v_i] <= 3)
// 	continue;

//       int max_indegree = int(vertex_id[*v_i] * .5);
//       if(max_indegree <= 2)
// 	continue;
//       cout << max_indegree << endl;
//       boost::uniform_int<base_generator_type> indegree_distr(generator, 0, max_indegree);
//       int indegree = indegree_distr(), num_attempts = 1;
//       if(indegree == 0) 
// 	continue;
      
//       boost::uniform_int<base_generator_type> nodeindex_distr(generator, 0, vertex_id[*v_i]-2);

//       while(boost::in_degree(*v_i, g) < indegree && num_attempts <= 100) {
// 	int start_source = nodeindex_distr();
// 	int end_source = start_source + cluster_indegree_distr();
// 	while(end_source > vertex_id[*v_i]-2)
// 	  --end_source;
	
// 	for(int i=start_source; i<=end_source; ++i) {
// 	  bool found = false;
// 	  for(boost::tie(in_i, in_end)=boost::in_edges(*v_i, g);
// 	      in_i!=in_end; ++in_i) {
// 	    if(vertex_id[boost::source(*in_i, g)] == i) {
// 	      found = true;
// 	      break;
// 	    }
// 	  }
// 	  if(!found)
// 	    boost::add_edge(i, vertex_id[*v_i], EdgeProperty(0), g);
// 	  else
// 	    ++num_attempts;
// 	}
	
//       }
//     }

//     score = measure(g, target, localScores);
//     os << "graph (" << num_nodes << ") " << score << endl << endl;

//     for(boost::tie(v_i, v_end) = boost::vertices(g); 
// 	v_i!=v_end; ++v_i) {
//       os << vertex_id[*v_i] << ' '
// 	 << localScores[vertex_id[*v_i]] << ' ';

//       for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g);
// 	  out_i!=out_end; ++out_i) {
// 	os << boost::target(*out_i, g) << ' ';
//       }
//       os << endl;
//     }
//     os << endl << endl;
  
//   }

// }


///////////////////////////////////////////////////////////////////////////////////////

