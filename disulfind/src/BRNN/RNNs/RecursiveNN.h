#ifndef _RECURSIVE_NN_H
#define _RECURSIVE_NN_H

#include "require.h"
#include "Options.h"
#include "ActivationFunctions.h"
#include "ErrorMinimizationProcedure.h"
#include "DataSet.h"
#include <boost/random.hpp> // for boost::random number generators
#include <cfloat>
#include <ctime>
#include <vector>
#include <fstream>
#include <iostream>
using std::cout;
using std::endl;

#define PR(x) cout << #x << ": " << x << endl
#define BIGRND 0x7fffffff
#define WFP int z; cin >> z

typedef boost::minstd_rand base_generator_type;

/*
  Declaration and non-inline definition of the class 
  that represents a RecursiveNN model for structured inputs as DPAGs.
  Future changes will include the implementation of the Bridge
  Pattern to separate network abstraction from multiple
  implementation with different space and time complexity
  (e.g. the present and an implemecvantation based on blitz::array class).
  Another natural change would be to implement different
  error minimization procedures (adding momentum to gradient descent,
  conjugate gradient) via the Strategy Pattern.
*/

template<class HA_Function, class OA_Function, class EMP>
class RecursiveNN {
  public: // quick remedy to the ``friend EMP'' problem - see below.

  // Useful indexes as reported in (Goller �3.2)
  int _n, _v, _m, _q, _r, _s;

  // Array of weights matrixes denoting strength of
  // the connection from layer k to layer k+1 in f folding part.
  float*** _f_layers_w;
  float*** _prev_f_layers_w;
  float** _delta_f_layers; // delta output unit values of f layers.

  // The same for b folding part.
  float*** _b_layers_w;
  float*** _prev_b_layers_w;
  float** _delta_b_layers; // delta output unit values of b layers.

  // And layers for the supersource trasduction
  float*** _g_layers_w;
  float*** _prev_g_layers_w;
  // The same for an eventual io-isomorf trasduction
  float*** _h_layers_w;
  float*** _prev_h_layers_w;
  float** _delta_h_layers; // Error signals in h output map layers

  // Array of matrixes of weights componenents of the
  // gradient in each layer.
  float*** _f_layers_gradient_w;
  float*** _b_layers_gradient_w;
  float*** _g_layers_gradient_w;
  float*** _h_layers_gradient_w;

  // This part declares, together with previuos g_layers,
  // structures needed to implement MLP g output function.
  // They are inside RNN because g is global with respect to
  // a structure.
  float** _g_layers_activations;
  float** _delta_g_layers;

  // Store number of units per layer (f(&|)b + g(&|)h), 
  // threshold not included, must be taken into account.
  std::vector<int> _lnunits;

  // Flag indicating whether to perform computation
  // only for the casual enconding network (f layers) or also
  // for the non-casual one (b layers).
  bool _process_dr;
  // Whether to implement a supersource trasduction
  bool _ss_tr;
  // Wheter to implement an io-isomorf structural trasduction
  bool _ios_tr;

  // Template parameters indicate the type
  // of hidden and output units activation function.
  HA_Function haf;
  OA_Function oaf;

  /* 
     Templatized Strategy Pattern. 
     The Net mantains an object that implements one 
     of the possibile error minimization procedures.
  */
  //friend EMP; // this is forbidden now: Section 7.1.5.3 paragraph 2 of Standard C++ (also: http://www.comeaucomputing.com/techtalk/templates/ What's wrong with saying friend class T within a template?);
              // EMP object must be able to easily update net weights
  EMP _wu_method;

  /* Private functions */

  // Generate a random number between 0.0 and 1.0
  float rnd01() {
    return ((float) random() / (float) BIGRND);
  }

  // Generate a random number between -1.0 and +1.0
  float nrnd01() {
    return ((rnd01() * 2.0) - 1.0);
  }

  // Specialized allocation&deallocation functions
  void allocFpart();
  void allocBpart();
  void allocGpart();
  void allocHpart();

  void deallocFpart();
  void deallocBpart();
  void deallocGpart();
  void deallocHpart();

  // Reset gradient components every time weights are updated.
  void resetFGradient();
  void resetBGradient();
  void resetGGradient();
  void resetHGradient();

  // Reset output values in g MLP layers
  void resetGvalues();
  
 public:
  /*
    Constructor used to train a RNN.
    Network weights are initialized to random values.
  */
  RecursiveNN();

  /* Constructor: read network parameters from file */
  RecursiveNN(const char*);

  /* Destructor */
  ~RecursiveNN();

    // Propagation routines for
  // each specific part of the Net.
  void fPropagateInput(std::vector<Node*>&, const DPAG&);
  void bPropagateInput(std::vector<Node*>&, const DPAG&);
  void gPropagateInput(std::vector<Node*>&);
  void hPropagateInput(Node*);

  // Error Back-Propagation Through Structures 
  // routines for each specific part of the Net.
  void fBackPropagateError(std::vector<Node*>&, const DPAG&, const std::vector<std::vector<Node*> >& = std::vector<std::vector<Node*> >());
  void bBackPropagateError(std::vector<Node*>&, const DPAG&, const std::vector<std::vector<Node*> >& = std::vector<std::vector<Node*> >());
  void gBackPropagateError(std::vector<Node*>&, const std::vector<float>&,
			   const std::vector<float>& = std::vector<float>());
  void hBackPropagateError(Node*, const std::vector<float>& = std::vector<float>(), bool brnn_birnn = false);

  void propagateStructuredInput(std::vector<Node*>&, const std::pair<DPAG, DPAG>&);
  void backPropagateError(std::vector<Node*>&, const std::pair<DPAG, DPAG>&, const std::vector<float>&, bool apply_softmax = false);

  // Implements weight update rule
  void adjustWeights(float = 0.0, float = 0.0, float = 0.0);

  // To restore previous parameters settings, so
  // we can adjust learning rate, in case we make a bad move
  void restorePrevWeights();

  // To reset gradient components, made public so training procedure
  // can use it to begin another training phase using the same network.
  void resetGradientComponents();
  
  // Evaluate error and performance of the net over a data set
  float evaluatePerformanceOnDataSet(DataSet*, bool = false);

  // Save learned Network parameters to file
  void saveParameters(const char*);

  // WARNING!!!!! 
  // Non portable function to get output from the net.
  float getGlobalOutput() {
    /* require(_lnunits[_r+_s-1] == 2, "Error in method RecursiveNN::getGlobalOutput"); */
/*     return ((2 * _g_layers_activations[_s-1][0] * _g_layers_activations[_s-1][1]) / */
/*       (_g_layers_activations[_s-1][0] + _g_layers_activations[_s-1][1])); */

    return _g_layers_activations[_s-1][0];
  }

  void setGlobalOutput(float value) { _g_layers_activations[_s-1][0] = value; }
  
  std::vector<float> getGlobalOutputs() {
    return std::vector<float>(_g_layers_activations[_s-1], 
			       _g_layers_activations[_s-1] + _lnunits[_r+_s-1]);
  }

  // Compute Error for a structure in case of Super-Source trasduction
  float computeSSTrError(const std::vector<float>&, bool = false);

  // Compute Error for a structure in case of IO-Isomorph structural trasduction
  float computeIOSTrError(const std::vector<Node*>&, bool = false);
  // Compute the (squared norm) of the weights. It is necessary to compute the
  // error when regularization is used (weight decay).
  float computeWeightsNorm();
};



/*********************************************************
  Non-inline template class member functions definitions
*********************************************************/


/* Private: F folding part allocation routine */
typedef	float*	floatref;
typedef	float**	floatrefref;

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::allocFpart() {
  //base_generator_type generator(rand());
  //boost::uniform_real<base_generator_type> distr(generator, -.05, .05);

  // Assume constructor has initialized required dimension quantities
  _f_layers_w = new floatrefref[_r];
  _prev_f_layers_w = new floatrefref[_r];
  _f_layers_gradient_w = new floatrefref[_r];
  if(_r > 1) {
    _delta_f_layers = new floatref[_r-1];
    _delta_f_layers[0] = new float[_lnunits[0]];
    memset(_delta_f_layers[0], 0, (_lnunits[0]) * sizeof(float));
  }

  // Allocate weights and gradient components matrixes
  // for f folding part. Connections from input layer
  // have special dimensions.

#ifdef DEBUG
  cout << endl << endl 
       << "(f) Layer 1: (" << (_n+_v*_m) + 1 << "," << _lnunits[0] << ")" 
       << endl;
#endif

  _f_layers_w[0] = new floatref[(_n+_v*_m) + 1];
  _prev_f_layers_w[0] = new floatref[(_n+_v*_m) + 1];
  _f_layers_gradient_w[0] = new floatref[(_n+_v*_m) + 1];

  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    _f_layers_w[0][i] = new float[_lnunits[0]];
    _prev_f_layers_w[0][i] = new float[_lnunits[0]];
    _f_layers_gradient_w[0][i] = new float[_lnunits[0]];

    // Reset gradient for corresponding weights
    memset(_f_layers_gradient_w[0][i], 0, _lnunits[0] * sizeof(float));
	
    // Assign weights a random number between -1.0 and +1.0
    for(int j=0; j<_lnunits[0]; j++) {
      _f_layers_w[0][i][j] = nrnd01()/float(_lnunits[0]);
      _prev_f_layers_w[0][i][j] = _f_layers_w[0][i][j];
      //cout << _f_layers_w[0][i][j] << " ";
    }
    //cout << endl;
  }
  
  // Allocate f weights&gradient matrixes for f folding part.
  for(int k=1; k<_r; k++) {
#ifdef DEBUG
    cout << endl << endl
	 << "(f) Layer " << k+1 << ": (" << _lnunits[k-1] + 1  << "," << _lnunits[k] << ")" 
	 << endl;
#endif
    // Allocate space for weight&delta matrix between layer i-1 and i.
    // Automatically include space for threshold unit in layer i-1
    _f_layers_w[k] = new floatref[_lnunits[k-1] + 1];
    _prev_f_layers_w[k] = new floatref[_lnunits[k-1] + 1];
    _f_layers_gradient_w[k] = new floatref[_lnunits[k-1] + 1];

    for(int i=0; i<_lnunits[k-1]+1; i++) {
      _f_layers_w[k][i] = new float[_lnunits[k]];
      _prev_f_layers_w[k][i] = new float[_lnunits[k]];
      _f_layers_gradient_w[k][i] = new float[_lnunits[k]];

      // Reset f gradient components for corresponding weights
      memset(_f_layers_gradient_w[k][i], 0, (_lnunits[k])*sizeof(float));
      
      // Assign f weights a random number between -1.0 and +1.0
      for(int j=0; j<_lnunits[k]; j++) {
	_f_layers_w[k][i][j] = nrnd01()/float(_lnunits[k]);
	_prev_f_layers_w[k][i][j] = _f_layers_w[k][i][j];
	//cout << _f_layers_w[k][i][j] << " ";
      }
      //cout << endl;
    }
    
    if(k < _r-1) {
      _delta_f_layers[k] = new float[_lnunits[k]];
      memset(_delta_f_layers[k], 0, (_lnunits[k]) * sizeof(float));
    }
  }  
}


/* Private: B folding part allocation routine */

template<class HA_Function, class OA_Function, class EMP>
  void RecursiveNN<HA_Function, OA_Function, EMP>::allocBpart() {
  //base_generator_type generator(rand());
  //boost::uniform_real<base_generator_type> distr(generator, -.05, .05);

  // Assume constructor has initialized required dimension quantities
  _b_layers_w = new floatrefref[_r];
  _prev_b_layers_w = new floatrefref[_r];
  _b_layers_gradient_w = new floatrefref[_r];
  if(_r > 1) {
    _delta_b_layers = new floatref[_r-1];
    _delta_b_layers[0] = new float[_lnunits[0]];
    memset(_delta_b_layers[0], 0, (_lnunits[0]) * sizeof(float));
  }
  
  // Allocate weights and gradient components matrixes
  // for b folding part. Connections from input layer
  // have special dimensions.
#ifdef DEBUG
  cout << endl << endl 
       << "(b) Layer 1: (" << (_n+_v*_m) + 1 << "," << _lnunits[0] << ")" 
       << endl;
#endif
  _b_layers_w[0] = new floatref[(_n+_v*_m) + 1];
  _prev_b_layers_w[0] = new floatref[(_n+_v*_m) + 1];
  _b_layers_gradient_w[0] = new floatref[(_n+_v*_m) + 1];

  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    _b_layers_w[0][i] = new float[_lnunits[0]];
    _prev_b_layers_w[0][i] = new float[_lnunits[0]];
    _b_layers_gradient_w[0][i] = new float[_lnunits[0]];

    // Reset gradient for corresponding weights
    memset(_b_layers_gradient_w[0][i], 0, _lnunits[0] * sizeof(float));
	
    // Assign weights a random number between -1.0 and +1.0
    for(int j=0; j<_lnunits[0]; j++) {
      _b_layers_w[0][i][j] = nrnd01()/float(_lnunits[0]);
      _prev_b_layers_w[0][i][j] = _b_layers_w[0][i][j];
      //cout << _b_layers_w[0][i][j] << " ";
    }
    //cout << endl;
  }
  
  // Allocate b weights&gradient matrixes for f folding part.
  for(int k=1; k<_r; k++) {
#ifdef DEBUG
    cout << endl << endl
	 << "(b) Layer " << k+1 << ": (" << _lnunits[k-1] + 1  << "," << _lnunits[k] << ")" 
	 << endl;
#endif
    // Allocate space for weight&delta matrix between layer i-1 and i.
    // Automatically include space for threshold unit in layer i-1
    _b_layers_w[k] = new floatref[_lnunits[k-1] + 1];
    _prev_b_layers_w[k] = new floatref[_lnunits[k-1] + 1];
    _b_layers_gradient_w[k] = new floatref[_lnunits[k-1] + 1];

    for(int i=0; i<_lnunits[k-1]+1; i++) {
      _b_layers_w[k][i] = new float[_lnunits[k]];
      _prev_b_layers_w[k][i] = new float[_lnunits[k]];
      _b_layers_gradient_w[k][i] = new float[_lnunits[k]];

      // Reset b gradient components for corresponding weights
      memset(_b_layers_gradient_w[k][i], 0, (_lnunits[k])*sizeof(float));

      // Assign b weights a random number between -1.0 and +1.0
      for(int j=0; j<_lnunits[k]; j++) {
	_b_layers_w[k][i][j] = nrnd01()/float(_lnunits[k]);
	_prev_b_layers_w[k][i][j] = _b_layers_w[k][i][j];
	//cout << _b_layers_w[k][i][j] << " ";
      }
      //cout << endl;
    }

    if(k < _r-1) {
      _delta_b_layers[k] = new float[_lnunits[k]];
      memset(_delta_b_layers[k], 0, (_lnunits[k]) * sizeof(float));
    }
  }
}


/* Private: G tranforming part allocation routine */

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::allocGpart() {
  //base_generator_type generator(rand());
  //boost::uniform_real<base_generator_type> distr(generator, -.05, .05);

  // Assume constructor has initialized required dimension quantities
  _g_layers_w = new floatrefref[_s];
  _prev_g_layers_w = new floatrefref[_s];
  _g_layers_gradient_w = new floatrefref[_s];
  _g_layers_activations = new floatref[_s];
  _delta_g_layers = new floatref[_s];

  // Allocate weights and gradient components matrixes
  // for g transforming part. Connections from input layers
  // have special dimensions.
#ifdef DEBUG
  cout << endl << endl 
       << "(g) Layer 1: (" << (2*_m) + 1 << "," << _lnunits[_r] << ")" 
       << endl;
#endif
  _g_layers_w[0] = new floatref[2*_m + 1];
  _prev_g_layers_w[0] = new floatref[2*_m + 1];
  _g_layers_gradient_w[0] = new floatref[2*_m + 1];
  
  // Allocate and reset g layers output activation units and delta values.
  _g_layers_activations[0] = new float[_lnunits[_r]];
  memset(_g_layers_activations[0], 0, (_lnunits[_r])*sizeof(float));
  _delta_g_layers[0] = new float[_lnunits[_r]];
  memset(_delta_g_layers[0], 0, (_lnunits[_r])*sizeof(float));


  for(int i=0; i<2*_m + 1; i++) {
    _g_layers_w[0][i] = new float[_lnunits[_r]];
    _prev_g_layers_w[0][i] = new float[_lnunits[_r]];
    _g_layers_gradient_w[0][i] = new float[_lnunits[_r]];

    // Reset gradient for corresponding weights
    memset(_g_layers_gradient_w[0][i], 0, _lnunits[_r] * sizeof(float));
	
    // Assign weights a random number between -1.0 and +1.0
    for(int j=0; j<_lnunits[_r]; j++) {
      _g_layers_w[0][i][j] = nrnd01()/float(_lnunits[_r]);
      _prev_g_layers_w[0][i][j] = _g_layers_w[0][i][j];
      //cout << _g_layers_w[0][i][j] << " ";
    }
    //cout << endl;
  }
  
  // Allocate weights&gradient matrixes for g transforming part
  for(int k=1; k<_s; k++) {
#ifdef DEBUG
    cout << endl << endl
	 << "(g) Layer " << k+1 << ": (" << _lnunits[_r+k-1] + 1  << "," << _lnunits[_r+k] << ")" 
	 << endl;
#endif
    // Allocate space for weight&gradient matrixes between layer i-1 and i.
    // Automatically include space for threshold unit in layer i-1
    _g_layers_w[k] = new floatref[_lnunits[_r+k-1] + 1];
    _prev_g_layers_w[k] = new floatref[_lnunits[_r+k-1] + 1];
    _g_layers_gradient_w[k] = new floatref[_lnunits[_r+k-1] + 1];

    // Allocate and reset g layers output activation units and delta layers values.
    _g_layers_activations[k] = new float[_lnunits[_r+k]];
    memset(_g_layers_activations[k], 0, (_lnunits[_r+k])*sizeof(float));

    _delta_g_layers[k] = new float[_lnunits[_r+k]];
    memset(_delta_g_layers[k], 0, (_lnunits[_r+k])*sizeof(float));

    for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
      _g_layers_w[k][i] = new float[_lnunits[_r+k]];
      _prev_g_layers_w[k][i] = new float[_lnunits[_r+k]];
      _g_layers_gradient_w[k][i] = new float[_lnunits[_r+k]];

      // Reset g gradient components for corresponding weights
      memset(_g_layers_gradient_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));

      // Assign g weights a random number between -1.0 and +1.0
      for(int j=0; j<_lnunits[_r+k]; j++) {
	_g_layers_w[k][i][j] = nrnd01()/float(_lnunits[_r+k]);
	_prev_g_layers_w[k][i][j] = _g_layers_w[k][i][j];
	//cout << _g_layers_w[k][i][j] << " ";
      }
      //cout << endl;
    } 
  }
}

/* Private: H tranforming part allocation routine */

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::allocHpart() {
  //base_generator_type generator(rand());
  //boost::uniform_real<base_generator_type> distr(generator, -.05, .05);

  // Assume constructor has initialized required dimension quantities
  _h_layers_w = new floatrefref[_s];
  _prev_h_layers_w = new floatrefref[_s];
  _h_layers_gradient_w = new floatrefref[_s];
  _delta_h_layers = new floatref[_s];

  // Allocate weights and gradient components matrixes
  // for h transforming part. Connections from input layers
  // have special dimensions.
#ifdef DEBUG
  cout << endl << endl 
       << "(h) Layer 1: (" << (2*_m) + _n + 1 << "," << _lnunits[_r] << ")" 
       << endl;
#endif
  _h_layers_w[0] = new floatref[2*_m + _n + 1];
  _prev_h_layers_w[0] = new floatref[2*_m + _n + 1];
  _h_layers_gradient_w[0] = new floatref[2*_m + _n + 1];
  
  // Allocate and reset h first layer delta values.
  _delta_h_layers[0] = new float[_lnunits[_r]];
  memset(_delta_h_layers[0], 0, (_lnunits[_r])*sizeof(float));

  for(int i=0; i<2*_m + _n + 1; i++) {
    _h_layers_w[0][i] = new float[_lnunits[_r]];
    _prev_h_layers_w[0][i] = new float[_lnunits[_r]];
    _h_layers_gradient_w[0][i] = new float[_lnunits[_r]];

    // Reset gradient for corresponding weights
    memset(_h_layers_gradient_w[0][i], 0, _lnunits[_r] * sizeof(float));
	
    // Assign weights a random number between -1.0 and +1.0
    for(int j=0; j<_lnunits[_r]; j++) {
      _h_layers_w[0][i][j] = nrnd01()/float(_lnunits[_r]);
      _prev_h_layers_w[0][i][j] = _h_layers_w[0][i][j];
      //cout << _h_layers_w[0][i][j] << " ";
    }
    //cout << endl;
  }
  
  // Allocate weights&gradient matrixes for h map
  for(int k=1; k<_s; k++) {
#ifdef DEBUG
    cout << endl << endl
	 << "(h) Layer " << k+1 << ": (" << _lnunits[_r+k-1] + 1  << "," << _lnunits[_r+k] << ")" 
	 << endl;
#endif
    // Allocate space for weight&gradient matrixes between layer i-1 and i.
    // Automatically include space for threshold unit in layer i-1
    _h_layers_w[k] = new floatref[_lnunits[_r+k-1] + 1];
    _prev_h_layers_w[k] = new floatref[_lnunits[_r+k-1] + 1];
    _h_layers_gradient_w[k] = new floatref[_lnunits[_r+k-1] + 1];

    // Allocate and reset h layers delta layers values.
    _delta_h_layers[k] = new float[_lnunits[_r+k]];
    memset(_delta_h_layers[k], 0, (_lnunits[_r+k])*sizeof(float));

    for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
      _h_layers_w[k][i] = new float[_lnunits[_r+k]];
      _prev_h_layers_w[k][i] = new float[_lnunits[_r+k]];
      _h_layers_gradient_w[k][i] = new float[_lnunits[_r+k]];

      // Reset h gradient components for corresponding weights
      memset(_h_layers_gradient_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));

      // Assign h weights a random number between -1.0 and +1.0
      for(int j=0; j<_lnunits[_r+k]; j++) {
	_h_layers_w[k][i][j] = nrnd01()/float(_lnunits[_r+k]);
	_prev_h_layers_w[k][i][j] = _h_layers_w[k][i][j];
	//cout << _h_layers_w[k][i][j] << " ";
      }
      //cout << endl;
    }
  }
}



/* Private: F folding part deallocation routine */

template<class HA_Function, class OA_Function, class EMP> 
void RecursiveNN<HA_Function, OA_Function, EMP>::deallocFpart() {
#ifdef DEBUG
  cout << "Deallocating (f) weights&delta matrix at layer 1" << endl;
#endif
  if(_f_layers_w[0]) {
    for(int i=0; i<(_n+_v*_m) + 1; i++) {
      if(_f_layers_w[0][i]) {
	delete[] _f_layers_w[0][i];
	delete[] _prev_f_layers_w[0][i];
      }
      _f_layers_w[0][i] = 0; _prev_f_layers_w[0][i] = 0;
    }
    delete[] _f_layers_w[0]; delete[] _prev_f_layers_w[0];
    _f_layers_w[0] = 0; _prev_f_layers_w[0] = 0;
  }
  if(_f_layers_gradient_w[0]) {
    for(int i=0; i<(_n+_v*_m) + 1; i++) {
      if(_f_layers_gradient_w[0][i])
	delete[] _f_layers_gradient_w[0][i];
      _f_layers_gradient_w[0][i] = 0;
    }
    delete[] _f_layers_gradient_w[0];
    _f_layers_gradient_w[0] = 0;
  }

  if(_r > 1 && _delta_f_layers[0]) {
    delete[] _delta_f_layers[0];
    _delta_f_layers[0] = 0;
  }

  for(int k=1; k<_r; k++) {
#ifdef DEBUG
    cout << "Deallocating (f) weights&delta matrix at layer: " << k+1 << endl;
#endif
    if(_f_layers_w[k]) {
      for(int i=0; i<_lnunits[k-1]+1; i++) {
	if(_f_layers_w[k][i]) {
	  delete[] _f_layers_w[k][i];
	  delete[] _prev_f_layers_w[k][i];
	}
	_f_layers_w[k][i] = 0; _prev_f_layers_w[k][i] = 0;
      }
      delete[] _f_layers_w[k]; delete[] _prev_f_layers_w[k];
      _f_layers_w[k] = 0; _prev_f_layers_w[k] = 0;
    }

    if(_f_layers_gradient_w[k]) {
      for(int i=0; i<_lnunits[k-1]+1; i++) {
	if(_f_layers_gradient_w[k][i])
	  delete[] _f_layers_gradient_w[k][i];
	_f_layers_gradient_w[k][i] = 0;
      }
      delete[] _f_layers_gradient_w[k];
      _f_layers_gradient_w[k] = 0;
    }
   
    if(k < _r-1 && _delta_f_layers[k]) {
      delete[] _delta_f_layers[k];
      _delta_f_layers[k] = 0;
    }
  }
  
  delete[] _f_layers_w; delete[] _prev_f_layers_w;
  delete[] _f_layers_gradient_w;
  if(_r > 1 &&_delta_f_layers) {
    delete[] _delta_f_layers;
    _delta_f_layers = 0;
  }
  _f_layers_w = 0; _prev_f_layers_w = 0; _f_layers_gradient_w = 0;
}


/* Private: B folding part deallocation routine */

template<class HA_Function, class OA_Function, class EMP> 
void RecursiveNN<HA_Function, OA_Function, EMP>::deallocBpart() {
#ifdef DEBUG
  cout << "Deallocating (b) weights&delta matrix at layer 1" << endl;
#endif
  if(_b_layers_w[0]) {
    for(int i=0; i<(_n+_v*_m) + 1; i++) {
      if(_b_layers_w[0][i]) {
	delete[] _b_layers_w[0][i];
	delete[] _prev_b_layers_w[0][i];
      }
      _b_layers_w[0][i] = 0; _prev_b_layers_w[0][i] = 0;
    }
    delete[] _b_layers_w[0]; delete[] _prev_b_layers_w[0]; 
    _b_layers_w[0] = 0; _prev_b_layers_w[0] = 0;
  }
  if(_b_layers_gradient_w[0]) {
    for(int i=0; i<(_n+_v*_m) + 1; i++) {
      if(_b_layers_gradient_w[0][i])
	delete[] _b_layers_gradient_w[0][i];
      _b_layers_gradient_w[0][i] = 0;
    }
    delete[] _b_layers_gradient_w[0];
    _b_layers_gradient_w[0] = 0;
  }

  if(_r > 1 && _delta_b_layers[0]) {
    delete[] _delta_b_layers[0];
    _delta_b_layers[0] = 0;
  }

  for(int k=1; k<_r; k++) {
#ifdef DEBUG
    cout << "Deallocating (b) weights&delta matrix at layer: " << k+1 << endl;
#endif
    if(_b_layers_w[k]) {
      for(int i=0; i<_lnunits[k-1]+1; i++) {
	if(_b_layers_w[k][i]) {
	  delete[] _b_layers_w[k][i];
	  delete[] _prev_b_layers_w[k][i];
	}
	_b_layers_w[k][i] = 0; _prev_b_layers_w[k][i] = 0;
      }
      delete[] _b_layers_w[k]; delete[] _prev_b_layers_w[k];
      _b_layers_w[k] = 0; _prev_b_layers_w[k] = 0;
    }

    if(_b_layers_gradient_w[k]) {
      for(int i=0; i<_lnunits[k-1]+1; i++) {
	if(_b_layers_gradient_w[k][i])
	  delete[] _b_layers_gradient_w[k][i];
	_b_layers_gradient_w[k][i] = 0;
      }
      delete[] _b_layers_gradient_w[k];
      _b_layers_gradient_w[k] = 0;
    }
   
    if(k < _r-1 && _delta_b_layers[k]) {
      delete[] _delta_b_layers[k];
      _delta_b_layers[k] = 0;
    }
  }
  
  delete[] _b_layers_w; delete[] _prev_b_layers_w; 
  delete[] _b_layers_gradient_w;
  if(_r > 1 && _delta_b_layers) {
    delete[] _delta_b_layers;
    _delta_b_layers = 0;
  }
  _b_layers_w = 0; _prev_b_layers_w = 0; _b_layers_gradient_w = 0;
}


/* Private: G folding part deallocation routine */

template<class HA_Function, class OA_Function, class EMP> 
void RecursiveNN<HA_Function, OA_Function, EMP>::deallocGpart() {
#ifdef DEBUG
  cout << "Deallocating (g) weights&delta matrix at layer 1" << endl;
#endif
  if(_g_layers_w[0]) {
    for(int i=0; i<2*_m + 1; i++) {
      if(_g_layers_w[0][i]) {
	delete[] _g_layers_w[0][i];
	delete[] _prev_g_layers_w[0][i];
      }
      _g_layers_w[0][i] = 0; _prev_g_layers_w[0][i] = 0;
    }
    delete[] _g_layers_w[0]; delete[] _prev_g_layers_w[0];
    _g_layers_w[0] = 0; _prev_g_layers_w[0] = 0;
  }
  if(_g_layers_gradient_w[0]) {
    for(int i=0; i<2*_m + 1; i++) {
      if(_g_layers_gradient_w[0][i])
	delete[] _g_layers_gradient_w[0][i];
      _g_layers_gradient_w[0][i] = 0;
    }
    delete[] _g_layers_gradient_w[0];
    _g_layers_gradient_w[0] = 0;
  }

  if(_g_layers_activations[0]) {
    delete[] _g_layers_activations[0];
    _g_layers_activations[0] = 0;
  }

  if(_delta_g_layers[0]) {
    delete[] _delta_g_layers[0];
    _delta_g_layers[0] = 0;
  }

  for(int k=1; k<_s; k++) {
#ifdef DEBUG
    cout << "Deallocating (g) weights&delta matrix at layer: " << k+1 << endl;
#endif
    if(_g_layers_w[k]) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	if(_g_layers_w[k][i]) {
	  delete[] _g_layers_w[k][i];
	  delete[] _prev_g_layers_w[k][i];
	}
	_g_layers_w[k][i] = 0; _prev_g_layers_w[k][i] = 0;
      }
      delete[] _g_layers_w[k]; delete[] _prev_g_layers_w[k]; 
      _g_layers_w[k] = 0; _prev_g_layers_w[k] = 0; 
    }

    if(_g_layers_gradient_w[k]) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	if(_g_layers_gradient_w[k][i])
	  delete[] _g_layers_gradient_w[k][i];
	_g_layers_gradient_w[k][i] = 0;
      }
      delete[] _g_layers_gradient_w[k];
      _g_layers_gradient_w[k] = 0;
    }
    
    if(_g_layers_activations[k]) {
      delete[] _g_layers_activations[k];
      _g_layers_activations[k] = 0;
    }

    if(_delta_g_layers[k]) {
      delete[] _delta_g_layers[k];
      _delta_g_layers[k] = 0;
    }
  }
  
  delete[] _g_layers_w; delete[] _prev_g_layers_w;
  delete[] _g_layers_gradient_w;
  delete[] _g_layers_activations;
  delete[] _delta_g_layers;
  _g_layers_w = 0; _prev_g_layers_w = 0; _g_layers_gradient_w = 0;
  _g_layers_activations = 0; _delta_g_layers = 0;

}


/* Private: H output map deallocation routine */

template<class HA_Function, class OA_Function, class EMP> 
void RecursiveNN<HA_Function, OA_Function, EMP>::deallocHpart() {
#ifdef DEBUG
  cout << "Deallocating (h) weights&delta matrix at layer 1" << endl;
#endif
  if(_h_layers_w[0]) {
    for(int i=0; i<2*_m + _n + 1; i++) {
      if(_h_layers_w[0][i]) {
	delete[] _h_layers_w[0][i];
	delete[] _prev_h_layers_w[0][i];
      }
      _h_layers_w[0][i] = 0; _prev_h_layers_w[0][i] = 0;
    }
    delete[] _h_layers_w[0]; delete[] _prev_h_layers_w[0];
    _h_layers_w[0] = 0; _prev_h_layers_w[0] = 0;
  }
  if(_h_layers_gradient_w[0]) {
    for(int i=0; i<2*_m + _n + 1; i++) {
      if(_h_layers_gradient_w[0][i])
	delete[] _h_layers_gradient_w[0][i];
      _h_layers_gradient_w[0][i] = 0;
    }
    delete[] _h_layers_gradient_w[0];
    _h_layers_gradient_w[0] = 0;
  }

  if(_delta_h_layers[0]) {
    delete[] _delta_h_layers[0];
    _delta_h_layers[0] = 0;
  }

  for(int k=1; k<_s; k++) {
#ifdef DEBUG
    cout << "Deallocating (h) weights&delta matrix at layer: " << k+1 << endl;
#endif
    if(_h_layers_w[k]) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	if(_h_layers_w[k][i]) {
	  delete[] _h_layers_w[k][i];
	  delete[] _prev_h_layers_w[k][i];
	}
	_h_layers_w[k][i] = 0; _prev_h_layers_w[k][i] = 0;
      }
      delete[] _h_layers_w[k]; delete[] _prev_h_layers_w[k]; 
      _h_layers_w[k] = 0; _prev_h_layers_w[k] = 0; 
    }

    if(_h_layers_gradient_w[k]) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	if(_h_layers_gradient_w[k][i])
	  delete[] _h_layers_gradient_w[k][i];
	_h_layers_gradient_w[k][i] = 0;
      }
      delete[] _h_layers_gradient_w[k];
      _h_layers_gradient_w[k] = 0;
    }

    if(_delta_h_layers[k]) {
      delete[] _delta_h_layers[k];
      _delta_h_layers[k] = 0;
    }
  }
  
  delete[] _h_layers_w; delete[] _prev_h_layers_w;
  delete[] _h_layers_gradient_w;
  delete[] _delta_h_layers;
  _h_layers_w = 0; _prev_h_layers_w = 0; _h_layers_gradient_w = 0;
  _delta_h_layers = 0;
}


/*** Gradient components resetting methods ***/
template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::resetGradientComponents() {
  resetFGradient();

  if(_process_dr)
    resetBGradient();

  if(_ss_tr)
    resetGGradient();

  if(_ios_tr)
    resetHGradient();
}

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::resetFGradient() {
  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    // Reset gradient for corresponding weights
    memset(_f_layers_gradient_w[0][i], 0, _lnunits[0] * sizeof(float));
  }

  for(int k=1; k<_r; k++) {
    for(int i=0; i<_lnunits[k-1]+1; i++) {	
      // Reset b gradient components for corresponding weights
      memset(_f_layers_gradient_w[k][i], 0, (_lnunits[k])*sizeof(float));
    }
  }
}

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::resetBGradient() {
  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    // Reset gradient for corresponding weights
    memset(_b_layers_gradient_w[0][i], 0, _lnunits[0] * sizeof(float));
  }

  for(int k=1; k<_r; k++) {
    for(int i=0; i<_lnunits[k-1]+1; i++) {	
      // Reset b gradient components for corresponding weights
      memset(_b_layers_gradient_w[k][i], 0, (_lnunits[k])*sizeof(float));
    }
  }
}

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::resetGGradient() {
  for(int i=0; i<2*_m + 1; i++) {
    // Reset gradient for corresponding weights
    memset(_g_layers_gradient_w[0][i], 0, _lnunits[_r] * sizeof(float));
  }

  for(int k=1; k<_s; k++) {
    for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
      // Reset g gradient components for corresponding weights
      memset(_g_layers_gradient_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));
    }
  }  
}

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::resetHGradient() {
  for(int i=0; i<2*_m + _n + 1; i++) {
    // Reset gradient for corresponding weights
    memset(_h_layers_gradient_w[0][i], 0, _lnunits[_r] * sizeof(float));
  }

  for(int k=1; k<_s; k++) {
    for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
      // Reset h gradient components for corresponding weights
      memset(_h_layers_gradient_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));
    }
  }  
}


// Reset output values in g MLP layers
template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::resetGvalues() {
  for(int k=0; k<_s; k++) {
    memset(_g_layers_activations[k], 0, (_lnunits[_r+k])*sizeof(float));
  }
}


/****** Public member functions ******/

/*
  Constructor
    - n: dimension for an input to a node
    - v: valence of the domain (maximum outdegree)
    - r: number of layers for the folding part
    - s: number of layers for the tranforming part
    - n_lunits: array where each slot indicates 
    number of units in each layer
    lnunits[r-1] <=> m, number of units in representation layer
    lnunits[r+s-1] <=> q, number of units in g output layer
    
    Obs: the input layer is constituted by n+v*m units grouped
    into a label part and parts for substructures.
*/
template<class HA_Function, class OA_Function, class EMP>
RecursiveNN<HA_Function, OA_Function, EMP>::RecursiveNN():
  _n(Options::instance()->getInputDimension()), _v(Options::instance()->getDomainOutDegree()), _lnunits(Options::instance()->getLayersUnitsNumber()), _process_dr(Options::instance()->getProcessingFlag()) {
  srand(time(0)); // Initialize random number generator
  // Get other fundamental parameters from Options class
  std::pair<int, int> indexes = Options::instance()->getLayersIndexes();
  _r = indexes.first; _s = indexes.second;
  require(_lnunits.size() == _r+_s, "Dim.Error");
  
  // Number of output units of the 
  // transforming part (g MLP ouput function)
  _q = _lnunits.back();

  // _r is the number of layers for f and b folding parts,
  // number of output units (_m) is the same for both.
  _m = _lnunits[_r-1];
  
  // get types of trasductions to implement
  TrasdType trasd = Options::instance()->getTrasductionType();
  _ios_tr = (trasd & 1)?true:false; _ss_tr = (trasd & 2)?true:false;
  
  allocFpart();
  if(_ss_tr) allocGpart();
  if(_ios_tr) allocHpart();
  if(_process_dr) allocBpart();

  // Allocate weight update method structures using _process_dr
  // to decide whether or not to instantiate its b internal structures.
  _wu_method.setInternals(this);

}

/* Constructor */
template<class HA_Function, class OA_Function, class EMP>
  RecursiveNN<HA_Function, OA_Function, EMP>::RecursiveNN(const char* network_filename) {
  std::ifstream is(network_filename);
  assure(is, network_filename);
  
  // First get types of trasductions and folding processing to implement
  is >> _ios_tr >> _ss_tr >> _process_dr;
  // Read node input dimension and max outdegree with
  // the network was trained (or initialized)
  is >> _n >> _v;
  // Perform necessary synchronization control
  // with global parameters, to prevent using the net
  // with values different from parameters of the other
  // cooperating classes
  TrasdType trasd = Options::instance()->getTrasductionType();
  //require(_ios_tr == (Options::instance()->getTrasductionType() & 1)?true:false && _ss_tr == (Options::instance()->getTrasductionType() & 2)?true:false && _process_dr == Options::instance()->getProcessingFlag(), "Synchronization Error between RecursiveNN and global parameters: check trasduction type and processing flag");
  //require(_n == Options::instance()->getInputDimension(), "Synchronization Error between RecursiveNN and global parameters: check nodes input dimension");
  //require(_v == Options::instance()->getDomainOutDegree(), "Synchronization Error between RecursiveNN and global parameters: check outdegree");

  // Then read values of r and s from file
  is >> _r >> _s;
  // Some other necessary controls
  std::pair<int, int> indexes = Options::instance()->getLayersIndexes();
  //require(_r == indexes.first && _s == indexes.second, "Synchronization Error between RecursiveNN and global parameters: check layers indexes");

  // Then the vector of number of units per layer
  int i = 0, lnu;
  while(i<_r+_s) {
    is >> lnu;
    _lnunits.push_back(lnu);
    ++i;
  }
  //require(_lnunits == Options::instance()->getLayersUnitsNumber(), "Synchronization Error between RecursiveNN and global parameters: check layers number of units");

  // Number of output units of the 
  // transforming part (g MLP ouput function)
  _q = _lnunits.back();

  // _r is the number of layers for f and b folding parts,
  // number of output units (_m) is the same for both.
  _m = _lnunits[_r-1];

  /*** IMPORTANT UPDATE ***/
  int default_precision = is.precision();
  is.precision(Options::instance()->getPrecision());
  is.setf(std::ios::scientific);
  /************************/
  
  _f_layers_w = new floatrefref[_r];
  _prev_f_layers_w = new floatrefref[_r];
  _f_layers_gradient_w = new floatrefref[_r];
  if(_r > 1) {
    _delta_f_layers = new floatref[_r-1];
    _delta_f_layers[0] = new float[_lnunits[0]];
    memset(_delta_f_layers[0], 0, (_lnunits[0]) * sizeof(float));
  }

  // Allocate weights and gradient components matrixes
  // for f folding part. Connections from input layer
  // have special dimensions.
  _f_layers_w[0] = new floatref[(_n+_v*_m) + 1];
  _prev_f_layers_w[0] = new floatref[(_n+_v*_m) + 1];
  _f_layers_gradient_w[0] = new floatref[(_n+_v*_m) + 1];

  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    _f_layers_w[0][i] = new float[_lnunits[0]];
    _prev_f_layers_w[0][i] = new float[_lnunits[0]];
    _f_layers_gradient_w[0][i] = new float[_lnunits[0]];

    // Reset gradient for corresponding weights
    memset(_f_layers_gradient_w[0][i], 0, _lnunits[0] * sizeof(float));
	
    // Read weights from file
    for(int j=0; j<_lnunits[0]; j++) {
      is >> _f_layers_w[0][i][j]; 
      _prev_f_layers_w[0][i][j] = _f_layers_w[0][i][j]; 
      //cout << _f_layers_w[0][i][j] << " ";
    }
    //cout << endl;
  }
  
  // Allocate f weights&gradient matrixes for f folding part.
  for(int k=1; k<_r; k++) {
    //cout << endl << endl;
    // Allocate space for weight&delta matrix between layer i-1 and i.
    // Automatically include space for threshold unit in layer i-1
    _f_layers_w[k] = new floatref[_lnunits[k-1] + 1];
    _prev_f_layers_w[k] = new floatref[_lnunits[k-1] + 1];
    _f_layers_gradient_w[k] = new floatref[_lnunits[k-1] + 1];

    for(int i=0; i<_lnunits[k-1]+1; i++) {
      _f_layers_w[k][i] = new float[_lnunits[k]];
      _prev_f_layers_w[k][i] = new float[_lnunits[k]];
      _f_layers_gradient_w[k][i] = new float[_lnunits[k]];

      // Reset f gradient components for corresponding weights
      memset(_f_layers_gradient_w[k][i], 0, (_lnunits[k])*sizeof(float));

      // Read weights from file
      for(int j=0; j<_lnunits[k]; j++) {
	is >> _f_layers_w[k][i][j];
	_prev_f_layers_w[k][i][j] = _f_layers_w[k][i][j];
	//cout << _f_layers_w[k][i][j] << " ";
      }
      //cout << endl;
    }
    
    if(k < _r-1) {
      _delta_f_layers[k] = new float[_lnunits[k]];
      memset(_delta_f_layers[k], 0, (_lnunits[k]) * sizeof(float));
    }
  }
  

  // Eventually read weights for g output function layers
  if(_ss_tr) {
    //cout << endl << endl;
    _g_layers_w = new floatrefref[_s];
    _prev_g_layers_w = new floatrefref[_s];
    _g_layers_gradient_w = new floatrefref[_s];
    _g_layers_activations = new floatref[_s];
    _delta_g_layers = new floatref[_s];

    _g_layers_w[0] = new floatref[2*_m + 1];
    _prev_g_layers_w[0] = new floatref[2*_m + 1];
    _g_layers_gradient_w[0] = new floatref[2*_m + 1];
  
    // Allocate and reset g layers output activation units.
    _g_layers_activations[0] = new float[_lnunits[_r]];
    memset(_g_layers_activations[0], 0, (_lnunits[_r])*sizeof(float));
    _delta_g_layers[0] = new float[_lnunits[_r]];
    memset(_delta_g_layers[0], 0, (_lnunits[_r])*sizeof(float));

    for(int i=0; i<2*_m + 1; i++) {
      _g_layers_w[0][i] = new float[_lnunits[_r]];
      _prev_g_layers_w[0][i] = new float[_lnunits[_r]];
      _g_layers_gradient_w[0][i] = new float[_lnunits[_r]];

      // Reset gradient for corresponding weights
      memset(_g_layers_gradient_w[0][i], 0, _lnunits[_r] * sizeof(float));
	
      // Read weights from file
      for(int j=0; j<_lnunits[_r]; j++) {
	is >> _g_layers_w[0][i][j];
	_prev_g_layers_w[0][i][j] = _g_layers_w[0][i][j];
	//cout << _g_layers_w[0][i][j] << " ";
      }
      //cout << endl;
    }
  
    // Allocate weights&gradient matrixes for g transforming part
    for(int k=1; k<_s; k++) {
      //cout << endl << endl;
      // Allocate space for weight&gradient matrixes between layer i-1 and i.
      // Automatically include space for threshold unit in layer i-1
      _g_layers_w[k] = new floatref[_lnunits[_r+k-1] + 1];
      _prev_g_layers_w[k] = new floatref[_lnunits[_r+k-1] + 1];
      _g_layers_gradient_w[k] = new floatref[_lnunits[_r+k-1] + 1];

      // Allocate and reset g layers output activation units
      _g_layers_activations[k] = new float[_lnunits[_r+k]];
      memset(_g_layers_activations[k], 0, (_lnunits[_r+k])*sizeof(float));

      _delta_g_layers[k] = new float[_lnunits[_r+k]];
      memset(_delta_g_layers[k], 0, (_lnunits[_r+k])*sizeof(float));

      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	_g_layers_w[k][i] = new float[_lnunits[_r+k]];
	_prev_g_layers_w[k][i] = new float[_lnunits[_r+k]];
	_g_layers_gradient_w[k][i] = new float[_lnunits[_r+k]];

	// Reset g gradient components for corresponding weights
	memset(_g_layers_gradient_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));

	// Read weights from file
	for(int j=0; j<_lnunits[_r+k]; j++) {
	  is >> _g_layers_w[k][i][j];
	  _prev_g_layers_w[k][i][j] = _g_layers_w[k][i][j];
	  //cout << _g_layers_w[k][i][j] << " ";
	}
	//cout << endl;
      }
    
    }
  }

  // Eventually read weights for h map layers
  if(_ios_tr) {
    //cout << endl << endl;
    _h_layers_w = new floatrefref[_s];
    _prev_h_layers_w = new floatrefref[_s];
    _h_layers_gradient_w = new floatrefref[_s];
    _delta_h_layers = new floatref[_s];

    _h_layers_w[0] = new floatref[2*_m + _n + 1];
    _prev_h_layers_w[0] = new floatref[2*_m + _n + 1];
    _h_layers_gradient_w[0] = new floatref[2*_m + _n + 1];
  
    _delta_h_layers[0] = new float[_lnunits[_r]];
    memset(_delta_h_layers[0], 0, (_lnunits[_r])*sizeof(float));

    for(int i=0; i<2*_m + _n + 1; i++) {
      _h_layers_w[0][i] = new float[_lnunits[_r]];
      _prev_h_layers_w[0][i] = new float[_lnunits[_r]];
      _h_layers_gradient_w[0][i] = new float[_lnunits[_r]];

      // Reset gradient for corresponding weights
      memset(_h_layers_gradient_w[0][i], 0, _lnunits[_r] * sizeof(float));
	
      // Read weights from file
      for(int j=0; j<_lnunits[_r]; j++) {
	is >> _h_layers_w[0][i][j];
	_prev_h_layers_w[0][i][j] = _h_layers_w[0][i][j];
	//cout << _h_layers_w[0][i][j] << " ";
      }
      //cout << endl;
    }
  
    // Allocate weights&gradient matrixes for h map
    for(int k=1; k<_s; k++) {
      //cout << endl << endl;
      // Allocate space for weight&gradient matrixes between layer i-1 and i.
      // Automatically include space for threshold unit in layer i-1
      _h_layers_w[k] = new floatref[_lnunits[_r+k-1] + 1];
      _prev_h_layers_w[k] = new floatref[_lnunits[_r+k-1] + 1];
      _h_layers_gradient_w[k] = new floatref[_lnunits[_r+k-1] + 1];

      _delta_h_layers[k] = new float[_lnunits[_r+k]];
      memset(_delta_h_layers[k], 0, (_lnunits[_r+k])*sizeof(float));

      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	_h_layers_w[k][i] = new float[_lnunits[_r+k]];
	_prev_h_layers_w[k][i] = new float[_lnunits[_r+k]];
	_h_layers_gradient_w[k][i] = new float[_lnunits[_r+k]];

	// Reset h gradient components for corresponding weights
	memset(_h_layers_gradient_w[k][i], 0, (_lnunits[_r+k])*sizeof(float));

	// Read weights from file
	for(int j=0; j<_lnunits[_r+k]; j++) {
	  is >> _h_layers_w[k][i][j];
	  _prev_h_layers_w[k][i][j] = _h_layers_w[k][i][j];
	  //cout << _h_layers_w[k][i][j] << " ";
	}
	//cout << endl;
      }
    }
  }

  // Ok, in case read b layers weights
  if(_process_dr) {
    _b_layers_w = new floatrefref[_r];
    _prev_b_layers_w = new floatrefref[_r];
    _b_layers_gradient_w = new floatrefref[_r];
    if(_r > 1) {
      _delta_b_layers = new floatref[_r-1];
      _delta_b_layers[0] = new float[_lnunits[0]];
      memset(_delta_b_layers[0], 0, (_lnunits[0]) * sizeof(float));
    }

    // Allocate weights and gradient components matrixes
    // for b folding part. Connections from input layer
    // have special dimensions.
    //cout << endl << endl;
    _b_layers_w[0] = new floatref[(_n+_v*_m) + 1];
    _prev_b_layers_w[0] = new floatref[(_n+_v*_m) + 1];
    _b_layers_gradient_w[0] = new floatref[(_n+_v*_m) + 1];

    for(int i=0; i<(_n+_v*_m) + 1; i++) {
      _b_layers_w[0][i] = new float[_lnunits[0]];
      _prev_b_layers_w[0][i] = new float[_lnunits[0]];
      _b_layers_gradient_w[0][i] = new float[_lnunits[0]];

      // Reset gradient for corresponding weights
      memset(_b_layers_gradient_w[0][i], 0, _lnunits[0] * sizeof(float));
	
      // Read weights from file
      for(int j=0; j<_lnunits[0]; j++) {
	is >> _b_layers_w[0][i][j];
	_prev_b_layers_w[0][i][j] = _b_layers_w[0][i][j];
	//cout << _b_layers_w[0][i][j] << " ";
      }
      //cout << endl;
    }
  
    // Allocate weights&gradient matrixes for b folding part.
    for(int k=1; k<_r; k++) {
      //cout << endl << endl;
      // Allocate space for weight&delta matrix between layer i-1 and i.
      // Automatically include space for threshold unit in layer i-1
      _b_layers_w[k] = new floatref[_lnunits[k-1] + 1];
      _prev_b_layers_w[k] = new floatref[_lnunits[k-1] + 1];
      _b_layers_gradient_w[k] = new floatref[_lnunits[k-1] + 1];

      for(int i=0; i<_lnunits[k-1]+1; i++) {
	_b_layers_w[k][i] = new float[_lnunits[k]];
	_prev_b_layers_w[k][i] = new float[_lnunits[k]];
	_b_layers_gradient_w[k][i] = new float[_lnunits[k]];

	// Reset b gradient components for corresponding weights
	memset(_b_layers_gradient_w[k][i], 0, (_lnunits[k])*sizeof(float));

	// Read weights from file
	for(int j=0; j<_lnunits[k]; j++) {
	  is >> _b_layers_w[k][i][j];
	  _prev_b_layers_w[k][i][j] = _b_layers_w[k][i][j];
	  //cout << _b_layers_w[k][i][j] << " ";
	}
	//cout << endl;
      }
      
      if(k < _r-1) {
	_delta_b_layers[k] = new float[_lnunits[k]];
	memset(_delta_b_layers[k], 0, (_lnunits[k]) * sizeof(float));
      }
    }
  }

  // Allocate weight update method structures using _process_dr
  // to decide whether or not to instantiate its b internal structures.
  _wu_method.setInternals(this);
}


/* Destructor */
template<class HA_Function, class OA_Function, class EMP>
RecursiveNN<HA_Function, OA_Function, EMP>::~RecursiveNN() {
  deallocFpart();
  if(_ss_tr) deallocGpart();
  if(_ios_tr) deallocHpart();
  if(_process_dr) deallocBpart();
}


/*
  This function is to be used to evaluate a structure in
  a causal or causal & non-causal fashion, compute its encoded 
  representation at root node ouput layers and evaluate it
  with the ouptput (g) function.
*/
template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::propagateStructuredInput(std::vector<Node*>& toNodes, const std::pair<DPAG, DPAG>& sdags) {

  // Reset output activations in nodes layers
  // if _ios_tr is set h output activations are reset
  for(std::vector<Node*>::iterator it=toNodes.begin(); it!=toNodes.end(); ++it) 
    (*it)->resetValues();
  
  // Reset output activations in output (g) function MLP layers
  if(_ss_tr) resetGvalues();

  // Structure propagation by unfolding into casual part
  fPropagateInput(toNodes, sdags.first);

  // Eventually unfold into non-casual part
  if(_process_dr) bPropagateInput(toNodes, sdags.second);

  // Evaluate current encoded structure (if supersource trasd.)
  if(_ss_tr) gPropagateInput(toNodes);

  // Compute output for each node (if io-isomorf trasd.)
  if(_ios_tr)
    for(std::vector<Node*>::iterator it=toNodes.begin(); it!=toNodes.end(); ++it)
      hPropagateInput(*it);
  
}


/*** Private Functions ***/

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::fPropagateInput(std::vector<Node*>& toNodes, const DPAG& sdag) {
  // A structured input is a sequence of nodes and a DAG with that vertices.
  // Nodes are passed by (non-const) reference to allow the net
  // storing output activations on each node for all of the
  // folding layers. 
  
  // nodes in vector are in topological order, so iterate from
  // last to first node to obtain reverse topological order.
  Vertex_d currentNode;
  VertexId vertex_id = boost::get(boost::vertex_index, sdag);
  cEdgeId edge_id = boost::get(boost::edge_index, sdag);
  outIter out_i, out_end;

  for(int t=toNodes.size()-1; t>=0; t--) {
    //cout << endl << "Node: " << t << endl;
    require(_n == toNodes[t]->_encodedInput.size(), "Error in input dim!");
    currentNode = boost::vertex(t, sdag); // Probably t must have conversion to Vst.

    // Remember: if k==1 (0 according to the indexing scheme) 
    // net input for each unit comes both from current node 
    // immediate successors and from the node input label.

    // We are in layer k == 1.
    // for each unit in layer 1 (0)
    for(int j=0; j<_lnunits[0]; j++) {
      // calculate weighted sum of its inputs
      float unit_input = 0.0;
      // firstly take into account current node input label
      for(int i=0; i<_n; i++) {
	unit_input += 
	  _f_layers_w[0][i][j] * toNodes[t]->_encodedInput[i];
      }

      // add to weighted sum contribution of the previously
      // computed representations of the immediate substructures.
      /*** IMPORTANT UPDATE: 23/07/2001 ***/
      // Eliminate control on max outdegree.
      // Ignore edges whose id is greater than max outdegree.
#ifndef RECURSIVE_FALSE
      for(boost::tie(out_i, out_end)=out_edges(currentNode, sdag); 
	  out_i!=out_end && edge_id[*out_i] < _v; ++out_i) {
	//require(0<=edge_id[*out_i] && edge_id[*out_i] < _v, "Valence assertion failed!");

	for(int i=_n + edge_id[*out_i]*_m; i<_n + _m*(edge_id[*out_i] + 1); i++) {
	  unit_input +=
	    _f_layers_w[0][i][j] * 
	    toNodes[target(*out_i, sdag)]->_f_layers_activations[_r-1][(i-_n)%_m];
	}
      }
#endif
      // if there are less children than the valence, missing children encoding 
      // (base step of recursion, 0) does not influence current unit input.
      // So do not add 0 to the sum.
      
      // Add threshold unit contribution (input == 1).
      // We assume threshold unit weight is last component
      // of the weight matrix.
      unit_input += _f_layers_w[0][_n+_v*_m][j];

      // calculate unit output activation
      toNodes[t]->_f_layers_activations[0][j] =
	evaluate(haf, unit_input);
    }

    for(int k=1; k<_r; k++) {
      // for each unit in layer k
      for(int j=0; j<_lnunits[k]; j++) {
	// calculate weighted sum of its input
	float unit_input = 0.0;
	for(int i=0; i<_lnunits[k-1]; i++) {
	  unit_input += 
	    _f_layers_w[k][i][j] * toNodes[t]->_f_layers_activations[k-1][i];
	}
	// Add threshold unit contribution (input == 1),
	// last component of the weight matrix.
	unit_input += _f_layers_w[k][_lnunits[k-1]][j];

	// calculate unit output activation
	toNodes[t]->_f_layers_activations[k][j] = 
	  evaluate(haf, unit_input);
      }
    }
  }
}


template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::bPropagateInput(std::vector<Node*>& toNodes, const DPAG& sdag) {

  Vertex_d currentNode;
  VertexId vertex_id = boost::get(boost::vertex_index, sdag);
  //  EdgeId edge_id = boost::get(edge_ordered_tuple_index_t(), sdag);
  cEdgeId edge_id = boost::get(boost::edge_index, sdag);
  outIter out_i, out_end;

  for(int t=0; t<toNodes.size(); t++) {
    require(_n == toNodes[t]->_encodedInput.size(), "Error!");
    currentNode = boost::vertex(t, sdag);

    // We are in layer k == 1.
    // for each unit in layer 1 (0)
    for(int j=0; j<_lnunits[0]; j++) {
      // calculate weighted sum of its inputs
      float unit_input = 0.0;
      // firstly take into account current node input label
      for(int i=0; i<_n; i++) {
	unit_input += 
	  _b_layers_w[0][i][j] * toNodes[t]->_encodedInput[i];
      }

      // add to weighted sum contribution of the previously
      // computed representations of the immediate substructures.
      /*** IMPORTANT UPDATE: 23/07/2001 ***/
      // Eliminate control on max outdegree.
      // Ignore edges whose id is greater than max outdegree.
#ifndef RECURSIVE_FALSE
      for(boost::tie(out_i, out_end)=out_edges(currentNode, sdag); 
	  out_i!=out_end && edge_id[*out_i] < _v; ++out_i) {
	//require(0<=edge_id[*out_i] && edge_id[*out_i] < _v, "Valence assertion failed!");

	for(int i=_n + edge_id[*out_i]*_m; i<_n + _m*(edge_id[*out_i] + 1); i++) {
	  unit_input +=
	    _b_layers_w[0][i][j] * 
	    toNodes[target(*out_i, sdag)]->_b_layers_activations[_r-1][(i-_n)%_m];
	}
      }
#endif
      // Add threshold unit contribution (input == 1).
      // We assume threshold unit weight is last component
      // of the weight matrix.
      unit_input += _b_layers_w[0][_n+_v*_m][j];

      // calculate unit output activation
      toNodes[t]->_b_layers_activations[0][j] =
	evaluate(haf, unit_input);
    }

    for(int k=1; k<_r; k++) {
      // for each unit in layer k
      for(int j=0; j<_lnunits[k]; j++) {
	// calculate weighted sum of its input
	float unit_input = 0.0;
	for(int i=0; i<_lnunits[k-1]; i++) {
	  unit_input += 
	    _b_layers_w[k][i][j] * toNodes[t]->_b_layers_activations[k-1][i];
	}
	// Add threshold unit contribution (input == 1),
	// last component of the weight matrix.
	unit_input += _b_layers_w[k][_lnunits[k-1]][j];

	// calculate unit output activation
	toNodes[t]->_b_layers_activations[k][j] = 
	  evaluate(haf, unit_input);
      }
    }
  }
}


template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::gPropagateInput(std::vector<Node*>& toNodes) {
  //cout<<endl;

  for(int j=0; j<_lnunits[_r]; j++) {
    // calculate weighted sum of its inputs
    float unit_input = 0.0;
    
    // add to weighted sum contribution of the previously
    // computed representations of the immediate substructures.
    int i;
    for(i=0; i<_m; i++) {
      unit_input +=
	_g_layers_w[0][i][j] * toNodes[0]->_f_layers_activations[_r-1][i];
    }

    if(_process_dr) {
      for(i=_m; i<2*_m; i++) {
	unit_input +=
	  _g_layers_w[0][i][j] * toNodes[toNodes.size()-1]->_b_layers_activations[_r-1][i-_m];
      }
    }
   
    // Add threshold unit contribution (input == 1).
    // We assume threshold unit weight is last component
    // of the weight matrix.
    unit_input += _g_layers_w[0][2*_m][j];

    // calculate units output activation
    if(0 < _s-1) {
      _g_layers_activations[0][j] = evaluate(haf, unit_input);
    } else {
      _g_layers_activations[0][j] = evaluate(oaf, unit_input);
    }
  }

  for(int k=1; k<_s; k++) {
    // for each unit in layer k
    for(int j=0; j<_lnunits[_r+k]; j++) {
      // calculate weighted sum of its input
      float unit_input = 0.0;
      for(int i=0; i<_lnunits[_r+k-1]; i++) {
	unit_input += 
	  _g_layers_w[k][i][j] * _g_layers_activations[k-1][i];
      }

      // Add threshold unit contribution (input == 1),
      // last component of the weight matrix.
      unit_input += _g_layers_w[k][_lnunits[_r+k-1]][j];

      // calculate unit output activation,
      // take into account being in hidden or output units.
      if(k < _s-1) {
	_g_layers_activations[k][j] = evaluate(haf, unit_input);
      } else {
	_g_layers_activations[k][j] = evaluate(oaf, unit_input);
      }
    }
  }
}


template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::hPropagateInput(Node* n) {
  for(int j=0; j<_lnunits[_r]; j++) {
    // calculate weighted sum of its inputs
    float unit_input = 0.0;
    
    // add to weighted sum contribution of the previously
    // computed representations of the immediate substructures.
    int i;
    for(i=0; i<_m; i++) {
      unit_input +=
	_h_layers_w[0][i][j] * n->_f_layers_activations[_r-1][i];
    }

    if(_process_dr) {
      for(i=_m; i<2*_m; i++) {
	unit_input +=
	  _h_layers_w[0][i][j] * n->_b_layers_activations[_r-1][i-_m];
      }
    }
   
    // Output label depend also on current node encoded input
    for(i=2*_m; i<2*_m + _n; i++) {
      unit_input +=
	_h_layers_w[0][i][j] * n->_encodedInput[i-2*_m];
    }

    // Add threshold unit contribution (input == 1).
    // We assume threshold unit weight is last component
    // of the weight matrix.
    unit_input += _h_layers_w[0][2*_m+_n][j];

    // calculate units output activation
    if(0 < _s-1) {
      n->_h_layers_activations[0][j] = evaluate(haf, unit_input);
    } else {
      n->_h_layers_activations[0][j] = evaluate(oaf, unit_input);
    }
  }

  for(int k=1; k<_s; k++) {
    // for each unit in layer k
    for(int j=0; j<_lnunits[_r+k]; j++) {
      // calculate weighted sum of its input
      float unit_input = 0.0;
      for(int i=0; i<_lnunits[_r+k-1]; i++) {
	unit_input += 
	  _h_layers_w[k][i][j] * n->_h_layers_activations[k-1][i];
      }

      // Add threshold unit contribution (input == 1),
      // last component of the weight matrix.
      unit_input += _h_layers_w[k][_lnunits[_r+k-1]][j];

      // calculate unit output activation,
      // take into account being in hidden or output units.
      if(k < _s-1) {
	n->_h_layers_activations[k][j] = evaluate(haf, unit_input);
      } else {
	n->_h_layers_activations[k][j] = evaluate(oaf, unit_input);
      }
    }
  }
}


/*** Public Functions ***/

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::backPropagateError(std::vector<Node*>& toNodes, const std::pair<DPAG, DPAG>& sdags, const std::vector<float>& targets, bool apply_softmax) {

  // if io-isomorf trasduction, compute for each node error of h map,
  // so as to add it to deltas error in representation layers coming
  // from node parents (with respect to f and b ordering)
  if(_ios_tr) {
    for(std::vector<Node*>::iterator it=toNodes.begin(); it!=toNodes.end(); ++it)
      if(apply_softmax) {
	std::vector<float> outputs((*it)->_h_layers_activations[_s-1], 
				   (*it)->_h_layers_activations[_s-1]+_lnunits[_r+_s-1]);
	
	float max = -FLT_MAX;
	for(int i=0; i<outputs.size(); ++i)
	  if(max < outputs[i])
	    max = outputs[i];
      
	float norm_factor = 0.0;
	for(int j=0; j<outputs.size(); ++j) {
	  outputs[j] = exp(outputs[j] - max);
	  norm_factor += outputs[j];
	}      	
	for(int j=0; j<outputs.size(); ++j)
	  outputs[j] /= norm_factor;

	hBackPropagateError(*it, outputs);
      } else
	hBackPropagateError(*it);
  }

  if(_ss_tr) {
    if(apply_softmax) {
      std::vector<float> outputs(_g_layers_activations[_s-1], 
				  _g_layers_activations[_s-1]+_lnunits[_r+_s-1]);

      float max = -FLT_MAX;
      for(int i=0; i<outputs.size(); ++i)
	if(max < outputs[i]) max = outputs[i];
      
      float norm_factor = 0.0;
      for(int j=0; j<outputs.size(); ++j) {
	outputs[j] = exp(outputs[j] - max);
	norm_factor += outputs[j];
      }      	
      for(int j=0; j<outputs.size(); ++j) outputs[j] /= norm_factor;

      gBackPropagateError(toNodes, targets, outputs);
    } else
      gBackPropagateError(toNodes, targets);
  }

  fBackPropagateError(toNodes, sdags.first);

  if(_process_dr)
    bBackPropagateError(toNodes, sdags.second);
}


/*** Private Functions ***/

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::fBackPropagateError(std::vector<Node*>& toNodes, const DPAG& sdag, 
								     const std::vector<std::vector<Node*> >& nodeMap) {

  // nodes in vector are in topological order, 
  // so iterate from first to last node.
  Vertex_d currentNode;
  VertexId vertex_id = boost::get(boost::vertex_index, sdag);
  //  EdgeId edge_id = boost::get(edge_ordered_tuple_index_t(), sdag);
  cEdgeId edge_id = boost::get(boost::edge_index, sdag);
  outIter out_i, out_end;

  for(int t=0; t<toNodes.size(); t++) {
    currentNode = boost::vertex(t, sdag);
    
    if(_r > 1) {
      for(int k=_r-2; k>=0; k--) {
	memset(_delta_f_layers[k], 0, (_lnunits[k])*sizeof(float));

	for(int i=0; i<_lnunits[k]; i++) {
	  float sum = 0.0;
	  if(k ==_r-2) {
	    for(int j=0; j<_lnunits[k+1]; j++) {
	      sum += _f_layers_w[k+1][i][j] * toNodes[t]->_f_delta_lr[j];
	    }
	  } else {
	    for(int j=0; j<_lnunits[k+1]; j++) {
	      sum += _f_layers_w[k+1][i][j] * _delta_f_layers[k+1][j];
	    }
	  }
	  _delta_f_layers[k][i] = 
	    derivate(haf, toNodes[t]->_f_layers_activations[k][i]) * sum;
	}

	if(k>0) {
	  for(int j=0; j<_lnunits[k]; j++) {
	    for(int i=0; i<_lnunits[k-1]; i++) {
	      _f_layers_gradient_w[k][i][j] -= 
		_delta_f_layers[k][j] * toNodes[t]->_f_layers_activations[k-1][i];
	    }
	    _f_layers_gradient_w[k][_lnunits[k-1]][j] -= _delta_f_layers[k][j];
	  }
	} else {
	  for(int j=0; j<_lnunits[0]; j++) {
	    // Get output from current node children and input label
	    // First consider input label
	    for(int i=0; i<_n; i++) {
	      _f_layers_gradient_w[0][i][j] -= 
		_delta_f_layers[k][j] * toNodes[t]->_encodedInput[i];
	    }
	    // Then consider ordered children
	    /*** IMPORTANT UPDATE: 23/07/2001 ***/
	    // Eliminate control on max outdegree.
	    // Ignore edges whose id is greater than max outdegree.
#ifndef RECURSIVE_FALSE
	    for(boost::tie(out_i, out_end)=out_edges(currentNode, sdag); 
		out_i!=out_end && edge_id[*out_i] < _v; ++out_i) {
	      //require(0<=edge_id[*out_i] && edge_id[*out_i] < _v, "Valence assertion failed!");
	    
	      for(int i=_n + edge_id[*out_i]*_m; i<_n + _m*(edge_id[*out_i] + 1); i++) {
		_f_layers_gradient_w[0][i][j] -=
		  _delta_f_layers[k][j] * toNodes[target(*out_i, sdag)]->_f_layers_activations[_r-1][(i-_n)%_m];
	      }
	    }
#endif
	  }
	}
      }
    } else { // deltas of current layer are already calculated
      for(int j=0; j<_lnunits[0]; j++) {
	// Get output from current node children and input label
	// First consider input label
	for(int i=0; i<_n; i++) {
	  _f_layers_gradient_w[0][i][j] -= 
	    toNodes[t]->_f_delta_lr[j] * toNodes[t]->_encodedInput[i];
	}
	// Then consider ordered children
	/*** IMPORTANT UPDATE: 23/07/2001 ***/
	// Eliminate control on max outdegree.
	// Ignore edges whose id is greater than max outdegree.
#ifndef RECURSIVE_FALSE
	for(boost::tie(out_i, out_end)=out_edges(currentNode, sdag); 
	    out_i!=out_end && edge_id[*out_i] < _v; ++out_i) {
	  //require(0<=edge_id[*out_i] && edge_id[*out_i] < _v, "Valence assertion failed!");
	    
	  for(int i=_n + edge_id[*out_i]*_m; i<_n + _m*(edge_id[*out_i] + 1); i++) {
	    _f_layers_gradient_w[0][i][j] -=
	      toNodes[t]->_f_delta_lr[j] * toNodes[target(*out_i, sdag)]->_f_layers_activations[_r-1][(i-_n)%_m];
	  }
	}
#endif
      }
    }

    /*** BRNN + BiRNN ***/
    // Distribute error on output layers of an eventually attached BRNN
    if(nodeMap.size()) {
      for(int i=0; i<_n; ++i) {
	if(nodeMap[t][i] == NULL) continue;
	
	float sum = .0;
	if(_r == 1) 
	  for(int j=0; j<_lnunits[0]; ++j)
	    sum += _f_layers_w[0][i][j] * toNodes[t]->_f_delta_lr[j];
	else
	  for(int j=0; j<_lnunits[0]; ++j)
	    sum += _f_layers_w[0][i][j] * _delta_f_layers[0][j];
      
	nodeMap[t][i]->_h_delta[0] +=
	  derivate(oaf, nodeMap[t][i]->_h_layers_activations[nodeMap[t][i]->_s-1][0]) * sum;
      }
    }

    // Distribute delta error among representation layers
    // of immediate successors of current node t
    // Ignore edges whose id is greater than max outdegree.

#ifndef RECURSIVE_FALSE
    for(boost::tie(out_i, out_end)=out_edges(currentNode, sdag); 
	out_i!=out_end && edge_id[*out_i] < _v; ++out_i) {
	    
      for(int i=_n + edge_id[*out_i]*_m; i<_n + _m*(edge_id[*out_i] + 1); i++) {
	float sum = 0.0;
	if(_r == 1) {
	  for(int j=0; j<_lnunits[0]; j++) {
	    sum += 
	      _f_layers_w[0][i][j] * toNodes[t]->_f_delta_lr[j];
	  }
	} else {
	  for(int j=0; j<_lnunits[0]; j++) {
	    sum += 
	      _f_layers_w[0][i][j] * _delta_f_layers[0][j];
	  }
	}

	// !!!
	// delta values for the representation layer of a node t
	// coming from different immediate predecessors have to be
	// summed up, before they are propagated deeper into the 
	// folding part of t and of its successors.
	toNodes[target(*out_i, sdag)]->_f_delta_lr[(i-_n)%_m] +=
	  derivate(haf, toNodes[target(*out_i, sdag)]->_f_layers_activations[_r-1][(i-_n)%_m]) * sum;
      }
    }
#endif
  }
}

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::bBackPropagateError(std::vector<Node*>& toNodes, const DPAG& sdag,
								     const std::vector<std::vector<Node*> >& nodeMap) {
  
  // nodes in vector are in reverse topological order
  // (with respect to sdag), so iterate from last to first node
  Vertex_d currentNode;
  VertexId vertex_id = boost::get(boost::vertex_index, sdag);
  //EdgeId edge_id = boost::get(edge_ordered_tuple_index_t(), sdag);
  cEdgeId edge_id = boost::get(boost::edge_index, sdag);
  outIter out_i, out_end;

  for(int t=toNodes.size()-1; t>=0; t--) {
    currentNode = boost::vertex(t, sdag);    
    if(_r > 1) {
      for(int k=_r-2; k>=0; k--) {
	memset(_delta_b_layers[k], 0, (_lnunits[k])*sizeof(float));

	for(int i=0; i<_lnunits[k]; i++) {
	  float sum = 0.0;
	  if(k ==_r-2) {
	    for(int j=0; j<_lnunits[k+1]; j++) {
	      sum += _b_layers_w[k+1][i][j] * toNodes[t]->_b_delta_lr[j];
	    }
	  } else {
	    for(int j=0; j<_lnunits[k+1]; j++) {
	      sum += _b_layers_w[k+1][i][j] * _delta_b_layers[k+1][j];
	    }
	  }
	  _delta_b_layers[k][i] = 
	    derivate(haf, toNodes[t]->_b_layers_activations[k][i]) * sum;
	}

	if(k>0) {
	  for(int j=0; j<_lnunits[k]; j++) {
	    for(int i=0; i<_lnunits[k-1]; i++) {
	      _b_layers_gradient_w[k][i][j] -= 
		_delta_b_layers[k][j] * toNodes[t]->_b_layers_activations[k-1][i];
	    }
	    _b_layers_gradient_w[k][_lnunits[k-1]][j] -= _delta_b_layers[k][j];
	  }
	} else {
	  for(int j=0; j<_lnunits[0]; j++) {
	    // Get output from current node children and input label
	    // First consider input label
	    for(int i=0; i<_n; i++) {
	      _b_layers_gradient_w[0][i][j] -= 
		_delta_b_layers[k][j] * toNodes[t]->_encodedInput[i];
	    }
	    // Then consider ordered children
	    /*** IMPORTANT UPDATE: 23/07/2001 ***/
	    // Eliminate control on max outdegree.
	    // Ignore edges whose id is greater than max outdegree.
#ifndef RECURSIVE_FALSE
	    for(boost::tie(out_i, out_end)=out_edges(currentNode, sdag); 
		out_i!=out_end && edge_id[*out_i] < _v; ++out_i) {
	      //require(0<=edge_id[*out_i] && edge_id[*out_i] < _v, "Valence assertion failed!");
	    
	      for(int i=_n + edge_id[*out_i]*_m; i<_n + _m*(edge_id[*out_i] + 1); i++) {
		_b_layers_gradient_w[0][i][j] -=
		  _delta_b_layers[k][j] * toNodes[target(*out_i, sdag)]->_b_layers_activations[_r-1][(i-_n)%_m];
	      }
	    }
#endif
	  }
	}
      }
    } else { // deltas of current layer are already calculated
      for(int j=0; j<_lnunits[0]; j++) {
	// Get output from current node children and input label
	// First consider input label
	for(int i=0; i<_n; i++) {
	  _b_layers_gradient_w[0][i][j] -= 
	    toNodes[t]->_b_delta_lr[j] * toNodes[t]->_encodedInput[i];
	}
	// Then consider ordered children
	/*** IMPORTANT UPDATE: 23/07/2001 ***/
	// Eliminate control on max outdegree.
	// Ignore edges whose id is greater than max outdegree.
#ifndef RECURSIVE_FALSE
	for(boost::tie(out_i, out_end)=out_edges(currentNode, sdag); 
	    out_i!=out_end && edge_id[*out_i] < _v; ++out_i) {
	  //require(0<=edge_id[*out_i] && edge_id[*out_i] < _v, "Valence assertion failed!");
	    
	  for(int i=_n + edge_id[*out_i]*_m; i<_n + _m*(edge_id[*out_i] + 1); i++) {
	    _b_layers_gradient_w[0][i][j] -=
	      toNodes[t]->_b_delta_lr[j] * toNodes[target(*out_i, sdag)]->_b_layers_activations[_r-1][(i-_n)%_m];
	  }
	}
#endif
      }
    }

    /*** BRNN + BiRNN ***/
    // Distribute error on output layers of an eventually attached BRNN
    if(nodeMap.size()) {
      for(int i=0; i<_n; ++i) {
	if(nodeMap[t][i] == NULL) continue;
	
	float sum = .0;
	if(_r == 1) 
	  for(int j=0; j<_lnunits[0]; ++j)
	    sum += _b_layers_w[0][i][j] * toNodes[t]->_b_delta_lr[j];
	else
	  for(int j=0; j<_lnunits[0]; ++j)
	    sum += _b_layers_w[0][i][j] * _delta_b_layers[0][j];
      
	nodeMap[t][i]->_h_delta[0] +=
	  derivate(oaf, nodeMap[t][i]->_h_layers_activations[nodeMap[t][i]->_s-1][0]) * sum;
      }
    }

    // Distribute delta errors among representation layers
    // of immediate successors of current node t
    // Ignore edges whose id is greater than max outdegree.

#ifndef RECURSIVE_FALSE
    for(boost::tie(out_i, out_end)=out_edges(currentNode, sdag); 
	out_i!=out_end && edge_id[*out_i] < _v; ++out_i) {
      for(int i=_n + edge_id[*out_i]*_m; i<_n + _m*(edge_id[*out_i] + 1); i++) {
	float sum = 0.0;
	if(_r == 1) {
	  for(int j=0; j<_lnunits[0]; j++) {
	    sum += 
	      _b_layers_w[0][i][j] * toNodes[t]->_b_delta_lr[j];
	  }
	} else {
	  for(int j=0; j<_lnunits[0]; j++) {
	    sum += 
	      _b_layers_w[0][i][j] * _delta_b_layers[0][j];
	  }
	}

	// !!!
	// delta values for the representation layer of a node t
	// coming from different immediate predecessors have to be
	// summed up, before they are propagated deeper into the 
	// folding part of t and of its successors.
	toNodes[target(*out_i, sdag)]->_b_delta_lr[(i-_n)%_m] +=
	  derivate(haf, toNodes[target(*out_i, sdag)]->_b_layers_activations[_r-1][(i-_n)%_m]) * sum;
      }
    }
#endif
  } 
}

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::gBackPropagateError(std::vector<Node*>& toNodes, const std::vector<float>& targets, 
								     const std::vector<float>& outputs) {

  int k=_s-1;
  memset(_delta_g_layers[k], 0, (_lnunits[_r+k])*sizeof(float));
  
  for(int j=0; j<_lnunits[_r+k]; j++) {
    if(outputs.size() > 0)
      _delta_g_layers[k][j] = targets[j] - outputs[j];
    else
      _delta_g_layers[k][j] =
	derivate(oaf, _g_layers_activations[k][j]) *  
	(targets[j] - _g_layers_activations[k][j]);
        
    if(k>0) {
      for(int i=0; i<_lnunits[_r+k-1]; i++) {
	_g_layers_gradient_w[k][i][j] -= 
	  _delta_g_layers[k][j] * _g_layers_activations[k-1][i];
      }
      _g_layers_gradient_w[k][_lnunits[_r+k-1]][j] -= 
	_delta_g_layers[k][j];

    } else {
      for(int i=0; i<_m; i++) {
	_g_layers_gradient_w[0][i][j] -=
	  _delta_g_layers[k][j] * toNodes[0]->_f_layers_activations[_r-1][i];
      }
      
      if(_process_dr) {
	for(int i=_m; i<2*_m; i++) {
	  _g_layers_gradient_w[0][i][j] -=
	    _delta_g_layers[k][j] * toNodes[toNodes.size()-1]->_b_layers_activations[_r-1][i-_m];
	}
      }
      _g_layers_gradient_w[0][2*_m][j] -= _delta_g_layers[k][j];
    }
  }

  for(--k; k>=0; k--) {
    memset(_delta_g_layers[k], 0, (_lnunits[_r+k])*sizeof(float));

    for(int i=0; i<_lnunits[_r+k]; i++) {
      float sum = 0.0;
      for(int j=0; j<_lnunits[_r+k+1]; j++) {
	sum += _g_layers_w[k+1][i][j] * _delta_g_layers[k+1][j];
      }
      _delta_g_layers[k][i] = 
	derivate(haf, _g_layers_activations[k][i]) * sum;
    }

    if(k>0) {
      for(int j=0; j<_lnunits[_r+k]; j++) {
	for(int i=0; i<_lnunits[_r+k-1]; i++) {
	  _g_layers_gradient_w[k][i][j] -= 
	    _delta_g_layers[k][j] * _g_layers_activations[k-1][i];
	}
	_g_layers_gradient_w[k][_lnunits[_r+k-1]][j] -=  _delta_g_layers[k][j];
      }
    } else {
      for(int j=0; j<_lnunits[_r+k]; j++) {
	for(int i=0; i<_m; i++) {
	  _g_layers_gradient_w[0][i][j] -=
	    _delta_g_layers[k][j] * toNodes[0]->_f_layers_activations[_r-1][i];
	}

	if(_process_dr) {
	  for(int i=_m; i<2*_m; i++) {
	    _g_layers_gradient_w[0][i][j] -=
	      _delta_g_layers[k][j] * toNodes[toNodes.size()-1]->_b_layers_activations[_r-1][i-_m];
	  }
	}
	_g_layers_gradient_w[0][2*_m][j] -= _delta_g_layers[k][j];
      }
    }
  }

  // calculate the error on input layer and 
  // redistribute it on immediate successors.
  for(int i=0; i<_m; i++) {
    float sum = 0.0;
    for(int j=0; j<_lnunits[_r]; j++) {
      sum += _g_layers_w[0][i][j] * _delta_g_layers[0][j];
    }
    toNodes[0]->_f_delta_lr[i] += 
      derivate(haf, toNodes[0]->_f_layers_activations[_r-1][i]) * sum;
  }

  if(_process_dr) {
    for(int i=0; i<_m; i++) {
      float sum = 0.0;
      for(int j=0; j<_lnunits[_r]; j++) {
	sum += _g_layers_w[0][_m+i][j] * _delta_g_layers[0][j];
      }
      toNodes[toNodes.size()-1]->_b_delta_lr[i] += 
	derivate(haf, toNodes[toNodes.size()-1]->_b_layers_activations[_r-1][i]) * sum;
    }
  }

}

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::hBackPropagateError(Node* n, const std::vector<float>& outputs, bool brnn_birnn) {

  int k=_s-1;
  memset(_delta_h_layers[k], 0, (_lnunits[_r+k])*sizeof(float));

  require(_lnunits[_r+k]==n->_otargets.size(), "node dim.error");
  for(int j=0; j<_lnunits[_r+k]; ++j) {
    if(brnn_birnn) _delta_h_layers[k][j] = n->_h_delta[j]; /*** BRNN+BiRNN ***/
    else if(outputs.size() > 0) // softmax
      _delta_h_layers[k][j] = n->_otargets[j] - outputs[j];
    else // normal outputs
      _delta_h_layers[k][j] =
	//derivate(oaf, n->_h_layers_activations[k][j]) *  
	(n->_otargets[j] - n->_h_layers_activations[k][j]);

    if(k>0) {
      for(int i=0; i<_lnunits[_r+k-1]; i++) {
	_h_layers_gradient_w[k][i][j] -= 
	  _delta_h_layers[k][j] * n->_h_layers_activations[k-1][i];
      }
      _h_layers_gradient_w[k][_lnunits[_r+k-1]][j] -= 
	_delta_h_layers[k][j];

    } else {
      for(int i=0; i<_m; i++) {
	_h_layers_gradient_w[0][i][j] -=
	  _delta_h_layers[k][j] * n->_f_layers_activations[_r-1][i];
      }
      
      if(_process_dr) {
	for(int i=_m; i<2*_m; i++) {
	  _h_layers_gradient_w[0][i][j] -=
	    _delta_h_layers[k][j] * n->_b_layers_activations[_r-1][i-_m];
	}
      }

      for(int i=2*_m; i<2*_m+_n; i++) {
	_h_layers_gradient_w[0][i][j] -=
	  _delta_h_layers[k][j] * n->_encodedInput[i-2*_m];
      }
      
      _h_layers_gradient_w[0][2*_m+_n][j] -= _delta_h_layers[k][j];
    }
  }

  for(--k; k>=0; k--) {
    memset(_delta_h_layers[k], 0, (_lnunits[_r+k])*sizeof(float));

    for(int i=0; i<_lnunits[_r+k]; i++) {
      float sum = 0.0;
      for(int j=0; j<_lnunits[_r+k+1]; j++) {
	sum += _h_layers_w[k+1][i][j] * _delta_h_layers[k+1][j];
      }
      _delta_h_layers[k][i] = 
	derivate(haf, n->_h_layers_activations[k][i]) * sum;
    }

    if(k>0) {
      for(int j=0; j<_lnunits[_r+k]; j++) {
	for(int i=0; i<_lnunits[_r+k-1]; i++) {
	  _h_layers_gradient_w[k][i][j] -= 
	    _delta_h_layers[k][j] * n->_h_layers_activations[k-1][i];
	}
	_h_layers_gradient_w[k][_lnunits[_r+k-1]][j] -=  _delta_h_layers[k][j];
      }
    } else {
      for(int j=0; j<_lnunits[_r+k]; j++) {
	for(int i=0; i<_m; i++) {
	  _h_layers_gradient_w[0][i][j] -=
	    _delta_h_layers[k][j] * n->_f_layers_activations[_r-1][i];
	}
	if(_process_dr) {
	  for(int i=_m; i<2*_m; i++) {
	    _h_layers_gradient_w[0][i][j] -=
	      _delta_h_layers[k][j] * n->_b_layers_activations[_r-1][i-_m];
	  }
	}
	for(int i=2*_m; i<2*_m+_n; i++) {
	  _h_layers_gradient_w[0][i][j] -=
	    _delta_h_layers[k][j] * n->_encodedInput[i-2*_m];
	}
	_h_layers_gradient_w[0][2*_m+_n][j] -= _delta_h_layers[k][j];
      }
    }
  }

  // calculate the error on input layer and 
  // redistribute on representation layers of current node.
  for(int i=0; i<_m; i++) {
    float sum = 0.0;
    for(int j=0; j<_lnunits[_r]; j++) {
      sum += _h_layers_w[0][i][j] * _delta_h_layers[0][j];
    }
    n->_f_delta_lr[i] += 
      derivate(haf, n->_f_layers_activations[_r-1][i]) * sum;
  }

  if(_process_dr) {
    for(int i=0; i<_m; i++) {
      float sum = 0.0;
      for(int j=0; j<_lnunits[_r]; j++) {
	sum += _h_layers_w[0][_m+i][j] * _delta_h_layers[0][j];
      }
      n->_b_delta_lr[i] += 
	derivate(haf, n->_b_layers_activations[_r-1][i]) * sum;
    }
  }
}

// Compute Error for a structure in case of Super-Source trasduction
template<class HA_Function, class OA_Function, class EMP>
float RecursiveNN<HA_Function, OA_Function, EMP>::computeSSTrError(const std::vector<float>& targets, bool apply_softmax) {
  // Assume calling training procedure has just 
  // propagated current pattern with target 'targets'.
  std::vector<float> outputs = 
    std::vector<float>(_g_layers_activations[_s-1], 
			_g_layers_activations[_s-1]+_lnunits[_r+_s-1]);
  if(apply_softmax) {
    float max = -FLT_MAX;
    for(int i=0; i<outputs.size(); ++i)
      if(max < outputs[i]) max = outputs[i];

    float norm_factor = 0.0;
    for(int j=0; j<outputs.size(); ++j) {
      outputs[j] = exp(outputs[j] - max);
      norm_factor += outputs[j];
    }      	
    for(int j=0; j<outputs.size(); ++j)
      outputs[j] /= norm_factor;
  }

  float error = 0.0;
  for(int j=0; j<_lnunits[_r+_s-1]; j++) {
    // Compute error contribution for current pattern
    float t = targets[j], o = outputs[j];

    // cross-entropy
    //if(t) error += t*log(t/o);
    //if(!apply_softmax && 1-t) error += (1-t)*log((1-t)/(1-o));

    // sum of error squares
    error += (targets[j] - _g_layers_activations[_s-1][j]) * 
      (targets[j] - _g_layers_activations[_s-1][j]);

  }
  return error;
}

// Compute Error for a structure in case of IO-Isomorph structural trasduction
template<class HA_Function, class OA_Function, class EMP>
float RecursiveNN<HA_Function, OA_Function, EMP>::computeIOSTrError(const std::vector<Node*>& toNodes, bool apply_softmax) {
  float error = 0.0;
  for(std::vector<Node*>::const_iterator it=toNodes.begin(); it!=toNodes.end(); ++it) {
    std::vector<float> outputs = 
	  std::vector<float>((*it)->_h_layers_activations[_s-1], 
			      (*it)->_h_layers_activations[_s-1]+_lnunits[_r+_s-1]);

    if(apply_softmax) {
      float max = -FLT_MAX;
      for(int i=0; i<outputs.size(); ++i)
	if(max < outputs[i])
	  max = outputs[i];

      float norm_factor = 0.0;
      for(int j=0; j<outputs.size(); ++j) {
	outputs[j] = exp(outputs[j] - max);
	norm_factor += outputs[j];
      }      	
      for(int j=0; j<outputs.size(); ++j)
	outputs[j] /= norm_factor;
    }

    for(int j=0; j<_lnunits[_r+_s-1]; j++) {
      float t=(*it)->_otargets[j]; //if(!(t == 0 || t == 1)) continue;
      float o = outputs[j];

      // cross-entropy 
      //if(t) error += t*log(t/o);
      //if(!apply_softmax && 1-t) error += (1-t)*log((1-t)/(1-o));

      // sum of error squares
      error += (t-o) * (t-o);
    }
  }
  
  return error;
}

template<class HA_Function, class OA_Function, class EMP>
float RecursiveNN<HA_Function, OA_Function, EMP>::computeWeightsNorm() {
  float norm = 0.0;

  if(_ss_tr) { // begin with layers of the MLP output function
    for(int i=0; i<_m; i++)
      for(int j=0; j<_lnunits[_r]; j++)
	norm += _g_layers_w[0][i][j] * _g_layers_w[0][i][j];
    
    if(_process_dr) {
      for(int i=_m; i<2*_m; i++)
	for(int j=0; j<_lnunits[_r]; j++)
	  norm += _g_layers_w[0][i][j] * _g_layers_w[0][i][j];
    }
    
    for(int j=0; j<_lnunits[_r]; j++)
      norm += _g_layers_w[0][2*_m][j] * _g_layers_w[0][2*_m][j];

    for(int k=1; k<_s; k++)
      for(int i=0; i<_lnunits[_r+k-1]+1; i++)
	for(int j=0; j<_lnunits[_r+k]; j++)
	  norm += _g_layers_w[k][i][j] * _g_layers_w[k][i][j];
  }

  if(_ios_tr) { // proceed with layers of the MLP h map
    for(int i=0; i<_m; i++)
      for(int j=0; j<_lnunits[_r]; j++)
	norm += _h_layers_w[0][i][j] * _h_layers_w[0][i][j];
    
    if(_process_dr) {
      for(int i=_m; i<2*_m; i++) 
	for(int j=0; j<_lnunits[_r]; j++) 
	  norm += _h_layers_w[0][i][j] * _h_layers_w[0][i][j];
    }
    
    for(int i=2*_m; i<2*_m + _n; i++) 
      for(int j=0; j<_lnunits[_r]; j++)
	norm += _h_layers_w[0][i][j] * _h_layers_w[0][i][j];
     
    for(int j=0; j<_lnunits[_r]; j++)
      norm += _h_layers_w[0][2*_m + _n][j] * _h_layers_w[0][2*_m + _n][j];

    for(int k=1; k<_s; k++)
      for(int i=0; i<_lnunits[_r+k-1]+1; i++)
	for(int j=0; j<_lnunits[_r+k]; j++) 
	  norm += _h_layers_w[k][i][j] * _h_layers_w[k][i][j];
  }

  // proceed with casual part folding layers (eventually the non-casual ones).
  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    for(int j=0; j<_lnunits[0]; j++) {
      norm += _f_layers_w[0][i][j] * _f_layers_w[0][i][j];

      if(_process_dr)
	norm += _b_layers_w[0][i][j] * _b_layers_w[0][i][j];      
    }
  }
    
  for(int k=1; k<_r; k++) {
    for(int i=0; i<_lnunits[k-1]+1; i++) {
      for(int j=0; j<_lnunits[k]; j++) {
	norm += _f_layers_w[k][i][j] * _f_layers_w[k][i][j];

	if(_process_dr)
	  norm += _b_layers_w[k][i][j] * _b_layers_w[k][i][j];
      }
    }
  }

  return norm;
}

// Evaluate error and performance of the net over a data set
template<class HA_Function, class OA_Function, class EMP>
float RecursiveNN<HA_Function, OA_Function, EMP>::
evaluatePerformanceOnDataSet(DataSet* ds, bool apply_softmax) {
  //cout << msg << flush;

  float error = 0.0;
  
  for(DataSet::iterator it=ds->begin(); it!=ds->end(); ++it) {
    propagateStructuredInput(*(it.currenTONodes()), *(it.currentDPAGs()));

    if(_ios_tr) {
      error += computeIOSTrError(*(it.currenTONodes()), apply_softmax);
    }

    if(_ss_tr)
      error += computeSSTrError(*(it.currentDPAGsTargets()), apply_softmax);
  }
  
  //cout << "E = " << error << endl;
  return error;
}

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::adjustWeights(float learning_rate, float momentum_term, float ni) {
  // An assertion to safely update weights...
  // In future put this control at the level of training procedure.
  require(0<=learning_rate && learning_rate<=1, "Learning rate interval assertion failed");
  require(0<=momentum_term && momentum_term<1, "Momentum term interval assertion failed");
  require(0<=ni && momentum_term<1, "Regularization coeff. interval assertion failed");

  // Call templatized strategy minimization procedure update method.
  // The optimization strategy type decides to use or not learning rate and/or momentum term
  _wu_method.updateWeights(this, learning_rate, momentum_term, ni);

  // Finally reset gradient components to restart their computation.
  // This works both for classic gradient descent and stochastic approximations.
  resetGradientComponents();
}

template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::restorePrevWeights() {
  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    memcpy(_f_layers_w[0][i], _prev_f_layers_w[0][i], _lnunits[0] * sizeof(float));
    if(_process_dr)
      memcpy(_b_layers_w[0][i], _prev_b_layers_w[0][i], _lnunits[0] * sizeof(float));
  }
  for(int k=1; k<_r; k++) {
    for(int i=0; i<_lnunits[k-1]+1; i++) {
      memcpy(_f_layers_w[k][i], _prev_f_layers_w[k][i], (_lnunits[k])*sizeof(float));
      if(_process_dr)
	memcpy(_b_layers_w[k][i], _prev_b_layers_w[k][i], (_lnunits[k])*sizeof(float));
    }
  }

  if(_ss_tr) {
    for(int i=0; i<2*_m + 1; i++) {
      memcpy(_g_layers_w[0][i], _prev_g_layers_w[0][i], _lnunits[_r] * sizeof(float));
    }
    for(int k=1; k<_s; k++) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	memcpy(_g_layers_w[k][i], _prev_g_layers_w[k][i], (_lnunits[_r+k])*sizeof(float));
      }
    }
  }

  if(_ios_tr) {
    for(int i=0; i<2*_m + _n + 1; i++) {
      memcpy(_h_layers_w[0][i], _prev_h_layers_w[0][i], _lnunits[_r] * sizeof(float));
    }
    for(int k=1; k<_s; k++) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	memcpy(_h_layers_w[k][i], _prev_h_layers_w[k][i], (_lnunits[_r+k])*sizeof(float));
      }
    }
  }
}

/*** Save network parameters to file ***/
template<class HA_Function, class OA_Function, class EMP>
void RecursiveNN<HA_Function, OA_Function, EMP>::saveParameters(const char* network_filename) {
  std::ofstream os(network_filename);
  assure(os, network_filename);

  // First output processing flags
  os << _ios_tr << " " << _ss_tr << " " << _process_dr << endl;
  // Output node input dimension and max outdegree
  // with which the network was trained
  os << _n << " " << _v << endl;
  // Ouput to file values of r and s.
  os << _r << " " << _s << endl;

  // N.B. All previous values must be synchronized
  // with global Options parameters to be sure to 
  // have consistent values across different objects (RNN, Node, DataSet...)

  // Then the vector of number of units per layer
  for(std::vector<int>::const_iterator it=_lnunits.begin();
      it!=_lnunits.end(); ++it)
    os << *it << " ";
  os << endl << endl;

  /*** IMPORTANT UPDATE ***/
  int default_precision = os.precision();
  os.precision(Options::instance()->getPrecision());
  os.setf(std::ios::scientific);
  /************************/

  // Then output network weights to file
  // First is f folding part
  for(int i=0; i<(_n+_v*_m) + 1; i++) {
    for(int j=0; j<_lnunits[0]; j++) {
      os << _f_layers_w[0][i][j] << " ";
    }
    os << endl;
  }
  os << endl;

  for(int k=1; k<_r; k++) {
    for(int i=0; i<_lnunits[k-1]+1; i++) {
      for(int j=0; j<_lnunits[k]; j++) {
	os << _f_layers_w[k][i][j] << " ";
      }
      os << endl;
    }
    os << endl;
  }
  os << endl;

  // Warning!! the following order of writing is important
  // for the constructor that read network from file.
  if(_ss_tr) {
    // Now output weights for g output function layers
    for(int i=0; i<2*_m + 1; i++) {
      for(int j=0; j<_lnunits[_r]; j++) {
	os << _g_layers_w[0][i][j] << " ";
      }
      os << endl;
    }
    os << endl;

    for(int k=1; k<_s; k++) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	for(int j=0; j<_lnunits[_r+k]; j++) {
	  os << _g_layers_w[k][i][j] << " ";
	}
	os << endl;
      }
      os << endl;
    }
    os << endl;
  }

  if(_ios_tr) {
    // Now output weights for h map layers
    for(int i=0; i<2*_m + _n + 1; i++) {
      for(int j=0; j<_lnunits[_r]; j++) {
	os << _h_layers_w[0][i][j] << " ";
      }
      os << endl;
    }
    os << endl;

    for(int k=1; k<_s; k++) {
      for(int i=0; i<_lnunits[_r+k-1]+1; i++) {
	for(int j=0; j<_lnunits[_r+k]; j++) {
	  os << _h_layers_w[k][i][j] << " ";
	}
	os << endl;
      }
      os << endl;
    }
    os << endl;
  }

  // Ok, in case output b layers weights
  if(_process_dr) {
    for(int i=0; i<(_n+_v*_m) + 1; i++) {
      for(int j=0; j<_lnunits[0]; j++) {
	os << _b_layers_w[0][i][j] << " ";
      }
      os << endl;
    }
    os << endl;

    for(int k=1; k<_r; k++) {
      for(int i=0; i<_lnunits[k-1]+1; i++) {
	for(int j=0; j<_lnunits[k]; j++) {
	  os << _b_layers_w[k][i][j] << " ";
	}
	os << endl;
      }
      os << endl;
    }
  }
}


#endif // _RECURSIVE_NN_H
