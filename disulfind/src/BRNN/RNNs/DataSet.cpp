#include "DataSet.h"
#include <dirent.h>
#include <sys/stat.h>
//#include <iterator>
#include <algorithm>
#include <sstream>
#include <iostream>
//#include <boost/directory.h>
using namespace std;

#define PR(x) cout << #x << ": " << x << endl
#define WFP int z; cin >> z

static int one(const struct dirent* unused) {
  return 1;
}

StructuredInstanceTemplate::StructuredInstanceTemplate(const string& id, const vector<vector<float> >& encodedInputs, const vector<vector<vector<float> > >& nodes_targets, const vector<pair<DPAG, DPAG> >& dpags, const vector<vector<float> >& dpags_targets):
  _id(id), _nodes_targets(nodes_targets), 
  _dpags(dpags), _dpags_targets(dpags_targets) {

  vector<vector<float> >::const_iterator it = encodedInputs.begin();
  while(it != encodedInputs.end()) {
    _toNodes.push_back(new Node(*it));
    ++it;
  }
}

DataSet::DataSet(const char* inputfile): _n(0) {
  // get max_outdegree domain parameter
  _v = Options::instance()->getDomainOutDegree();
  
  int num_processed_files = 0;
  if(loadInstances(inputfile))
     num_processed_files++;
  
  if(!num_processed_files) {
    cerr << "DataSet::DataSet(const char*): reading " << inputfile 
	 << "results in empty data set.\n";
    exit(1);
  }
}


DataSet::DataSet(const char* dirname, const char* list_of_files): _n(0) {
  // get max_outdegree domain parameter
  _v = Options::instance()->getDomainOutDegree();

  // open file listing data set files, one per line
  ifstream ifs(list_of_files);
  assure(ifs, list_of_files);
  
  string prefix(dirname);
  if(prefix[prefix.length()-1] != '/')
      prefix += '/';

  // Instead of generating instances according to list order (biasing
  // network online training towards first seen graphs) store instances file names
  // and put their data in sequence having performed a random shuffle of the
  // order of the elements
  vector<int> file_indexes;
  vector<string> file_names;

  string line;
  while(getline(ifs, line)) {
    // Line corresponds to file name...
    // Skip empty lines
    int pos = line.find_first_not_of(" \t\n");
    if(pos == string::npos)
      continue;
    istringstream iss(line);
    string name;
    iss >> name;
    string complete_file_name = prefix + name;

    file_names.push_back(complete_file_name);
    int index = file_indexes.size();
    file_indexes.push_back(index);
  }

  // Ok, now we have to permutate file names order and read data
  // in the resulting new order. Of course, perfoms shuffling
  // on the array of indexes...

  // Use current date as seed. The sequences must be repeatable,
  // with the same data set list.
  srand(190600); 

  CustomRandom crand;
  random_shuffle(file_indexes.begin(), file_indexes.end(), crand);

  int num_processed_files = 0;
  for(vector<int>::iterator it=file_indexes.begin();
      it!=file_indexes.end(); ++it)
    if(loadInstances(file_names[*it].c_str()))
      num_processed_files++;

  if(!num_processed_files) {
    cerr << "Error! " << list_of_files << " reading results in an empty data set. ";
    require(0, "Aborting...");
  }
}

DataSet::~DataSet() {
  // Deallocate set of template instances (e.g. training/testing set)
  StructuredInstancesSetIter it = _template_instances.begin();
  while(it != _template_instances.end()) {
    delete *it; *it = 0;
    ++it;
  }
}

bool DataSet::loadInstances(const char* filename) {
  ifstream ifs(filename);
  assure(ifs, filename);

  string line, dummy_string, id;
  int old_n = _n, _gtd, _ntd;

  ifs >> dummy_string >> id
      >> dummy_string >> _gtd
      >> dummy_string >> _ntd
      >> dummy_string >> _n;

  //PR(_gtd); PR(_ntd); PR(_n); PR(_v);

  if(old_n && _n!=old_n)
    require(0, "Error! Different files have different node inputs dimensions\nin call to DataSet::loadInstances(const char*). Aborting...\n");

  vector<pair<DPAG, DPAG> > dpags;
  vector<vector<float> > dpags_targets;
  vector<vector<float> > nodes_inputs;
  vector<vector<vector<float> > > nodes_targets;
  vector<bool> goal_mask;
  vector<bool> prop_mask;

  /*** UPDATE 27/07/2001 ***/
  // Get input mask
  vector<bool> input_mask = Options::instance()->getInputMask();

  int ng = 1;
  while(getline(ifs, line)) {
    if(line.find("node") != string::npos) {
      // Found specification of node input, assumed in sequence
      istringstream iss(line);
      iss >> dummy_string;
      
      nodes_inputs.push_back(vector<float>(_n, 0.0));
      // read node input label
      for(int j=0; j<_n; j++) {
	iss >> (nodes_inputs.back())[j];
	/*** UPDATE 27/07/2001 ***/
	// Eventually inhibit this input component
	if(input_mask.size() && !input_mask[j])
	  (nodes_inputs.back())[j] = 0.0;
      }
      //copy(nodes_inputs.back().begin(), nodes_inputs.back().end(), ostream_iterator<float>(cout, " ")); int z; cin >> z;
      continue;
    }
    if(line.find("graph") != string::npos) {
      // Found beginning of a graph specification
      //if(!(ng++%100)) cout << "." << flush;

      istringstream iss(line);
      iss >> dummy_string;

      // read number of nodes in graph
      char dummy_char;
      int num_nodes;
      iss >> dummy_char >> num_nodes >> dummy_char;

      // Insert a new dpags target
      dpags_targets.push_back(vector<float>(_gtd, 0.0));

      // and a new sequence of node target label
      nodes_targets.push_back(vector<vector<float> >(num_nodes, vector<float>(_ntd, 0.0)));

      // read global target label
      // NON PORTABLE UPDATE!!! 16/05/2002
      float score = 1.0;
      for(int j=0; j<_gtd; j++) {
	iss >> (dpags_targets.back())[j];
	score *= (dpags_targets.back())[j];
      }
      
      if(score == 1.0) goal_mask.push_back(true);
      else goal_mask.push_back(false);
      prop_mask.push_back(false);

      // Skip blank line
      getline(ifs, line);
      
      DPAG dpag(num_nodes);
      int num_node;
      // read data for each node
      for(int i=0; i<num_nodes; i++) {
	getline(ifs, line);
	istringstream iss1(line);

	iss1 >> num_node;
	require(i==num_node, "Error! Index and number of node do not match\nin call to DataSet::loadInstance(const char*). Aborting...\n");

	// read node target output label
	for(int j=0; j<_ntd; j++) {
	  iss1 >> (nodes_targets.back())[num_node][j];
	}

	// then read node children and build corresponding edges
	int target;
	while(iss1 >> target) {
	  boost::add_edge(num_node, target, EdgeProperty(0), dpag);
	}
      }

      // Add resulting direct and reverse graph to sequence of rnn dpags
      //printDPAG(dpag); int z; cin >> z;
      dpags.push_back(dpag_to_rnndpag(dpag));

      /*** IMPORTANT UPDATE: 23/07/2001 ***/
      // Eliminate control on max outdegree.
      // All graphs are loaded into dataset and it is up to the net
      // to ignore edges whose id is greater than max outdegree.

      // Determine if max_outdegree passed as argument
      // is too much conservative
      //require((_v > getMaxOutDegree(dpags.back().first) && _v > getMaxOutDegree(dpags.back().second)), "Error! Max Outdegree passed as argument is less than\none found for a set of graphs\nin method DataSet::loadInstances(const char*). Aborting...\n");
      
    }
  }

  // Now can insert the new StructuredInstanceTemplate
  _template_instances.push_back(new StructuredInstanceTemplate(id, nodes_inputs, nodes_targets, dpags, dpags_targets));
  (_template_instances.back())->_goal_mask = goal_mask;
  (_template_instances.back())->_prop_mask = prop_mask;

  return true; // Ok on reading...
}

int DataSet::size() {
  int size = 0;
  iterator it = begin();
  while(it != end()) {
    ++size;
    ++it;
  }
  return size;
}


/*
int main(int argc, char* argv[]) {
  DataSet ds(argv[1], atoi(argv[2]), 1, 1, vector<int>(2, 1), SS);

  DataSet::iterator it = ds.begin();
  while(it != ds.end()) {
    cout << "*** ";
    copy(it.currentDPAGsTargets()->begin(), it.currentDPAGsTargets()->end(), ostream_iterator<float>(cout, " ")); cout << endl;
    int num_nodes = it.currenTONodes()->size();
    for(int i=0; i<num_nodes; i++) {
      cout << "Node: " << i << " --> ";
      copy((*(it.currenTONodes()))[i]->_otargets.begin(), (*(it.currenTONodes()))[i]->_otargets.end(), ostream_iterator<float>(cout, " ")); cout << endl;
      copy((*(it.currenTONodes()))[i]->_encodedInput.begin(), (*(it.currenTONodes()))[i]->_encodedInput.end(), ostream_iterator<float>(cout, " ")); cout << endl;
    }
    cout << endl;
    printDPAG(it.currentDPAGs()->first);
    cout << endl;
    printDPAG(it.currentDPAGs()->second);
    cout << "***" << endl << endl;
    ++it;
  }
}
*/
