// Author: Laszlo Kajan
// Copyright (C) 2011 Technical University of Munich
//  
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//  
#ifndef REQUIRE_H
#define REQUIRE_H

#include <fstream>
#include <string>
#include <sstream>
#include <stdexcept>

inline void require( bool __requirement, const std::string& __msg = "requirement failed" )
{
  if( !__requirement ) throw new std::runtime_error( __msg );
}

inline void requireArgs( int __argc, int __args, const std::string& __msg = "wrong number of arguments, expected " )
{
  if( __argc != __args + 1 )
  {
	  std::stringstream ss;
    ss << __msg << __args;
    throw new std::runtime_error( ss.str() );
  }
}

inline void requireMinArgs( int __argc, int __minArgs, const std::string& __msg = "too few arguments, expected at least " )
{
  if( __argc < __minArgs + 1 )
  {
	  std::stringstream ss;
    ss << __msg << __minArgs;
    throw new std::runtime_error( ss.str() );
  }
}
  
inline void assure( std::ifstream& __in, const std::string& __filename = "" )
{
  if( !__in ) throw new std::runtime_error( std::string("could not open file '") + __filename + "' for input" );
}

inline void assure( std::ofstream& __in, const std::string& __filename = "" )
{
  if( !__in ) throw new std::runtime_error( std::string("could not open file '") + __filename + "' for output" );
}
#endif // REQUIRE_H by Laszlo Kajan

// vim:et:ts=2:ai:
