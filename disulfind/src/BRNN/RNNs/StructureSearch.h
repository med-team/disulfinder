#ifndef _STRUCTURE_SEARCH_H
#define _STRUCTURE_SEARCH_H

#include "require.h"
#include "DPAG.h"
#include "Node.h"
#include <boost/random.hpp> // for boost::random number generators
#include <list>
#include <algorithm>
//#include <map>
//#include <set> // to implement a priority queue with no duplicates
#include <vector>
using std::cout;
using std::endl;
using std::flush;
using std::ofstream;

#define VERBOSE(level, x) if (debugLevel>=level) {cout << x;}

typedef boost::minstd_rand base_generator_type;

/* 
   Node data structure for search trees.
   A node in the search tree corresponds
   to a certain graph with a score associated.
*/
typedef struct SearchTreeNode {
  DPAG _dpag;
  int _depth;
  float _score;
  std::vector<float> _localScores;

  SearchTreeNode(const DPAG& dpag, int depth, float score, const std::vector<float>& localScores = std::vector<float>()):
    _dpag(dpag), _depth(depth), _score(score), _localScores(localScores) {}

  SearchTreeNode(const SearchTreeNode* snode):
    _dpag(snode->_dpag), _depth(snode->_depth), _score(snode->_score), _localScores(snode->_localScores) {}

  SearchTreeNode(const SearchTreeNode& snode):
    _dpag(snode._dpag), _depth(snode._depth), _score(snode._score), _localScores(snode._localScores) {}
  
  /*
  friend ostream& operator<<(ostream& os, const SearchTreeNode& stn) {
    return os;
  }
  */
} SearchTreeNode;

// Priority predicate for Nodes (used by beam search)
struct GT_SNode {
  bool operator()(const SearchTreeNode* n1, const SearchTreeNode* n2) {
    return n1->_score > n2->_score;
  }
};  


class StructureSearch {
  bool _ios_tr, _ss_tr;
  std::vector<std::vector<float> > _nodesInputs;

  // This measure is based on a trained Recursive Neural Network
  // and it is an heuristic one
  template<typename T1, typename T2, typename T3, 
    template<typename, typename, typename> class RNN>
  float 
    RNNMeasure(const DPAG&, RNN<T1, T2, T3>* const, 
	       std::vector<Node*>&, std::vector<float>&);

 public:
  StructureSearch() {
    TrasdType trasd = Options::instance()->getTrasductionType();
    _ios_tr = (trasd & 1)?true:false;
    _ss_tr = (trasd & 2)?true:false;
    if(_ios_tr && _ss_tr) {
      require(0, "Cannot apply search with both IOS and SS");
    }
  }

  StructureSearch(const std::vector<std::vector<float> >& nodesInputs):
    _nodesInputs(nodesInputs) {
    TrasdType trasd = Options::instance()->getTrasductionType();
    _ios_tr = (trasd & 1)?true:false;
    _ss_tr = (trasd & 2)?true:false;
    if(_ios_tr && _ss_tr) {
      require(0, "Cannot apply search with both IOS and SS");
    }
  }

  /*
  ~StructureSearch() {
    for(std::vector<Node*>::iterator it=_toNodes.begin();
	it!=_toNodes.end(); ++it) {
      if(*it) {
	delete *it; *it = 0;
      }
    }
  }
  */

  // Search Simulation method
/*   void simpleSearch(const char*, const std::string&, const DPAG&); */

/*   void BeamSearch(const char*, const std::string&, const DPAG&,  */
/* 		  float (*measure)(const DPAG&, const DPAG&,  */
/* 				   std::vector<float>&),  */
/* 		  int = 0, bool = true); */

  // Search guided by Recursive Neural Network heuristic
  // graph evaluation function

/*   template<typename T1, typename T2, typename T3,  */
/*     template<typename, typename, typename> class RNN>  */
/*     DPAG  */
/*     BeamSearch(const DPAG& target,  */
/* 	       RNN<T1, T2, T3>* const net_scorer,  */
/* 	       float = 0.0); */
  
/*   // Search guided by Recursive Neural Network heuristic */
/*   // graph evaluation function */
/*   template<typename T1, typename T2, typename T3,  */
/*     template<typename, typename, typename> class RNN>  */
/*     float  */
/*     exploratoryLearningSearch(const DPAG& target, std::vector<Node*>&,  */
/* 			      RNN<T1, T2, T3>* const net_scorer,  */
/* 			      float, float, int, int = 0); */
  
/*   template<typename T1, typename T2, typename T3,  */
/*     template<typename, typename, typename> class RNN>  */
/*     DPAG  */
/*     simpleSearch(RNN<T1, T2, T3>* const net_scorer); */

/*   void ConnectivityPatternSearch(const char*, const std::string&,  */
/* 				 const std::map<int, std::vector<float> >&,  */
/* 				 const DPAG&,  */
/* 				 float (*measure)(const DPAG&,  */
/* 						  const DPAG&,  */
/* 						  std::vector<float>&)); */

  void ConnectivityPatternSearch(std::ostream&,
				 const std::string&,
				 const std::map<int, std::vector<float> >&, 
				 const DPAG&, 
				 float (*measure)(const DPAG&, 
						  const DPAG&, 
						  std::vector<float>&));

  template<typename T1, typename T2, typename T3, 
    template<typename, typename, typename> class RNN> 
    DPAG 
    ConnectivityPatternSearch(RNN<T1, T2, T3>* const net_scorer, 
			      const std::vector<DPAG>& = std::vector<DPAG>());
  
  template<typename T1, typename T2, typename T3, 
    template<typename, typename, typename> class RNN> 
    std::vector<DPAG>  
    ConnectivityPatternSearch(RNN<T1, T2, T3>* 
			      const net_scorer, 
			      const std::vector<DPAG>&, 
			      std::vector<float>& scores);
};


/*** Member template definitions ***/

// This measure is based on a trained Recursive Neural Network
// and it is an heuristic one
template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>
float StructureSearch::RNNMeasure(const DPAG& g, RNN<T1, T2, T3>* const net_scorer, std::vector<Node*>& toNodes, std::vector<float>& localScores) {

  // Propagate graph through the net and compute its score
  // Pass a couple of direct and reverse graph, so the structure
  // can be processed by the net
  net_scorer->propagateStructuredInput(toNodes, dpag_to_rnndpag(g));

  // Nodes now contain their output values and these can
  // be used to compute the global score in a way similar
  // to that of PRMeasure.

  float score = 0.0; // Graph global score as a function of nodes local scores

  if(_ios_tr) {
    float weight = 1.0 / float(boost::num_vertices(g));
    // WARNING!! Assume nodes output is a scalar
    require(boost::num_vertices(g) == toNodes.size(), "Error in RNNMeasure");
    localScores.resize(boost::num_vertices(g), 0.0);
    for(int i=0; i<toNodes.size(); i++) {
      localScores[i] = toNodes[i]->_h_layers_activations[toNodes[i]->_s-1][0];
      score += localScores[i] * weight;
    }
  } else if(_ss_tr) {
    score = net_scorer->getGlobalOutput();
  }
  
  /*******************************************/
  return score;
}


/* template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>  */
/* DPAG StructureSearch::BeamSearch(const DPAG& target, RNN<T1, T2, T3>* const net_scorer, float epsilon) {//, std::ofstream& ofs) { */
/*   // Initialize random number generator with current time seed */
/*   srand(time(0)); */
/*   int rand_seed = rand(); */
/*   base_generator_type generator((rand_seed)?rand_seed:1); */
/*   boost::uniform_01<base_generator_type> distr(generator); */
/*   boost::uniform_real<base_generator_type> distr1(generator, -.05, .05); */

/*   // Initialize a vector of topological ordered nodes to be */
/*   // be used by the net for its computations... */
/*   std::vector<Node*> _toNodes; */
/*   std::vector<std::vector<float> >::const_iterator it = _nodesInputs.begin(); */
/*   while(it != _nodesInputs.end()) { */
/*     // each node internally catches dimension parameters */
/*     // from global configuration options to be sure to be synchronized */
/*     // with Recursive Neural Network */
/*     _toNodes.push_back(new Node(*it)); */
/*     ++it; */
/*   } */

/*   int num_nodes = _toNodes.size(); */
/*   //int beamsize = atoi((Options::instance()->getParameter("beamsize")).c_str());  */
/*   int beamsize = 1; */

/*   // Initialize the search tree using the 0-edges graph */
/*   std::list<SearchTreeNode*> frontier; */

/*   // This structure is set to the best graph the search procedure finds (LOCAL maxima). */
/*   SearchTreeNode* bestSubOptimal; */
  
/*   // Start state is the graph which connects all the node */
/*   // adjacent in sequence --> this is almost true in practice, */
/*   // adjacent secondary structure segments are near in 3-d space */
/*   DPAG initgraph(num_nodes); */
/*   //for(int i=1; i<num_nodes; ++i) */
/*   //boost::add_edge(i-1, i, EdgeProperty(0), initgraph); */

/*   std::vector<float> localInitScores; */
/*   float initscore = RNNMeasure(initgraph, net_scorer, _toNodes, localInitScores); */
/*   frontier.push_back(new SearchTreeNode(initgraph, 0, initscore, localInitScores)); */
/*   VERBOSE(3, "Successor score: " << initscore); */
/*   //float trueinitscore = PRMeasure(initgraph, target, localInitScores); */
    
/*   // Initialize Best Graph with initial graph */
/*   int depth = 0; */
/*   bestSubOptimal = new SearchTreeNode(initgraph, depth, initscore); */
  
/*   while(!frontier.empty()) { */
/*     SearchTreeNode* to_expand; */
    
/*     // Mantains a list of the beamsize best candidates */
/*     //priority_queue<SearchTreeNode*, std::vector<SearchTreeNode*>, LT_SNode> newCandidates; */
/*     //set<SearchTreeNode*, GT_SNode> newCandidates; */
/*     std::vector<SearchTreeNode*> newCandidates; */
/*     //newCandidates.reserve(beamsize * frontier.size()); */
  
/*     float softmax_norm = 0.0; */

/*     // Iterates through candidates and generate successors */
/*     for(std::list<SearchTreeNode*>::iterator iter=frontier.begin();  */
/* 	iter!=frontier.end(); iter++) { */
/*       to_expand = *iter; */
/*       depth = to_expand->_depth + 1; */
       
/*       VertexId vertex_id = boost::get(boost::vertex_index, to_expand->_dpag); */
/*       vertexIt v_open, v_begin, v_i, v_i1, v_end; */
/*       outIter out_i, out_end; */

/*       // get start and end guards iterators */
/*       boost::tie(v_begin, v_end)=boost::vertices(to_expand->_dpag); */

/*       for(boost::tie(v_i, v_end)=boost::vertices(to_expand->_dpag); */
/* 	  v_i!=v_end; ++v_i) { */
/* 	//if(distr() > .5) */
/* 	//continue; */

/* 	// Generate all successors by adding one edge linking current popped node. */
/* 	// First try edges to current node from its predecessors. */
/* 	for(v_i1 = v_begin; v_i1 != v_i; ++v_i1) { */
/* 	  //if(v_i1 == v_i-1) */
/* 	  //continue; */
/* 	  DPAG successor(to_expand->_dpag); */
/* 	  bool found = false; */
/* 	  for(boost::tie(out_i, out_end)=boost::out_edges(*v_i1, to_expand->_dpag); */
/* 	      out_i!=out_end; ++out_i) { */
/* 	    if(boost::target(*out_i, to_expand->_dpag) == *v_i) { */
/* 	      found = true; */
/* 	      break; */
/* 	    } */
/* 	  } */
/* 	  if(!found) { */
/* 	    boost::add_edge(vertex_id[*v_i1], vertex_id[*v_i], EdgeProperty(0), successor); */
/* 	    float succ_score; */
/* 	    std::vector<float> localScores; */
/* 	    succ_score = RNNMeasure(successor, net_scorer, _toNodes, localScores); */

/* 	    VERBOSE(3, "Successor score: " << succ_score << endl); */
/* 	    if(newCandidates.size() < beamsize) { */
/* 	      // Add this successor to the list of new candidates. */
/* 	      // Do not add if it is a duplicate. */
/* 	      bool duplicate = false; */
	    
/* 	      std::vector<SearchTreeNode*>::iterator iter = newCandidates.begin(); */
/* 	      while(iter != newCandidates.end()) { */
/* 		if(equal(successor, (*iter)->_dpag)) { */
/* 		  duplicate = true; */
/* 		  break; */
/* 		} */
/* 		++iter; */
/* 	      } */
	    
/* 	      if(!duplicate) { */
/* 		// Insert successor into list of new candidates only if it is not a duplicate.  */
/* 		// Nodes are then sorted into descending order. */
/* 		newCandidates.push_back(new SearchTreeNode(successor, depth, succ_score, localScores)); */
/* 		sort(newCandidates.begin(), newCandidates.end(), GT_SNode()); */
/* 		softmax_norm += exp(succ_score); */

/* 		VERBOSE(3, " Inserted (room in beam)" << endl); */
/* 	      } else { */
/* 		VERBOSE(3, " Rejected (duplicate)" << endl); */
/* 	      } */
/* 	    } else { */
/* 	      if(succ_score > newCandidates[beamsize-1]->_score) { */
/* 		// Add this successor to the list of new candidates. */
/* 		// Do not add if it is a duplicate. */
/* 		bool duplicate = false; */
/* 		std::vector<SearchTreeNode*>::iterator iter = newCandidates.begin(); */
/* 		while(iter != newCandidates.end()) { */
/* 		  if(equal(successor, (*iter)->_dpag)) { */
/* 		    duplicate = true; */
/* 		    break; */
/* 		  } */
/* 		  ++iter; */
/* 		} */
	      
/* 		if(!duplicate) { */
/* 		  // Insert successor into list of new candidates only if it is not a duplicate.  */
/* 		  // Nodes are then sorted into descending order. */
/* 		  newCandidates.push_back(new SearchTreeNode(successor, depth, succ_score, localScores)); */
/* 		  sort(newCandidates.begin(), newCandidates.end(), GT_SNode()); */
/* 		  softmax_norm += exp(succ_score); */

/* 		  VERBOSE(3, " Inserted! (better than " << newCandidates[beamsize-1]->_score << ")" << endl); */
/* 		} else { */
/* 		  VERBOSE(3, " Rejected (duplicate)" << endl); */
/* 		} */
/* 	      } else { */
/* 		VERBOSE(3, " Rejected (beam full and no better)" << endl); */
/* 	      } */
/* 	    } */
/* 	  } */
/* 	} */
      
/* 	// Then try edges from current node to its successors */
/* 	for(v_i1 = v_i+1; v_i1 != v_end; ++v_i1) { */
/* 	  //if(v_i1 == v_i+1) */
/* 	  //continue; */
/* 	  DPAG successor(to_expand->_dpag); */
/* 	  bool found = false; */
/* 	  for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, to_expand->_dpag); */
/* 	      out_i!=out_end; ++out_i) { */
/* 	    if(boost::target(*out_i, to_expand->_dpag) == *v_i1) { */
/* 	      found = true; */
/* 	      break; */
/* 	    } */
/* 	  } */
/* 	  if(!found) { */
/* 	    boost::add_edge(vertex_id[*v_i], vertex_id[*v_i1], EdgeProperty(0), successor); */
/* 	    float succ_score; */
/* 	    std::vector<float> localScores; */
/* 	    succ_score = RNNMeasure(successor, net_scorer, _toNodes, localScores); */

/* 	    VERBOSE(3, "Successor score: " << succ_score << endl); */
/* 	    if(newCandidates.size() < beamsize) { */
/* 	      // Add this successor to the list of new candidates. */
/* 	      // Do not add if it is a duplicate. */
/* 	      bool duplicate = false; */
	    
/* 	      std::vector<SearchTreeNode*>::iterator iter = newCandidates.begin(); */
/* 	      while(iter != newCandidates.end()) { */
/* 		if(equal(successor, (*iter)->_dpag)) { */
/* 		  duplicate = true; */
/* 		  break; */
/* 		} */
/* 		++iter; */
/* 	      } */
	      
/* 	      if(!duplicate) { */
/* 		// Insert successor into list of new candidates only if */
/* 		// it is not a duplicate. Nodes are automatically sorted into */
/* 		// descending order. */
/* 		newCandidates.push_back(new SearchTreeNode(successor, depth, succ_score, localScores)); */
/* 		sort(newCandidates.begin(), newCandidates.end(), GT_SNode()); */
/* 		softmax_norm += exp(succ_score); */

/* 		VERBOSE(3, " Inserted (room in beam)" << endl); */
/* 	      } else { */
/* 		VERBOSE(3, " Rejected (duplicate)" << endl); */
/* 	      } */
/* 	    } else { */
/* 	      if(succ_score > newCandidates[beamsize-1]->_score) { */
/* 		// Add this successor to the list of new candidates. */
/* 		// Do not add if it is a duplicate. */
/* 		bool duplicate = false; */
/* 		std::vector<SearchTreeNode*>::iterator iter = newCandidates.begin(); */
/* 		while(iter != newCandidates.end()) { */
/* 		  if(equal(successor, (*iter)->_dpag)) { */
/* 		    duplicate = true; */
/* 		    break; */
/* 		  } */
/* 		  ++iter; */
/* 		} */
	      
/* 		if(!duplicate) { */
/* 		  // Insert successor into list of new candidates only if it is not a duplicate.  */
/* 		  // Nodes are automatically sorted into descending order. */
/* 		  newCandidates.push_back(new SearchTreeNode(successor, depth, succ_score, localScores)); */
/* 		  sort(newCandidates.begin(), newCandidates.end(), GT_SNode()); */
/* 		  softmax_norm += exp(succ_score); */

/* 		  VERBOSE(3, " Inserted! (better than " << newCandidates[beamsize-1]->_score << ")" << endl); */
/* 		} else { */
/* 		  VERBOSE(3, " Rejected (duplicate)" << endl); */
/* 		} */
/* 	      } else { */
/* 		VERBOSE(3, " Rejected (beam full and no better)" << endl); */
/* 	      } */
/* 	    } */
/* 	  } */
/* 	} */
/*       } */
/*     } */

/*     std::vector<float> dummy;  */
/*     float fScore = PRMeasure(bestSubOptimal->_dpag, target, dummy); */ 
/*     float pScore = PrecisionMeasure(bestSubOptimal->_dpag, target, dummy); */
/*     float rScore = RecallMeasure(bestSubOptimal->_dpag, target, dummy); */
/*     float hScore = HammingDistanceMeasure(bestSubOptimal->_dpag, target, dummy); */
/*     cout << "Network score: " << bestSubOptimal->_score << " " */
/* 	 << "F-Measure score: " << fScore << " " */ 
/* 	 << "P-Measure score: " << pScore << " " */ 
/* 	 << "R-Measure score: " << rScore << " " */ 
/* 	 << "HD-Measure score: " << hScore << " " */ 
/* 	 << "# candidates: " << newCandidates.size() << endl; */

/*     // Update highest score suboptimal graph */
/*     if(newCandidates.size() &&  */
/*        fabs((*(newCandidates.begin()))->_score - bestSubOptimal->_score) >= 5e-3) { */
/*       delete bestSubOptimal; */
/*       // Copy construct because pointer to nodes are deallocated */
/*       bestSubOptimal = new SearchTreeNode(*(*(newCandidates.begin()))); */

/*       //std::vector<float> dummy; */
/*       //float truescore = PRMeasure(bestSubOptimal->_dpag, target, dummy); */
/*       //ofs << current_step++ << '\t' << bestSubOptimal->_score << '\t' << truescore << endl; */
/*     } else { */
/*       cout << endl << "Reached Local Maxima. Stopping search procedure..." << endl << endl; */
/*       break; */
/*     } */

/*     // Update frontier with the best candidates. */
/*     // First remove current elements... */
/*     for(std::list<SearchTreeNode*>::iterator iter=frontier.begin(); iter!=frontier.end(); iter++) { */
/*       if (*iter) { */
/* 	delete *iter; */
/* 	*iter = 0; */
/*       } */
/*     } */
/*     frontier.clear(); // Remove previous elements */

/*     // Then insert beamsize best new candidates after resorting */
/*     std::stable_sort(newCandidates.begin(), newCandidates.end(), GT_SNode()); */
/*     std::vector<SearchTreeNode*>::iterator it = newCandidates.begin(); */

/*     int i = 1; */
/*     while(it != newCandidates.end()) { */
/*       if(i++<=beamsize && (exp((*it)->_score) / softmax_norm) >= epsilon) */
/* 	frontier.push_back(*it); */
/*       else { */
/* 	delete *it; */
/* 	*it = 0; */
/*       } */
/*       ++it; */
/*     } */
/*     newCandidates.clear(); */

/*     //std::vector<float> dummy; */
/*     //float true_score = PRMeasure(bestSubOptimal->_dpag, target, dummy); */

/*     //int d1, d2, d3, d4, d5, d6; float d7; */
/*     //std::pair<float, float> global_pr = globalPRMeasure(bestSubOptimal->_dpag, target,  */
/*     //						d1, d2, d3, d4, d5, d6, d7); */
/*     //float true_score = (2 * global_pr.first * global_pr.second) / (global_pr.first + global_pr.second); */
/*     cout.precision(4); */
/*     cout << '(' << bestSubOptimal->_score << ','  <<  getPredictionIndexes(bestSubOptimal->_dpag, target).getFmeasure() << ") " << flush; */
/*   } */

/*   // this is to clean up memory after (sub)optimal goal */
/*   for(std::list<SearchTreeNode*>::iterator iter=frontier.begin(); iter!=frontier.end(); iter++) { */
/*     if (*iter) { */
/*       delete *iter; */
/*       *iter = 0; */
/*     } */
/*   } */
/*   frontier.clear(); // Remove previous elements */

  // Open file where to store the solution.
  //ofstream os(outfilename);
  //assure(os, outfilename);
  //printDPAG(bestSubOptimal->_dpag, os);

/*   DPAG predicted(bestSubOptimal->_dpag); */
/*   delete bestSubOptimal; bestSubOptimal = 0; */
/*   return predicted; */
/* } */

/* //////////////////////////////////////////////////////////////////////////// */

/* template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>  */
/* float StructureSearch::exploratoryLearningSearch(const DPAG& target, std::vector<Node*>& toNodes, RNN<T1, T2, T3>* const net_scorer, float eta, float alpha, int rand_seed, int epoch) { */
/*   base_generator_type generator((rand_seed)?rand_seed:1); */
/*   boost::uniform_01<base_generator_type> distr(generator); */

/*   int num_nodes = toNodes.size(); */
/*   float weight = 1.0 / static_cast<float>(num_nodes); */
    
/*   // Start state is the graph which connects all the node */
/*   // adjacent in sequence --> this is almost always true in practice, */
/*   // adjacent secondary structure segments are near in 3-d space */
/*   DPAG initgraph(num_nodes); */
/*   //  for(int i=1; i<num_nodes; ++i) */
/*   //boost::add_edge(i-1, i, EdgeProperty(0), initgraph); */

/*   float net_score = 0.0; */
/*   float true_score = 0.0; */
/*   float r_current = 0.0; */

/*      int d1, d2, d3, d4, d5, d6; float d7; */
/*      std::pair<float, float> global_pr =  */ 
/*        globalPRMeasure(initgraph, target,  */ 
/*      	    d1, d2, d3, d4, d5, d6, d7); */ 
/*      float init_score =  */ 
/*        (2 * global_pr.first * global_pr.second) / (global_pr.first + global_pr.second); */ 

/*   // Start with the empty graph */
/*   SearchTreeNode* currentState =  */
/*     new SearchTreeNode(initgraph, 0, getPredictionIndexes(initgraph, target).getFmeasure()); */

/*   int max_depth = boost::num_edges(target);// - boost::num_vertices(target) + 1; */
/*   while(currentState && currentState->_depth <= max_depth) { */
/*     //std::pair<DPAG, DPAG> currentDPAGpair = dpag_to_rnndpag(currentState->_dpag); */
/*     //net_scorer->propagateStructuredInput(toNodes, currentDPAGpair); */
/*     //true_score = PRMeasure(currentState->_dpag, target, localScores); */
/*     //for(int i=0; i<num_nodes; ++i) */
/*     //toNodes[i]->loadTarget(std::vector<float>(1, localScores[i])); */
    
/*     //net_scorer->backPropagateError(toNodes, currentDPAGpair,  */
/*     //			   std::vector<float>(1, true_score)); */
/*     //net_scorer->adjustWeights(eta, alpha); */

/*        global_pr = globalPRMeasure(currentState->_dpag, target, d1, d2, d3, d4, d5, d6, d7); */ 
/*        true_score = (2 * global_pr.first * global_pr.second) / (global_pr.first + global_pr.second); */ 
/*        cout << true_score << ' ' << flush; */ 
/*        r_current = true_score; */ 
/*     cout << getPredictionIndexes(currentState->_dpag, target).getFmeasure() << ' ' << flush; */
    
/*     // this contains children of current expanded node */
/*     std::vector<SearchTreeNode*> children; */
         
/*     VertexId vertex_id = boost::get(boost::vertex_index, currentState->_dpag); */
/*     vertexIt v_begin, v_i, v_i1, v_end; */
/*     outIter out_i, out_end; */

/*     // get start and end guards iterators */
/*     boost::tie(v_begin, v_end)=boost::vertices(currentState->_dpag); */
/*     // iterate through vertices and generate successor states */
/*     for(boost::tie(v_i, v_end)=boost::vertices(currentState->_dpag); */
/* 	v_i!=v_end; ++v_i) { */
/*       if(distr() > .5) */
/* 	continue; */
/*       // Generate all successors by adding one edge linking current popped node	 */
/*       for(v_i1 = v_begin; v_i1 != v_i; ++v_i1) { */
/* 	//if(v_i1 == v_i-1) */
/* 	//continue; */
/* 	DPAG successor(currentState->_dpag); */
/* 	bool found = false; */
/* 	for(boost::tie(out_i, out_end)=boost::out_edges(*v_i1, currentState->_dpag); */
/* 	    out_i!=out_end; ++out_i) { */
/* 	  if(boost::target(*out_i, currentState->_dpag) == *v_i) { */
/* 	    found = true; */
/* 	    break; */
/* 	  } */
/* 	} */
/*  	if(!found) { */
/*  	  boost::add_edge(vertex_id[*v_i1], vertex_id[*v_i], EdgeProperty(0), successor); */
	  
/* 	  // Add this successor to the list of new candidates. */
/* 	  // Do not add if it is a duplicate. */
/*  	  bool duplicate = false; */
	  
/* 	  std::vector<SearchTreeNode*>::iterator iter = children.begin(); */
/* 	  while(iter != children.end()) { */
/* 	    if(equal(successor, (*iter)->_dpag)) { */
/* 	      duplicate = true; */
/* 	      break; */
/* 	    }  */
/* 	    ++iter; */
/* 	  } */
	  
/* 	  if(!duplicate) { */
/* 	    //cout << "*" << flush; */
/* 	    //currentDPAGpair = dpag_to_rnndpag(successor); */
/* 	    //net_scorer->propagateStructuredInput(toNodes, currentDPAGpair); */
/* 	    //net_score = 0.0; */
/* 	    //for(int i=0; i<toNodes.size(); i++) { */
/* 	    //localScores[i] = toNodes[i]->_h_layers_activations[toNodes[i]->_s-1][0]; */
/* 	    //net_score += localScores[i] * weight; */
/* 	    //} */
/* 	    //children.push_back(new SearchTreeNode(successor, currentState->_depth+1, net_score)); */

/* 	    //true_score = PRMeasure(successor, target, localScores); */

/* 	    //global_pr = globalPRMeasure(successor, target, d1, d2, d3, d4, d5, d6, d7); */
/* 	    //true_score = (2 * global_pr.first * global_pr.second) / (global_pr.first + global_pr.second); */
/* 	    children.push_back(new SearchTreeNode(successor, currentState->_depth+1, getPredictionIndexes(successor, target).getFmeasure())); */
/* 	  } */
/* 	} */
	
/*       } */
      
/*       // Then try edges from current node to its successors in sequence */
/*       for(v_i1 = v_i+1; v_i1 != v_end; ++v_i1) { */
/* 	//if(v_i1 == v_i+1) */
/* 	//continue; */
/* 	DPAG successor(currentState->_dpag); */
/* 	bool found = false; */
/* 	for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, currentState->_dpag); */
/* 	    out_i!=out_end; ++out_i) { */
/* 	  if(boost::target(*out_i, currentState->_dpag) == *v_i1) { */
/* 	    found = true; */
/* 	    break; */
/* 	  }  */
/* 	} */
/* 	if(!found) { */
/* 	  boost::add_edge(vertex_id[*v_i], vertex_id[*v_i1], EdgeProperty(0), successor); */
/* 	  // Add this successor to the list of new candidates. */
/* 	  // Do not add if it is a duplicate. */
/* 	  bool duplicate = false; */
	  
/* 	  std::vector<SearchTreeNode*>::iterator iter = children.begin(); */
/* 	  while(iter != children.end()) { */
/* 	    if(equal(successor, (*iter)->_dpag)) { */
/* 	      duplicate = true; */
/* 	      break; */
/* 	    } */
/* 	    ++iter; */
/* 	  } */
	  
/* 	  if(!duplicate) { */
/* 	    //cout << "*" << flush; */
/* 	    //currentDPAGpair = dpag_to_rnndpag(successor); */
/* 	    //net_scorer->propagateStructuredInput(toNodes, currentDPAGpair); */
/* 	    //net_score = 0.0; */
/* 	    //for(int i=0; i<toNodes.size(); i++) { */
/* 	    //localScores[i] = toNodes[i]->_h_layers_activations[toNodes[i]->_s-1][0]; */
/* 	    //net_score += localScores[i] * weight; */
/* 	    //} */
/* 	    //children.push_back(new SearchTreeNode(successor, currentState->_depth+1, net_score)); */

/* 	    //true_score = PRMeasure(successor, target, localScores); */
	    
/* 	    //global_pr = globalPRMeasure(successor, target, d1, d2, d3, d4, d5, d6, d7); */
/* 	    //true_score = (2 * global_pr.first * global_pr.second) / (global_pr.first + global_pr.second); */
/* 	    children.push_back(new SearchTreeNode(successor, currentState->_depth+1, getPredictionIndexes(successor, target).getFmeasure())); */
/* 	  } */
/* 	} */
/*       } */
/*     } */
    
/*     // sort by true_score (descending) the list of current state children */
/*     std::stable_sort(children.begin(), children.end(), GT_SNode()); */

/*     if(currentState) { */
/*       delete currentState; */
/*       currentState = 0; */
/*     } */

/*     if(children.size() > 1) { */
/*       for(int i=0; i<children.size(); i+=children.size()/2) { */
/* 	std::pair<DPAG, DPAG> DPAGpair = dpag_to_rnndpag(children[i]->_dpag); */
/* 	net_scorer->propagateStructuredInput(toNodes, DPAGpair); */

/* 	//true_score = PRMeasure(children[i]->_dpag, target, localScores); */
/* 	//for(int i=0; i<num_nodes; ++i) */
/* 	//toNodes[i]->loadTarget(std::vector<float>(1, localScores[i])); */
	
/* 	//net_scorer->backPropagateError(toNodes, DPAGpair,  */
/* 	//		     std::vector<float>(1, true_score)); */

/* 	//global_pr = globalPRMeasure(children[i]->_dpag, target, d1, d2, d3, d4, d5, d6, d7); */

/* 	ConfusionTable ct = getPredictionIndexes(children[i]->_dpag, target); */
/* 	std::vector<float> targets; */
/* 	targets.push_back(ct.getPrecision()); targets.push_back(ct.getRecall()); */

/* 	net_scorer->backPropagateError(toNodes, DPAGpair, targets); */
/* 	net_scorer->adjustWeights(eta, alpha); */
/*       } */

/*       // Semi-uniform distributed exploration */
/*       int succ_index; */
/*       succ_index = 0; */
/*       //if(distr() < .5) */
/*       //succ_index = 0; // pure exploitation */
/*       //else  */
/*       //// random exploration */
/*       //succ_index = (int)(float(children.size()-1)*rand()/(RAND_MAX+1.0)); */
      
/*       currentState = new SearchTreeNode(children[succ_index]); */
/*     } */

/*     // garbage collect memory allocated in children */
/*     for(std::vector<SearchTreeNode*>::iterator it=children.begin();  */
/* 	it!=children.end(); ++it) { */
/*       delete *it; */
/*       *it = 0; */
/*     } */
/*     children.clear(); */
/*   } */

/*   if(currentState) { */
/*     delete currentState; */
/*     currentState = 0; */
/*   } */

/*   return r_current; */
/* } */

////////////////////////////////////////////////////////////////////////////

/* template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>  */
/* DPAG StructureSearch::CombinatorialSearch(const DPAG& target, RNN<T1, T2, T3>* const net_scorer, float epsilon) { */
/*   // Initialize random number generator with current time seed */
/*   srand(time(0)); */
/*   int rand_seed = rand(); */
/*   rand_seed = (rand_seed)?rand_seed:1; */
/*   base_generator_type generator(rand_seed); */

/*   // Initialize a vector of topological ordered nodes to be */
/*   // be used by the net for its computations... */
/*   std::vector<Node*> _toNodes; */
/*   std::vector<std::vector<float> >::const_iterator it = _nodesInputs.begin(); */
/*   while(it != _nodesInputs.end()) { */
/*     // each node internally catches dimension parameters */
/*     // from global configuration options to be sure to be synchronized */
/*     // with Recursive Neural Network */
/*     _toNodes.push_back(new Node(*it)); */
/*     ++it; */
/*   } */

/*   int num_nodes = _toNodes.size(); */

/*   DPAG template_g(num_nodes); */
/*   for(int i=1; i<boost::num_vertices(template_g); ++i) */
/*     boost::add_edge(i-1, i, EdgeProperty(0), template_g); */

/*   // This structure is set to the best graph the search  */
/*   // procedure finds.It is a LOCAL maxima. */
/*   DPAG* bestSubOptimal = new DPAG(template_g); */
/*   std::vector<float> localScores; */
/*   float subopt_score =  */
/*     RNNMeasure(*bestSubOptimal, net_scorer, _toNodes, localScores); */

/*   boost::uniform_int<base_generator_type> outdegree_distr(generator, 0, 10); */
/*   //boost::exponential_distribution<base_generator_type>  */
/*   //nodeindex_distr(generator, .5);//1.0 / boost::num_vertices(template_g)); */

/*   Vertex_d node; */
/*   vertexIt v_i, v_end; */
/*   outIter out_i, out_end; */

/*   int counter = 0; */
/*   while(counter++ < 100 && num_nodes > 1) { */
/*     DPAG g(template_g); */
/*     VertexId vertex_id = boost::get(boost::vertex_index, g); */

/*     for(boost::tie(v_i, v_end)=boost::vertices(g); v_i!=v_end; ++v_i) { */
/*       if(vertex_id[*v_i] == num_nodes-2) */
/* 	break; */
/*       int near = vertex_id[*v_i]+1, far = near + 10, outdegree = outdegree_distr(), num_attempts = 1; */
/*       if(far > num_nodes-1) */
/* 	far = num_nodes-1; */

/*       boost::uniform_int<base_generator_type> nodeindex_distr(generator, near, far); */

/*       while(boost::out_degree(*v_i, g) < outdegree && num_attempts <= 100) { */
/* 	int target = nodeindex_distr(); */
/* 	//int target = vertex_id[*v_i] + 1 + static_cast<int>(floor(nodeindex_distr())); */
/* 	//if(target > boost::num_vertices(g)-1) */
/* 	//target = boost::num_vertices(g)-1; */
/* 	bool found = false; */
/* 	for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g); */
/* 	    out_i!=out_end; ++out_i) { */
/* 	  if(vertex_id[boost::target(*out_i, g)] == target) { */
/* 	    found = true; */
/* 	    break; */
/* 	  } */
/* 	} */
/* 	if(!found) */
/* 	  boost::add_edge(vertex_id[*v_i], target, EdgeProperty(0), g); */
/* 	else */
/* 	  ++num_attempts; */
/*       } */
/*     } */
    
/*     float candidate_score = RNNMeasure(g, net_scorer, _toNodes, localScores); */
/*     if(subopt_score < candidate_score) { */
/*       if(bestSubOptimal) */
/* 	delete bestSubOptimal; */
/*       bestSubOptimal = new DPAG(g); */
/*       subopt_score = candidate_score; */
/*     } */
/*     //printDPAG(g); cout << endl << endl; */
/*   } */

/*   return *bestSubOptimal; */
/* } */

/* template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN>  */
/* DPAG StructureSearch::simpleSearch(RNN<T1, T2, T3>* const net_scorer) { */
/*   // Initialize a vector of topological ordered nodes to be */
/*   // be used by the net for its computations... */
/*   std::vector<Node*> _toNodes; */
/*   std::vector<std::vector<float> >::const_iterator it = _nodesInputs.begin(); */
/*   while(it != _nodesInputs.end()) { */
/*     // each node internally catches dimension parameters */
/*     // from global configuration options to be sure to be synchronized */
/*     // with Recursive Neural Network */
/*     _toNodes.push_back(new Node(*it)); */
/*     ++it; */
/*   } */
/*   int num_nodes = _toNodes.size(); */

/*   DPAG* bestSubOpt = new DPAG(num_nodes); */
/*   std::vector<float> localScores; */
/*   float prev_score =  */
/*     RNNMeasure(*bestSubOpt, net_scorer, _toNodes, localScores); */
/*   for(int i=0; i<num_nodes; ++i) { */
/*     for(int j=i+1; j<num_nodes; ++j) { */
/*       DPAG nextDpag(*bestSubOpt); */
/*       boost::add_edge(i, j, nextDpag); */
/*       float next_score =  */
/* 	RNNMeasure(nextDpag, net_scorer, _toNodes, localScores); */
/*       if(next_score > prev_score) { */
/* 	if(bestSubOpt) */
/* 	  delete bestSubOpt; */
/* 	bestSubOpt = new DPAG(nextDpag); */
/* 	prev_score = next_score; */
/*       } */
/*     } */
/*   } */
  
/*   DPAG predicted(*bestSubOpt); */
/*   delete bestSubOpt; */

/*   for(std::vector<Node*>::iterator it=_toNodes.begin(); */
/*       it!=_toNodes.end(); ++it) if(*it) { delete *it; *it = 0; } */
  
/*   return predicted; */
/* } */

void connectivityPatterns(const std::set<int>&, const std::set<std::pair<int, int> >&, std::vector<std::set<std::pair<int, int> > >&);

template<typename T1, typename T2, typename T3, template<typename, typename, typename> class RNN> 
  DPAG 
  StructureSearch::ConnectivityPatternSearch(RNN<T1, T2, T3>* const net_scorer,
					     const std::vector<DPAG>& candidates) {
  DPAG* bestSubOptimal = 0;

  // Initialize a vector of topological ordered nodes to be
  // be used by the net for its computations...
  std::vector<Node*> _toNodes;
  std::vector<std::vector<float> >::const_iterator it = _nodesInputs.begin();
  while(it != _nodesInputs.end()) {
    // each node internally catches dimension parameters
    // from global configuration options to be sure to be synchronized
    // with Recursive Neural Network
    _toNodes.push_back(new Node(*it));
    ++it;
  }
  int num_nodes = _toNodes.size();

  float subopt_score = -1.0;
  if(candidates.size()) {
    for(int i=0; i<candidates.size(); ++i) {
      if(boost::num_vertices(candidates[i]) != _toNodes.size())
	continue;

      std::vector<float> localScores;
      float candidate_score = 
	RNNMeasure(candidates[i], net_scorer, _toNodes, localScores);

      if(subopt_score < candidate_score) {
	if(bestSubOptimal) delete bestSubOptimal;
	bestSubOptimal = new DPAG(candidates[i]);
	subopt_score = candidate_score;
      }
    }
  } else {
    std::vector<int> V;
    for(int i=0; i<num_nodes; ++i)
      V.push_back(i);
    std::set<int> S(V.begin(), V.end());

    std::vector<std::set<std::pair<int, int> > > connPatterns;
    connectivityPatterns(S, std::set<std::pair<int, int> >(), connPatterns);
    require(connPatterns.size() > 0, "Error in connectivity patterns generation");
  
    subopt_score = -1.0;
    //std::vector<std::pair<float, int> > Scores;
    //int target_index = -1;
    //ofstream ofs("scores.dat", std::ios::app);

    std::vector<std::set<std::pair<int, int> > >::iterator v_it = 
      connPatterns.begin();
    while(v_it != connPatterns.end()) {
      DPAG candidate(num_nodes);
      for(std::set<std::pair<int, int> >::iterator s_it=(*v_it).begin(); 
	  s_it!=(*v_it).end(); ++s_it)
	boost::add_edge((*s_it).first, (*s_it).second, candidate); 

      std::vector<float> localScores;
      float candidate_score = RNNMeasure(candidate, net_scorer, _toNodes, localScores);

      //int index = Scores.size();
      //Scores.push_back(make_pair(candidate_score, index));
      //if(equal(candidate, target))
      //target_index = index;

      if(subopt_score < candidate_score) {
	if(bestSubOptimal) delete bestSubOptimal;
	bestSubOptimal = new DPAG(candidate);
	subopt_score = candidate_score;
      }

      ++v_it;
    }
  }

  DPAG predicted(*bestSubOptimal);
  delete bestSubOptimal;

  for(std::vector<Node*>::iterator it=_toNodes.begin();
      it!=_toNodes.end(); ++it) {
    if(*it) {
      delete *it; *it = 0;
    }
  }
  return predicted;
}

struct gtPattern {
  bool operator()(const SearchTreeNode* s1, const SearchTreeNode* s2) const {
    return s1->_score > s2->_score;
  }
};

template<typename T1, typename T2, typename T3, 
  template<typename, typename, typename> class RNN> 
  std::vector<DPAG>
StructureSearch::ConnectivityPatternSearch(RNN<T1, T2, T3>* const net_scorer, 
					   const std::vector<DPAG>& candidates, 
					   std::vector<float>& scores) {  
  // Initialize a vector of topological ordered nodes to be
  // be used by the net for its computations...
  std::vector<Node*> _toNodes;
  for(int i=0; i<_nodesInputs.size(); ++i)
    _toNodes.push_back(new Node(_nodesInputs[i]));

  int num_nodes = _toNodes.size();
  std::set<SearchTreeNode*, gtPattern> bestOptimals;

  //cout << candidates.size() << endl << flush;
  for(int i=0; i<candidates.size(); ++i) {
    if(boost::num_vertices(candidates[i]) != _toNodes.size())
      continue;

    std::vector<float> localScores;
    float candidate_score = 
      RNNMeasure(candidates[i], net_scorer, _toNodes, localScores);
  
    bestOptimals.insert(new SearchTreeNode(candidates[i], 0, 
    //bestOptimals.push_back(new SearchTreeNode(candidates[i], 0, 
					      candidate_score));
    
  }

  //std::sort(bestOptimals.begin(), bestOptimals.end(), GT_SNode());
  std::vector<DPAG> bestPatterns;
  //for(int i=0; i<bestOptimals.size(); ++i) {
  for(std::set<SearchTreeNode*, gtPattern>::iterator it=bestOptimals.begin();
      it!=bestOptimals.end(); ++it) {
    bestPatterns.push_back((*it)->_dpag);
    scores.push_back((*it)->_score);
    //bestPatterns.push_back(bestOptimals[i]->_dpag);
    //scores.push_back(bestOptimals[i]->_score);
  }

  for(std::vector<Node*>::iterator it=_toNodes.begin();
      it!=_toNodes.end(); ++it)
    if(*it) { delete *it; *it = 0; }
  /* for(std::vector<SearchTreeNode*>::iterator it=bestOptimals.begin(); */
/*       it!=bestOptimals.end(); ++it)  */
/*     if(*it) { delete *it; *it = 0; } */
  
  return bestPatterns;
}
#endif // _STRUCTURE_SEARCH_H
