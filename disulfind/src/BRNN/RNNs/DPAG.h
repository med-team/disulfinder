#ifndef _DPAG_H
#define _DPAG_H

#include <utility> // for std::pair
#include <boost/utility.hpp> // for BOOST_INSTALL_PROPERTY().
#include <boost/property_map/property_map.hpp>
#include <boost/graph/graph_traits.hpp> // for boost::graph_traits
#include <boost/graph/adjacency_list.hpp>
//#include <hash_map>
#include <vector>
#include <iostream>

/* struct edge_ordered_tuple_index_t {}; */
/* namespace boost { */
/*   BOOST_INSTALL_PROPERTY(edge, ordered_tuple_index); */
/* } */

//typedef boost::property<edge_ordered_tuple_index_t, unsigned int> EdgeProperty;
typedef boost::property<boost::edge_index_t, unsigned int> EdgeProperty;
typedef boost::property<boost::vertex_index_t, unsigned int> VertexProperty;


// Create a typedef for the DPAG type.
// We can change it according to space
// and time complexity needs.
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, VertexProperty, EdgeProperty> DPAG;

// Other related useful type declarations...
typedef boost::graph_traits<DPAG>::vertex_descriptor Vertex_d;
typedef boost::graph_traits<DPAG>::vertices_size_type Vst;
typedef boost::graph_traits<DPAG>::vertex_iterator vertexIt;
typedef boost::property_map<DPAG, boost::vertex_index_t>::type VertexId;
//typedef boost::property_map<DPAG, edge_ordered_tuple_index_t>::type EdgeId;
typedef boost::property_map<DPAG, boost::edge_index_t>::type EdgeId;
typedef boost::property_map<DPAG, boost::edge_index_t>::const_type cEdgeId;

typedef std::pair<int, int> Edge;
typedef boost::graph_traits<DPAG>::edge_descriptor Edge_d;
typedef boost::graph_traits<DPAG>::edge_iterator eIter;
typedef boost::graph_traits<DPAG>::in_edge_iterator ieIter;
typedef boost::graph_traits<DPAG>::out_edge_iterator outIter;
typedef boost::graph_traits<DPAG>::adjacency_iterator adjIter;

// Data type storing numbers which 
// form precision/recall factors
typedef struct ConfusionTable {
  int true_pos, true_neg;
  int false_pos, false_neg;
  
  ConfusionTable(int tp = 0, int tn = 0, int fp = 0, int fn = 0): 
    true_pos(tp), true_neg(tn), false_pos(fp), false_neg(fn) {
    //std::cout << true_pos << ' ' << true_neg << ' '
    // << false_pos << ' ' << false_neg << ' ';
  }

  float getPrecision() const {
    if(true_pos + false_pos == 0) return 1.0;
    return float(true_pos) / float(true_pos + false_pos);
  }

  float getNegPrecision() const {
    if(true_neg + false_neg == 0) return 1.0;
    return float(true_neg) / float(true_neg + false_neg);
  }

  float getRecall() const {
    if(true_pos + false_neg == 0) return 1.0;
    return float(true_pos) / float(true_pos + false_neg);
  }

  float getFmeasure(float beta = 1.0) const {
    float precision = getPrecision();
    float recall = getRecall();
    
    if(precision == 0.0 || recall == 0.0) return .0;
    return ((beta * beta + 1.0) * precision * recall) / 
      (beta * beta * precision + recall);
  }

  std::vector<float> getLocalFmeasures(float beta = 1.0) { return std::vector<float>(); }
} ConfusionTable;

// Declaration for a function that prints DPAGs
void printDPAG(DPAG&, std::ostream& out = std::cout);

// F-Measure. This is one of the possible function 
// that assigns a score to a DPAG and it is useful 
// for simulation purposes...
float PRMeasure(const DPAG&, const DPAG&, std::vector<float>&);
// Same as F-Measure, but add noise contribution
//float NoisyPRMeasure(const DPAG&, const DPAG&, std::vector<float>&);

// Precision Measure. This is one of the possible function 
// that assigns a score to a DPAG and it is useful for simulation purposes...
float PrecisionMeasure(const DPAG&, const DPAG&, std::vector<float>&);

// Recall Measure. This is one of the possible function 
// that assigns a score to a DPAG and it is useful for simulation purposes...
float RecallMeasure(const DPAG&, const DPAG&, std::vector<float>&);

std::pair<float, float> 
globalPRMeasure(const DPAG&, const DPAG&, int&, int&, int&, int&, 
		int&, int&, float&);

// Return true positives/negatives, false positives/negatives
// of a preditcted graph given the true one.
ConfusionTable getPredictionIndexes(const DPAG&, const DPAG&, std::vector<float>&);

// Hamming DistanceMeasure. This is one of the possible function 
// that assigns a score to a DPAG and it is useful for simulation purposes...
float HammingDistanceMeasure(const DPAG&, const DPAG&, std::vector<float>&);

// Check if two DPAGs are equal
bool equal(const DPAG&, const DPAG&);

// Functions to trasform a DPAG into a DPAG processable 
// by a Bi-Recursive Neural Network and vice-versa
std::pair<DPAG, DPAG> dpag_to_rnndpag(const DPAG&);
DPAG rnndpag_to_dpag(const std::pair<DPAG, DPAG>&);

// Function to return maximum outdegree of a given graph
int getMaxOutDegree(const DPAG&);

// Function to produce integer coding of a DPAG
//unsigned long long int 
float getDPAGcoding(const DPAG&);

struct hash_dpag {
  size_t operator()(const DPAG& g) const {
    long coding = 0; 
    eIter e_i, e_end;
    for(boost::tie(e_i, e_end)=boost::edges(g); e_i!=e_end; ++e_i)
      coding += (boost::target(*e_i, g)+boost::source(*e_i, g)) *
	(boost::target(*e_i, g)-boost::source(*e_i, g));
    return coding;
  }
};

struct eqdpag {
  bool operator() (const DPAG& g1, const DPAG& g2) const {
    return equal(g1, g2);
  }
};

#endif // _DPAG_H
