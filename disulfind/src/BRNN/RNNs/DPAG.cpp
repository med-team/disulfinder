#include "require.h"
#include "Options.h"
#include "DPAG.h"
#include <boost/random.hpp> // for boost::random number generators
#include <set>
#include <sstream>
using namespace std;

// Functor to estabilish an order relation on pair of integers
// representing graph edges. It is introduced to used std::set
// to represent the edge set of a graph.
struct ltEdge {
  bool operator()(const Edge& e1, const Edge& e2) const {
    return e1.first < e2.first || 
      (!(e2.first < e1.first) && e1.second < e2.second);
  }
};

// boost::random documentation reports boost::mt19937
// as a good and efficient random generator, but
// currently it does not compile with gcc
typedef boost::minstd_rand base_generator_type;

void printDPAG(DPAG& dpag, ostream& out) {
  VertexId vertex_id = boost::get(boost::vertex_index, dpag);
  //  EdgeId edge_id = boost::get(edge_ordered_tuple_index_t(), dpag);
  EdgeId edge_id = boost::get(boost::edge_index, dpag);
  vertexIt v_i, v_i1, v_end;
  outIter out_i, out_end;
  for(boost::tie(v_i, v_end) = boost::vertices(dpag); v_i!=v_end; ++v_i) {
    out << vertex_id[*v_i] << " --> ";
    for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, dpag);
	out_i!=out_end; ++out_i) {
      out << vertex_id[boost::target(*out_i, dpag)] << " ("
	  << edge_id[*out_i] << "), ";
    }
    out << endl;
  }
}

/*******************************************/

float PRMeasure(const DPAG& g, const DPAG& gtarget, vector<float>& localScores) {
  float score = 0.0; // graph global score
  float beta_to_2nd = 1.0; //0.25 // F_beta measure parameter

  // global (precision, recall) denominators and numerator
  int global_predicted_positives = boost::num_edges(g),
    global_actual_positives = boost::num_edges(gtarget),
    global_true_positives = 0;

  Vertex_d graphNode, targetNode;
  VertexId vertex_id = boost::get(boost::vertex_index, g);
  vertexIt v_i, v_end;
  outIter out_i, out_end, out_i1, out_end1;
  ieIter in_i, in_end, in_i1, in_end1;

  localScores.resize(boost::num_vertices(g), 0.0);
  for(boost::tie(v_i, v_end) = boost::vertices(g); v_i!=v_end; ++v_i) {
    // Get target graph node with the same id
    targetNode = boost::vertex(vertex_id[*v_i], gtarget);
    // current node precision denominator
    int local_predicted_positives = boost::out_degree(*v_i, g) + boost::in_degree(*v_i, g);
    // current node recall denominator
    int local_actual_positives = boost::out_degree(targetNode, gtarget) + boost::in_degree(targetNode, gtarget);
    // current node precision/recall numerator
    int local_true_positives = 0;

    for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g);
	out_i!=out_end; ++out_i) {
      for(boost::tie(out_i1, out_end1)=boost::out_edges(targetNode, gtarget);
	  out_i1!=out_end1; ++out_i1) {
	if(boost::target(*out_i, g) == boost::target(*out_i1, gtarget)) {
	  ++global_true_positives;
	  ++local_true_positives;
	  break;
	}
      }
    }
    for(boost::tie(in_i, in_end)=boost::in_edges(*v_i, g);
	in_i!=in_end; ++in_i) {
      for(boost::tie(in_i1, in_end1)=boost::in_edges(targetNode, gtarget);
	  in_i1!=in_end1; ++in_i1) {
	if(boost::source(*in_i, g) == boost::source(*in_i1, gtarget)) {
	  ++global_true_positives;
	  ++local_true_positives;
	  break;
	}
      }
    }
    
    float node_recall;
    if(local_actual_positives == 0) {
      if(local_true_positives == 0) {
	node_recall = 1.0;
      } else {
	std::cerr << "this cannot happen!" << endl;
	exit(1);
      }
    } else {
      node_recall = 
	float(local_true_positives) / float(local_actual_positives); 
    }
    
    float node_precision;

    if(local_predicted_positives == 0) {
      if(local_true_positives == 0) {
	node_precision = 1.0;
      } else {
	std::cerr << "this cannot happen!" << endl;
	exit(1);
      }
    } else {
      node_precision = 
	float(local_true_positives) / float(local_predicted_positives); 
    }

    // Apply F_beta measure to determine score of current node
    // and add result to global score
    //    cout << "Node:" << vertex_id[*v_i] << " " << node_recall << " " << node_precision << endl;

    if(node_recall && node_precision) {
      // float weight = float(local_actual_positives) / 
      // (2*float(global_actual_positives? (global_actual_positives):1));
      float weight = 1.0 / boost::num_vertices(g);
      
      float local_score = 
	((beta_to_2nd + 1) * node_precision * node_recall) / 
	(beta_to_2nd * node_precision + node_recall);
      //      cout << " local_act_pos=" << local_actual_positives 
      //	   << " global_act_pos=" << global_actual_positives 
      //	   << " w=" << weight << " loc_sco=" << local_score << endl;
      score += local_score * weight;
      localScores[vertex_id[*v_i]] = local_score;
    } else
      localScores[vertex_id[*v_i]] = 0.0;

  }
  //  score /= boost::num_vertices(g);

  /*
  float global_recall;
  if(!global_actual_positives)
    if(!global_true_positives)
      global_recall = 1.0;
    else
      global_recall = 0.0;
  else
    global_recall =  float(global_true_positives) / float(2*global_actual_positives); 
  // NB: 2|E| since the graph is undirected

  float global_precision;
  if(!global_predicted_positives)
    if(!global_true_positives)
      global_precision = 1.0;
    else
      global_precision = 0.0;
  else
    global_precision = float(global_true_positives) / float(2*global_predicted_positives);
  // NB: 2|E| since the graph is undirected

  float global_score = 0;
  if(global_recall && global_precision)
    global_score = ((beta_to_2nd + 1) * global_precision * global_recall) / (beta_to_2nd * global_precision + global_recall);
  
   ofstream os("scores.dat", std::ios::app);
   os << score << " " 
      << global_recall << " " 
      << global_precision << " "
      << global_score 
      << endl;
   os.close();
  */

  return score;
}

/*******************************************/

// float NoisyPRMeasure(const DPAG& g, const DPAG& gtarget, vector<float>& localScores) {
//   // Initialize random number generator with a seed
//   // given by a call to std::rand() function.  
//   int rand_seed = rand();
//   rand_seed = (rand_seed)?rand_seed:1;
//   base_generator_type generator(rand_seed);

//   // Here choose among different distributions and parameters
//   float param1 = atof((Options::instance()->getParameter("param1")).c_str());
//   float param2 = atof((Options::instance()->getParameter("param2")).c_str());
//   boost::uniform_real<base_generator_type> distr(generator, param1, param2);
//   //boost::normal_distribution<base_generator_type> distr(generator, param1, param2);

//   float score = 0.0; // graph global score
//   float beta_to_2nd = 1.0; //0.25 // F_beta measure parameter

//   // global (precision, recall) denominators and numerator
//   int global_predicted_positives = boost::num_edges(g),
//     global_actual_positives = boost::num_edges(gtarget),
//     global_true_positives = 0;

//   Vertex_d graphNode, targetNode;
//   VertexId vertex_id = boost::get(boost::vertex_index, g);
//   vertexIt v_i, v_end;
//   outIter out_i, out_end, out_i1, out_end1;
//   ieIter in_i, in_end, in_i1, in_end1;

//   localScores.resize(boost::num_vertices(g), 0.0);
//   for(boost::tie(v_i, v_end) = boost::vertices(g); v_i!=v_end; ++v_i) {
//     // Get target graph node with the same id
//     targetNode = boost::vertex(vertex_id[*v_i], gtarget);
//     // current node precision denominator
//     int local_predicted_positives = boost::out_degree(*v_i, g) + boost::in_degree(*v_i, g);
//     // current node recall denominator
//     int local_actual_positives = boost::out_degree(targetNode, gtarget) + boost::in_degree(targetNode, gtarget);
//     // current node precision/recall numerator
//     int local_true_positives = 0;

//     for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g);
// 	out_i!=out_end; ++out_i) {
//       for(boost::tie(out_i1, out_end1)=boost::out_edges(targetNode, gtarget);
// 	  out_i1!=out_end1; ++out_i1) {
// 	if(boost::target(*out_i, g) == boost::target(*out_i1, gtarget)) {
// 	  ++global_true_positives;
// 	  ++local_true_positives;
// 	  break;
// 	}
//       }
//     }
//     for(boost::tie(in_i, in_end)=boost::in_edges(*v_i, g);
// 	in_i!=in_end; ++in_i) {
//       for(boost::tie(in_i1, in_end1)=boost::in_edges(targetNode, gtarget);
// 	  in_i1!=in_end1; ++in_i1) {
// 	if(boost::source(*in_i, g) == boost::source(*in_i1, gtarget)) {
// 	  ++global_true_positives;
// 	  ++local_true_positives;
// 	  break;
// 	}
//       }
//     }
    
//     float node_recall;
//     if(local_actual_positives == 0) {
//       if(local_true_positives == 0) {
// 	node_recall = 1.0;
//       } else {
// 	std::cerr << "this cannot happen!" << endl;
// 	exit(1);
//       }
//     } else {
//       node_recall = 
// 	float(local_true_positives) / float(local_actual_positives); 
//     }
    
//     float node_precision;

//     if(local_predicted_positives == 0) {
//       if(local_true_positives == 0) {
// 	node_precision = 1.0;
//       } else {
// 	std::cerr << "this cannot happen!" << endl;
// 	exit(1);
//       }
//     } else {
//       node_precision = 
// 	float(local_true_positives) / float(local_predicted_positives); 
//     }

//     // Apply F_beta measure to determine score of current node
//     // and add result to global score
//     // cout << "Node:" << vertex_id[*v_i] << " " << node_recall << " " << node_precision << endl;
//     if(node_recall && node_precision) {
//       // float weight = float(local_actual_positives) / 
//       // (2*float(global_actual_positives? (global_actual_positives):1));
//       float weight = 1.0 / boost::num_vertices(g);
      
//       float local_score = 
// 	((beta_to_2nd + 1) * node_precision * node_recall) / 
// 	(beta_to_2nd * node_precision + node_recall);
      
//       // Perturbate true localscore with a number
//       // randomly drawn from the chosen distribution.
//       local_score += distr();
//       if(local_score > 1.0)
// 	local_score = 1.0;
//       else if(local_score < 0.0)
// 	local_score = 0.0;

//       score += local_score * weight;
//       localScores[vertex_id[*v_i]] = local_score;
//     }
//   }

//   return score;
// }

/*******************************************/

float PrecisionMeasure(const DPAG& g, const DPAG& gtarget, vector<float>& localScores) {
  float score = 0.0; // graph global score

  Vertex_d graphNode, targetNode;
  VertexId vertex_id = boost::get(boost::vertex_index, g);
  vertexIt v_i, v_end;
  outIter out_i, out_end, out_i1, out_end1;
  ieIter in_i, in_end, in_i1, in_end1;

  localScores.resize(boost::num_vertices(g), 0.0);
  for(boost::tie(v_i, v_end) = boost::vertices(g); v_i!=v_end; ++v_i) {
    // Get target graph node with the same id
    targetNode = boost::vertex(vertex_id[*v_i], gtarget);
    // current node precision denominator
    int local_predicted_positives = boost::out_degree(*v_i, g) + boost::in_degree(*v_i, g);
    // current node precision numerator
    int local_true_positives = 0;

    for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g);
	out_i!=out_end; ++out_i) {
      for(boost::tie(out_i1, out_end1)=boost::out_edges(targetNode, gtarget);
	  out_i1!=out_end1; ++out_i1) {
	if(boost::target(*out_i, g) == boost::target(*out_i1, gtarget)) {
	  ++local_true_positives;
	  break;
	}
      }
    }
    for(boost::tie(in_i, in_end)=boost::in_edges(*v_i, g);
	in_i!=in_end; ++in_i) {
      for(boost::tie(in_i1, in_end1)=boost::in_edges(targetNode, gtarget);
	  in_i1!=in_end1; ++in_i1) {
	if(boost::source(*in_i, g) == boost::source(*in_i1, gtarget)) {
	  ++local_true_positives;
	  break;
	}
      }
    }
    
    float node_precision;
    if(local_predicted_positives == 0) {
      if(local_true_positives == 0) {
	node_precision = 1.0;
      } else {
	std::cerr << "this cannot happen!" << endl;
	exit(1);
      }
    } else {
      node_precision = 
	float(local_true_positives) / float(local_predicted_positives); 
    }

    if(node_precision) {
      score += node_precision;
      localScores[vertex_id[*v_i]] = node_precision;
    }
  }
  
  return (score / boost::num_vertices(g));
}

pair<float, float> globalPRMeasure(const DPAG& g, const DPAG& gtarget, int& pred_true_contacts, int& pred_contacts, int& true_contacts, int& pred_true_noncontacts, int& pred_noncontacts, int& true_noncontacts, float& nc_macro_precision) {
  Vertex_d graphNode, targetNode, targetNode1;
  VertexId vertex_id = boost::get(boost::vertex_index, g);
  vertexIt v_i, v_end;
  outIter out_i, out_end, out_i1, out_end1;

  int ptc = 0, ptnc = 0;
  int pc = boost::num_edges(g); 
  int pnc = (boost::num_vertices(g) * (boost::num_vertices(g) - 1)) / 2 - pc;
  int tc = boost::num_edges(gtarget); //- boost::num_vertices(gtarget) + 1;
  int tnc = (boost::num_vertices(gtarget) * (boost::num_vertices(gtarget) - 1)) / 2 - tc;

  for(boost::tie(v_i, v_end) = boost::vertices(g); v_i!=v_end; ++v_i) {
    // Get target graph node with the same id
    targetNode = boost::vertex(vertex_id[*v_i], gtarget);

    for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g);
	out_i!=out_end; ++out_i) {
      // skip performance counting if target is adjacent
      //if(boost::target(*out_i, g) == *(v_i+1))
      //continue;

      for(boost::tie(out_i1, out_end1)=boost::out_edges(targetNode, gtarget);
	  out_i1!=out_end1; ++out_i1) {
	if(boost::target(*out_i, g) == boost::target(*out_i1, gtarget)) {
	  ++ptc;
	  break;
	}
      }
    }

    for(vertexIt v_i1=v_i+1; v_i1!=v_end; ++v_i1) {
      bool pred_non_contact = true;
      for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g);
	  out_i!=out_end; ++out_i) {
	if(boost::target(*out_i, g) == *v_i1) {
	  pred_non_contact = false;
	  break;
	}
      }

      bool target_non_contact = true;
      targetNode1 = boost::vertex(vertex_id[*v_i1], gtarget);
      for(boost::tie(out_i1, out_end1)=boost::out_edges(targetNode, gtarget);
	  out_i1!=out_end1; ++out_i1) {
	if(boost::target(*out_i1, gtarget) == targetNode1) {
	  target_non_contact = false;
	  break;
	}
      }
      
      if(pred_non_contact && target_non_contact)
	++ptnc;
    }
    
  }

  //cout << ptc << ' ' << pc << ' ' << tc << ' ' << ptnc << ' ' << pnc << ' ' << tnc << ' ';
  float precision, recall;

  if(pc == 0) {
    if(ptc == 0)
      precision = 1.0;
    else {
      std::cerr << "this cannot happen!" << endl;
      exit(1);
    }
  } else
    precision = (static_cast<float>(ptc) / 
		 static_cast<float>(pc));

  if(pnc == 0) {
    if(ptnc == 0)
      nc_macro_precision += 1.0;
    else {
      std::cerr << "this cannot happen!" << endl;
      exit(1);
    }
  } else
    nc_macro_precision += (static_cast<float>(ptnc) / 
		 static_cast<float>(pnc));
  
  if(tc == 0) {
    if(ptc == 0)
      recall = 1.0;
    else {
      std::cerr << "this cannot happen!" << endl;
      exit(1);
    }
  } else 
    recall = (static_cast<float>(ptc) / 
	      static_cast<float>(tc));

  //cout << precision << ' ' << recall << endl; 

  pred_true_contacts += ptc;
  pred_contacts += pc;
  true_contacts += tc;

  pred_true_noncontacts += ptnc;
  pred_noncontacts += pnc;
  true_noncontacts += tnc;

  return make_pair(precision, recall);
}


ConfusionTable getPredictionIndexes(const DPAG& gpred, const DPAG& gtarget, vector<float>& localScores) {
  require(boost::num_vertices(gpred) == boost::num_vertices(gtarget), 
	  "Cannot compare graph with different number of vertices");

  float weight = 1.0 / boost::num_vertices(gpred);
  // Predicted and target edge sets representing contacts and non contacts
  set<Edge, ltEdge> predPos, predNeg, targetPos, targetNeg;

  // Declarations for traversing vertices and edges sets
  VertexId vertex_id = boost::get(boost::vertex_index, gpred);
  Vertex_d targetNode, targetNode1;
  vertexIt v_i, v_end;
  outIter out_i, out_end, out_i1, out_end1;
  ieIter in_i, in_end, in_i1, in_end1;

  localScores.resize(boost::num_vertices(gpred), .0);
  for(boost::tie(v_i, v_end) = boost::vertices(gpred); v_i!=v_end; ++v_i) {
    // Get target graph node with the same id
    targetNode = boost::vertex(vertex_id[*v_i], gtarget);

    // current node precision denominator
    int local_predicted_positives = boost::out_degree(*v_i, gpred)+boost::in_degree(*v_i, gpred);
    // current node recall denominator
    int local_actual_positives = boost::out_degree(targetNode, gtarget)+boost::in_degree(targetNode, gtarget);
    int local_true_positives = 0; // current node precision/recall numerator

    for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, gpred);
	out_i!=out_end; ++out_i) {
      for(boost::tie(out_i1, out_end1)=boost::out_edges(targetNode, gtarget);
	  out_i1!=out_end1; ++out_i1) {
	if(boost::target(*out_i, gpred) == boost::target(*out_i1, gtarget)) {
	  ++local_true_positives;
	  break;
	}
      }
    }
    for(boost::tie(in_i, in_end)=boost::in_edges(*v_i, gpred);
	in_i!=in_end; ++in_i) {
      for(boost::tie(in_i1, in_end1)=boost::in_edges(targetNode, gtarget);
	  in_i1!=in_end1; ++in_i1) {
	if(boost::source(*in_i, gpred) == boost::source(*in_i1, gtarget)) {
	  ++local_true_positives;
	  break;
	}
      }
    }
    
    float node_recall;
    if(local_actual_positives == 0)
      if(local_true_positives == 0) node_recall = 1.0;
      else { std::cerr << "this cannot happen!" << endl; exit(1); }
    else node_recall = float(local_true_positives) / float(local_actual_positives); 
    
    
    float node_precision;
    if(local_predicted_positives == 0) 
      if(local_true_positives == 0) node_precision = 1.0;
      else { std::cerr << "this cannot happen!" << endl; exit(1); }
    else node_precision = float(local_true_positives) / float(local_predicted_positives); 
    
    if(node_recall && node_precision)
      localScores[vertex_id[*v_i]] = (2*node_precision*node_recall)/(node_precision+node_recall);
    else localScores[vertex_id[*v_i]] = 0.0;
    
    for(vertexIt v_i1=v_i+1; v_i1!=v_end; ++v_i1) {
      // Get target graph node with the same id
      targetNode1 = boost::vertex(vertex_id[*v_i1], gtarget);
      
      if(boost::edge(*v_i, *v_i1, gpred).second == true)
	predPos.insert(Edge(vertex_id[*v_i], vertex_id[*v_i1]));
      else
	predNeg.insert(Edge(vertex_id[*v_i], vertex_id[*v_i1]));

      if(boost::edge(targetNode, targetNode1, gtarget).second == true)
	targetPos.insert(Edge(vertex_id[targetNode], vertex_id[targetNode1]));
      else
	targetNeg.insert(Edge(vertex_id[targetNode], vertex_id[targetNode1]));
    }
  }

  int true_positives = 0, true_negatives = 0, false_positives = 0, false_negatives = 0;
  for(set<Edge, ltEdge>::iterator it=predPos.begin(); it!=predPos.end(); ++it) {
    if(targetPos.find(*it) != targetPos.end()) ++true_positives;
    else ++false_positives;
  }

  for(set<Edge, ltEdge>::iterator it=predNeg.begin(); it!=predNeg.end(); ++it) {
    if(targetNeg.find(*it) != targetNeg.end()) ++true_negatives;
    else ++false_negatives;
  }

  // final check on returned values
  require(true_positives + true_negatives + 
	  false_positives + false_negatives ==
	  boost::num_vertices(gpred) * (boost::num_vertices(gpred)-1) / 2,
	  "Error on pos and neg counts");

  return ConfusionTable(true_positives, true_negatives, 
			false_positives, false_negatives);
}

/*******************************************/

float RecallMeasure(const DPAG& g, const DPAG& gtarget, vector<float>& localScores) {
  float score = 0.0; // graph global score

  Vertex_d graphNode, targetNode;
  VertexId vertex_id = boost::get(boost::vertex_index, g);
  vertexIt v_i, v_end;
  outIter out_i, out_end, out_i1, out_end1;
  ieIter in_i, in_end, in_i1, in_end1;

  localScores.resize(boost::num_vertices(g), 0.0);
  for(boost::tie(v_i, v_end) = boost::vertices(g); v_i!=v_end; ++v_i) {
    // Get target graph node with the same id
    targetNode = boost::vertex(vertex_id[*v_i], gtarget);
    // current node recall denominator
    int local_actual_positives = boost::out_degree(targetNode, gtarget) + boost::in_degree(targetNode, gtarget);
    // current node precision/recall numerator
    int local_true_positives = 0;

    for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g);
	out_i!=out_end; ++out_i) {
      for(boost::tie(out_i1, out_end1)=boost::out_edges(targetNode, gtarget);
	  out_i1!=out_end1; ++out_i1) {
	if(boost::target(*out_i, g) == boost::target(*out_i1, gtarget)) {
	  ++local_true_positives;
	  break;
	}
      }
    }
    for(boost::tie(in_i, in_end)=boost::in_edges(*v_i, g);
	in_i!=in_end; ++in_i) {
      for(boost::tie(in_i1, in_end1)=boost::in_edges(targetNode, gtarget);
	  in_i1!=in_end1; ++in_i1) {
	if(boost::source(*in_i, g) == boost::source(*in_i1, gtarget)) {
	  ++local_true_positives;
	  break;
	}
      }
    }
    
    float node_recall;
    if(local_actual_positives == 0) {
      if(local_true_positives == 0) {
	node_recall = 1.0;
      } else {
	std::cerr << "this cannot happen!" << endl;
	exit(1);
      }
    } else {
      node_recall = 
	float(local_true_positives) / float(local_actual_positives); 
    }

    if(node_recall) {
      score += node_recall;
      localScores[vertex_id[*v_i]] = node_recall;
    }
  }
  
  return (score / boost::num_vertices(g));
}


/*******************************************/

float HammingDistanceMeasure(const DPAG& g, const DPAG& gtarget, vector<float>& localScores) {
  // Note: HD is just the number of edge mismatches, because an absence of an edge
  // in a graph that is present in the other one indicates a trasformation to one
  // of these two graphs.

  eIter e_i, e_end, e_i1, e_end1; // edge set iterators
  int num_edge_mismatches = 0; // number of edge mismatches between the graphs

  //localScores.resize(boost::num_vertices(g), 0.0);

  // Find if edges in first graph are present in the second one.
  for(boost::tie(e_i, e_end)=boost::edges(g);
      e_i!=e_end; ++e_i) {
    bool edge_found = false;
    for(boost::tie(e_i1, e_end1)=boost::edges(gtarget);
	e_i1!=e_end1; ++e_i1) {
      if(boost::source(*e_i, g) == boost::source(*e_i1, gtarget) && 
	 boost::target(*e_i, g) == boost::target(*e_i1, gtarget)) {
	edge_found = true;
	break;
      }
    }
    if(!edge_found)
      num_edge_mismatches++;
  }
	
  // Repeat the same procedure with the second graph with respect to the first one, 
  // because an edge that is present in the second graph but not in the first, is not 
  // considered in the number of mismatches during the loop above.
  for(boost::tie(e_i, e_end)=boost::edges(gtarget);
      e_i!=e_end; ++e_i) {
    bool edge_found = false;
    for(boost::tie(e_i1, e_end1)=boost::edges(g);
	e_i1!=e_end1; ++e_i1) {
      if(boost::source(*e_i, gtarget) == boost::source(*e_i1, g) && 
	 boost::target(*e_i, gtarget) == boost::target(*e_i1, g)) {
	edge_found = true;
	break;
      }
    }
    if(!edge_found)
      num_edge_mismatches++;
  }
    
  int num_nodes = boost::num_vertices(g);
  float hd = (1.0 - float(2 * num_edge_mismatches) / float(num_nodes * (num_nodes - 1)));
  if(isnan(hd))
    return 1.0;
  return hd;
}

/*******************************************/

// Check if two DPAGs are equal.
bool equal(const DPAG& g1, const DPAG& g2) {
  if(boost::num_vertices(g1) != boost::num_vertices(g2))
    return false;

  Vertex_d node;
  VertexId vertex_id = boost::get(boost::vertex_index, g1);
  vertexIt v_i, v_end;
  outIter out_i, out_i1, out_end, out_end1;
  adjIter adj_i, adj_i1, adj_end, adj_end1;

  for(boost::tie(v_i, v_end) = boost::vertices(g1); v_i!=v_end; ++v_i) {
    /*
    boost::tie(adj_i, adj_end) = boost::adjacent_vertices(*v_i, g1);
    boost::tie(adj_i1, adj_end1) = boost::adjacent_vertices(boost::vertex(vertex_id[*v_i], g2), g2);

    if((adj_end - adj_i) == (adj_end1 - adj_i1)) {
      //if(!equal(adj_i, adj_end, adj_i1)) 
      //return false;
    } else 
      return false;
    */
    /*
    boost::tie(adj_i, adj_end) = boost::adjacent_vertices(*v_i, g1);
    vector<int> children_1(adj_i, adj_end);

    boost::tie(adj_i, adj_end) = boost::adjacent_vertices(boost::vertex(vertex_id[*v_i], g2), g2);
    vector<int> children_2(adj_i, adj_end);

    sort(children_1.begin(), children_1.end());
    sort(children_2.begin(), children_2.end());

    if(!(children_1 == children_2))
      return false;
    */
    /*
    set<int> children_1(adj_i, adj_end);
    node = boost::vertex(vertex_id[*v_i], g2);
    boost::tie(adj_i, adj_end) = boost::adjacent_vertices(node, g2);
    set<int> children_2(adj_i, adj_end);
    if(!(children_1 == children_2))
      return false;
    */
    int num_nodes_g1 = boost::out_degree(*v_i, g1), num_nodes_g2 = boost::out_degree(boost::vertex(vertex_id[*v_i], g2), g2);
    if(num_nodes_g1 != num_nodes_g2)
      return false;
    else {
      int index = 0;
      vector<int> children_1(num_nodes_g1);//, children_2;
      for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g1);
	  out_i!=out_end; ++out_i) {
	children_1[index++] = boost::target(*out_i, g1);
	//children_1.push_back(boost::target(*out_i, g1));
      }
      for(boost::tie(out_i, out_end)=boost::out_edges(boost::vertex(vertex_id[*v_i], g2), g2);
	  out_i!=out_end; ++out_i) {
	//if(find(children_1.begin(), children_1.end(), boost::target(*out_i, g2)) == children_1.end())
	int target_g2 = boost::target(*out_i, g2);
	bool found = false;
	for(int i=0; i<children_1.size(); i++) {
	  if(target_g2 == children_1[i]) {
	    found = true;
	    break;
	  }
	}
	if(!found)
	  return false;
      }

      /*
      for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g1), boost::tie(out_i1, out_end1)=boost::out_edges(boost::vertex(vertex_id[*v_i], g2), g2);
	  out_i!=out_end; ++out_i, ++out_i1) {
	children_1.push_back(boost::target(*out_i, g1));
	children_2.push_back(boost::target(*out_i1, g2));
      }
      */
      /*
      boost::tie(out_i, out_end) = boost::out_edges(*v_i, g1);
      boost::tie(out_i1, out_end1) = boost::out_edges(boost::vertex(vertex_id[*v_i], g2), g2);

      while(out_i != out_end) {
	children_1.push_back(boost::target(*out_i, g1));
	children_2.push_back(boost::target(*out_i1, g2));
	++out_i;
	++out_i1;
      }
      */

      /*
      for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g1);
	  out_i!=out_end; ++out_i) {
	children_1.push_back(boost::target(*out_i, g1));
      }
        
      for(boost::tie(out_i, out_end)=boost::out_edges(boost::vertex(vertex_id[*v_i], g2), g2);
	  out_i!=out_end; ++out_i) {
	children_2.push_back(boost::target(*out_i, g2));
      }
      */
      /*
      vector<int>::iterator it = children_1.begin();
      while(it != children_1.end()) {
	if(find(children_2.begin(), children_2.end(), *it) == children_2.end())
	  return false;
	++it;
      }
      */
      /*
      sort(children_1.begin(), children_1.end());
      sort(children_2.begin(), children_2.end());
      
      int size = children_1.size();
      for(int i=0; i<size; i++)
	if(children_1[i] != children_2[i] || children_1[size-i-1] != children_2[size-i-1])
	  return false;
      */
      //if(!(children_1 == children_2)) // Operator overloading overhead
      //return false;
    }
  }
  return true;
}

// Function to trasform a DPAG
// into a DPAG processable by a Recursive Neural Network
pair<DPAG, DPAG> dpag_to_rnndpag(const DPAG& dpag) {
  Vertex_d node;
  VertexId vertex_id = boost::get(boost::vertex_index, dpag);
  vertexIt v_i, v_end;
  outIter out_i, out_end, out_i1;

  map<int, vector<int> > r_edges;
  int n_vertices = boost::num_vertices(dpag);
  for(int i=0; i<n_vertices; i++) {
    r_edges[i] = vector<int>();
  }

  DPAG d_dpag(n_vertices);
  for(boost::tie(v_i, v_end) = boost::vertices(dpag); v_i!=v_end; ++v_i) {
    vector<int> children;
    for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, dpag);
	out_i!=out_end; ++out_i) {
      children.push_back(boost::target(*out_i, dpag));
      r_edges[boost::target(*out_i, dpag)].push_back(boost::source(*out_i, dpag));
    }
    if(vertex_id[*v_i] != n_vertices-1) {
      int edge_index = 0;
      if(!children.size()) {
	boost::add_edge(vertex_id[*v_i], vertex_id[*v_i]+1, EdgeProperty(edge_index), d_dpag);
      } else {
	sort(children.begin(), children.end());

	if(children.front() == vertex_id[*v_i]+1)
	  edge_index++; 
	else 
	  boost::add_edge(vertex_id[*v_i], vertex_id[*v_i]+1, EdgeProperty(edge_index++), d_dpag);
	 
	for(int i=0; i<children.size(); i++)
	  boost::add_edge(vertex_id[*v_i], children[i], EdgeProperty(edge_index++), d_dpag);
      }
    }
  }

  DPAG r_dpag(n_vertices);
  for(boost::tie(v_i, v_end) = boost::vertices(dpag); v_i!=v_end; ++v_i) {
    if(!vertex_id[*v_i])
      continue;
    map<int, vector<int> >::iterator it = r_edges.find(vertex_id[*v_i]);
    if(it != r_edges.end()) {
      int edge_index = 0;
      if(!(*it).second.size()) {
	boost::add_edge(vertex_id[*v_i], vertex_id[*v_i]-1, EdgeProperty(edge_index), r_dpag);
      } else {
	sort((*it).second.begin(), (*it).second.end(), greater<int>());

	if((*it).second.front() == vertex_id[*v_i]-1)
	  edge_index++; 
	else 
	  boost::add_edge(vertex_id[*v_i], vertex_id[*v_i]-1, EdgeProperty(edge_index++), r_dpag);
	 
	for(int i=0; i<(*it).second.size(); i++)
	  boost::add_edge(vertex_id[*v_i], ((*it).second)[i], EdgeProperty(edge_index++), r_dpag);
      }
    }
  }

  return make_pair(d_dpag, r_dpag);
}

DPAG rnndpag_to_dpag(const pair<DPAG, DPAG>& dpagpair) { 
  DPAG g(dpagpair.first);
  VertexId vertex_id = boost::get(boost::vertex_index, g);;
  vertexIt v_i, v_end;
  EdgeId edge_id = boost::get(boost::edge_index, g);
  for(boost::tie(v_i, v_end) = boost::vertices(g); v_i!=v_end; ++v_i) {
    vertexIt v_i1=v_i+1;
    pair<Edge_d, bool> edge_pair = boost::edge(*v_i, *v_i1, g);
    if(edge_pair.second == true && edge_id[edge_pair.first] == 0)
      boost::remove_edge(*v_i, *v_i1, g);
  }
  return g;
}

// Function to return maximum outdegree of a given graph
int getMaxOutDegree(const DPAG& dpag) {
  //VertexId vertex_id = boost::get(boost::vertex_index, dpag);
  vertexIt v_i, v_end;
  
  int max_outdegree = 0;
  for(boost::tie(v_i, v_end) = boost::vertices(dpag); v_i!=v_end; ++v_i) {
    int current_node_outdegree = boost::out_degree(*v_i, dpag);
    if(max_outdegree < current_node_outdegree)
      max_outdegree = current_node_outdegree;
  }

  return max_outdegree;
}

// Function to produce integer coding of a DPAG
//unsigned long long int 
float getDPAGcoding(const DPAG& g) {
  // int n = boost::num_vertices(g);
//   unsigned long long int coding = 0; // obviously, THERE'S A (SMALL) LIMIT ON THE NUMBER OF VERTICES!!!
  
//   vertexIt v_i, v_end;
//   outIter out_i, out_end;
//   for(boost::tie(v_i, v_end) = boost::vertices(g); v_i!=v_end; ++v_i) {
//     for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, g);
// 	out_i!=out_end; ++out_i) {
//       int i = boost::source(*out_i, g), j = boost::target(*out_i, g);
//       //coding |= (1 << boost::source(*out_i, g)*n + boost::target(*out_i, g));
//       coding |= (1 << (i*(2*n-i-1)/2 + j-1-i));
//     }
//   }

  float coding = .0; 
  eIter e_i, e_end;
  for(boost::tie(e_i, e_end)=boost::edges(g); e_i!=e_end; ++e_i)
    coding += sqrt(double(boost::target(*e_i, g)+boost::source(*e_i, g) +
			  boost::target(*e_i, g)-boost::source(*e_i, g)));
  
//   set<pair<int, int> > edges;
//   eIter e_i, e_end;
//   for(boost::tie(e_i, e_end)=boost::edges(g); e_i!=e_end; ++e_i)
//     edges.insert(make_pair(boost::source(*e_i, g), boost::target(*e_i, g)));

//   ostringstream oss;
//   for(set<pair<int, int> >::iterator it=edges.begin(); it!=edges.end(); ++it)
//     oss << (it->first+1) << (it->second+1);
//   return oss.str();

  // long coding = 0; 
//   eIter e_i, e_end;
//   for(boost::tie(e_i, e_end)=boost::edges(g); e_i!=e_end; ++e_i)
//     coding += boost::target(*e_i, g)*boost::target(*e_i, g) - boost::source(*e_i, g)*boost::source(*e_i, g);

  return coding;
}
