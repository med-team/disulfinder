// From "The C++ Standard Library" (N.Josuttis)
// Introduce a function object that acts as random random generator
// It is better then a direct call to rand().

#include <cstddef>
#include <cstdlib>

class CustomRandom {
public:
  std::ptrdiff_t operator()(std::ptrdiff_t max) {
    double tmp;
    tmp = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
    return static_cast<std::ptrdiff_t>(tmp * max);
  }
};
