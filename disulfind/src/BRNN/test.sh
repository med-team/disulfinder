#!/bin/sh

cp disulfind-conn test
cd test
for i in `ls Pssm/`; do ./disulfind-conn $i Pssm/$i xxx 4 ascii; done
\rm disulfind-conn
cd ..
echo ""
echo "Check the results in test/Predictions/Connectivity."
echo "Bye."