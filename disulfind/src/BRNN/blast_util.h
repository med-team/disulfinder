/*
  A simple set of routines to read psiblast reports 
  and get useful informations. At the moment
  it is only able to read a PSSM

  Author: Alessandro Vullo, email: vullo@dsi.unifi.it
*/

#ifndef _BLAST_UTIL_H_
#define _BLAST_UTIL_H_

#include <cassert>
#include <string>
#include <exception>
#include <map>
#include <vector>

typedef struct Profile {
  char AA;
  float profile[20];

  float getDegreeCons(int pos) {
    assert(pos >= 0 && pos < 20);
    return profile[pos];
  }
} Profile;

class PSSM: public std::vector<Profile> {
  std::map<char, int> _aa_at_profile_pos;
 public:
  class BadRequest: public std::exception {
    std::string _reason;
  public:
    BadRequest(std::string reason): _reason(reason) {}
    ~BadRequest() throw() {}

    const char* explain() const {
      return _reason.c_str();
    }
  };

  bool isEmpty() const { return size() == 0; }
  int getAAProfilePosition(char aa) {
    if(_aa_at_profile_pos.find(aa) == _aa_at_profile_pos.end())
      return -1;
    return _aa_at_profile_pos[aa];
  }
  
  void parsePsiBlastReport(const char*) throw(BadRequest);
};
#endif // _BLAST_UTIL_H_
