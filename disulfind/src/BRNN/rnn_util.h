#ifndef _RNN_UTIL_H_
#define _RNN_UTIL_H_

#include <string>
#include <set>
#include <vector>
#include <utility>
#include <algorithm>

void bipartite_matchings(const std::set<int>&, 
			 const std::set<std::pair<int, int> >&, 
			 std::vector<std::set<std::pair<int, int> > >&);

int number_of_disulphides(const char*);

void rnn_predict_bonding_state(const std::string&, 
			       const std::string&, 
			       const std::string&,
			       const std::string&);

void create_connectivity_input_data(const std::string&,
				    const std::string&,
				    int,
				    int,
				    const std::string&);

void rnn_predict_disulphides(const std::string&, 
			     const std::string&,
			     const std::string&,
			     const std::string&, 
			     const std::string&,
			     const std::string&, int = 1);

#endif // _RNN_UTIL_H_
