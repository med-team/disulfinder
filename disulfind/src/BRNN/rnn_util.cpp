#include "rnn_util.h"
#include "Options.h"
#include "DPAG.h"
#include "DataSet.h"
#include "RecursiveNN.h"
#include "StructureSearch.h"
#include "protein_util.h"
#include "blast_util.h"
#include <map>
#include <algorithm>
#include <fstream>
#include <iostream>
using namespace std;

/***
    A recursive method to generate all possible 
    bipartite matchings definable on n nodes.
***/
void bipartite_matchings(const set<int>& S, const set<pair<int, int> >& P, 
			 vector<set<pair<int, int> > >& patterns) {
  set<pair<int, int> > C;
  set<int>::iterator it=S.begin(); ++it;
  for(; it!=S.end(); ++it) C.insert(make_pair(*(S.begin()), *it));

  if(C.size() == 1) {
    set<pair<int, int> > P_final(P); P_final.insert(*(C.begin()));
    patterns.push_back(P_final);
    return;
  }

  for(set<pair<int, int> >::iterator it=C.begin(); it!=C.end(); ++it) {
    int a[] = {(*it).first, (*it).second };
    set<int> PairSet(a, a+2);
    set<int> X_new; 
    set<pair<int, int> > P_new(P); P_new.insert(*it);

    set_difference(S.begin(), S.end(), PairSet.begin(), PairSet.end(), 
		   inserter(X_new, X_new.begin()));

    if(X_new.size() != 0)
      bipartite_matchings(X_new, P_new, patterns);
  }
}

/***
    Compute B, the number of disulphides
    from predicted bonding state file.
***/
int number_of_disulphides(const char* inputfile) {
  ifstream ifs(inputfile);
  if(!ifs) { 
    cerr << "number_of_bridges: cannot open " << inputfile << endl; 
    exit(1); 
  }
  int pred, B2 = 0;
  
  while(ifs >> pred) if(pred) ++B2;
  if(B2%2) {
    cerr << "number_of_bridges: odd number of cystines.\n";
    exit(1);
  }
  return B2/2;
}

/***
    Predict bonding state with BRNN model.
 ***/
void rnn_predict_bonding_state(const string& config, 
			       const string& model,
			       const string& input,
			       const string& output) {
  // get RNN option (unique) instance
  Options* rnn_option = Options::instance();

  // set BRNN parameters from bonding state config file
  rnn_option->parse_config(config);
  rnn_option->setParameter("netname", model);
  
  // Create test set with only the given input 
  DataSet *testset = new DataSet(input.c_str());
  if(!testset) {
    cerr << "brnn_predict_bonding_state: failed dataset creation.\n";
    exit(1);
  }

  // initialize BRNN
  RecursiveNN<TanH, Sigmoid, MGradientDescent>* 
    rnn = new RecursiveNN<TanH, Sigmoid, MGradientDescent>(model.c_str());
  if(!rnn) {
    cerr << "brnn_predict_bonding_state: unable "
      "to read brnn file " << model << ".\n";
    exit(1);
  }

  // predict
  DataSet::iterator it=testset->begin(); // only one input chain
  vector<Node*> *nodes = it.currenTONodes();
  std::pair<DPAG, DPAG> *dpags = it.currentDPAGs();
  rnn->propagateStructuredInput(*nodes, *dpags);

  // write predictions to output file
  ofstream ofs(output.c_str());
  if(!ofs) {
    cerr << "brnn_predict_bonding_state: unable to open file "
	 << output << ".\n";
    exit(1);
  }

  for(std::vector<Node*>::iterator nit=nodes->begin(); 
      nit!=nodes->end(); ++nit) {
    float py1 = (*nit)->_h_layers_activations[(*nit)->_s-1][0];
    ofs << (1-py1) << ' ' << py1 << endl;
  }
  
  // deallocate Recursive Network structure
  delete rnn; rnn = 0;
  // deallocate dataset
  if(testset) { delete testset; testset = 0; }
}

/*** 
     Output RNN disulphide 
     prediction input file.
***/
void out_connectivity_input_data(const string& outfname, 
				 const map<int, vector<float> >& nodeInputs) {
  ofstream os(outfname.c_str());
  if(!os) {
    cerr << "out_connectivity_input_data: cannot open " << outfname << endl;
    exit(1);
  }

  /* determine some informations to print graphs */
  // Assume all nodes have the same input dimension
  int nodes_input_dim = (*nodeInputs.begin()).second.size();

  // Node output dim is fixed to 1, because graphs
  // measures assume nodes output dimension == 1
  int nodes_output_dim = 1;
  // Same observation for graphs global output dimension
  int graphs_output_dim = 1;

  os << "id xxxx" << endl 
     << "global_target_dim " << graphs_output_dim << endl
     << "local_target_dim " << nodes_output_dim << endl
     << "nodes_input_dim " << nodes_input_dim << endl << endl;
  
  // print node inputs
  for(map<int, vector<float> >::const_iterator it=nodeInputs.begin(); 
      it!=nodeInputs.end(); ++it) {
    os << "node ";
    copy((*it).second.begin(), (*it).second.end(), 
	 ostream_iterator<float>(os, " "));
    os << endl;
  }
  os << endl;

  // create dummy DPAG to output together with nodes input
  DPAG dummydpag(nodeInputs.size());
  for(map<int, vector<float> >::const_iterator it=nodeInputs.begin();
      it!=nodeInputs.end(); ++it) {
    int cysindex = it->first;
    if(!(cysindex%2))
      boost::add_edge(cysindex, cysindex+1, dummydpag);
  }

  // declarations to iterate through graph 
  // vertices and print information.
  VertexId vertex_id = boost::get(boost::vertex_index, dummydpag);
  vertexIt v_i, v_end;
  outIter out_i, out_end;
    
  // print dummy graph structure
  os << "graph (" << boost::num_vertices(dummydpag) << ") 1" 
     << endl << endl;
  
  for(boost::tie(v_i, v_end) = boost::vertices(dummydpag); 
      v_i!=v_end; ++v_i) {
    os << vertex_id[*v_i] << " 1 ";

    for(boost::tie(out_i, out_end)=boost::out_edges(*v_i, dummydpag);
	out_i!=out_end; ++out_i) {
      os << boost::target(*out_i, dummydpag) << ' ';
    }
    os << endl;
  }
  os << endl << endl;
}

/*** 
     Create RNN input file from profiles 
     and bonding state prediction. 
***/
void create_connectivity_input_data(const string& predfname,
				    const string& pssmfname,
				    int win_size,
				    int B,
				    const string& outfname) {
  if(B<2 || B > 5) {
    cerr << "create_connectivity_input_data: wrong number of bridges B == "
	 << B << endl;
    exit(1);
  }
  int max_seq_length = -1; 
  switch(B) {
  case 2: max_seq_length = 1527; break;
  case 3: max_seq_length = 4544; break;
  case 4: max_seq_length = 2871; break;
  case 5: max_seq_length = 976; break;
  default: cerr << "Wrong number of bridges " << B << endl; exit(1);
  }
  if(max_seq_length == -1) {
    cerr << "create_connectivity_input_data: wrong number of bridges B == "
	 << B << endl;
    exit(1);
  }

  // Get chain profile
  PSSM pssm;
  pssm.parsePsiBlastReport(pssmfname.c_str());

  // Read prediction and get bonded cystein indices
  ifstream predfs(predfname.c_str());
  if(!predfs) {
    cerr << "create_connectivity_input_data: unable to open bonding state prediction file " << predfs << "\n";
    exit(1);
  }

  
  // Register profile bonded cystein positions
  set<int> bcyspos;
  int bondstate; 
  for(int i=0; i<pssm.size(); ++i)
    if(pssm[i].AA == 'C') {
      if(predfs >> bondstate) { 
	assert(bondstate == 0 || bondstate == 1);
	if(bondstate) bcyspos.insert(i+1);
      } else {
	cerr << "create_connectivity_input_data: predicted cystein are less than those in profile\n";
	exit(1);
      }
    }

  if(predfs >> bondstate) {
    cerr << "create_connectivity_input_data: pred cystein are more than those in profile\n";
    exit(1);
  }

  int numbcys = bcyspos.size();
  if(numbcys%2) { cerr << "Wrong number of bonded cysteins\n"; exit(1); }
  
  // skip if number of bridges is == 1 or > 5
  if(numbcys < 4) {
    cerr << "create_connectivity_input_data: trivial prediction (B=1).\n";
    //exit(1);
  } else if(numbcys > 10) {
    cerr << "create_connectivity_input_data: connectivity can be computed only for up to 5 bonds.\n";
    //exit(1);
  }
  
  map<int, vector<float> > nodesInputs;  
  int win_size_2 = win_size / 2, 
    cyscounter = 0, 
    seq_length = pssm.size();

  for(set<int>::iterator it=bcyspos.begin();
      it!=bcyspos.end(); ++it, ++cyscounter) {
    nodesInputs.insert(make_pair(cyscounter, vector<float>()));
    int central_pos = *it - 1;
    if(pssm[central_pos].AA != 'C') { // re-check
      cerr << "create_connectivity_input_data: error in AA position.\n";
      exit(1);
    }
    
    for(int i=central_pos-win_size_2; 
	i<=central_pos+win_size_2; ++i) {
      if(i >=0 && i < pssm.size())
	for(int j=0; j<20; ++j)
	  nodesInputs[cyscounter].push_back(pssm[i].profile[j]);
      else
	for(int j=0; j<20; ++j)
	  nodesInputs[cyscounter].push_back(0.0);
    }
    // add information on sequence length and sequence fraction spanned
    nodesInputs[cyscounter].push_back(float(*it)/float(seq_length));
    nodesInputs[cyscounter].push_back(float(pssm.size())/float(max_seq_length));
  }
  
  out_connectivity_input_data(outfname, nodesInputs);
}

/*** 
     Predict disulphide connectivity with 
     RNN model and training set candidates.
***/
void rnn_predict_disulphides(const string& config,
			     const string& model,
			     const string& traindir,
			     const string& trainlist,
			     const string& input,
			     const string& output,
			     int K) {    
  if(K <= 0) {
    cerr << "rnn_predict_disulphides: number of alternatives <= 0.\n";
    exit(1);
  }
  if(K > 3) {
    cerr << "rnn_predict_disulphides: you trust too much the model, "
      "setting the number of alternatives to 3.\n";
    K = 3;
  }

  // get RNN option (unique) instance
  Options* rnn_option = Options::instance();

  // set RNN parameters from connectivity config file

  //rnn_option->setParameter("conf_file", brnnconfig);
  rnn_option->parse_config(config); 
  rnn_option->setParameter("netname", model);
  
  // Create test set with only the given input 
  DataSet *trainingset = NULL, *testset = NULL;

  //cout << "Loading training set ... " << flush;
  trainingset = new DataSet(traindir.c_str(), trainlist.c_str());
  //cout << "done\nLoading test set ... " << flush;
  testset = new DataSet(input.c_str());
  //cout << "done\n";
  if(!trainingset || !testset) {
    cerr << "rnn_predict_disulphides: failed creation of datasets.\n";
    exit(1);
  }

  // Cycle through train instances and determine the set of candidate 
  // connectivity patterns over which to search the right one
  int B = -1;
  vector<DPAG> candidates;
  for(DataSet::iterator it=trainingset->begin(); 
      it!=trainingset->end(); ++it) {
    if(it.currentIsGoal()) {
      // Warning! Dataset contains modified graph with added edges
      // between adjacent nodes whenever they are not present in the
      // true target graph. Need to remove these dummy edges in order
      // to create a perfect matching.
      DPAG candidate((*(it.currentDPAGs())).first);
      
      VertexId vertex_id = boost::get(boost::vertex_index, candidate);
      vertexIt v_i, v_end;
      EdgeId edge_id = boost::get(boost::edge_index, candidate);
      for(boost::tie(v_i, v_end) = boost::vertices(candidate); v_i!=v_end; ++v_i) {
	vertexIt v_i1=v_i+1;
	pair<Edge_d, bool> edge_pair = boost::edge(*v_i, *v_i1, candidate);
	if(edge_pair.second == true && edge_id[edge_pair.first] == 0)
	  boost::remove_edge(*v_i, *v_i1, candidate);
      }
      if(B < 0)	B = boost::num_edges(candidate);
      else {
	require(B == boost::num_edges(candidate) && 
		B >= 2 && B <= 5, "rnn_predict_disulphides: different "
		"number of bridges in training set"
		" or wrong number of bridges");
      }
      candidates.push_back(candidate);
    }
  }

  // initialize RNN
  RecursiveNN<TanH, Sigmoid, MGradientDescent>* 
    rnn = new RecursiveNN<TanH, Sigmoid, MGradientDescent>(model.c_str());
  if(!rnn) {
    cerr << "rnn_predict_disulphides: unable "
      "to read rnn file " << model << ".\n";
    exit(1);
  }

  DataSet::iterator it=testset->begin(); // only one input file
  vector<Node*> *nodes = it.currenTONodes();
  if(!it.currentIsGoal()) {
    cerr << "rnn_predict_disulphides: something wrong with "
      "the creation of input file.\n";
    exit(1);
  }
  vector<vector<float> > nodesInputs;
  for(vector<Node*>::iterator v_it=(*(it.currenTONodes())).begin();
      v_it!=(*(it.currenTONodes())).end(); ++v_it) {
    nodesInputs.push_back((*v_it)->_encodedInput);
  }
      
  // Predict most likely configurations among the patterns
  vector<float> bestScores;
  // Initialise search for target connectivity pattern
  StructureSearch search(nodesInputs);

  vector<DPAG> bestPatterns = 
    search.ConnectivityPatternSearch(rnn, candidates, bestScores);

  // output only the first K most likely configurations
  ofstream ofs(output.c_str());
  if(!ofs) {
    cerr << "rnn_predict_disulphides: cannot open " << output << endl;
    exit(1);
  }

  for(int j=0; j<K && j<bestPatterns.size(); ++j) {
    require(B == boost::num_edges(bestPatterns[j]) &&
	    B == boost::num_vertices(bestPatterns[j]) / 2, 
	    "Error in predicted number of disulfide bridges");
    
    // first output score (confidence?)
    ofs << bestScores[j] << endl;
    VertexId vertex_id = boost::get(boost::vertex_index, bestPatterns[j]);
    vertexIt v_i, v_end;
    outIter out_i, out_end;  
    for(boost::tie(v_i, v_end) = boost::vertices(bestPatterns[j]); 
	v_i!=v_end; ++v_i) {
      boost::tie(out_i, out_end)=boost::out_edges(*v_i, bestPatterns[j]);
      if(out_i != out_end) 
	ofs << (vertex_id[*v_i]+1) << '\t' 
	    << (vertex_id[boost::target(*out_i, bestPatterns[j])]+1) 
	    << endl;
    }
  }

  // deallocate Recursive Network structure
  delete rnn; rnn = 0;
  // deallocate datasets
  if(trainingset) { delete trainingset; trainingset = 0; }
  if(testset) { delete testset; testset = 0; }
}
