disulfinder (1.2.11-12) unstable; urgency=medium

  * Upstream download vanished
  * Standards-Version: 4.6.0 (routine-update)
  * Add missing build dependency on dh addon.
  * Apply multi-arch hints.
    + disulfinder-data: Add Multi-Arch: foreign.

 -- Andreas Tille <tille@debian.org>  Thu, 20 Jan 2022 11:36:56 +0100

disulfinder (1.2.11-11) unstable; urgency=medium

  * Team Upload.
  * d/rules: Force -std=c++14 (Closes: #984036)

 -- Nilesh Patra <nilesh@debian.org>  Fri, 22 Oct 2021 21:59:19 +0530

disulfinder (1.2.11-10) unstable; urgency=medium

  * Team Upload.
  * Fix FTCBFS by not hardcoding build arch compiler
    - Thank you, Helmut Grohne! (Closes: #982290)
  * Remove useless .dirs where no data is installed
  * Migrate to secure URLs

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 22 Feb 2021 23:39:27 +0530

disulfinder (1.2.11-9) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.

  [ Andreas Tille ]
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set field Upstream-Name in debian/copyright.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.
  * watch file standard 4 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 24 Nov 2020 10:12:51 +0100

disulfinder (1.2.11-8) unstable; urgency=medium

  [ Steffen Moeller ]
  * debian/upstream/metadata:
    - yamllint cleanliness
    - added references to registries

  [ Andreas Tille ]
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Do not parse d/changelog

 -- Andreas Tille <tille@debian.org>  Sun, 23 Sep 2018 17:15:15 +0200

disulfinder (1.2.11-7) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * debhelper 10
  * Standards-Version: 4.1.0 (no changes needed)
  * Avoid duplicated definition of guard macro (Thanks for the tip to
    Christian Seiler <christian@iwakd.de>)
    Closes: #853375

 -- Andreas Tille <tille@debian.org>  Sun, 27 Aug 2017 00:04:42 +0200

disulfinder (1.2.11-6) unstable; urgency=medium

  * Team upload.

  [Tatiana Malygina]
  * fix in install override in debian/rules
  * add harderning
  * add simple testsuite
  * cme fix dpkg-control
  * add README.test

  [Gert Wollny]
  * d/p/gcc-6: Add patch to fix compilation with gcc-6, Closes: #811876
  * d/rules: enable parallel builds

 -- Tatiana Malygina <merlettaia@gmail.com>  Sun, 26 Jun 2016 05:46:50 +0300

disulfinder (1.2.11-5) unstable; urgency=medium

  * moved debian/upstream to debian/upstream/metadata
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Fri, 08 Jan 2016 13:10:52 +0100

disulfinder (1.2.11-4) unstable; urgency=medium

  * Included patch to solve 'FTBFS with clang' (Closes: #741559)
  * Patched out 'unrecognized escape sequences'

 -- Laszlo Kajan <lkajan@debian.org>  Sun, 07 Sep 2014 09:56:52 +0200

disulfinder (1.2.11-3) unstable; urgency=low

  * debian/upstream: Valid BibTeX syntax in author strings
  * debian/control:
     - cme fix dpkg-control
     - Priority: s/extra/optional/
     - canonical Vcs URLs
     - debhelper 9
  * debian/copyright: DEP5
  * debian/patches/hardening.patch: Propagate hardening options
  * debian/rules: use dh consequently
  * README.Debian: references are now in debian/upstream - install this
    instead into doc directory

 -- Andreas Tille <tille@debian.org>  Sun, 17 Nov 2013 08:46:05 +0100

disulfinder (1.2.11-2) unstable; urgency=low

  * added mkdir debian/disulfinder-data/usr/share/disulfinder/ to
    debian/rules before moving Models to disulfinder-data package to
    resolve building issue on certain architectures (Closes: #639571)

 -- Laszlo Kajan <lkajan@rostlab.org>  Wed, 31 Aug 2011 20:47:26 +0200

disulfinder (1.2.11-1) unstable; urgency=low

  * New upstream release

 -- Laszlo Kajan <lkajan@rostlab.org>  Fri, 26 Aug 2011 13:02:59 +0200

disulfinder (1.2.10-1) unstable; urgency=low

  * New upstream release
  * The new upstream contains a rewritten version of require.h: the new
    require.h is no longer based on any code with restricted license,
    it is released under the GPL (see source)
  [Steffen]
  * moved to dh7
  * added data package

 -- Laszlo Kajan <lkajan@rostlab.org>  Thu, 18 Aug 2011 11:28:02 +0200

disulfinder (1.2.9-2) UNRELEASED; urgency=low

  [Steffen]
  * Added build-arch/-indep targets to help lintian

 -- Laszlo Kajan <lkajan@rostlab.org>  Sun, 24 Jul 2011 15:51:08 +0200

disulfinder (1.2.9-1) unstable; urgency=low

  * Initial release (Closes: #634177)

 -- Laszlo Kajan <lkajan@rostlab.org>  Sat, 09 Jul 2011 11:54:46 +0200
