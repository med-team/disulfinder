#!/usr/bin/make -f
# Remove binaries and shared objects (.so): find disulfind \( \( -name '*.o' \) -o \( -type f -exec bash -c '[[ $(file "{}") =~ ELF.*(executable|object) ]];' \; \) \) -ls -delete
PACKAGE := disulfinder
# upstream version is 1.2, our version is 8
VERSION := 1.2.11
DISTDIR := $(PACKAGE)-$(VERSION)

DISULFARCH := disulfind.tgz
DISULFDIR := disulfind

MAN1PAGES := disulfinder.1

prefix := /usr
docdir := $(prefix)/share/doc/$(PACKAGE)

CXXFLAGS := -DDEFAULT_PKGDATADIR=\"$(prefix)/share/disulfinder\"
export CXXFLAGS

.PHONY: all
all: disulfinder doc

.PHONY: disulfinder
disulfinder:
	$(MAKE) -C disulfind/src disulfinder

.PHONY: doc
doc: $(MAN1PAGES)

%.1: %.pod
	sed -e 's|__datadir__|$(datadir)|g;s|__docdir__|$(docdir)|g;s|__pkgdatadir__|$(pkgdatadir)|g;s|__PREFIX__|$(prefix)|g;s|__sysconfdir__|$(sysconfdir)|g;s|__VERSION__|$(VERSION)|g;' "$<" | \
	pod2man -c 'User Commands' -r '$(VERSION)' -name $(shell echo "$(basename $@)" | tr '[:lower:]' '[:upper:]') > "$@"

.PHONY: untar
untar: $(DISULFDIR)

$(DISULFDIR):
	tar -xvzf $(DISULFARCH) --exclude 'disulfind/src/BRNN/boost*' --exclude 'disulfind/src/BRNN/test' --exclude 'disulfind/Models/conn/data/sequences_w5' \
		--exclude disulfind/src/tmp/disulfind_conn_train.tgz --exclude disulfind/src/BRNN/RNNs.tgz && rm -f initial-clean-stamp patch-stamp

install:
	$(MAKE) -C $(DISULFDIR)/src install
	mkdir -p $(DESTDIR)$(prefix)/share/man/man1 && cp $(MAN1PAGES) $(DESTDIR)$(prefix)/share/man/man1/
	mkdir -p $(DESTDIR)$(docdir)/examples && cp examples/res_id_* $(DESTDIR)$(docdir)/examples/

clean:
	if [ -e "$(DISULFDIR)/src/Makefile" ]; then $(MAKE) -C $(DISULFDIR)/src clean; fi
	rm -f *.[12345678] *.[12345678].gz

distclean: clean
	#if [ -d "$(DISULFDIR)" ]; then rm -rf $(DISULFDIR); fi
	if [ -d "$(DISTDIR)" ]; then rm -rf "$(DISTDIR)"; fi
	rm -rf	patch-stamp \
		initial-clean-stamp \
		"$(DISTDIR).tar.gz" \
		.pc

dist: distdir
	tar chof - "$(DISTDIR)" | gzip -c > "$(DISTDIR).tar.gz"
	rm -rf "$(DISTDIR)"

distdir: clean
	rm -rf "$(DISTDIR)" && mkdir -p "$(DISTDIR)" && \
	rsync -avC \
		--exclude /*-stamp \
		--exclude .*.swp \
		--exclude *.o \
		examples \
		ChangeLog \
		COPYING \
		Makefile \
		disulfind \
		disulfinder.pod \
		Makefile \
		"$(DISTDIR)/";

help:
	@echo "Rules:"
	@echo "all*"
	@echo "*disulfinder"
	@echo "*doc"
	@echo "install"
	@echo "  set DESTDIR and prefix as needed"
	@echo "clean"
	@echo "distclean"
	@echo "dist - create tar.gz package"
	@echo "untar"
	@echo "help"

# vim:ai:
